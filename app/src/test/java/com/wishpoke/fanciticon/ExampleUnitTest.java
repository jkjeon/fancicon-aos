package com.wishpoke.fanciticon;

import com.wishpoke.fanciticon.util.UDTextUtils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void addition_isCorrect() {
        try {
            assertEquals( "%E3%85%8B%E3%85%8B%E3%85%8B%E3%85%8B", UDTextUtils.decode("%E3%85%8B%E3%85%8B%E3%85%8B%E3%85%8B"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
