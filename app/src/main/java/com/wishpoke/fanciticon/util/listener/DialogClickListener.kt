package com.wishpoke.fanciticon.util.listener

interface DialogClickListener<T> {
    fun onClick(value: T)
}