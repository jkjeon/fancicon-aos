package com.wishpoke.fanciticon.util

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wishpoke.fanciticon.data.model.HiddenContent
import com.wishpoke.fanciticon.data.model.pung.Post
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import java.util.*
import kotlin.collections.ArrayList

object HiddenContentModule {

    enum class ContentType {
        POST, REPLY
    }

    // 숨길 컨텐츠를 추가한다.
    fun addHiddenContent(context: Context, type: ContentType, id: Int?) {
        if (id != null) {
            val storage = AppStorage(context)
            val hiddenContent = HiddenContent(id, Date().time / 1000)
            val list = getHiddenContentList(context, type)
            if (list.indexOfFirst { it.id == hiddenContent.id } == -1) {
                list.add(hiddenContent)
            }
            clearOldContent(context, type)
            storage.put(type.name + ".HiddenContents", Gson().toJson(list))
        }
    }


    // 숨겨진 컨텐츠를 제외한 목록을 반환한다.
    fun <T> processHideContent(context: Context, type: ContentType, list: List<T>): List<T> {
        val hiddenContentList = getHiddenContentList(context, type)
        return if (hiddenContentList.size == 0) {
            list
        } else {
            list.filter { item ->
                if (type == ContentType.POST) {
                    item as Post
                    // 숨김 목록에서 찾을 수 없을때
                    hiddenContentList.indexOfFirst { item.postNo == it.id } == -1
                } else {
                    item as Post.Reply
                    hiddenContentList.indexOfFirst { item.replyNo == it.id } == -1
                }
            }
        }
    }

    // 24시간이 지난 컨텐츠는 삭제한다.
    private fun clearOldContent(context: Context, type: ContentType) {
        val storage = AppStorage(context)
        val list = getHiddenContentList(context, type)
        // 하루 전의 날짜를 얻어온다
        val calendar = Calendar.getInstance().apply {
            add(Calendar.HOUR_OF_DAY, -24)
        }
        val criteriaTime = calendar.timeInMillis / 1000
        val filteredList = list.filter { it.createdAt > criteriaTime }
        storage.put(type.name + ".HiddenContents", Gson().toJson(filteredList))
    }

    // 숨겨진 컨텐츠 목록을 가져온다.
    private fun getHiddenContentList(context: Context, type: ContentType): ArrayList<HiddenContent> {
        val storage = AppStorage(context)
        var list = ArrayList<HiddenContent>()
        val rawString = storage.getString(type.name + ".HiddenContents")
        if (rawString != null) {
            val itemType = object : TypeToken<List<HiddenContent>>() {}.type
            list = Gson().fromJson(rawString, itemType)
        }
        return list
    }
}