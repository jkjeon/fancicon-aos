package com.wishpoke.fanciticon.util;

import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;

import com.wishpoke.fanciticon.R;
import com.wishpoke.fanciticon.presenation.resource.ui.BounceInterpolator;

public class Animator {
    public static Animation getClickAnimation(Context context) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.bounce);
        BounceInterpolator interpolator = new BounceInterpolator();
        animation.setInterpolator(interpolator);
        return animation;
    }

    public static Animation getClickSlightAnimation(Context context) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.bounce_slight);
        BounceInterpolator interpolator = new BounceInterpolator();
        animation.setInterpolator(interpolator);
        return animation;
    }

    public static Animation getFavoriteClickAnimation(Context context) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.bounce_favorite);
        animation.setInterpolator(new BounceInterpolator());
        return animation;
    }

    public static Animation getCopiedClickAnimation(Context context) {
        Animation startAnim = AnimationUtils.loadAnimation(context, R.anim.scale_copied);
        startAnim.setInterpolator(new DecelerateInterpolator());
        return startAnim;
    }

    public static Animation getWidgetGrowAnimation(Context context) {
        Animation startAnim = AnimationUtils.loadAnimation(context, R.anim.grow);
        startAnim.setInterpolator(new DecelerateInterpolator());
        startAnim.setFillEnabled(true);
        startAnim.setFillAfter(true);
        return startAnim;
    }

    public static Animation getWidgetShrinkAnimation(Context context) {
        Animation startAnim = AnimationUtils.loadAnimation(context, R.anim.shrink2);
        startAnim.setInterpolator(new DecelerateInterpolator());
        startAnim.setFillEnabled(true);
        startAnim.setFillAfter(true);
        return startAnim;
    }

    public static ValueAnimator getItemClickAnimation(Context context, final View v, int colorFromId, int colorToId, int totalDuration) {
        int colorFrom = context.getResources().getColor(colorFromId);
        int colorTo = context.getResources().getColor(colorToId);

        ValueAnimator bgStartAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        bgStartAnimation.setDuration(totalDuration / 2);
        final ValueAnimator bgEndAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorTo, colorFrom);
        bgEndAnimation.setDuration(totalDuration / 2);

        bgStartAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                v.setBackgroundColor((int) animator.getAnimatedValue());
            }
        });
        bgEndAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                v.setBackgroundColor((int) animator.getAnimatedValue());
            }
        });

        // 시작 애니메이션 종료 후에 종료 애니메이션 시작
        bgStartAnimation.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(android.animation.Animator animation) {
                bgEndAnimation.start();
                super.onAnimationEnd(animation);
            }
        });

        return bgStartAnimation;
    }

    public static void slideUp(Activity context) {
        context.overridePendingTransition(R.anim.slide_up, R.anim.nothing);
    }

    public static void slideDown(Activity context) {
        context.overridePendingTransition(R.anim.nothing, R.anim.slide_down);
    }
}
