package com.wishpoke.fanciticon.util

import android.text.Editable
import java.net.URLDecoder
import java.net.URLEncoder

class UDTextUtils {
    companion object {
        @JvmStatic
        fun getStartOf(sentence: String, strong: String): Int {
            return Math.max(sentence.indexOf(strong), 0)
        }

        @JvmStatic
        fun getEndOf(sentence: String, strong: String): Int {
            return Math.max(sentence.indexOf(strong), 0) + strong.length
        }

        @JvmStatic
        fun isEqual(a: CharSequence?, b: CharSequence?): Boolean {
            return a == b
        }

        @JvmStatic
        fun isEmpty(text: String?): Boolean {
            return text.isNullOrEmpty()
        }

        @JvmStatic
        fun isEmpty(text: Editable?): Boolean {
            return text.isNullOrEmpty()
        }

        @JvmStatic
        fun isEmpty(text: CharSequence?): Boolean {
            return text.isNullOrEmpty()
        }

        @JvmStatic
        fun join(delimiter: CharSequence?, tokens: Iterable<String>?): String {
            val it: Iterator<String>? = tokens?.iterator()
            if (it == null || !it.hasNext()) return ""
            val sb = StringBuilder()
            sb.append(it.next())
            while (it.hasNext()) {
                sb.append(delimiter)
                sb.append(it.next())
            }
            return sb.toString()
        }

        @JvmStatic
        fun decode(text: String?): String {
            var decodedText = text
            return try {
                decodedText = decodedText?.replace("%(?![0-9a-fA-F]{2})/g", "%25")
                decodedText = decodedText?.replace("\\+/g", "%2B")
                URLDecoder.decode(text, "UTF-8")
            } catch (e: Exception) {
                e.printStackTrace()
                decodedText ?: ""
            }
        }

        @JvmStatic
        fun trim(text: String?): String {
            val textArray = text?.split("\n")
            val builder = StringBuilder()
            return if (textArray != null) {
                for ((index, line) in textArray.withIndex()) {
                    if (line.isNotEmpty()) {
                        builder.append(line)
                        if (index != textArray.size - 1) {
                            builder.append("\n")
                        }
                    }
                }
                builder.toString()
            } else {
                ""
            }
        }
    }
}