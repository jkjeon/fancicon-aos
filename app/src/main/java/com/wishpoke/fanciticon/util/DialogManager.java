package com.wishpoke.fanciticon.util;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.wishpoke.fanciticon.R;
import com.wishpoke.fanciticon.data.model.MissionHistory;
import com.wishpoke.fanciticon.presenation.resource.AppStorage;
import com.wishpoke.fanciticon.presenation.resource.ui.FormEditText;
import com.wishpoke.fanciticon.presenation.view.general.WebViewActivity;
import com.wishpoke.fanciticon.util.constants.Constants;
import com.wishpoke.fanciticon.util.listener.DialogClickListener;

public class DialogManager {

    private static DialogManager instance;

    public static DialogManager getInstance() {
        if (instance == null) {
            instance = new DialogManager();
        }
        return instance;
    }

    private DialogManager() {
    }

    public @Nullable AlertDialog createReviewDialog(final Context context) {
        AppStorage storage = new AppStorage(context);
        if (storage.countLaunch()) {
            return new AlertDialog.Builder(context)
                    .setMessage(R.string.review_popup)
                    .setNegativeButton(R.string.later, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            final String appPackageName = context.getPackageName();
                            try {
                                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                        }
                    }).create();
        } else {
            return null;
        }
    }

    public AlertDialog showPermissionDialog(final Context context, View.OnClickListener clickListener) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_permission, null);
        LinearLayout btnConfirm = view.findViewById(R.id.btn_confirm);
        TextView tvAlert = view.findViewById(R.id.tv_alert);
        TextView tvCamera = view.findViewById(R.id.tv_camera);
        TextView tvGallery = view.findViewById(R.id.tv_gallery);

        String strong = context.getString(R.string.perm_alert);
        String sentence = context.getString(R.string.perm_alert_desc);
        SpannableString spannableString = new SpannableString(sentence);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD),
                UDTextUtils.getStartOf(sentence, strong),
                UDTextUtils.getEndOf(sentence, strong),
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        tvAlert.setText(spannableString);

        strong = context.getString(R.string.perm_camera);
        sentence = context.getString(R.string.perm_camera_desc);
        spannableString = new SpannableString(sentence);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD),
                UDTextUtils.getStartOf(sentence, strong),
                UDTextUtils.getEndOf(sentence, strong),
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        tvCamera.setText(spannableString);

        strong = context.getString(R.string.perm_picture);
        sentence = context.getString(R.string.perm_picture_desc);
        spannableString = new SpannableString(sentence);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD),
                UDTextUtils.getStartOf(sentence, strong),
                UDTextUtils.getEndOf(sentence, strong),
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        tvGallery.setText(spannableString);

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickListener != null) {
                    clickListener.onClick(v);
                }
            }
        });
        return new AlertDialog.Builder(context)
                .setView(view)
                .setCancelable(false)
                .create();
    }

    public AlertDialog createSingleChoiceDialog(Context context, String[] itemList, int index, DialogInterface.OnClickListener clickListener) {
        return new AlertDialog.Builder(context)
                .setSingleChoiceItems(itemList, index, clickListener)
                .create();
    }

    public AlertDialog createLoadingDialog(Context context) {
        return createLoadingDialog(context, false);
    }

    public AlertDialog createLoadingDialog(Context context, boolean isCancelable) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_loading, null);

        AlertDialog dialog = new AlertDialog.Builder(context)
                .setView(view)
                .setCancelable(isCancelable)
                .create();

        Window window = dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        return dialog;
    }

    public AlertDialog createNChoiceDialog(Context context, String[] buttonLabels, final DialogClickListener<Integer> listener) {
        int size = buttonLabels.length;
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        int layoutRes = context.getResources().getIdentifier("dialog_" + size + "_choice", "layout", context.getPackageName());
        View view = LayoutInflater.from(context).inflate(layoutRes, null);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(v.getId());
                }
            }
        };

        for (int i = 0; i < size; i++) {
            int buttonRes = context.getResources().getIdentifier("btn_" + (i+1), "id", context.getPackageName());
            TextView item = view.findViewById(buttonRes);
            item.setText(buttonLabels[i]);
            item.setOnClickListener(onClickListener);
        }

        builder.setView(view);
        return builder.create();
    }


    public AlertDialog createYoutubeLinkDialog(Context context, DialogClickListener<String> listener) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_youtube_link, null);
        TextView tvValid = view.findViewById(R.id.tv_valid);
        View llYoutube = view.findViewById(R.id.ll_youtube);
        View tvCancel = view.findViewById(R.id.tv_cancel);
        View btnConfirm = view.findViewById(R.id.btn_confirm);
        EditText etUrl = view.findViewById(R.id.et_url);

        AlertDialog dialog = builder.setView(view).create();

        tvCancel.setOnClickListener((v -> {
            dialog.dismiss();
        }));

        btnConfirm.setOnClickListener((v -> {
            String url = etUrl.getText().toString().trim();
            if (!YoutubeUtils.isYoutube(url)) {
                tvValid.setVisibility(View.VISIBLE);
            } else {
                tvValid.setVisibility(View.GONE);
            }
            if (YoutubeUtils.isYoutube(url)) {
                dialog.dismiss();
                if (listener != null) {
                    listener.onClick(url);
                }
            }
        }));

        llYoutube.setOnClickListener((v -> {
            Intent appIntent = context.getPackageManager().getLaunchIntentForPackage("com.google.android.youtube");
            Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com"));
            try {
                context.startActivity(appIntent);
            } catch (ActivityNotFoundException e) {
                context.startActivity(webIntent);
            }
        }));

        return dialog;
    }

    public AlertDialog createCheckPinDialog(Context context, MissionHistory missionHistory) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_check_pin, null);
        TextView tvContent = view.findViewById(R.id.tv_content);
        View btnConfirm = view.findViewById(R.id.btn_confirm);

        String sentence = String.format(context.getString(R.string.redeem_check_pin_content),
                String.valueOf(Util.parseInt(missionHistory.getCouponPrice())),
                missionHistory.getCouponTts());
        String strong = context.getString(R.string.redeem_check_pin_content_strong);
        Spannable spannable = new SpannableString(sentence);
        spannable.setSpan(new UnderlineSpan(),
                UDTextUtils.getStartOf(sentence, strong),
                UDTextUtils.getEndOf(sentence, strong),
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        spannable.setSpan(new ClickableSpan() {
            @Override
            public void onClick(@NonNull View view) {
                // 이용안내 페이지로 이동
                Intent intent = new Intent(context, WebViewActivity.class);
                intent.putExtra("url", strong);
                context.startActivity(intent);
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(context.getResources().getColor(R.color.pink));
            }
        },
                UDTextUtils.getStartOf(sentence, strong),
                UDTextUtils.getEndOf(sentence, strong),
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        tvContent.setText(spannable);
        tvContent.setMovementMethod(LinkMovementMethod.getInstance());

        AlertDialog dialog = builder.setView(view).create();

        btnConfirm.setOnClickListener((v) -> {
            Util.copyToClipboard(context, missionHistory.getCouponTts());
            Toaster.INSTANCE.show(context.getApplicationContext(), R.string.toast_copied_pin);
            dialog.dismiss();
        });

        return dialog;
    }

    private AlertDialog createGuideDialog(Context context, boolean isChat, boolean isWarn, DialogClickListener<View> clickListener) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_chat_guide, null);
        TextView tvTitle = view.findViewById(R.id.tv_title);
        TextView tvContent = view.findViewById(R.id.tv_content);
        ImageView ivClose = view.findViewById(R.id.iv_close);
        TextView tvQuote = view.findViewById(R.id.tv_quote);
        TextView tvOk = view.findViewById(R.id.tv_ok);
        TextView tvSkip = view.findViewById(R.id.tv_skip);

        if (!isWarn) {
            tvSkip.setVisibility(View.VISIBLE);
        } else {
            tvSkip.setVisibility(View.GONE);
        }

        // 제목 스타일
        String sentence = context.getString(isChat ?
                R.string.chat_guide_title : R.string.pung_guide_title);
        String strong = context.getString(isChat ? R.string.chat_guide_title_strong
                : R.string.pung_guide_title_strong);
        SpannableString spannable = new SpannableString(sentence);
        spannable.setSpan(new UnderlineSpan(), UDTextUtils.getStartOf(sentence, strong),
                UDTextUtils.getEndOf(sentence, strong), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        spannable.setSpan(new StyleSpan(Typeface.BOLD), UDTextUtils.getStartOf(sentence, strong),
                UDTextUtils.getEndOf(sentence, strong), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        spannable.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.purple)),
                UDTextUtils.getStartOf(sentence, strong),
                UDTextUtils.getEndOf(sentence, strong), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        tvTitle.setText(spannable);
        // 본문 스타일
        sentence = context.getString(R.string.chat_guide_content);
        String strong1 = context.getString(R.string.chat_guide_content_strong1);
        String strong2 = context.getString(R.string.chat_guide_content_strong2);
        spannable = new SpannableString(sentence);
        spannable.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.pink)),
                UDTextUtils.getStartOf(sentence, strong1),
                UDTextUtils.getEndOf(sentence, strong1), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        spannable.setSpan(new UnderlineSpan(),
                UDTextUtils.getStartOf(sentence, strong2),
                UDTextUtils.getEndOf(sentence, strong2), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        spannable.setSpan(new ClickableSpan() {
                              @Override
                              public void onClick(@NonNull View view) {
                                  // 이용약관 열기
                                  Intent intent = new Intent(context, WebViewActivity.class);
                                  intent.putExtra("url", Constants.Url.TERMS);
                                  context.startActivity(intent);
                              }

                              @Override
                              public void updateDrawState(@NonNull TextPaint ds) {
                                  super.updateDrawState(ds);
                                  ds.setColor(context.getResources().getColor(R.color.textEmoji));
                              }
                          },
                UDTextUtils.getStartOf(sentence, strong2),
                UDTextUtils.getEndOf(sentence, strong2), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        tvContent.setMovementMethod(LinkMovementMethod.getInstance());
        tvContent.setText(spannable);
        // 명언 스타일
        sentence = context.getString(R.string.chat_guide_quote);
        strong = context.getString(R.string.chat_guide_quote_strong);
        spannable = new SpannableString(sentence);
        spannable.setSpan(new StyleSpan(Typeface.BOLD), UDTextUtils.getStartOf(sentence, strong),
                UDTextUtils.getEndOf(sentence, strong), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        tvQuote.setText(spannable);

        AlertDialog dialog = builder.setView(view).create();

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (clickListener != null) {
                    clickListener.onClick(view);
                }
            }
        };

        ivClose.setOnClickListener(onClickListener);
        tvSkip.setOnClickListener(onClickListener);
        tvOk.setOnClickListener(onClickListener);

        return dialog;
    }

    public AlertDialog createChatGuideDialog(Context context, DialogClickListener<View> clickListener) {
        return createGuideDialog(context, true, false, clickListener);
    }

    public AlertDialog createChatWarningDialog(Context context) {
        return createGuideDialog(context, true, true, null);
    }

    public AlertDialog createPungGuideDialog(Context context, DialogClickListener<View> clickListener) {
        return createGuideDialog(context, false, false, clickListener);
    }

    public AlertDialog createPungWarningDialog(Context context) {
        return createGuideDialog(context, false, true, null);
    }

    public AlertDialog createCustomEmojiDetailDialog(Context context, String emoji) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_custom_emoji_detail, null);
        ImageView ivClose = view.findViewById(R.id.iv_close);
        TextView tvEmoji = view.findViewById(R.id.tv_emoji);
        tvEmoji.setText(emoji);

        AlertDialog dialog = builder.setView(view).create();

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        return dialog;
    }

    /**
     * 패스워드 입력 받는 다이얼로그
     * @param context
     * @param isValid 서버 결과값 맞는지
     * @param password 입력한 패스워드
     * @param clickListener
     * @return
     */
    public AlertDialog createPungPasswordDialog(Context context, int message, boolean isValid, String password, DialogClickListener<String> clickListener) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_delete_pung, null);
        ImageView ivClose = view.findViewById(R.id.iv_close);
        FormEditText etConfirm = view.findViewById(R.id.et_password);
        TextView textView = view.findViewById(R.id.tv_message);
        textView.setText(message);
        etConfirm.setText(password);
        if (password != null) {
            etConfirm.setSelection(password.length());
        }
        etConfirm.setValid(isValid);
        AlertDialog dialog = builder.setView(view)
                .setPositiveButton(R.string.yes, (dialog12, which) -> {
                    String passwordInput = etConfirm.getText().toString().trim();
                    dialog12.dismiss();
                    clickListener.onClick(passwordInput);
                })
                .setNegativeButton(R.string.no, (dialog1, which) -> dialog1.dismiss())
                .create();

        ivClose.setOnClickListener(view1 -> dialog.dismiss());

        return dialog;
    }

    public AlertDialog showNChoiceDialog(Activity activity, String[] buttonLabels, final DialogClickListener<Integer> listener) {
        int size = buttonLabels.length;
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        int layoutRes = activity.getResources().getIdentifier("dialog_" + size + "_choice", "layout", activity.getPackageName());
        View view = LayoutInflater.from(activity).inflate(layoutRes, null);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(v.getId());
                }
            }
        };

        for (int i = 0; i < size; i++) {
            int buttonRes = activity.getResources().getIdentifier("btn_" + (i+1), "id", activity.getPackageName());
            TextView item = view.findViewById(buttonRes);
            item.setText(buttonLabels[i]);
            item.setOnClickListener(onClickListener);
        }

        builder.setView(view);
        return builder.create();
    }

    public AlertDialog createToastDialog(Activity activity, String text, String textStrong) {
        View view = LayoutInflater.from(activity).inflate(R.layout.dialog_toast, null);
        TextView textView = view.findViewById(R.id.tv_message);

        if (!UDTextUtils.isEmpty(textStrong)) {
            SpannableString spannable = new SpannableString(text);
            spannable.setSpan(new StyleSpan(Typeface.BOLD),
                    UDTextUtils.getStartOf(text, textStrong),
                    UDTextUtils.getEndOf(text, textStrong),
                    Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            textView.setText(spannable);
        } else {
            textView.setText(text);
        }

        AlertDialog dialog = new AlertDialog.Builder(activity)
                .setView(view)
                .create();

        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            window.setDimAmount(0.2f);
        }

        return dialog;
    }

}
