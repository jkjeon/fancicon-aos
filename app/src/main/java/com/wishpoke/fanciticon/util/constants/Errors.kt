package com.wishpoke.fanciticon.util.constants

object Errors {
    // 공통에러
    const val TOKEN_IS_NOT_VALID = -101
    const val TOKEN_EXPIRED = -102
    const val TIMEZONE_SETTING_INVALID = -103
    const val AUTHORIZATION_NOT_FOUND = -104
    const val TIMESTAMP_NOT_FOUND = -105
    const val NOT_ENOUGH_COUPON_OR_POINT = -107
    const val INVALID_REFERRAL_CODE = -112
    const val PERMISSION_DENIED = -114
    const val UNKNOWN_EXCEPTION = -9999

    // 회원관련에러
    const val NOT_FOUND_USER_ID = -1001
    const val NOT_FOUND_USER_PW = -1002
    const val NOT_FOUND_USER_NACD = -1003
    const val NOT_FOUND_USER_PHONE = -1004
    const val EXIST_USER = -1010
    const val REGISTERED_PHONE = -1011
    const val NON_EXIST_USER = -1012
    const val PASSWORDS_DO_NOT_MATCH = -1013
    const val REGISTERED_ALIAS = -1017
    const val PASSWORD_REQUIRED = -1018

    // 앱 내 에러
    const val NETWORK = 19500
    const val FILE_NOT_FOUND = 19404
    const val FIREBASE_VERIFICATION_REQUEST_FAILED = 19501
    const val SERVER = 19502
}
