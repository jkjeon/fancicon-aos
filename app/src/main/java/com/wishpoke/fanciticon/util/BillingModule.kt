package com.wishpoke.fanciticon.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.BillingProcessor.IBillingHandler
import com.anjlab.android.iab.v3.Constants.BILLING_RESPONSE_RESULT_USER_CANCELED
import com.anjlab.android.iab.v3.TransactionDetails
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import com.wishpoke.fanciticon.util.constants.Constants

class BillingModule(val context: Context) : IBillingHandler {

    interface BillingCallback {
        fun onPurchaseCompleted()
        fun onPurchaseCanceled() {}
    }

    private var storage: AppStorage = AppStorage(context)

    var billingCallback: BillingCallback? = null
    var adLoader: FacebookAdLoader? = null

    private var bp: BillingProcessor = BillingProcessor(context, Constants.GooglePlayLicenseKey, this)

    init {
        bp.initialize()
    }

    override fun onBillingInitialized() {
        bp.loadOwnedPurchasesFromGoogle()
        onResume()

    }

    override fun onPurchaseHistoryRestored() {
        onResume()
    }

    override fun onProductPurchased(productId: String, details: TransactionDetails?) {
        bp.loadOwnedPurchasesFromGoogle()
        billingCallback?.onPurchaseCompleted()
        onResume()
    }

    override fun onBillingError(errorCode: Int, error: Throwable?) {
        if (errorCode != BILLING_RESPONSE_RESULT_USER_CANCELED) {
            billingCallback?.onPurchaseCanceled()
        }
    }

    fun purchase(activity: Activity) {
        bp.purchase(activity, Constants.NoAdsSku)
    }

    fun onResume() {
        storage.setPurchasedRemoveAds(bp.isPurchased(Constants.NoAdsSku))
        adLoader?.onResume()
    }

    fun onDestroy() {
        bp.release()
    }


    fun handleActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        return bp.handleActivityResult(requestCode, resultCode, data)
    }
}