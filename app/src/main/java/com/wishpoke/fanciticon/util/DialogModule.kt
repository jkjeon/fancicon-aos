package com.wishpoke.fanciticon.util

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.PungMore
import com.wishpoke.fanciticon.util.listener.DialogClickListener

object DialogModule {
    fun createPungMoreDialog(context: Context, isMine: Boolean, isAnonymous: Boolean, clickListener: DialogClickListener<PungMore>): AlertDialog {
        val buttonLabels = when {
            isAnonymous -> arrayOf(
                    context.getString(R.string.modify),
                    context.getString(R.string.delete),
                    context.getString(R.string.hide),
                    context.getString(R.string.report)
            )
            isMine -> {
                arrayOf(
                        context.getString(R.string.modify),
                        context.getString(R.string.delete)
                )
            }
            else -> {
                arrayOf(
                        context.getString(R.string.hide),
                        context.getString(R.string.report)
                )
            }
        }
        return DialogManager.getInstance().createNChoiceDialog(context, buttonLabels, object: DialogClickListener<Int> {
            override fun onClick(value: Int) {
                when {
                    isMine || isAnonymous -> {
                        when (value) {
                            R.id.btn_1 -> clickListener.onClick(PungMore.MODIFY)
                            R.id.btn_2 -> clickListener.onClick(PungMore.DELETE)
                            R.id.btn_3 -> clickListener.onClick(PungMore.HIDE)
                            R.id.btn_4 -> clickListener.onClick(PungMore.REPORT)
                        }
                    }
                    else -> {
                        when (value) {
                            R.id.btn_1 -> clickListener.onClick(PungMore.HIDE)
                            R.id.btn_2 -> clickListener.onClick(PungMore.REPORT)
                        }
                    }
                }
            }
        })
    }
}