package com.wishpoke.fanciticon.util.listener

interface PermissionDoneListener {
    fun onDone(granted: Boolean)
}