package com.wishpoke.fanciticon.util.constants;

object ChatActionTypes {
    const val MESSAGE = 0
    const val ENTER = 1
    const val EXIT = 2
    const val LIKE = 3
    const val SHARE = 4
    const val KICK = 5
    const val CLOSE = 6
}