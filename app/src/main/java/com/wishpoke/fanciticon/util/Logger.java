package com.wishpoke.fanciticon.util;


public class Logger {
    public static void d(String tag, String msg) {
        android.util.Log.d(tag, msg);
    }

    public static void d(String tag, String msg, Throwable t) {
        android.util.Log.d(tag, msg, t);
    }

    public static void e(String tag, String msg) {
        android.util.Log.e(tag, msg);
    }

    public static void e(String tag, String msg, Throwable t) {
        android.util.Log.e(tag, msg, t);
    }

    public static void e(String msg) {
        android.util.Log.e("Test", msg);
    }
}