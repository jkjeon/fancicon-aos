package com.wishpoke.fanciticon.util.constants

object PrefKeys {
    const val FCM_TOKEN = "fcmToken"

    const val STAMP_TIME = "stamptime"
    const val AUTHORIZATION = "authorization"
    const val PASSWORD = "password"
    const val USER = "user"

    const val SAW_CHAT_GUIDE = "sawChatGuide"
    const val SAW_PUNG_GUIDE = "sawPungGuide"

    const val REWARD_AMOUNT = "rewardAmount"
    const val REWARD_USER_ID = "rewardUserId"
    const val REWARD_PROVIDER = "rewardProvider"

    const val VERSION_CHECKED = "version"
}