package com.wishpoke.fanciticon.util.constants

object Constants {
    /*
    구서버 : inssaticon.com
    신서버 : api.inssaticon.com
    채팅 구서버 : 34.97.23.180
    채팅 신서버 : chat.inssaticon.com
     */

    // IronSource doc: https://developers.ironsrc.com/ironsource-mobile/android/offerwall-integration-android/#step-1
    const val AppAllAppKey = "184d07668ba2c9394bc0e46e28f8b9d4981c0883"
    const val IronSourceAppKey = "c4f2f305"
    const val AdisonAppKey = "hXKghKcmZh8veZgZJy3ZqM9e"
    const val AdisonTestAppKey = "oZQE8f1zx3LzDjTMWRCFYpdm"

    const val ChatHostName = "chat.inssaticon.com"
    const val NoAdsSku = "no_ads"
    const val NewLabel = "\$new\$"
    const val FcmTopic = "app"
    const val GooglePlayLicenseKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAookSND5Uq4W9TFq4U5ffseUYc/Uhat2o8sd0NLi8t9NP/UNlc+l92QOpkt6A1z3LMI3jl0bSR6qz5jmCDKTV0R/JbOcby02LMcazTZHQM0JEHWCxl+cWno5g4YYcI6LwDKmDg7y4EPNId6upxFTZuXEz0rcQHvG6B+xk9EI54ePBaUYws5UfGE0jMummTflnZJrBeY8QjV5WTq0j1sw0UqLmNVFahWeFcN2N0qVzTAonYcyb77XtGf4BW0KBakoZtkNq1xZcYI4o7TltA0+MlWdVk48VrWqHalDP58H0awoDWTlQoDiM303ohFOAcBh8ip+IrFfWRfHmYlf3lz4yWwIDAQAB"

    object Url {
        const val BASE = "http://api.inssaticon.com"
        const val POLICY = "https://www.notion.so/c8fb5b0250fa4be3a1b57b458b4f0596"
        const val TERMS = "https://www.notion.so/a5e2e736bf944c6baaca93abdc78b2a5"
        const val DEVELOPER_GOOGLE_PLAY = "https://play.google.com/store/apps/dev?id=5307005341376124612"
        const val FEEDBACK = "https://goo.gl/forms/ZMoCF3eUlDtTj0Wt1"

        const val CHAT_SHARE_PREFIX = "http://welcometo.inssaticon.com/"
        const val YOUTUBE = "https://goo.gl/5BbZSH"
        const val YOUTUBE_EN = "https://youtu.be/0YWq8ATFseQ"
    }
    const val VibrateTime = 20L
    const val ScrollOffset = 120
}