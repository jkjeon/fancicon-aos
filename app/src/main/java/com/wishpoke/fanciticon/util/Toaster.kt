package com.wishpoke.fanciticon.util

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.AppThrowable
import com.wishpoke.fanciticon.util.constants.Errors
import com.wishpoke.fanciticon.util.extention.dp2px


object Toaster {

    fun show(context: Context, message: String, error: Throwable) {
        if (message.isNotEmpty()) {
            val errorCode = if (error is AppThrowable) {
                error.code
            } else {
                Errors.NETWORK
            }
            showCustom(context, "$message [$errorCode]")
        }
    }

    fun show(context: Context, message: Int, error: Throwable) {
        show(context, context.getString(message), error)
    }

    fun show(context: Context, message: Int, errorCode: Int?) {
        showCustom(context, context.getString(message) + " [${errorCode ?: 0}]")
    }

    fun show(context: Context, message: String, errorCode: Int?) {
        if (message.isNotEmpty()) {
            showCustom(context, "$message [${errorCode ?: 0}]")
        }
    }

    fun show(context: Context, message: Int) {
        showCustom(context, context.getString(message))
    }

    fun show(context: Context, message: String) {
        showCustom(context, message)
    }

    private fun showCustom(context: Context, message: String, length: Int = Toast.LENGTH_LONG) {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout: View = inflater.inflate(R.layout.view_custom_toast, null)

        val tvMessage: TextView = layout.findViewById(R.id.tv_message) as TextView
        tvMessage.text = message

        val toast = Toast(context)
        toast.duration = length
        toast.view = layout
        toast.setGravity(Gravity.TOP or Gravity.CENTER_HORIZONTAL, 0, context.dp2px(50F))
        toast.show()
    }


    fun showCustomWhite(context: Context, message: Int) {
        showCustomWhite(context, context.getString(message))
    }

    fun showCustomWhite(context: Context, message: Int, errorCode: Int?) {
        showCustomWhite(context, "$context.getString(message) [${errorCode ?: 0}]")
    }

    fun showCustomWhite(context: Context, message: String) {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout: View = inflater.inflate(R.layout.view_custom_toast_white, null)

        val tvMessage: TextView = layout.findViewById(R.id.tv_message) as TextView
        tvMessage.text = message

        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.setGravity(Gravity.CENTER, 0, context.dp2px(50F))
        toast.show()
    }
}