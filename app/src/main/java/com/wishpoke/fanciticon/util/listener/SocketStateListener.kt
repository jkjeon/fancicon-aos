package com.wishpoke.fanciticon.util.listener

interface SocketStateListener {
    fun onSocketOpened()
}