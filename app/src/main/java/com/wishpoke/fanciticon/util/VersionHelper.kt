package com.wishpoke.fanciticon.util

import android.content.Context
import com.wishpoke.fanciticon.data.model.VersionInfo

object VersionHelper {
    @JvmStatic
    fun getVersion(context: Context): String {
        return try {
            val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
            pInfo.versionName
        } catch (e: Exception) {
            e.printStackTrace()
            "0.0.0"
        }
    }

    @JvmStatic
    fun compareVersion(version: String, compareVersion: String): Boolean {
        val version1Array = version.split(".")
        val version2Array = compareVersion.split(".")
        val maxLength = Math.min(version1Array.size, version2Array.size)

        for (i in 0..maxLength) {
            val number = version1Array[i].toInt()
            val number2 = version2Array[i].toInt()
            if (number != number2) {
                return number > number2
            }
        }
        return false
    }

    @JvmStatic
    fun isUpdateRequired(context: Context, versionInfo: VersionInfo): Boolean {
        val serverVersion = versionInfo.android.version ?: "0.0.0"
        val currentVersion = getVersion(context)
        val isOldVersion: Boolean = try {
            // 서버 버전이 더 클때
            compareVersion(serverVersion, currentVersion)
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
        return versionInfo.android.required == 1 && isOldVersion
    }
}