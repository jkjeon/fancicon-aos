package com.wishpoke.fanciticon.util.listener

interface DoneListener {
    fun onDone() {}
}