package com.wishpoke.fanciticon.util

import android.app.Activity
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.Phonenumber
import com.wishpoke.fanciticon.data.model.PhoneInfo
import java.util.*
import java.util.concurrent.TimeUnit


class PhoneUtils {

    interface CodeSentCallback {
        fun onVerificationCompleted()
        fun onVerificationFailed(e: FirebaseException)
        fun onCodeSent(verificationId: String)
    }

    interface CodeVerifyCallback {
        fun onSucceed()
        fun onFailed(e: Exception?)
    }

    companion object {
        // 문자열에서 국제번호(+ 미포함)만 반환
        fun getCountryCode(phone: String?): String? {
            var phone = phone
            val phoneUtil: PhoneNumberUtil = PhoneNumberUtil.getInstance()
            return try {
                if (phone!!.contains("+") && phone.indexOf("+") != phone.lastIndexOf("+")) { // +82+8201012341234
                    phone = phone.substring(phone.lastIndexOf("+"))
                }
                // phone must begin with '+'
                if (!phone.startsWith("+")) {
                    phone = "+$phone"
                }
                phone = phone.replace("\\s+".toRegex(), "") // 띄어쓰기 제거
                val numberProto: Phonenumber.PhoneNumber = phoneUtil.parse(phone, "")
                val countryCode: Int = numberProto.countryCode
                "+$countryCode"
            } catch (e: Exception) {
                e.printStackTrace()
                phone
            }
        }

        fun getPhoneNumberOnly(phone: String?): String? {
            var phone = phone
            val nacd = getCountryCode(phone)
            if (phone != null) {
                val items = phone.split(" ").toTypedArray()
                phone = items[items.size - 1]
                phone = phone.replace("\\s+".toRegex(), "") // 띄어쓰기 제거
                if (!nacd.equals("+$phone")) {
                    // phone must begin with '+'
                    if (!phone.startsWith("+")) {
                        phone = "+$phone";
                    }
                }
                phone = phone.replace("" + nacd, "") // 국제 번호 제거
                phone = phone.replace("\\D*".toRegex(), "") // 숫자 제외 모두 제거
            }
            return phone
        }

        // 현재 지역의 국제번호를 반환
        fun getDefaultCountryCode(): String? {
            val country: String = Locale.getDefault().getCountry()
            val phoneUtil: PhoneNumberUtil = PhoneNumberUtil.getInstance()
            return "+" + phoneUtil.getCountryCodeForRegion(country)
        }

        fun sendCode(activity: Activity, phoneInfo: PhoneInfo, codeSentCallback: CodeSentCallback) {
            val phoneNumber = phoneInfo.nacd + phoneInfo.phone
            PhoneAuthProvider.getInstance().verifyPhoneNumber(phoneNumber, 60 * 2, TimeUnit.SECONDS, activity, object: PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
                    codeSentCallback.onVerificationCompleted()
                }

                override fun onVerificationFailed(e: FirebaseException) {
                    codeSentCallback.onVerificationFailed(e)
                }

                override fun onCodeSent(verificationId: String, forceResendingToken: PhoneAuthProvider.ForceResendingToken) {
                    super.onCodeSent(verificationId, forceResendingToken)
                    codeSentCallback.onCodeSent(verificationId)
                }
            })
        }

        fun verifyCode(phoneInfo: PhoneInfo, codeVerifyCallback: CodeVerifyCallback) {
            try {
                val credential =
                        PhoneAuthProvider.getCredential(phoneInfo.verificationId
                                ?: "", phoneInfo.code ?: "")
                FirebaseAuth.getInstance().signInWithCredential(credential)
                        .addOnCompleteListener {
                            if (it.isSuccessful) {
                                val user = FirebaseAuth.getInstance().currentUser
                                if (user != null) {
                                    user.delete().addOnCompleteListener {
                                        codeVerifyCallback.onSucceed()
                                    }
                                } else {
                                    codeVerifyCallback.onSucceed()
                                }
                            } else {
                                codeVerifyCallback.onFailed(it.exception)
                            }
                        }
            } catch (e: Exception) {
                codeVerifyCallback.onFailed(e)
            }
        }
    }
}