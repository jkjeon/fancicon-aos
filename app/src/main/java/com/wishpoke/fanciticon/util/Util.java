package com.wishpoke.fanciticon.util;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;

import com.wishpoke.fanciticon.R;
import com.wishpoke.fanciticon.data.model.UrlMetadata;
import com.wishpoke.fanciticon.util.constants.Constants;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {
    public static long lastClickTime = 0;
    private static int TYPE_WIFI = 1;
    private static int TYPE_MOBILE = 2;
    private static int TYPE_NOT_CONNECTED = 0;

    public static boolean getRandom(int percent) {
        int random = (((int) (Math.random() * 100)) + 1);
        return random <= percent;
    }

    public static String getImageDownloadUrl(String fileName) {
        return Constants.Url.BASE + "/fileApi/getFile?downloadFileName=" + fileName;
    }

    public static String formatLikeCount(int count) {
        int MAX_VALUE = 99999;
        if (count > MAX_VALUE) {
            return NumberFormat.getInstance().format(MAX_VALUE) + "+";
        } else {
            return NumberFormat.getInstance().format(count);
        }
    }

    public static String getVersionName(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "0.0.0";
        }
    }

    public static void openGooglePlay(Activity activity) {
        final String appPackageName = activity.getPackageName();
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public static void copyToClipboard(Context context, String text) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(context.getResources().getString(R.string.app_name), text);
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        return getConnectivityStatus(context) != 0;
    }

    private static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static UrlMetadata getMetadataFromUrl(String url) {
        try {
            UrlMetadata urlMetadata = new UrlMetadata();
            urlMetadata.url = url;
            Document doc = Jsoup.connect(url).get();
            urlMetadata.title = doc.select("meta[property=og:title]").first().attr("content");
            urlMetadata.description = doc.select("meta[property=og:description]").get(0).attr("content");
            urlMetadata.imageUrl = doc.select("meta[property=og:image]").get(0).attr("content");
            return urlMetadata;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String shorten(String text) {
        try {
            int MAX_LENGTH = 30;
            if (text.length() > MAX_LENGTH) {
                return text.substring(0, MAX_LENGTH) + "...";
            } else {
                return text;
            }
        } catch (Exception e) {
            return text;
        }
    }


    public static String extractUrlFromText(String text) {
        try {
            String urlRegex = "((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
            Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
            Matcher urlMatcher = pattern.matcher(text);

            ArrayList<String> urls = new ArrayList<>();
            while (urlMatcher.find()) {
                urls.add(text.substring(urlMatcher.start(0), urlMatcher.end(0)));
            }

            String resultUrl = null;
            for (String url : urls) {
                if (Validator.isUrl(url)) {
                    resultUrl = url;
                    break;
                }
            }

            return resultUrl;
        } catch (Exception e) {
            return "";
        }
    }

    public static int extractNumberFromText(String text) {
        int result = 0;
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(text);
        try {
            if (m.find()) {
                result = Integer.parseInt(m.group());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static int parseInt(String str) {
        try {
            str = str.replaceAll(",", "");
            Pattern p = Pattern.compile("[0-9]+");
            Matcher m = p.matcher(str);
            while (m.find()) {
                str = m.group();
            }
            return Integer.parseInt(str);
        } catch (Exception e) {
            return 0;
        }
    }

    public static String getUUID(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }


    public static boolean isKorean() {
        return Locale.getDefault().getLanguage().equals("ko");
    }
}
