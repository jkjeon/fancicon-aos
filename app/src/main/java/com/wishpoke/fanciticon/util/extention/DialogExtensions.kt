package com.wishpoke.fanciticon.util.extention

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.wishpoke.fanciticon.R


inline fun Context.createDialog(
        title: String? = null,
        message: String? = null,
        positiveLabel: Int = R.string.confirm,
        negativeLabel: Int = 0,
        crossinline positiveBlock: () -> Unit = {},
        crossinline negativeBlock: () -> Unit = {},
        cancelable: Boolean = true
): AlertDialog {
    val builder = AlertDialog.Builder(this)
            .setMessage(message)
            .setCancelable(cancelable)

    if (!title.isNullOrEmpty()) {
        builder.setTitle(title)
    }
    if (positiveLabel != 0) {
        builder.setPositiveButton(getString(positiveLabel)) { dialogInterface, which ->
            dialogInterface.dismiss()
            positiveBlock()
        }
    }
    if (negativeLabel != 0) {
        builder.setNegativeButton(getString(negativeLabel)) { dialogInterface, which ->
            dialogInterface.dismiss()
            negativeBlock()
        }
    }
    return builder.create()
}

inline fun Context.createDialog(
        title: Int = 0,
        message: Int = 0,
        positiveLabel: Int = R.string.confirm,
        negativeLabel: Int = 0,
        crossinline positiveBlock: () -> Unit = {},
        crossinline negativeBlock: () -> Unit = {},
        cancelable: Boolean = true
): AlertDialog {
    return createDialog(
            if (title != 0) getString(title) else "",
            if (message != 0) getString(message) else "",
            positiveLabel,
            negativeLabel,
            positiveBlock,
            negativeBlock,
            cancelable
    )
}
