package com.wishpoke.fanciticon.util;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
    private static final String googleQuery = "https://www.google.com/search?q=";

public static boolean isUrl(String text) {
    Pattern p = Pattern.compile("^(?:https?:\\/\\/)?(?:www\\.)?[a-zA-Z0-9./]+$");
    Matcher m = p.matcher(text);
    if  (m.matches()) return true;
    URL u = null;
    try {
        u = new URL(text);
    } catch (MalformedURLException e) {
        return false;
    }
    try {
        u.toURI();
    } catch (URISyntaxException e) {
        return false;
    }
    return true;
}


public static String extractUrl(String content){
    try {
        String REGEX = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
        Pattern p = Pattern.compile(REGEX, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(content);
        if (m.find()) {
            return m.group();
        }
        return "";
    } catch (Exception e) {
        return "";
    }
}

    public static String getDomainName(String url) {
        try {
            URI uri = new URI(url);
            String domain = uri.getHost();
            return domain != null && domain.startsWith("www.") ? domain.substring(4) : domain;
        } catch (Exception e) {
            e.printStackTrace();
            return url;
        }
    }

    public static String validateUrl(String url) {
        if (Validator.isUrl(url)) {
            if (!url.contains("http://") && !url.contains("https://")) {
                url = "http://" + url;
            }
        } else {
            url = googleQuery + url;
        }
        return url;
    }

    public static String getUrl(String text) {
        Pattern p = Pattern.compile("^(?:https?:\\/\\/)?(?:www\\.)?[a-zA-Z0-9./]+$");
        Matcher m = p.matcher(text);
        return m.group();
    }

    public static boolean isValidUserId(String id) {
        return id.trim().length() >= 4 && id.matches("[A-Za-z0-9]+") && id.trim().length() <= 12;
    }

    public static boolean isValidPassword(String password) {
        String regExpn = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[()*;!@#$%^&+=])(?=\\S+$).{8,}$";
        Pattern pattern = Pattern.compile(regExpn,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }
}
