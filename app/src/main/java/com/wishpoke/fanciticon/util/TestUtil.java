package com.wishpoke.fanciticon.util;

import com.wishpoke.fanciticon.BuildConfig;

import java.util.Locale;

public class TestUtil {
    public static boolean isDebugMode() {
        return BuildConfig.DEBUG_MODE.equals("true");
    }

    public static boolean shouldHideAd() {
        return BuildConfig.DEBUG_MODE.equals("true") && false;
    }

    // NBT 애디슨 사용여부
   public static boolean shouldShowNBT() {
        return Locale.getDefault().getLanguage().equals("ko");
    }

    // 익명 글 삭제
    public static boolean allowAnonymousDelete() {
        return TestUtil.isDebugMode();
    }
}
