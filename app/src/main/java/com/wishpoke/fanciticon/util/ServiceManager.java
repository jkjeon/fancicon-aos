package com.wishpoke.fanciticon.util;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import androidx.core.view.ViewCompat;

import com.wishpoke.fanciticon.R;
import com.wishpoke.fanciticon.presenation.service.FloatingWidgetService;

import static android.content.Context.WINDOW_SERVICE;

public class ServiceManager {
    public static void stopService(Context context) {
        try {
            WindowManager windowManager = (WindowManager) context.getSystemService(WINDOW_SERVICE);
            View widgetView = LayoutInflater.from(context).inflate(R.layout.view_floating_widget, null);
            if (ViewCompat.isAttachedToWindow(widgetView)) {
                windowManager.removeView(widgetView);
            }
            context.stopService(new Intent(context, FloatingWidgetService.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void startService(Context context, int categoryIndex) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(context)) {
                Intent intent = new Intent(context, FloatingWidgetService.class);
                intent.putExtra("categoryIndex", categoryIndex);
                context.startService(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
