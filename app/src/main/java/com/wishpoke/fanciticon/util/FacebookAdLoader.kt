package com.wishpoke.fanciticon.util

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.LinearLayout
import com.google.android.gms.ads.*
import com.iapxk.sdvem.nkzspq.op.EdgeViewCallback
import com.iapxk.sdvem.nkzspq.op.ISN
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import com.wishpoke.fanciticon.util.extention.isVisibleWhen

class FacebookAdLoader(
    private val context: Context,
    private val bannerContainer: ViewGroup? = null,
    private val isnContainer: LinearLayout? = null,
    private val isnWebView: WebView? = null
) {

    private val storage = AppStorage(context)
    private var adView: AdView? = null
    private var interstitialAd: InterstitialAd? = null

    init {
        // 배너 광고 세팅
        if (bannerContainer != null) {
            bannerContainer.removeAllViews()
            adView = AdView(context)
            adView?.adSize = AdSize.BANNER
            adView?.adUnitId = context.getString(R.string.admob_banner_id)
            bannerContainer.addView(adView)
            adView?.loadAd(AdRequest.Builder().build())
        }
        if (ISNCounter.isISNTurn && isnContainer != null && isnWebView != null) {
            if (isnContainer.visibility == View.GONE) {
                ISN.EdgeView(context, isnWebView, object: EdgeViewCallback {
                    override fun onSuccess() {
                        update()
                    }

                    override fun onFail() {
                        ISNCounter.isISNTurn = false
                        update()
                    }

                })
            }
        }
        // 전면광고 세팅
        interstitialAd = InterstitialAd(context)
        interstitialAd?.adUnitId = context.getString(R.string.admob_interstitial_id)
        interstitialAd?.loadAd(AdRequest.Builder().build())
    }

    fun update() {
        val isnAvailable = isnContainer != null && isnWebView != null && ISNCounter.isISNTurn
        isnContainer?.isVisibleWhen(isnAvailable && !storage.purchasedRemoveAds())
        bannerContainer?.isVisibleWhen(!isnAvailable && !storage.purchasedRemoveAds())
    }

    fun showInterstitial(callback: AdCallback? = null) {
        if (!storage.purchasedRemoveAds()) {
            if (interstitialAd?.isLoaded == true) {
                val interstitialAdListener = object: AdListener() {
                    override fun onAdFailedToLoad(p0: LoadAdError?) {
                        super.onAdFailedToLoad(p0)
                        callback?.onNext()
                    }

                    override fun onAdClosed() {
                        super.onAdClosed()
                        interstitialAd?.loadAd(AdRequest.Builder().build())
                        callback?.onNext()
                    }
                }
                interstitialAd?.adListener = interstitialAdListener
                interstitialAd?.show()
            } else {
                callback?.onNext()
            }
        } else {
            callback?.onNext()
        }
    }

    fun onResume() {
        update()
    }

    fun onDestroy() {
        adView?.destroy()
    }

    interface AdCallback {
        fun onNext()
    }
}