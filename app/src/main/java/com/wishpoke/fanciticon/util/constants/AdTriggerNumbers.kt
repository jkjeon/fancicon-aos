package com.wishpoke.fanciticon.util.constants

// 광고를 몇번마다 노출할지 저장
object AdTriggerNumbers {
    const val TAB_EMOTICON_CATEGORY = 10 // 이모티콘 탭 클릭
    const val LIKE_IN_CHAT_LIST = 47 // 찐톡 리스트 화면에서 박수
    const val LIKE_IN_CHAT_ROOM = 43 // 찐톡 내에서 박수
    const val LIKE_IN_PUNG_LIST = 46 // 인싸펑 리스트 화면에서 박수
    const val LIKE_IN_PUNG_POST = 48 // 인싸펑 내에서 박수
}