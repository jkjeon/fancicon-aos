package com.wishpoke.fanciticon.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class Moment {

    public static class TIME_MAXIMUM {
        static final int SEC = 60;
        static final int MIN = 60;
        static final int HOUR = 24;
    }

    public static String formatTimer(int timeInSec) {
        timeInSec *= 1000;
        final long min = TimeUnit.MILLISECONDS.toMinutes(timeInSec);
        final long sec = TimeUnit.MILLISECONDS.toSeconds(timeInSec - TimeUnit.MINUTES.toMillis(min));
        return String.format("%02d:%02d", min, sec);
    }

    public static String getPungTime(long createTime) {
        return getPungTime(new Date(createTime * 1000));
    }

    public static long getPungTimeToLong(long createTime) {
        long pungTime = getPungDate(new Date(createTime * 1000)).getTime();
        long currentTime = new Date().getTime();
        return (pungTime - currentTime) / 1000;
    }

    public static String getPungTime(Date createDate) {
        String text = null;
        long pungTime = getPungDate(createDate).getTime();
        long currentTime = new Date().getTime();
        long diffTime = (pungTime - currentTime) / 1000;
        long originalDiffTime = diffTime;

        if (diffTime < Moment.TIME_MAXIMUM.SEC) {
            text = "0분";
        } else if ((diffTime /= Moment.TIME_MAXIMUM.SEC) < Moment.TIME_MAXIMUM.MIN) {
            text = diffTime + "분";
        } else if ((diffTime /= Moment.TIME_MAXIMUM.MIN) < Moment.TIME_MAXIMUM.HOUR) {
            text = diffTime + "시간 ";
            text += ((originalDiffTime / Moment.TIME_MAXIMUM.MIN) % Moment.TIME_MAXIMUM.MIN) + "분";
        }

        text += " 후 펑";
        return text;
    }

    public static String getWriteTime(Date createDate) {
        String text = null;
        long writeTime = createDate.getTime();
        long currentTime = System.currentTimeMillis();
        long diffTime = (currentTime - writeTime) / 1000;

        if (diffTime < Moment.TIME_MAXIMUM.SEC) {
            text = "방금";
        } else if ((diffTime /= Moment.TIME_MAXIMUM.SEC) < Moment.TIME_MAXIMUM.MIN) {
            text = diffTime + "분 ";
        } else if ((diffTime /= Moment.TIME_MAXIMUM.MIN) < Moment.TIME_MAXIMUM.HOUR) {
            text = diffTime + "시간 ";
        }

        text += " 전";
        return text;
    }


    public static Date getPungDate(Date createDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(createDate);
        calendar.setTimeZone(TimeZone.getDefault());
        calendar.add(Calendar.HOUR_OF_DAY, 24);
        return calendar.getTime();
    }

    public static String format(String date, String fromFormat, String toFormat) {
        Date returnDate = parse(date, fromFormat);
        if (returnDate != null) {
            return format(returnDate.getTime() / 1000, toFormat);
        } else {
            return "";
        }
    }
    public static String format(long unixTime, String format) {
        Date date = new Date(unixTime * 1000);
        SimpleDateFormat mFormat = new SimpleDateFormat(format, Locale.getDefault());
        mFormat.setTimeZone(TimeZone.getDefault());
        return mFormat.format(date);
    }


    public static Date parse(String date, String format) {
        SimpleDateFormat mFormat = new SimpleDateFormat(format);
        mFormat.setTimeZone(TimeZone.getDefault());
        try {
            return mFormat.parse(date);
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean isAfterOneDay(long time) {
        // 자정 12시 이후로 하루 이상 지났는지 체크
        long nowTime = new Date().getTime() / 1000;
        long oneDay = 86400;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(time * 1000));
        Calendar now = Calendar.getInstance();
        return calendar.get(Calendar.DAY_OF_MONTH) != now.get(Calendar.DAY_OF_MONTH)
                || (nowTime - time) > oneDay;
    }

}
