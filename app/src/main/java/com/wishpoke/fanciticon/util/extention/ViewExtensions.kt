package com.wishpoke.fanciticon.util.extention

import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.annotation.LayoutRes
import com.wishpoke.fanciticon.util.Util

inline fun <T: View> T.afterMeasured(crossinline block: T.() -> Unit) {
    viewTreeObserver.addOnGlobalLayoutListener(object: ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            if (measuredHeight > 0 && measuredWidth > 0) {
                viewTreeObserver.removeOnGlobalLayoutListener(this)
                block()
            }
        }
    })
}

fun View.isVisibleWhen(flag: Boolean) {
    visibility = if (flag) View.VISIBLE else View.GONE
}

fun View.isVisible() = visibility == View.VISIBLE

fun View.isGone() = visibility == View.GONE

fun ViewGroup.inflate(@LayoutRes resId: Int): View =
        LayoutInflater.from(context).inflate(resId, this, false)


fun View.setOnSingleClickListener(block: (view: View) -> Unit) {
    this.setOnClickListener(object : View.OnClickListener {
        override fun onClick(v: View) {
            if (SystemClock.elapsedRealtime() - Util.lastClickTime < 350) return
            else block(v)

            Util.lastClickTime = SystemClock.elapsedRealtime()
        }
    })
}
