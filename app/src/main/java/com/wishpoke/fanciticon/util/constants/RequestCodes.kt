package com.wishpoke.fanciticon.util.constants

object RequestCodes {
    const val CAMERA = 1900
    const val GALLERY = 1901
    const val YOUTUBE = 1902
    const val MEDIA_PERMISSION = 1903
}