package com.wishpoke.fanciticon.util.listener

interface ValueCallback<T> {
    fun onDone(t: T) {}
}