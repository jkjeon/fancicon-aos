package com.wishpoke.fanciticon.util.listener

interface RequestListener<T> {
    fun onSuccess(result: T?)
    fun onFailure()
}