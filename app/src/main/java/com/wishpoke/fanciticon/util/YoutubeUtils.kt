package com.wishpoke.fanciticon.util

import java.util.regex.Pattern

class YoutubeUtils {
    /*
        # 참고한 URL
        ref: https://medium.com/@rishabhaggarwal2/tutorial-how-to-do-full-screen-youtube-video-embeds-without-any-black-bars-faa778db499c
        ref: https://github.com/cprcrack/VideoEnabledWebView
        ref: https://stackoverflow.com/questions/50101902/webview-and-iframe-video-full-screen-issue
     */
    companion object {
        @JvmStatic
        fun getThumbnail(url: String): String {
            val vId = getVideoId(url)
            return "https://img.youtube.com/vi/$vId/hqdefault.jpg"
        }

        @JvmStatic
        fun getVideoId(url: String?): String {
            var vId = ""
            val pattern = Pattern.compile(
                    "^.*(?:(?:youtu\\.be\\/|v\\/|vi\\/|u\\/\\w\\/|embed\\/)|(?:(?:watch)?\\?v(?:i)?=|\\&v(?:i)?=))([^#\\&\\?]*).*",
                    Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(url ?: "")
            if (matcher.matches()) {
                vId = matcher.group(1) ?: ""
            }
            return vId
        }

        @JvmStatic
        fun isYoutube(url: String?): Boolean {
            return getVideoId(url).isNotEmpty()
        }
    }
}