package com.wishpoke.fanciticon.util.extention

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Uri
import android.text.Editable
import android.text.TextWatcher
import android.util.TypedValue
import android.widget.EditText

fun Context.dp2px(dp: Float): Int {
    return TypedValue
            .applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    dp,
                    resources.displayMetrics
            ).toInt()
}

fun Context.isNetworkAvailable(): Boolean {
    val activeNetwork = (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo
    if (activeNetwork != null) {
        if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) return true
        if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) return true
    }
    return false
}

inline fun <reified T: Activity> Context.open(block: Intent.() -> Unit = {}) {
    val intent = Intent(this, T::class.java).apply(block)
    startActivity(intent)
}

inline fun <reified T: Activity> Activity.openForResult(block: Intent.() -> Unit = {}, requestCode: Int) {
    val intent = Intent(this, T::class.java).apply(block)
    startActivityForResult(intent, requestCode)
}

fun Context.openBrowser(url: String) {
    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
    startActivity(intent)
}

fun EditText.onTextChanged(block: (s: CharSequence?, start: Int, before: Int, count: Int) -> Unit) {
    addTextChangedListener(object: TextWatcher {
        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            block(s, start, before, count)
        }
    })
}