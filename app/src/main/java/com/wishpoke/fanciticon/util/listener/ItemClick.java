package com.wishpoke.fanciticon.util.listener;

import android.view.View;

public interface ItemClick {
    void onItemClick(View view, int position);
}
