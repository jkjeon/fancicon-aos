package com.wishpoke.fanciticon.data.model;


import java.util.HashMap;

public class LanguageFontSizes extends HashMap<String, Integer> {
    public LanguageFontSizes() {
        put("ko", 25);
        put("pt", 25);
        put("de", 25);
        put("es", 25);
        put("ja", 22);
        put("fr", 22);
    }
}
