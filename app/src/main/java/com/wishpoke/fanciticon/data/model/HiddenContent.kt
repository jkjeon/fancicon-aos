package com.wishpoke.fanciticon.data.model

data class HiddenContent(
        var id: Int,
        var createdAt: Long
)