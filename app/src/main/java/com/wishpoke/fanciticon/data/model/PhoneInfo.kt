package com.wishpoke.fanciticon.data.model

data class PhoneInfo(var nacd: String? = null,
                     var phone: String? = null,
                     var isVerified: Boolean = false,
                     var verificationId: String? = null,
                     var code: String? = null) {

    fun isReliable(): Boolean {
        return isVerified && !nacd.isNullOrEmpty() && !phone.isNullOrEmpty()
    }
}