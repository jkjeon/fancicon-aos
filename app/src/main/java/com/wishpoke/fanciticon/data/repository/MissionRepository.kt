package com.wishpoke.fanciticon.data.repository

import android.app.Application
import com.wishpoke.fanciticon.data.api.RetrofitService
import com.wishpoke.fanciticon.data.model.MissionHistory
import com.wishpoke.fanciticon.data.model.response.DefaultResponse
import com.wishpoke.fanciticon.data.model.response.ObjectResponse
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import com.wishpoke.fanciticon.presenation.resource.SingleResponse
import com.wishpoke.fanciticon.util.constants.Errors
import com.wishpoke.fanciticon.util.constants.PrefKeys
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class MissionRepository(application: Application) {
    val storage: AppStorage = AppStorage(application)
    fun getTicket(couponPrice: Int, singleResponse: SingleResponse<DefaultResponse>): Disposable {
        return RetrofitService.getApi().getTicket(
                storage.userId,
                storage.getString(PrefKeys.PASSWORD),
                storage.getLong(PrefKeys.STAMP_TIME),
                storage.getString(PrefKeys.AUTHORIZATION),
                couponPrice)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ singleResponse.accept(it) }, { singleResponse.accept(it, Errors.NETWORK) })
    }

    suspend fun getPointHistoryList(updateType: Int): Response<ObjectResponse<List<MissionHistory>>> {
        return RetrofitService.getAsyncApi().getPointHistoryList(
                storage.userId,
                storage.getString(PrefKeys.PASSWORD),
                storage.getLong(PrefKeys.STAMP_TIME),
                storage.getString(PrefKeys.AUTHORIZATION),
                updateType)
    }

    suspend fun earnRoulettePoint(point: Int): Response<ObjectResponse<Any>> {
        return RetrofitService.getAsyncApi()
                .earnRoulettePoint(
                        storage.userId,
                        storage.getString(PrefKeys.PASSWORD),
                        storage.getLong(PrefKeys.STAMP_TIME),
                        storage.getString(PrefKeys.AUTHORIZATION),
                        point
                )
    }

    suspend fun getLastRouletteTime(): Response<ObjectResponse<Long>> {
        return RetrofitService.getAsyncApi()
                .getLastRouletteTime(
                        storage.userId,
                        storage.getString(PrefKeys.PASSWORD),
                        storage.getLong(PrefKeys.STAMP_TIME),
                        storage.getString(PrefKeys.AUTHORIZATION))
    }
}