package com.wishpoke.fanciticon.data.model.response

data class SuccessResponse(var success: Boolean,
                           var msg: String? = null)