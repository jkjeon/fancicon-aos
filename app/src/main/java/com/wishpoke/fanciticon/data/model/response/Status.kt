package com.wishpoke.fanciticon.data.model.response

data class Status(val code: Int,
                  val msg: String = "")