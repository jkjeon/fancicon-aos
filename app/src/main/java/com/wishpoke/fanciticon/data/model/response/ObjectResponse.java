package com.wishpoke.fanciticon.data.model.response;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class ObjectResponse<T> extends BaseResponse {
    @SerializedName("object")
    public T object;
    public T get() {
        return object;
    }
}
