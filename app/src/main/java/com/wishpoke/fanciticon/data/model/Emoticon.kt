package com.wishpoke.fanciticon.data.model

import com.wishpoke.fanciticon.util.constants.Constants

data class Emoticon(val text: String? = null,
                    var isFavorite: Boolean = false) {

    fun strip() = text?.replace(Constants.NewLabel, "")

    override fun equals(other: Any?): Boolean {
        if (other == null)
            return false
        if (javaClass != other.javaClass)
            return false
        other as Emoticon
        if (strip() != other.strip())
            return false
        if (isFavorite != other.isFavorite)
            return false
        return true
    }
}