package com.wishpoke.fanciticon.data.model;

public class UrlMetadata {
    public String imageUrl;
    public String url;
    public String title;
    public String description;

    int MAX_LENGTH = 30;

    public String shorten(String text) {
        if (text.length() > MAX_LENGTH) {
            return text.substring(0, MAX_LENGTH) + "...";
        } else {
            return text;
        }
    }
}
