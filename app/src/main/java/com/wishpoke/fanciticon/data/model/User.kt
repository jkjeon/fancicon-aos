package com.wishpoke.fanciticon.data.model

data class User(var userId: String? = null,
                var createTime: Long = 0,
                var userPoint: Int = 0,
                var nacd: String? = null,
                var phone: String? = null,
                var authorization: String? = null,
                var referralCode: String? = null,
                var password: String? = null)