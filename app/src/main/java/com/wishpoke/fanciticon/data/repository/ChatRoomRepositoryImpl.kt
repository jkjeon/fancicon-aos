package com.wishpoke.fanciticon.data.repository

import com.wishpoke.fanciticon.data.api.RetrofitService
import com.wishpoke.fanciticon.data.model.chat.ChatRoom
import com.wishpoke.fanciticon.data.model.response.ObjectResponse
import retrofit2.Response

interface ChatRoomRepository {
    suspend fun getChatRoomList(userId: String?, requestPage: Int = 0, boardNo: Int, orderType: Int): Response<ObjectResponse<List<ChatRoom>>>
    suspend fun createChatRoom(userId: String?, chatRoom: ChatRoom, fcmToken: String): Response<ObjectResponse<ChatRoom>>
    suspend fun canJoinToChatRoom(userId: String?, roomId: String?, password: String?, stageName: String?): Response<ObjectResponse<Any>>
    suspend fun getPopularChatRoomIds(boardNo: Int = 0): Response<ObjectResponse<List<String>>>
}

class ChatRoomRepositoryImpl : ChatRoomRepository {
    override suspend fun getChatRoomList(userId: String?, requestPage: Int, boardNo: Int, orderType: Int): Response<ObjectResponse<List<ChatRoom>>> {
        return RetrofitService.getAsyncApi().getChatRoomList(
                userId, requestPage, boardNo, orderType
        )
    }

    override suspend fun canJoinToChatRoom(
        userId: String?,
        roomId: String?,
        password: String?,
        stageName: String?
    ): Response<ObjectResponse<Any>> {
        return RetrofitService.getAsyncApi().canJoinToChatRoomV2(
                userId, roomId ?: "", password ?: "", stageName ?: ""
        )
    }

    override suspend fun getPopularChatRoomIds(boardNo: Int): Response<ObjectResponse<List<String>>> {
        return RetrofitService.getAsyncApi().getPopularChatRoomIdsV2(boardNo)
    }

    override suspend fun createChatRoom(userId: String?, chatRoom: ChatRoom, fcmToken: String): Response<ObjectResponse<ChatRoom>> {
        return RetrofitService.getAsyncApi().createChatRoomV2(
                userId,
                fcmToken,
                chatRoom.stageName,
                chatRoom.title,
                chatRoom.boardNo,
                chatRoom.isPrivate,
                chatRoom.password,
                chatRoom.fileName,
                chatRoom.fontColorType
        )
    }
}