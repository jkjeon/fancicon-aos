package com.wishpoke.fanciticon.data.repository

import com.wishpoke.fanciticon.data.api.RetrofitService
import com.wishpoke.fanciticon.data.model.chat.Chat
import com.wishpoke.fanciticon.data.model.chat.ChatMember
import com.wishpoke.fanciticon.data.model.chat.ChatRoom
import com.wishpoke.fanciticon.data.model.response.DefaultResponse
import com.wishpoke.fanciticon.data.model.response.ObjectResponse
import com.wishpoke.fanciticon.presenation.resource.SingleResponse
import com.wishpoke.fanciticon.util.Logger
import com.wishpoke.fanciticon.util.constants.ChatActionTypes
import com.wishpoke.fanciticon.util.constants.Constants
import com.wishpoke.fanciticon.util.constants.Errors
import com.wishpoke.fanciticon.util.listener.SocketStateListener
import com.wishpoke.fanciticon.util.listener.ValueCallback
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import retrofit2.Response
import ua.naiksoftware.stomp.Stomp
import ua.naiksoftware.stomp.StompClient
import ua.naiksoftware.stomp.dto.LifecycleEvent
import ua.naiksoftware.stomp.dto.StompMessage
import java.util.*

interface ChatRepository {
    fun connectSocket(socketStateListener: SocketStateListener): Disposable?
    fun enterChatRoom(roomId: String, valueCallback: ValueCallback<StompMessage>): Disposable?
    fun joinChatRoom(userId: String?, roomId: String, stageName: String, fontColorType: Int = 0, fcmToken: String): Disposable?
    fun exitChatRoom(userId: String?, roomId: String, stageName: String): Disposable?
    fun closeChatRoom(roomId: String): Disposable?
    fun sendMessage(userId: String?, roomId: String, stageName: String, fontColorType: Int = 0, message: String, fileName: String): Disposable?
    fun kickChatMember(roomId: String, userId: String, stageName: String, userNo: Int): Disposable?

    fun registerFcmToken(userId: String, fcmToken: String, singleResponse: SingleResponse<DefaultResponse>): Disposable
    suspend fun loadMessages(userId: String?, roomId: String, messageNo: Int = 0): Response<ObjectResponse<List<Chat>>>
    suspend fun togglePush(userId: String?, roomId: String): Response<ObjectResponse<Boolean>>
    suspend fun likeChatRoom(roomId: String): Response<ObjectResponse<Any>>
    suspend fun shareChatRoom(roomId: String): Response<ObjectResponse<Any>>
    suspend fun getChatRoomDetail(userId: String?, roomId: String): Response<ObjectResponse<ChatRoom>>
    suspend fun getChatRoomMembers(roomId: String): Response<ObjectResponse<List<ChatMember>>>
}

class ChatRepositoryImpl : ChatRepository {

    private var stompClient: StompClient? = null

    companion object {
        const val DESTINATION_RECEIVE = "/subscribeChannelReceiver"
        const val DESTINATION_SEND = "/app/subscribeSenderV2"
        const val DESTINATION_ENTER = "/app/roomEventV2/subscribe" // 구독시작을 서버 및 앱 사용자들에게 공지
        const val DESTINATION_EXIT = "/app/roomEventV2/unsubscribe" // 구독 취소 공지
        const val DESTINATION_KICK = "/app/roomEventV2/banMember"
        const val DESTINATION_CLOSE = "/app/roomEventV2/roomEnded" // 방 폭파
    }

    fun init() {
        try {
            stompClient = Stomp.over(Stomp.ConnectionProvider.OKHTTP, "ws://" + Constants.ChatHostName + "/stomp/websocket")
            stompClient?.connect()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun connectSocket(socketStateListener: SocketStateListener): Disposable? {
        init()
        return if (stompClient != null) {
            stompClient!!.lifecycle()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe ({ lifeCycleEvent ->
                        if (lifeCycleEvent.type == LifecycleEvent.Type.OPENED) {
                            socketStateListener.onSocketOpened()
                        }
                    }, {
                        Logger.e("Test", "connectSocket Throwable: ${it.message}")
                    })
        } else {
            null
        }
    }

    override fun enterChatRoom(roomId: String, valueCallback: ValueCallback<StompMessage>): Disposable? {
        return if (stompClient != null) {
            stompClient!!.topic("$DESTINATION_RECEIVE/${roomId}")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        valueCallback.onDone(it)
                    }, {
                        Logger.e("Test", "subscribeChatRoom Throwable: ${it.message}")
                    })
        } else {
            null
        }
    }

    override fun joinChatRoom(userId: String?, roomId: String, stageName: String, fontColorType: Int, fcmToken: String): Disposable? {
        return try {
            val payload = JSONObject().apply {
                put("actionType", ChatActionTypes.ENTER)
                put("userId", userId)
                put("stageName", stageName)
                put("fontColorType", fontColorType)
                put("token", fcmToken)
            }
            stompClient!!.send("$DESTINATION_ENTER/${roomId}", payload.toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe()
        } catch (e: Exception) {
            null
        }
    }

    override fun exitChatRoom(userId: String?, roomId: String, stageName: String): Disposable? {
        return try {
            val payload = JSONObject().apply {
                put("actionType", ChatActionTypes.EXIT)
                put("userId", userId)
                put("stageName", stageName)
            }
            stompClient!!.send("$DESTINATION_EXIT/${roomId}", payload.toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe()
        } catch (e: Exception) {
            null
        }
    }

    override fun sendMessage(userId: String?, roomId: String, stageName: String, fontColorType: Int, message: String, fileName: String): Disposable? {
        return try {
            val payload = JSONObject().apply {
                put("actionType", ChatActionTypes.MESSAGE)
                put("userId", userId)
                put("createTime", Date().time / 1000)
                put("fileName", if (fileName.isEmpty()) {
                    null
                } else {
                    fileName
                })
                put("msg", message)
                put("stageName", stageName)
                put("fontColorType", fontColorType)    
            }
            return sendPayload(roomId, payload.toString())
        } catch (e: Exception) {
            null
        }
    }

    private fun sendPayload(roomId: String, payload: String): Disposable? {
        return try {
            stompClient!!
                    .send("$DESTINATION_SEND/${roomId}", payload)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe()
        } catch (e: Exception) {
            null
        }
    }

    override fun closeChatRoom(roomId: String): Disposable? {
        return if (stompClient != null) {
            val payload = JSONObject()
            stompClient!!.send("$DESTINATION_CLOSE/${roomId}", payload.toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe()
        } else {
            null
        }
    }

    override fun kickChatMember(roomId: String, userId: String, stageName: String, userNo: Int): Disposable? {
        return try {
            val payload = JSONObject().apply {
                put("actionType", ChatActionTypes.KICK)
                put("userId", userId)
                put("stageName", stageName)
                put("userNo", userNo)
            }
            stompClient!!.send("$DESTINATION_KICK/${roomId}", payload.toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe()
        } catch (e: Exception) {
            null
        }
    }

    override suspend fun likeChatRoom(roomId: String): Response<ObjectResponse<Any>> {
        return RetrofitService.getAsyncApi().likeChatRoomV2(roomId)
    }

    override suspend fun shareChatRoom(roomId: String): Response<ObjectResponse<Any>> {
        return RetrofitService.getAsyncApi().shareChatRoomV2(roomId)
    }


    override suspend fun getChatRoomDetail(userId: String?, roomId: String): Response<ObjectResponse<ChatRoom>> {
        return RetrofitService.getAsyncApi().getChatRoomDetailV2(userId, roomId)
    }

    override suspend fun getChatRoomMembers(roomId: String): Response<ObjectResponse<List<ChatMember>>> {
        return RetrofitService.getAsyncApi().getChatRoomMembersV2(roomId)
    }

    override suspend fun togglePush(userId: String?, roomId: String): Response<ObjectResponse<Boolean>> {
        return RetrofitService.getAsyncApi().togglePushV2(userId, roomId)
    }

    override suspend fun loadMessages(userId: String?, roomId: String, messageNo: Int): Response<ObjectResponse<List<Chat>>> {
        return RetrofitService.getAsyncApi().loadPreviousMessages(userId, roomId, messageNo)
    }

    override fun registerFcmToken(userId: String, fcmToken: String, singleResponse: SingleResponse<DefaultResponse>): Disposable {
        return RetrofitService.getApi().insertToken(userId, fcmToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ singleResponse.accept(it) }, { singleResponse.accept(it, Errors.NETWORK) })
    }

}