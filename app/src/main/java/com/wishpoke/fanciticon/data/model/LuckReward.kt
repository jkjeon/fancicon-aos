package com.wishpoke.fanciticon.data.model

import java.util.*

data class LuckReward(val degree: Float, val point: Int = 0) {
    companion object {
        const val REWARD4000_DEGREE = 135
        const val REWARD200_DEGREE = 45

        // 꽝인 각도를 랜덤으로 가져온다.
        fun getRandomDegree(): Float {
            var random = 0
            do {
                random = Random().nextInt(360) + 1
            } while (random == REWARD4000_DEGREE || random == REWARD200_DEGREE)
            return random.toFloat()
        }
    }


}