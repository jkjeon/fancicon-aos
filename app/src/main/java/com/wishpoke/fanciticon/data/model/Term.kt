package com.wishpoke.fanciticon.data.model

data class Term(
        var type: String = "",
        var content: String = ""
)