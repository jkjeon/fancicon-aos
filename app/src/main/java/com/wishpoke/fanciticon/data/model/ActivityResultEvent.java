package com.wishpoke.fanciticon.data.model;

import android.content.Intent;

import lombok.Data;

@Data
public class ActivityResultEvent {
    public int requestCode;
    public int resultCode;
    public Intent data;

    public ActivityResultEvent(int requestCode, int resultCode, Intent data) {
        this.requestCode = requestCode;
        this.resultCode = resultCode;
        this.data = data;
    }
}

