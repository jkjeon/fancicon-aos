package com.wishpoke.fanciticon.data.model.response;

import lombok.Data;

@Data
public class BaseResponse {
    public StatusInfoBean status;

    @Data
    public static class StatusInfoBean {
        /**
         * code : 0
         * msg : 성공
         */
        public int code;
        public String msg;
    }

    public int maxSize;

    public boolean isSuccessful() {
        return status.code == 0;
    }
}
