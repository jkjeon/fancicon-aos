package com.wishpoke.fanciticon.data.model

import com.google.gson.annotations.SerializedName

class VersionInfo(@SerializedName("Android") var android: Android) {
    data class Android(var version: String = "0.0.0", var required: Int = 0)
}