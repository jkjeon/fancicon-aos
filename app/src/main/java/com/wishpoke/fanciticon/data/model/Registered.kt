package com.wishpoke.fanciticon.data.model

data class Registered(val isRegistered: Int = 0)