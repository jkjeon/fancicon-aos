package com.wishpoke.fanciticon.data.repository

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.api.RetrofitService
import com.wishpoke.fanciticon.data.model.Banner
import com.wishpoke.fanciticon.data.model.Category
import com.wishpoke.fanciticon.data.model.VersionInfo
import com.wishpoke.fanciticon.data.model.response.ObjectResponse
import com.wishpoke.fanciticon.data.model.response.ResultResponse
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import com.wishpoke.fanciticon.presenation.resource.SingleResponse
import com.wishpoke.fanciticon.util.FileManager
import com.wishpoke.fanciticon.util.Util
import com.wishpoke.fanciticon.util.constants.Errors
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.Retrofit
import java.io.File
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset
import java.text.Normalizer
import java.util.*
import kotlin.collections.ArrayList

class MainRepository(val application: Application) {
    val recent = MutableLiveData<ArrayList<String>>()
    private val appStorage: AppStorage = AppStorage(application)
    var emojiCategoryStart = 0

    init {
        recent.postValue(appStorage.recentArrayList)
    }

    // 이미지를 사용하는 카테고리 - 순서에 따라 (설정된 첫번째 스크린 제외)
    val categories: List<Category>
        get() {
            val localeCode = Locale.getDefault().language
            val categoryIdsArray = application.resources.getStringArray(R.array.categories)
            val categoryList = ArrayList<Category>()
            val categoryIds = Arrays.asList(*categoryIdsArray)
            // 이미지를 사용하는 카테고리 - 순서에 따라 (설정된 첫번째 스크린 제외)
            val firstScreen = appStorage.getString(AppStorage.FIRST_SCREEN_CATEGORY_ID, "favorite")
            var imageCategories = arrayOf("favorite", "pung", "chat", "custom")

            val imageCategoryList: MutableList<String> = ArrayList()
            imageCategoryList.add(firstScreen)
            for (categoryId in imageCategories) {
                if (categoryId != firstScreen) {
                    imageCategoryList.add(categoryId)
                }
            }
            // 카테고리를 가나다 순으로 정렬
            for (categoryId in categoryIds) {
                if (!imageCategoryList.contains(categoryId)) {
                    val categoryResId = application.resources.getIdentifier(categoryId, "string", application.packageName)
                    val category = application.resources.getString(categoryResId)
                    if (categoryId != "gaegaekki" || Util.isKorean()) {
                        categoryList.add(Category(categoryId, category))
                    }
                }
            }
            // 움라우트가 있는 랭귀지 리스트
            val latinLanguages = arrayOf("fr", "de", "es", "pt")
            val latinLanguageList = listOf(*latinLanguages)
            categoryList.sortWith(Comparator { o1, o2 ->
                var o1Name = o1.name
                var o2Name = o2.name
                if (latinLanguageList.contains(localeCode)) {
                    try {
                        o1Name = String(Normalizer.normalize(o1Name, Normalizer.Form.NFKD).toByteArray(charset("ascii")), Charset.forName("ascii")).replace("\\?".toRegex(), "")
                        o2Name = String(Normalizer.normalize(o2Name, Normalizer.Form.NFKD).toByteArray(charset("ascii")), Charset.forName("ascii")).replace("\\?".toRegex(), "")
                    } catch (e: UnsupportedEncodingException) {
                        e.printStackTrace()
                    }
                }
                o1Name.compareTo(o2Name!!)
            })
            // 한국에만 있는 카테고리들
            if (!Util.isKorean()) {
                imageCategoryList.remove("pung")
                imageCategoryList.remove("mission")
                imageCategoryList.remove("chat")
                imageCategoryList.remove("market")
            }
            // 이미지 카테고리 추가
            emojiCategoryStart = imageCategoryList.size
            for (i in imageCategoryList.indices.reversed()) {
                val categoryId = imageCategoryList[i]
                categoryList.add(0, Category(categoryId, categoryId))
            }
            return categoryList
        }

    fun getEmojiList(categoryId: String): List<String> {
        val resId = application.resources.getIdentifier(categoryId, "array", application.packageName)
        val emojiList = application.resources.getStringArray(resId)
        return listOf(*emojiList)
    }

    fun addRecent(emoji: String?) {
        recent.postValue(appStorage.addRecent(emoji))
    }

    fun removeRecent(emoji: String?) {
        recent.postValue(appStorage.removeRecent(emoji))
    }

    suspend fun getBanners(): Response<ObjectResponse<List<Banner>>> {
        return RetrofitService.getAsyncApi().getBanners("")
    }

    suspend fun getVersion(): Response<ObjectResponse<VersionInfo>> {
        return RetrofitService.getAsyncApi().getVersion()
    }

    fun downloadFile(fileName: String, saveFileName: String, singleResponse: SingleResponse<File>): Disposable {
        return RetrofitService.getApi().downloadFile(fileName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    val filePath = FileManager.getDownloadFilePath(fileName, saveFileName)
                    val file = FileManager.writeResponseBodyToDisk(filePath, it)
                    if (file != null) {
                        singleResponse.accept(file)
                    } else {
                        singleResponse.accept(Throwable("File not found"), Errors.FILE_NOT_FOUND)
                    }
                }, { singleResponse.accept(it, Errors.NETWORK) })
    }
}