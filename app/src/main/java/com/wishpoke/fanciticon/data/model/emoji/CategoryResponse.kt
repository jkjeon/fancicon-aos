package com.wishpoke.fanciticon.data.model.emoji

import com.wishpoke.fanciticon.data.model.Category

data class CategoryResponse(
        val categories: List<Category> = ArrayList(),
        val emojiCategoryStartPosition: Int = 0
)