package com.wishpoke.fanciticon.data.repository

import com.wishpoke.fanciticon.data.model.Category
import com.wishpoke.fanciticon.data.model.emoji.CategoryResponse
import com.wishpoke.fanciticon.util.Util
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset
import java.text.Normalizer
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList

class EmojiRepositoryImpl() : EmojiRepository {

    override suspend fun getCategoryEmojis(categoryId: String?) {
        // TODO:
    }

    override suspend fun getCategories(firstScreen: String, categoryHashMap: HashMap<String, String>): CategoryResponse {
        val categoryList = ArrayList<Category>()

        val localeCode = Locale.getDefault().language
        // 이미지를 사용하는 카테고리 - 순서에 따라 (설정된 첫번째 스크린 제외)
        val imageCategoryIds = arrayOf("favorite", "pung", "chat", "custom")

        val imageCategoryList: MutableList<String> = ArrayList()
        imageCategoryList.add(firstScreen)
        for (categoryId in imageCategoryIds) {
            if (categoryId != firstScreen) {
                imageCategoryList.add(categoryId)
            }
        }
        // 카테고리를 가나다 순으로 정렬
        val ids = categoryHashMap.map {
            it.key
        }
        for (categoryId in ids) {
            if (!imageCategoryList.contains(categoryId)) {
                val name = categoryHashMap[categoryId]
                if (categoryId != "gaegaekki" || Util.isKorean()) {
                    categoryList.add(Category(categoryId, name))
                }
            }
        }
        // 움라우트가 있는 랭귀지 리스트
        val latinLanguages = arrayListOf("fr", "de", "es", "pt")
        categoryList.sortWith(Comparator { o1, o2 ->
            var o1Name = o1.name
            var o2Name = o2.name
            if (latinLanguages.contains(localeCode)) {
                try {
                    o1Name = String(Normalizer.normalize(o1Name, Normalizer.Form.NFKD).toByteArray(charset("ascii")),
                            Charset.forName("ascii")).replace("\\?".toRegex(), "")
                    o2Name = String(Normalizer.normalize(o2Name, Normalizer.Form.NFKD).toByteArray(charset("ascii")),
                            Charset.forName("ascii")).replace("\\?".toRegex(), "")
                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                }
            }
            o1Name.compareTo(o2Name!!)
        })
        // 한국에만 있는 카테고리들
        if (!Util.isKorean()) {
            imageCategoryList.remove("pung")
            imageCategoryList.remove("chat")
        }
        // 이미지 카테고리 추가
        val emojiCategoryStartPosition = imageCategoryList.size
        for (i in imageCategoryList.indices.reversed()) {
            val categoryId = imageCategoryList[i]
            categoryList.add(0, Category(categoryId, categoryId))
        }
        return CategoryResponse(
                categoryList,
                emojiCategoryStartPosition
        )
    }
}

interface EmojiRepository {
    suspend fun getCategoryEmojis(categoryId: String?)

    suspend fun getCategories(firstScreen: String, categoryHashMap: HashMap<String, String>): CategoryResponse
}