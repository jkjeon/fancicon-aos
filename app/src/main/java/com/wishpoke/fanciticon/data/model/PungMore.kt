package com.wishpoke.fanciticon.data.model

enum class PungMore {
    MODIFY,
    DELETE,
    HIDE,
    REPORT
}