package com.wishpoke.fanciticon.data.model

data class Nickname(
        val uuid: String? = null,
        val userName: String? = null
)