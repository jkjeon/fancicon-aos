package com.wishpoke.fanciticon.data.model

data class RepoResponse<T> (val result: T? = null, val throwable: AppThrowable? = null)