package com.wishpoke.fanciticon.data.model

import com.wishpoke.fanciticon.data.model.response.BaseResponse

class AppThrowable(var code: Int, var customMessage: String) : Throwable() {
    constructor(statusInfo: BaseResponse.StatusInfoBean) : this(statusInfo.code, statusInfo.msg)
}