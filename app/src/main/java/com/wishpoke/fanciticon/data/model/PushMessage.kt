package com.wishpoke.fanciticon.data.model

data class PushMessage(val execution: String? = null,
                       val badge: Int = 0,
                       val actionType: Int = 0,
                       val title: String? = null,
                       val body: String? = null)