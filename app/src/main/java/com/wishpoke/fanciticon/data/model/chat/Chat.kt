package com.wishpoke.fanciticon.data.model.chat

data class Chat(var messageNo: Int = 0,
                var msg: String? = null,
                var userId: String? = null,
                var stageName: String = "",
                var roomId: String? = null,
                var actionType: Int = 0,
                var fileName: String? = null,
                var createTime: Long = 0,
                var fontColorType: Int = 0,
                var isTemp: Boolean = false) {
    fun isEntered(): Boolean {
        return msg?.contains("님이 입장했습니다.") == true
    }

    fun isNotEmpty(): Boolean {
        return !msg.isNullOrEmpty() || !fileName.isNullOrEmpty()
    }
}