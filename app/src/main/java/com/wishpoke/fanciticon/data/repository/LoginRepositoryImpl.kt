package com.wishpoke.fanciticon.data.repository

import com.wishpoke.fanciticon.data.api.RetrofitService
import com.wishpoke.fanciticon.data.model.PhoneInfo
import com.wishpoke.fanciticon.data.model.Registered
import com.wishpoke.fanciticon.data.model.User
import com.wishpoke.fanciticon.data.model.response.DefaultResponse
import com.wishpoke.fanciticon.data.model.response.ObjectResponse
import com.wishpoke.fanciticon.presenation.resource.SingleResponse
import com.wishpoke.fanciticon.util.constants.Errors
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import java.util.*

class LoginRepository {

    private val countryCode = Locale.getDefault().country.toLowerCase()

    suspend fun login(user: User, fcmToken: String): Response<ObjectResponse<User>> {
        return RetrofitService.getAsyncApi().login(
                user.userId ?: "",
                user.password ?: "",
                user.createTime,
                user.authorization,
                fcmToken
        )
    }

    suspend fun logout(userId: String?, fcmToken: String): Response<ObjectResponse<Any>> {
        return RetrofitService.getAsyncApi().logout(userId ?: "", fcmToken)
    }

    fun getUserDetail(userId: String?, singleResponse: SingleResponse<ObjectResponse<User>>): Disposable {
        return RetrofitService.getApi().getUserDetail(userId, "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ singleResponse.accept(it) }, { singleResponse.accept(it, Errors.NETWORK) })
    }

    fun checkId(userId: String, referralCode: String, singleResponse: SingleResponse<DefaultResponse>): Disposable  {
        return handleRequest(RetrofitService.getApi().checkId(userId, referralCode),
                singleResponse)

    }

    fun leave(user: User, singleResponse: SingleResponse<DefaultResponse>): Disposable {
        return handleRequest(RetrofitService.getApi().leave(
                user.userId, user.password ?: "",
                user.createTime,
                user.authorization, ""),
                singleResponse)
    }

    fun insertUser(user: User, singleResponse: SingleResponse<ObjectResponse<User>>): Disposable {
        return handleRequest(RetrofitService.getApi()
                .insertUser(user.userId ?: "",
                        user.password ?: "",
                        user.createTime,
                        user.referralCode,
                        user.nacd ?: "",
                        user.phone ?: ""),
                singleResponse)
    }

    private fun <T> handleRequest(request: Single<T>, singleResponse: SingleResponse<T>): Disposable {
        return request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ singleResponse.accept(it) }, { singleResponse.accept(it, Errors.NETWORK) })
    }

    fun findId(phoneInfo: PhoneInfo, singleResponse: SingleResponse<ObjectResponse<Any>>): Disposable {
        return RetrofitService.getApi().findId(phoneInfo.nacd ?: "", phoneInfo.phone ?: "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ singleResponse.accept(it) }, { singleResponse.accept(it, Errors.NETWORK) })
    }

    fun updatePassword(phoneInfo: PhoneInfo, password: String, singleResponse: SingleResponse<DefaultResponse>): Disposable {
        return RetrofitService.getApi().updatePassword(
                password,
                phoneInfo.nacd ?: "", phoneInfo.phone ?: "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ singleResponse.accept(it) }, { singleResponse.accept(it, Errors.NETWORK) })
    }

    fun updateUserPoint(user: User, amount: Int, provider: String, singleResponse: SingleResponse<DefaultResponse>): Disposable {
        return RetrofitService.getApi().updateUserPoint(
                user.userId,
                user.password,
                user.createTime,
                user.authorization, provider, amount)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ singleResponse.accept(it) }, { singleResponse.accept(it, Errors.NETWORK) })
    }

    suspend fun updateProfile(user: User, updateId: String?, updatePassword: String?, updateUserName: String?): Response<ObjectResponse<Any>> {
        return RetrofitService.getAsyncApi().updateProfile(
                countryCode,
                user.userId,
                user.password,
                user.createTime,
                user.authorization,
                updateId, updatePassword, updateUserName
        )
    }

    suspend fun checkNickname(userName: String?): Response<ObjectResponse<Registered>> {
        return RetrofitService.getAsyncApi().checkNickname(
                countryCode, userName
        )
    }

    fun getToken(user: User, singleResponse: SingleResponse<ObjectResponse<Any>>): Disposable {
        return RetrofitService.getApi().getToken(
                user.userId, user.password, user.createTime, "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ singleResponse.accept(it) }, { singleResponse.accept(it, Errors.NETWORK) })
    }
}