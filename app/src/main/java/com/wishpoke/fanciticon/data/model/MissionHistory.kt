package com.wishpoke.fanciticon.data.model

data class MissionHistory(var createTime: Long = 0,
                          var updateType: Int = 1, // 3: 룰렛 적립
                          var referralId: String? = null, // 나를 추천인으로 한 userId. 덕분에 500인싸력이 적립되었다는 얘기
                          var updateHistory: Int = 0,
                          var couponTts: String? = null,
                          var couponPrice: String? = null,
                          var lastPoint: Int = 0,
                          var purchaseCode: String? = null)