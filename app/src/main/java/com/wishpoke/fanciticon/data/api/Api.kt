package com.wishpoke.fanciticon.data.api

import com.wishpoke.fanciticon.data.model.*
import com.wishpoke.fanciticon.data.model.chat.Chat
import com.wishpoke.fanciticon.data.model.chat.ChatMember
import com.wishpoke.fanciticon.data.model.chat.ChatRoom
import com.wishpoke.fanciticon.data.model.pung.Post
import com.wishpoke.fanciticon.data.model.pung.Post.Reply
import com.wishpoke.fanciticon.data.model.response.DefaultResponse
import com.wishpoke.fanciticon.data.model.response.ObjectResponse
import com.wishpoke.fanciticon.data.model.response.ResultResponse
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*
import java.util.*

interface Api {
    /* 휴대전화번호 제외, 모든 필드들을 encode해서 보냄 */
    /* ============= INSSATICON API ============= */

    // 인싸펑 닉네임 찾기
    @FormUrlEncoded
    @POST("/aliasApi/detail")
    suspend fun getNickname(
            @Header("countryCode") countryCode: String?,
            @Field("uuid") uuid: String?
    ): Response<ObjectResponse<Nickname?>>

    // Alias 체크
    @FormUrlEncoded
    @POST("/aliasApi/aliasValidateCheck")
    suspend fun checkNickname(
            @Header("countryCode") countryCode: String?,
            @Field("userName") username: String?
    ): Response<ObjectResponse<Registered>>

    // 회원 ID, PW, Alias 동시변경
    @FormUrlEncoded
    @POST("/userApi/updateAll")
    suspend fun updateProfile(
            @Header("countryCode") countryCode: String?,
            @Header("userid") userId: String?,
            @Header("password") password: String?,
            @Header("stamptime") stampTime: Long,
            @Header("authorization") authorization: String?,
            @Field("updateId") updateId: String?,
            @Field("updatePassword") updatePassword: String?,
            @Field("updateUserName") updateUsername: String?
    ): Response<ObjectResponse<Any>>

    // 게시글 금은동싸 키값찾기
    @FormUrlEncoded
    @POST("/postApiV2/getPopularPost")
    suspend fun getPopularPostNo(@Header("countryCode") countryCode: String?,
                         @Field("boardNo") boardNo: Int): Response<ObjectResponse<List<Int>>>

    // 게시글 불러오기 (코루틴)
    @FormUrlEncoded
    @POST("/postApiV2/list")
    suspend fun getPostList(
            @Header("countryCode") countryCode: String?,
            @Header("userId") userId: String?,
            @Field("orderType") orderType: Int,
            @Field("boardNo") boardNo: Int,
            @Field("requestPage") requestPage: Int): Response<ObjectResponse<List<Post>>>


    // 게시글 상세
    @FormUrlEncoded
    @POST("/postApiV2/detail")
    suspend fun getPostDetail(@Header("countryCode") countryCode: String?,
                              @Header("userId") userId: String?,
                              @Field("postNo") postNo: Int): Response<ObjectResponse<Post>>

    // 게시글 등록 V2
    @FormUrlEncoded
    @POST("/postApiV2/insert")
    suspend fun insertPostV2(
            @Header("countryCode") countryCode: String?,
            @Header("userId") userId: String?,
            @Header("token") token: String?,
            @Field("boardNo") boardNo: Int,
            @Field("userName") userName: String?,
            @Field("comment") comment: String?,
            @Field("password") password: String?,
            @Field("uuid") uuid: String?,
            @Field("linkUrl") linkUrl: String?,
            @Field("fileName") fileName: String?,
            @Field("movieName") movieName: String?): Response<ObjectResponse<Any>>

    // 게시글 수정
    @FormUrlEncoded
    @POST("/postApiV2/update")
    suspend fun updatePost(
            @Header("countryCode") countryCode: String?,
            @Header("userId") userId: String?,
            @Field("postNo") postNo: Int,
            @Field("comment") comment: String?,
            @Field("uuid") uuid: String?,
            @Field("fileName") fileName: String?,
            @Field("movieName") movieName: String?,
            @Field("linkUrl") linkUrl: String?,
            @Field("password") password: String?): Response<ObjectResponse<Any>>

    // 게시글 수정/삭제 전 권한 체크
    @FormUrlEncoded
    @POST("/postApiV2/updateValidate")
    suspend fun validatePost(
            @Header("countryCode") countryCode: String?,
            @Header("userId") userId: String?,
            @Field("postNo") postNo: Int,
            @Field("uuid") uuid: String?,
            @Field("password") password: String?): Response<ObjectResponse<Any>>

    // 게시글 삭제
    @FormUrlEncoded
    @POST("/postApiV2/delete")
    suspend fun deletePost(
            @Header("countryCode") countryCode: String?,
            @Header("userId") userId: String?,
            @Field("postNo") postNo: Int,
            @Field("uuid") uuid: String?,
            @Field("password") password: String?): Response<ObjectResponse<Any>>



    // 게시글의 좋아요 카운트 + 1
    @FormUrlEncoded
    @POST("/postApiV2/likeCntUp")
    suspend fun likeCountUp(@Field("postNo") postNo: Int): Response<ObjectResponse<Any>>

    // 공유 카운트 + 1
    @FormUrlEncoded
    @POST("/postApiV2/shareCntUp")
    suspend fun shareCountUp(@Field("postNo") postNo: Int): Response<ObjectResponse<Any>>

    // 댓글 리스트 불러오기
    @FormUrlEncoded
    @POST("/replyApiV2/list")
    suspend fun getReplyList(
            @Header("countryCode") countryCode: String?,
            @Header("userId") userId: String?,
            @Field("postNo") postNo: Int,
            @Field("requestPage") requestPage: Int): Response<ObjectResponse<List<Reply>>>

    // 댓글의 좋아요 카운트 + 1
    @FormUrlEncoded
    @POST("/replyApiV2/likeCntUp")
    suspend fun likeCountUpReply(@Field("replyNo") replyNo: Int): Response<ObjectResponse<Any>>

    // 댓글 등록
    @FormUrlEncoded
    @POST("/replyApiV2/insert")
    suspend fun insertReply(
            @Header("countryCode") countryCode: String?,
            @Header("userId") userId: String?,
            @Field("postNo") postNo: Int,
            @Field("userName") username: String?,
            @Field("comment") comment: String?,
            @Field("uuid") uuid: String?,
            @Field("password") password: String?,
            @Field("fileName") fileName: String?,
            @Field("movieName") movieName: String?): Response<ObjectResponse<Any>>

    // 댓글 수정, 삭제전 권한체크
    @FormUrlEncoded
    @POST("/replyApiV2/updateValidate")
    suspend fun validateReply(
            @Header("countryCode") countryCode: String?,
            @Header("userId") userId: String?,
            @Field("replyNo") replyNo: Int?,
            @Field("uuid") uuid: String?,
            @Field("password") password: String?
    ): Response<ObjectResponse<Any>>

    // 댓글 수정
    @FormUrlEncoded
    @POST("/replyApiV2/update")
    suspend fun updateReply(
            @Header("countryCode") countryCode: String?,
            @Header("userId") userId: String?,
            @Field("replyNo") replyNo: Int?,
            @Field("uuid") uuid: String?,
            @Field("password") password: String?,
            @Field("comment") comment: String?,
            @Field("fileName") fileName: String?,
            @Field("movieName") movieName: String?
    ): Response<ObjectResponse<Any>>

    // 댓글 삭제
    @FormUrlEncoded
    @POST("/replyApiV2/delete")
    suspend fun deleteReply(@Header("countryCode") countryCode: String?,
                    @Header("userId") userId: String?,
                    @Field("replyNo") replyNo: Int?,
                    @Field("uuid") uuid: String?,
                    @Field("password") password: String?): Response<ObjectResponse<Any>>


    // 신고 등록 (게시물)
    @FormUrlEncoded
    @POST("/reportApi/insert")
    suspend fun report(@Header("countryCode") countryCode: String?,
               @Header("userId") userId: String?,
               @Field("requestType") requestType: String?,
               @Field("requestKey") requestKey: String?,
               @Field("comment") comment: String?): Response<ObjectResponse<Any>>

    // 파일 다운로드
    @GET("/fileApi/getFile")
    fun downloadFile(@Query("downloadFileName") downloadFileName: String?): Single<ResponseBody>

    // 파일 업로드
    @Multipart
    @POST("/fileApi/insert")
    suspend fun uploadFile(@Part file: MultipartBody.Part): Response<String>

    // 파일 업로드
    @FormUrlEncoded
    @POST("/fileApi/delete")
    suspend fun deleteFile(@Field("deleteFileName") deleteFileName: String?): Response<Boolean>

    /* ============= NEW API ============= */
    @FormUrlEncoded
    @POST("api/banner/list/V2")
    suspend fun getBanners(@Field("") empty: String?): Response<ObjectResponse<List<Banner>>>

    // 아이디 중복 체크
    @FormUrlEncoded
    @POST("userApi/checkId")
    fun checkId(@Header("userid") userId: String?,
                @Field("referralCode") referralCode: String?): Single<DefaultResponse>

    // 회원 가입
    @FormUrlEncoded
    @POST("userApi/insert")
    fun insertUser(@Header("userid") userId: String?,
                   @Header("password") password: String?,
                   @Header("stamptime") stampTime: Long,
                   @Header("recommendedcode") recommendedcode: String?,
                   @Field("nacd") nacd: String?,
                   @Field("phone") phone: String?): Single<ObjectResponse<User>>

    // 룰렛 포인트 적립
    @FormUrlEncoded
    @POST("userApi/updateRoulettePoint")
    suspend fun earnRoulettePoint(@Header("userid") userId: String?,
                                  @Header("password") password: String?,
                                  @Header("stamptime") stampTime: Long,
                                  @Header("authorization") authorization: String?,
                                  @Field("userPoint") userPoint: Int): Response<ObjectResponse<Any>>

    // 룰렛 마지막 적립시간
    @FormUrlEncoded
    @POST("pointApi/getLastRouletteTime")
    suspend fun getLastRouletteTime(@Header("userid") userId: String?,
                                    @Header("password") password: String?,
                                    @Header("stamptime") stampTime: Long,
                                    @Header("authorization") authorization: String?): Response<ObjectResponse<Long>>

    // 회원 탈퇴
    @FormUrlEncoded
    @POST("userApi/dropOut")
    fun leave(@Header("userid") userId: String?,
              @Header("password") password: String?,
              @Header("stamptime") stampTime: Long,
              @Header("authorization") authorization: String?,
              @Field("") empty: String?): Single<DefaultResponse>

    // 로그인
    @FormUrlEncoded
    @POST("userApi/login")
    suspend fun login(@Header("userid") userId: String?,
              @Header("password") password: String?,
              @Header("stamptime") stampTime: Long,
              @Header("authorization") authorization: String?,
              @Field("token") fcmToken: String?): Response<ObjectResponse<User>>

    // 로그아웃
    @FormUrlEncoded
    @POST("userApi/logout")
    suspend fun logout(@Header("userid") userId: String?,
               @Field("token") fcmToken: String?): Response<ObjectResponse<Any>>

    // 유저 상세
    @FormUrlEncoded
    @POST("userApi/detail")
    fun getUserDetail(@Header("userid") userId: String?,
                      @Field("") empty: String?): Single<ObjectResponse<User>>

    // 아이디 찾기
    @FormUrlEncoded
    @POST("userApi/findId")
    fun findId(@Field("nacd") nacd: String?,
               @Field("phone") phone: String?): Single<ObjectResponse<Any>>

    // 패스워드 변경
    @FormUrlEncoded
    @POST("userApi/updatePassword")
    fun updatePassword(@Header("password") password: String?,
                       @Field("nacd") nacd: String?,
                       @Field("phone") phone: String?): Single<DefaultResponse>

    // 인싸력 변경
    @FormUrlEncoded
    @POST("userApi/updatePoint")
    fun updateUserPoint(@Header("userid") userId: String?,
                        @Header("password") password: String?,
                        @Header("stamptime") stampTime: Long,
                        @Header("authorization") authorization: String?,
                        @Header("provider") provider: String?,
                        @Field("userPoint") userPoint: Int): Single<DefaultResponse>

    // 쿠폰 발급 요청
    @FormUrlEncoded
    @POST("userApi/getTicket")
    fun getTicket(@Header("userid") userId: String?,
                  @Header("password") password: String?,
                  @Header("stamptime") stampTime: Long,
                  @Header("authorization") authorization: String?,
                  @Field("couponPrice") couponPrice: Int): Single<DefaultResponse>

    // 토큰 재발급
    @FormUrlEncoded
    @POST("userApi/tokenReIssue")
    fun getToken(@Header("userid") userId: String?,
                 @Header("password") password: String?,
                 @Header("stamptime") stampTime: Long,
                 @Field("") empty: String?): Single<ObjectResponse<Any>>

    // 활동 내역
    @FormUrlEncoded
    @POST("pointApi/list")
    suspend fun getPointHistoryList(@Header("userid") userId: String?,
                            @Header("password") password: String?,
                            @Header("stamptime") stampTime: Long,
                            @Header("authorization") authorization: String?,
                            @Field("updateType") updateType: Int): Response<ObjectResponse<List<MissionHistory>>>

    /* ============= CHAT API ============= */ // 채팅 리스트

    // 채팅방 리스트 불러오기
    @FormUrlEncoded
    @POST("chatRoomApiV2/list")
    suspend fun getChatRoomList(
            @Field("userId") userId: String?,
            @Field("requestPage") requestPage: Int?,
            @Field("boardNo") boardNo: Int?,
            @Field("orderType") orderType: Int?
    ): Response<ObjectResponse<List<ChatRoom>>>

    // 채팅방 생성 V2
    @FormUrlEncoded
    @POST("chatRoomApiV2/insert")
    suspend fun createChatRoomV2(
            @Field("userId") userId: String?,
            @Field("token") token: String?,
            @Field("stageName") stageName: String?,
            @Field("title") title: String?,
            @Field("boardNo") boardNo: Int,
            @Field("isPrivate") isPrivate: Int,
            @Field("password") password: String?,
            @Field("fileName") fileName: String?,
            @Field("fontColorType") fontColorType: Int
    ): Response<ObjectResponse<ChatRoom>>

    // 채팅방 참여 가능여부 V2
    @FormUrlEncoded
    @POST("chatRoomApiV2/joinAble")
    suspend fun canJoinToChatRoomV2(
        @Field("userId") userId: String?,
        @Field("roomId") roomId: String?,
        @Field("password") password: String?,
        @Field("stageName") stageName: String?
    ): Response<ObjectResponse<Any>>

    // 채팅방 상세정보 호출 V2
    @FormUrlEncoded
    @POST("chatRoomApiV2/detail")
    suspend fun getChatRoomDetailV2(
            @Field("userId") userId: String?,
            @Field("roomId") roomId: String?
    ): Response<ObjectResponse<ChatRoom>>

    // 채팅방 좋아요 V2
    @FormUrlEncoded
    @POST("chatRoomApiV2/likeCntUp")
    suspend fun likeChatRoomV2(
            @Field("roomId") roomId: String?
    ): Response<ObjectResponse<Any>>

    // 채팅방 공유 V2
    @FormUrlEncoded
    @POST("chatRoomApiV2/shareCntUp")
    suspend fun shareChatRoomV2(
            @Field("roomId") roomId: String?
    ): Response<ObjectResponse<Any>>

    // 채팅방 유저 리스트 V2
    @FormUrlEncoded
    @POST("chatUserApiV2/list")
    suspend fun getChatRoomMembersV2(
            @Field("roomId") roomId: String?
    ): Response<ObjectResponse<List<ChatMember>>>


    // 인기 채팅방의 키 값 불러오기 V2
    @FormUrlEncoded
    @POST("chatRoomApiV2/getPopular")
    suspend fun getPopularChatRoomIdsV2
            (@Field("boardNo") boardNo: Int): Response<ObjectResponse<List<String>>>

    // 해당 방에서의 푸시 ON/OFF toggle V2
    @FormUrlEncoded
    @POST("chatUserApi/togglePush")
    suspend fun togglePushV2(
            @Field("userId") userId: String?,
            @Field("roomId") roomId: String?
    ): Response<ObjectResponse<Boolean>>

    // 채팅방 이전 대화 불러오기
    @FormUrlEncoded
    @POST("chatMessageApiV2/list")
    suspend fun loadPreviousMessages(
            @Field("userId") userId: String?,
            @Field("roomId") roomId: String?,
            @Field("messageNo") messageNo: Int
    ): Response<ObjectResponse<List<Chat>>>

    // 토큰 추가
    @FormUrlEncoded
    @POST("api/token/insert")
    fun insertToken(@Field("userId") userId: String?,
                    @Field("token") token: String?): Single<DefaultResponse>

    /* ============= ETC API ============= */
    // 버전 체크
    @FormUrlEncoded
    @POST("versionApi/versionMap")
    suspend fun getVersion(@Field("") empty: String = ""): Response<ObjectResponse<VersionInfo>>
}