package com.wishpoke.fanciticon.data.model.emoji;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "favorite_emojis")
public class Emoji {
    @PrimaryKey(autoGenerate = false)
    @NonNull
    private String emoji;

    public Emoji(String emoji) {
        this.emoji = emoji;
    }

    public String getEmoji() {
        return emoji;
    }

    public void setEmoji(String emoji) {
        this.emoji = emoji;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Emoji)) {
            return false;
        }
        Emoji otherEmoji = (Emoji) obj;
        return otherEmoji.getEmoji().equals(emoji);
    }
}
