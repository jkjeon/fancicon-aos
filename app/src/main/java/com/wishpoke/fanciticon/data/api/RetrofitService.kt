package com.wishpoke.fanciticon.data.api

import com.google.gson.GsonBuilder
import com.wishpoke.fanciticon.util.constants.Constants
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit


object RetrofitService {

    private var instance: Api? = null

    @JvmStatic
    fun getApi(): Api {
        if (instance == null) {
            instance = getApi(Constants.Url.BASE)
        }
        return instance as Api
    }

    private val okHttpClient = OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .build()

    private val gsonFactory = GsonBuilder ()
            .setLenient()
            .create()

    private fun getApi(url: String): Api {
        val retrofitBuilder = Retrofit.Builder()

        retrofitBuilder
                .addConverterFactory(GsonConverterFactory.create(gsonFactory))
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .baseUrl(url)
        return retrofitBuilder.build().create(Api::class.java)
    }

    private val retrofit = Retrofit.Builder()
            .baseUrl(Constants.Url.BASE)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gsonFactory))
            .build()

    fun getAsyncApi(): Api = retrofit.create(Api::class.java)

    @JvmStatic
    fun getRequestBodyByKeyValuePair(pair: HashMap<String, Any?>): RequestBody? {
        val requestObject = JSONObject()
        return try {
            for (key in pair.keys) {
                requestObject.put(key, pair[key])
            }
            RequestBody.create(MediaType.parse("application/json"),
                    requestObject.toString())
        } catch (e: JSONException) {
            e.printStackTrace()
            null
        }
    }
}