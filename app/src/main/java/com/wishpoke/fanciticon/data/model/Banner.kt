package com.wishpoke.fanciticon.data.model

import com.google.gson.annotations.SerializedName

data class Banner(
        @SerializedName("bannerName") var bannerName: String = "",
        @SerializedName("linkUrl") var linkUrl: String = "",
        @SerializedName("imageUrl") var imageUrl: String = ""
)