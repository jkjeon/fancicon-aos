package com.wishpoke.fanciticon.data.model.chat

data class ChatMember(var userNo: Int = 0,
                      var roomId: String? = null,
                      var userId: String? = null,
                      var stageName: String? = null,
                      var isHost: Int = 0,
                      var fontColorType: Int = 0,
                      var pushOn: Int = 0)