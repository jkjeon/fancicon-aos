package com.wishpoke.fanciticon.data.repository

import com.wishpoke.fanciticon.data.api.RetrofitService
import com.wishpoke.fanciticon.data.model.Nickname
import com.wishpoke.fanciticon.data.model.pung.Post
import com.wishpoke.fanciticon.data.model.response.ObjectResponse
import com.wishpoke.fanciticon.util.UDTextUtils
import retrofit2.Response
import java.util.*

class PungRepositoryImpl : PungRepository {

    private val countryCode = Locale.getDefault().country.toLowerCase()

    override suspend fun getPopularPostNo(userId: String?, boardNo: Int?): Response<ObjectResponse<List<Int>>> {
        return RetrofitService.getAsyncApi().getPopularPostNo(countryCode, boardNo
                ?: 0)
    }

    override suspend fun getPostList(userId: String?, orderType: Int?, boardNo: Int?, requestPage: Int?): Response<ObjectResponse<List<Post>>> {
        return RetrofitService.getAsyncApi()
                .getPostList(countryCode,
                        userId,
                        orderType ?: 1,
                        boardNo ?: 0,
                        requestPage ?: 0)
    }

    override suspend fun getPostDetail(userId: String?, postNo: Int?): Response<ObjectResponse<Post>> {
        return RetrofitService.getAsyncApi()
                .getPostDetail(countryCode, userId, postNo ?: 0)
    }

    override suspend fun likeCountUp(postNo: Int): Response<ObjectResponse<Any>> {
        return RetrofitService.getAsyncApi()
                .likeCountUp(postNo)
    }

    override suspend fun shareCountUp(postNo: Int): Response<ObjectResponse<Any>> {
        return RetrofitService.getAsyncApi()
                .shareCountUp(postNo)
    }

    override suspend fun reportPost(userId: String?, postNo: Int?, comment: String?): Response<ObjectResponse<Any>> {
        return RetrofitService.getAsyncApi()
                .report(countryCode, userId, "post", postNo.toString(), comment)
    }

    override suspend fun updatePost(userId: String?, postNo: Int, uuid: String?, password: String?,
                                    comment: String?, fileName: String?, movieName: String?, linkUrl: String?): Response<ObjectResponse<Any>> {
        return RetrofitService.getAsyncApi()
                .updatePost(countryCode, userId, postNo, UDTextUtils.trim(comment), uuid, fileName, movieName, linkUrl, password)
    }

    override suspend fun validatePost(userId: String?, postNo: Int, uuid: String?, password: String?): Response<ObjectResponse<Any>> {
        return RetrofitService.getAsyncApi()
                .validatePost(countryCode, userId, postNo, uuid, password)
    }

    override suspend fun getNickname(uuid: String?): Response<ObjectResponse<Nickname?>> {
        return RetrofitService.getAsyncApi()
                .getNickname(countryCode, uuid)
    }

    override suspend fun uploadPost(userId: String?, fcmToken: String?, uuid: String?, password: String?, username: String?,
                                    boardNo: Int, comment: String?, fileName: String?, movieName: String?, linkUrl: String?): Response<ObjectResponse<Any>> {
        return RetrofitService.getAsyncApi()
                .insertPostV2(countryCode, userId, fcmToken, boardNo, username,
                    UDTextUtils.trim(comment), password, uuid, linkUrl, fileName, movieName)
    }

    override suspend fun deletePost(userId: String?, postNo: Int, uuid: String?, password: String?): Response<ObjectResponse<Any>> {
        return RetrofitService.getAsyncApi()
                .deletePost(countryCode, userId, postNo, uuid, password)
    }
}

interface PungRepository {
    suspend fun getPopularPostNo(userId: String?, boardNo: Int?): Response<ObjectResponse<List<Int>>>
    suspend fun getPostList(userId: String?, orderType: Int?, boardNo: Int?, requestPage: Int?): Response<ObjectResponse<List<Post>>>
    suspend fun getPostDetail(userId: String?, postNo: Int?): Response<ObjectResponse<Post>>

    suspend fun likeCountUp(postNo: Int): Response<ObjectResponse<Any>>
    suspend fun shareCountUp(postNo: Int): Response<ObjectResponse<Any>>

    suspend fun uploadPost(userId: String?, fcmToken: String?, uuid: String?, password: String?, username: String?, boardNo: Int, comment: String?,
                           fileName: String?, movieName: String?, linkUrl: String?): Response<ObjectResponse<Any>>
    suspend fun deletePost(userId: String?, postNo: Int, uuid: String?, password: String?): Response<ObjectResponse<Any>>

    suspend fun reportPost(userId: String?, postNo: Int?, comment: String?): Response<ObjectResponse<Any>>

    suspend fun updatePost(userId: String?, postNo: Int, uuid: String?, password: String?, comment: String?,
                           fileName: String?, movieName: String?, linkUrl: String?): Response<ObjectResponse<Any>>

    suspend fun validatePost(userId: String?, postNo: Int, uuid: String?, password: String?): Response<ObjectResponse<Any>>

    suspend fun getNickname(uuid: String?): Response<ObjectResponse<Nickname?>>
}