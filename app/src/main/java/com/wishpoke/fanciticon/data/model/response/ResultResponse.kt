package com.wishpoke.fanciticon.data.model.response

data class ResultResponse<T>(val status: Status, val results: T)