package com.wishpoke.fanciticon.data.repository

import com.wishpoke.fanciticon.data.api.RetrofitService
import com.wishpoke.fanciticon.data.model.pung.Post
import com.wishpoke.fanciticon.data.model.response.ObjectResponse
import com.wishpoke.fanciticon.util.UDTextUtils
import retrofit2.Response
import java.util.*

class PungReplyRepositoryImpl : PungReplyRepository {
    private val countryCode = Locale.getDefault().country.toLowerCase()

    override suspend fun getReplyList(userId: String?, postNo: Int?, requestPage: Int): Response<ObjectResponse<List<Post.Reply>>> {
        return RetrofitService.getAsyncApi().getReplyList(countryCode, userId, postNo ?: 0, requestPage)
    }

    override suspend fun likeCountUpReply(replyNo: Int): Response<ObjectResponse<Any>> {
        return RetrofitService.getAsyncApi().likeCountUpReply(replyNo)
    }

    override suspend fun reportReply(userId: String?, replyNo: Int?, comment: String?): Response<ObjectResponse<Any>> {
        return RetrofitService.getAsyncApi()
                .report(countryCode, userId, "reply", replyNo.toString(), comment)
    }

    override suspend fun uploadReply(userId: String?, postNo: Int, uuid: String?, password: String?, username: String?,
                                     comment: String?, fileName: String?, movieName: String?): Response<ObjectResponse<Any>> {
        return RetrofitService.getAsyncApi()
                .insertReply(countryCode, userId, postNo, username, UDTextUtils.trim(comment),
                uuid, password, fileName, movieName)
    }

    override suspend fun validateReply(userId: String?, replyNo: Int?, uuid: String?, password: String?): Response<ObjectResponse<Any>> {
        return RetrofitService.getAsyncApi()
                .validateReply(countryCode, userId, replyNo, uuid, password)
    }

    override suspend fun updateReply(userId: String?, replyNo: Int?, uuid: String?, password: String?, comment: String?, fileName: String?, movieName: String?): Response<ObjectResponse<Any>> {
        return RetrofitService.getAsyncApi()
                .updateReply(countryCode, userId, replyNo, uuid, password, UDTextUtils.trim(comment), fileName, movieName)
    }

    override suspend fun deleteReply(userId: String?, replyNo: Int?, uuid: String?, password: String?): Response<ObjectResponse<Any>> {
        return RetrofitService.getAsyncApi()
                .deleteReply(countryCode, userId, replyNo, uuid, password)
    }
}

interface PungReplyRepository {
    suspend fun getReplyList(userId: String?, postNo: Int?, requestPage: Int): Response<ObjectResponse<List<Post.Reply>>>
    suspend fun likeCountUpReply(replyNo: Int): Response<ObjectResponse<Any>>
    suspend fun reportReply(userId: String?, replyNo: Int?, comment: String?): Response<ObjectResponse<Any>>
    suspend fun uploadReply(userId: String?, postNo: Int, uuid: String?, password: String?, username: String?,
                            comment: String?, fileName: String?, movieName: String?): Response<ObjectResponse<Any>>

    suspend fun validateReply(
            userId: String?, replyNo: Int?,
            uuid: String?, password: String?): Response<ObjectResponse<Any>>

    suspend fun updateReply(
            userId: String?, replyNo: Int?,
            uuid: String?, password: String?,
            comment: String?, fileName: String?, movieName: String?
    ): Response<ObjectResponse<Any>>

    suspend fun deleteReply(
            userId: String?, replyNo: Int?,
            uuid: String?, password: String?
    ): Response<ObjectResponse<Any>>
}