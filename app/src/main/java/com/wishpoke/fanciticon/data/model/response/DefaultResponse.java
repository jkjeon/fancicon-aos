package com.wishpoke.fanciticon.data.model.response;


import lombok.Data;

/**
 * object 가 있는 기본 Response
 */
@Data
public class DefaultResponse extends ObjectResponse {
}
