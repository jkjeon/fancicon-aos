package com.wishpoke.fanciticon.data.model.chat

data class ChatRoom(var roomId: String? = null,
                    var title: String? = null,
                    var roomHost: String? = null, // 방장 ID
                    var hostStageName: String? = null, // 방장 닉네임
                    var stageName: String? = null, // 방에 참여이력이 없으면 null, 있을땐 사용한 닉네임
                    var createTime: Long = 0,
                    var modifiedTime: Long = 0, // 마지막 메세지 시간
                    var shareCnt: Int = 0,
                    var likeCnt: Int = 0,
                    var memberCnt: Int = 0, // 참여중인 회원 수
                    var boardNo: Int = 0, // 카테고리
                    var isPrivate: Int = 0, // 비밀번호 여부
                    var password: String? = null, // 비밀번호
                    var rank: Int = 0, // 금은동 표현, 안드로이드 임의
                    var fileName: String? = null, // 채팅방 이미지 파일명
                    var fontColorType: Int = 0,
                    var msg: String? = null,
                    var tempStageName: String? = null, // 입장전 가지고있는 닉네임
                    var pushOn: Int = 0) {

    fun joined(): Boolean {
        return !stageName.isNullOrEmpty()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as ChatRoom
        ?
        return roomId == that?.roomId
    }
}
