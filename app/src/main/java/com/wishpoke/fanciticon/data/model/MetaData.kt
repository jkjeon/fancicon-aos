package com.wishpoke.fanciticon.data.model

data class MetaData(var image: String? = null,
                    var title: String? = null,
                    var desc: String? = null,
                    var domain: String? = null,
                    var url: String? = null) {
    fun isEmpty(): Boolean {
        return image.isNullOrEmpty() && title.isNullOrEmpty()
    }
}