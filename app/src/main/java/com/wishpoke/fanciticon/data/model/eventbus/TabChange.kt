package com.wishpoke.fanciticon.data.model.eventbus

data class TabChange(val position: Int = 0)