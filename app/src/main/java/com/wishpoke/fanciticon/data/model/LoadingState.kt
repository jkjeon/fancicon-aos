package com.wishpoke.fanciticon.data.model

import com.wishpoke.fanciticon.util.constants.Errors

data class LoadingState<T> private constructor(val status: Status, val data: T? = null, val throwable: AppThrowable? = null) {
    companion object {
        fun <T>loading() = LoadingState<T>(Status.LOADING)
        fun <T>success(data: T?) = LoadingState(Status.SUCCESS, data)
        fun <T>error(throwable: Throwable?) = LoadingState<T>(Status.FAILED, null, AppThrowable(
                Errors.NETWORK, throwable?.message ?: ""))
        fun <T>error(code: Int = 0, message: String? = null) = LoadingState<T>(Status.FAILED, null, AppThrowable(code, message ?: ""))
    }

    fun isSuccessful(): Boolean {
        return status == Status.SUCCESS
    }

    fun isFailed(): Boolean {
        return status == Status.FAILED
    }

    fun <T> get(): T {
        return data as T
    }

    enum class Status {
        LOADING,
        SUCCESS,
        FAILED
    }

    fun isDone(): Boolean {
        return status != Status.LOADING
    }

    fun isLoading(): Boolean {
        return status == Status.LOADING
    }
}