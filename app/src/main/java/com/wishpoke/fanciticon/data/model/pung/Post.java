package com.wishpoke.fanciticon.data.model.pung;

import androidx.annotation.Nullable;

import com.wishpoke.fanciticon.util.Util;
import com.wishpoke.fanciticon.util.YoutubeUtils;

public class Post extends PungPost {
    public int replyCnt;
    public int boardNo;
    public String linkUrl;

    public static class Reply extends PungPost {
        public int replyNo;
        @Override
        public boolean equals(Object obj) {
            if (obj == null) return false;
            if (!(obj instanceof Reply)) {
                return false;
            }
            Reply other = (Reply) obj;
            return replyNo == other.replyNo;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Post)) return false;
        Post other = (Post) obj;
        if (postNo != other.postNo) return false;
        if (replyCnt != other.replyCnt) return false;
        if (boardNo != other.boardNo) return false;
        if (shareCnt != other.shareCnt) return false;
        if (likeCnt != other.likeCnt) return false;
        if (createTime != other.createTime) return false;
        if (comment != null && !comment.equals(other.comment)) return false;
        if (linkUrl != null && !linkUrl.equals(other.linkUrl)) return false;
        if (userName != null && !userName.equals(other.userName)) return false;
        return true;
    }

    public @Nullable String getYoutubeUrl() {
        return YoutubeUtils.isYoutube(linkUrl) ? linkUrl : movieName;
    }

    public @Nullable String getMetaDataUrl() {
        return linkUrl == null ? Util.extractUrlFromText(comment) : linkUrl;
    }

}
