package com.wishpoke.fanciticon.data.model.pung;

public class PungPost {
    public String uuid;
    public int isCreator;
    public int postNo;
    public int shareCnt;
    public String comment;
    public String userName;
    public String userId;
    public int likeCnt;
    public long createTime;
    public String fileName;
    public String movieName;
    public int rank;
    public String password;
}
