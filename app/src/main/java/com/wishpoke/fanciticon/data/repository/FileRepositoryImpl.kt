package com.wishpoke.fanciticon.data.repository

import com.wishpoke.fanciticon.data.api.RetrofitService
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import java.io.File

class FileRepositoryImpl : FileRepository {
    override suspend fun uploadFile(file: File): Response<String> {
        val requestFile = RequestBody.create(MediaType.parse("image/*"), file)
        val filePart = MultipartBody.Part.createFormData("file", file.name, requestFile)
        return RetrofitService.getAsyncApi().uploadFile(filePart)
    }

    override suspend fun updateFile() {

    }

    override suspend fun deleteFile(fileName: String?): Response<Boolean> {
        return RetrofitService.getAsyncApi().deleteFile(fileName)
    }

    override suspend fun getFile() {

    }

}


interface FileRepository {
    suspend fun uploadFile(file: File): Response<String>
    suspend fun updateFile()
    suspend fun deleteFile(fileName: String?): Response<Boolean>
    suspend fun getFile()
}