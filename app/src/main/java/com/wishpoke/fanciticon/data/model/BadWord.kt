package com.wishpoke.fanciticon.data.model

data class BadWord(var name: String = "",
                   var lang: String = "")