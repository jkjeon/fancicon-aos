package com.wishpoke.fanciticon.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.wishpoke.fanciticon.data.model.emoji.CustomEmoji
import com.wishpoke.fanciticon.database.dao.OldCustomEmojiDao

@Database(entities = [CustomEmoji::class], version = 1)
abstract class OldDatabase : RoomDatabase() {
    abstract fun customEmojiDao(): OldCustomEmojiDao
}