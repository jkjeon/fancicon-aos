package com.wishpoke.fanciticon.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.wishpoke.fanciticon.data.model.emoji.Emoji

@Dao
interface EmojiDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(emoji: Emoji?)

    @Delete
    fun delete(emoji: Emoji?)

    @Query("SELECT * FROM favorite_emojis")
    fun getFavoriteEmojiList(): LiveData<MutableList<Emoji>>
}