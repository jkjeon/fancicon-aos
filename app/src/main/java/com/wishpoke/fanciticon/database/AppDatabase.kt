package com.wishpoke.fanciticon.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.wishpoke.fanciticon.data.model.emoji.CustomEmoji
import com.wishpoke.fanciticon.data.model.emoji.Emoji
import com.wishpoke.fanciticon.database.dao.CustomEmojiDao
import com.wishpoke.fanciticon.database.dao.EmojiDao

@Database(entities = [Emoji::class, CustomEmoji::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun emojiDao() : EmojiDao
    abstract fun customEmojiDao() : CustomEmojiDao
}