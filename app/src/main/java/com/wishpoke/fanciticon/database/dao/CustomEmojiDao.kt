package com.wishpoke.fanciticon.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.wishpoke.fanciticon.data.model.emoji.CustomEmoji

@Dao
interface CustomEmojiDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(emoji: CustomEmoji?)

    @Delete
    fun delete(emoji: CustomEmoji?)

    @get:Query("SELECT * FROM custom_emojis")
    val customEmojiList: LiveData<List<CustomEmoji>>
}
