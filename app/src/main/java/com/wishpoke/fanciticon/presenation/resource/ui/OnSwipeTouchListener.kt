package com.wishpoke.fanciticon.presenation.resource.ui

import android.content.Context
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewConfiguration
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.util.Animator
import com.wishpoke.fanciticon.util.extention.dp2px


open class OnSwipeTouchListener(val context: Context?) : OnTouchListener {

    open fun onCustomActionStart(offset: Float) {}
    open fun onCustomActionEnd() {}
    open fun onLongPressed() {}

    private var dx = 0F
    private var dy = 0F
    private val dragThreshold = context?.dp2px(12F) ?: 0
    private var clickThreshold = 100

    private fun attemptClaimDrag(view: View, enable: Boolean) {
        if (view.parent != null) {
            view.parent.requestDisallowInterceptTouchEvent(enable)
        }
    }

    private var longPressHandlerStarted = false
    private val handler: Handler = Handler()
    private var mLongPressed = Runnable {
        longPressHandlerStarted = false
        onLongPressed()
    }

    override fun onTouch(v: View, e: MotionEvent): Boolean {
        val duration: Long = e.eventTime - e.downTime
        when (e.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                // Long Press 시작
                if (!longPressHandlerStarted) {
                    longPressHandlerStarted = true
                    handler.postDelayed(mLongPressed, ViewConfiguration.getLongPressTimeout().toLong())
                }
                dx = v.x - e.rawX
                dy = v.y - e.rawY
                attemptClaimDrag(v, false)
            }
            MotionEvent.ACTION_MOVE -> {
                if (context != null) {
                    val offset = Math.min(e.rawX + dx, context.dp2px(60F).toFloat())
                    if (offset > 0) {
                        onCustomActionStart(offset)
                    }
                    if (offset > dragThreshold) {
                        // Long Press 제거
                        longPressHandlerStarted = false
                        handler.removeCallbacks(mLongPressed)
                    }
                    // Scroll 씹힘 방지를 위해 Y 값의 변화가 크면 부모 뷰 이벤트로 넘김
                    attemptClaimDrag(v, offset > dragThreshold)
                }
            }
            MotionEvent.ACTION_CANCEL,
            MotionEvent.ACTION_UP -> {
                onCustomActionEnd()
                attemptClaimDrag(v, false)
                if (e.actionMasked == MotionEvent.ACTION_UP && (e.rawX + dx) < dragThreshold &&
                        (v.id == R.id.iv_image || v.id == R.id.ll_metadata) && duration < clickThreshold) {
                    v.startAnimation(Animator.getClickSlightAnimation(context))
                    v.performClick()
                }
                // Long Press 제거
                longPressHandlerStarted = false
                handler.removeCallbacks(mLongPressed)
            }
        }
        return true
    }

}