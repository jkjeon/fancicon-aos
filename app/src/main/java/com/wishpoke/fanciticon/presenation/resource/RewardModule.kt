package com.wishpoke.fanciticon.presenation.resource

import android.content.Context
import com.wishpoke.fanciticon.util.constants.PrefKeys

class RewardModule private constructor(context: Context) {

    private var storage = AppStorage(context)

    companion object {
        @Volatile
        private var instance: RewardModule? = null

        @JvmStatic
        fun getInstance(context: Context): RewardModule =
                instance ?: synchronized(this) {
                    return if (instance == null) {
                        instance = RewardModule(context)
                        instance!!
                    } else {
                        instance!!
                    }
                }
    }

    // 서버 문제로 적립되지 못한 인싸력을 저장
    fun saveFailedUserPoint(amount: Int, provider: String) {
        val current = storage.getInt(PrefKeys.REWARD_AMOUNT, 0)
        storage.put(PrefKeys.REWARD_AMOUNT, current + amount)
        storage.put(PrefKeys.REWARD_USER_ID, storage.userId)
        storage.put(PrefKeys.REWARD_PROVIDER, provider)
    }

    // 적립할 인싸력이있고, 조건이 된다면
    fun getFailedUserPoint(): Int {
        val amount = storage.getInt(PrefKeys.REWARD_AMOUNT, 0)
        val userId = storage.getString(PrefKeys.REWARD_USER_ID, "")
        return if (!storage.userId.isNullOrEmpty() && (userId == storage.userId || userId.isEmpty()) && amount > 0) {
            amount
        } else {
            0
        }
    }

    fun clearFailedUserPoint() {
        storage.put(PrefKeys.REWARD_AMOUNT, 0)
        storage.put(PrefKeys.REWARD_USER_ID, "")
        storage.put(PrefKeys.REWARD_PROVIDER, "")
    }
}