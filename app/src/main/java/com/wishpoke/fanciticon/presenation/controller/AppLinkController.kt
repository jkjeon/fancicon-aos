package com.wishpoke.fanciticon.presenation.controller

import android.content.Intent

object AppLinkController {
    fun handle(intent: Intent): String? {
        var chatRoomId: String? = ""
        val appLinkData = intent.data
        try {
            when (appLinkData?.scheme) {
                "inssaticon" -> {
                    val extras = intent.extras
                    if (extras != null) {
                        chatRoomId = extras.getString("roomId")
                    }
                    if (chatRoomId?.isEmpty() == true) {
                        chatRoomId = appLinkData.getQueryParameter("roomId")
                    }
                }
                "http" -> {
                    chatRoomId = appLinkData.pathSegments[appLinkData.pathSegments.size - 1]
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return chatRoomId
    }
}