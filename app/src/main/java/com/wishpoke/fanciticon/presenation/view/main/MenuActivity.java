package com.wishpoke.fanciticon.presenation.view.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager;
import com.wishpoke.fanciticon.R;
import com.wishpoke.fanciticon.data.model.Category;
import com.wishpoke.fanciticon.presenation.base.BaseActivity;
import com.wishpoke.fanciticon.util.Animator;
import com.wishpoke.fanciticon.util.FacebookAdLoader;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MenuActivity extends BaseActivity {
    @BindView(R.id.rv_menu_category)
    RecyclerView rvMenuCategory;

    @BindView(R.id.banner_container)
    LinearLayout bannerContainer;

    private List<Category> categories;
    private CategoryAdapter menuAdapter;
    private int position;

    public static final int REQ_MENU = 7788;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
        adLoader = new FacebookAdLoader(this, bannerContainer, null, null);
        getMMainViewModel().parseUpdateCategories();
        categories = getMMainViewModel().getCategories();
        setMenuView();

        position = getIntent().getIntExtra("position", 0);
        menuAdapter.setCurrentItem(position);
    }

    // 플로팅 버튼 메뉴 탭
    private void setMenuView() {
        menuAdapter = new CategoryAdapter(this, new CategoryAdapter.OnClickCategoryListener() {
            @Override
            public void onClick(final View v) {
                final int position = (int) v.getTag();
                menuAdapter.setCurrentItem(position);
                Intent data = new Intent();
                data.putExtra("position", position);
                setResult(RESULT_OK, data);
                onBackPressed();
            }
        });
        menuAdapter.isMenu(true);
        menuAdapter.setList(categories);

        ChipsLayoutManager chipsLayoutManager = ChipsLayoutManager.newBuilder(this)
                .setOrientation(ChipsLayoutManager.HORIZONTAL)
                .build();
        rvMenuCategory.setLayoutManager(chipsLayoutManager);
        rvMenuCategory.setHasFixedSize(true);
        rvMenuCategory.getItemAnimator().setChangeDuration(0);
        rvMenuCategory.setAdapter(menuAdapter);
    }

    @OnClick(R.id.btn_menu_close)
    public void onClick(View v) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        Animator.slideDown(this);
        finish();
    }
}
