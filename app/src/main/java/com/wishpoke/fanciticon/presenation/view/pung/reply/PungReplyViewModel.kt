package com.wishpoke.fanciticon.presenation.view.pung.reply

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.pung.Post
import com.wishpoke.fanciticon.domain.usecase.file.DeleteFileUseCase
import com.wishpoke.fanciticon.domain.usecase.file.UploadFileUseCase
import com.wishpoke.fanciticon.domain.usecase.pung.*
import com.wishpoke.fanciticon.presenation.base.BaseViewModel
import com.wishpoke.fanciticon.presenation.resource.ResponseHandler
import com.wishpoke.fanciticon.util.HiddenContentModule
import com.wishpoke.fanciticon.util.UDTextUtils
import com.wishpoke.fanciticon.util.Util
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File

class PungReplyViewModel(
        application: Application,
        private val getReplyListUseCase: GetReplyListUseCase,
        private val likeCountUpUseCase: LikeCountUpUseCase,
        private val uploadReplyUseCase: UploadReplyUseCase,
        private val reportReplyUseCase: ReportReplyUseCase,
        private val uploadFileUseCase: UploadFileUseCase,
        private val deleteFileUseCase: DeleteFileUseCase,
        private val validateReplyUseCase: ValidateReplyUseCase,
        private val updateReplyUseCase: UpdateReplyUseCase,
        private val deleteReplyUseCase: DeleteReplyUseCase,
        private val getNicknameUseCase: GetNicknameUseCase
) : BaseViewModel(application) {

    private val _replyList = MutableLiveData<LoadingState<List<Post.Reply>>>()
    val replyList: LiveData<LoadingState<List<Post.Reply>>>
        get() = _replyList

    private val _uploadReply = MutableLiveData<LoadingState<Any>>()
    val uploadReply: LiveData<LoadingState<Any>>
        get() = _uploadReply

    private val _validateReply = MutableLiveData<LoadingState<Any>>()
    val validateReply: LiveData<LoadingState<Any>>
        get() = _validateReply

    private val _updateReply = MutableLiveData<LoadingState<String?>>()
    val updateReply: LiveData<LoadingState<String?>>
        get() = _updateReply

    private val _nickname = MutableLiveData<LoadingState<String>>()
    val nickname: LiveData<LoadingState<String>>
        get() = _nickname

    fun getReplyList(postNo: Int?, requestPage: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _replyList.postValue(LoadingState.loading())
                val response =
                        getReplyListUseCase(storage.userId, postNo, requestPage)
                var loadingState = ResponseHandler.handle(response)
                if (loadingState.isSuccessful()) {
                    loadingState.data?.let {
                        // 숨긴 콘텐츠 처리
                        val list = HiddenContentModule
                                .processHideContent(getApplication(), HiddenContentModule.ContentType.REPLY, it)
                        loadingState = LoadingState.success(list)
                    }
                }
                _replyList.postValue(loadingState)
            } catch (e: Exception) {
                _replyList.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun likeCountUpReply(replyNo: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                likeCountUpUseCase(replyNo)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun reportReply(replyNo: Int?, comment: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                HiddenContentModule.addHiddenContent(getApplication(),
                        HiddenContentModule.ContentType.REPLY, replyNo)
                reportReplyUseCase(storage.userId, replyNo, comment)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun uploadReply(postNo: Int, username: String?,
                    comment: String?, file: File?, movieName: String?, password: String?) {
        var fileName: String? = null
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _uploadReply.postValue(LoadingState.loading())
                if (file != null && movieName == null) {
                    // 파일 업로드
                    val fileResponse = uploadFileUseCase(file)
                    if (fileResponse.isSuccessful) {
                        fileName = fileResponse.body()
                    }
                }
                val response =
                        uploadReplyUseCase(
                                storage.userId, postNo,
                                getUUID(), password, username, comment, fileName, movieName)
                _uploadReply.postValue(ResponseHandler.handle(response))
            } catch (e: Exception) {
                if (fileName != null) {
                    try { deleteFileUseCase(fileName) } catch (e: java.lang.Exception) {}
                }
                _uploadReply.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun validateReply(replyNo: Int?, password: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _validateReply.postValue(LoadingState.loading())
                var uuid: String? = null
                if (!storage.isLoggedIn) {
                    uuid = Util.getUUID(getApplication())
                }
                val response = validateReplyUseCase(storage.userId, replyNo, uuid, password)
                _validateReply.postValue(ResponseHandler.handle(response))
            } catch (e: java.lang.Exception) {
                _validateReply.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun deleteReply(replyNo: Int?, password: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                deleteReplyUseCase(storage.userId, replyNo ?: 0, getUUID(), password)
            } catch (e: java.lang.Exception) {

            }
        }
    }

    fun updateReply(replyNo: Int?, password: String?, comment: String?, file: File?, fileName: String?, movieName: String?) {
        var newFileName: String? = null
        var fileName = fileName
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _updateReply.postValue(LoadingState.loading())
                if (movieName != null) {
                    fileName = null
                } else {
                    if (file != null) {
                        // 파일 업로드
                        val fileResponse = uploadFileUseCase(file)
                        if (fileResponse.isSuccessful) {
                            newFileName = fileResponse.body()
                        }
                    }
                }
                val response = updateReplyUseCase(storage.userId, replyNo, getUUID(), password, comment,
                                newFileName ?: fileName, movieName)
                val loadingState = ResponseHandler.handle(response)
                if (loadingState.isSuccessful()) {
                    _updateReply.postValue(LoadingState.success(newFileName ?: fileName))
                } else {
                    _updateReply.postValue(LoadingState.error(loadingState.throwable))
                }
            } catch (e: java.lang.Exception) {
                if (newFileName != null && file != null) {
                    try { deleteFileUseCase(fileName) } catch (e: java.lang.Exception) {}
                }
                _updateReply.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun getNickname() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _nickname.postValue(LoadingState.loading())
                val response = getNicknameUseCase(getUUID())
                val loadingState = ResponseHandler.handle(response)
                _nickname.postValue(LoadingState.success(UDTextUtils.decode(loadingState.data?.userName)))
            } catch (e: java.lang.Exception) {
                _nickname.postValue(ResponseHandler.handle(e))
            }
        }
    }


    fun hideReply(replyNo: Int?) {
        HiddenContentModule.addHiddenContent(getApplication(), HiddenContentModule.ContentType.REPLY, replyNo)
    }
}