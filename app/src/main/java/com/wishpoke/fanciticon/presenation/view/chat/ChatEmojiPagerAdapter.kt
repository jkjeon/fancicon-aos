package com.wishpoke.fanciticon.presenation.view.chat

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.wishpoke.fanciticon.data.model.Category

class ChatEmojiPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    var categories: List<Category> = ArrayList()
    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            ChatFavoriteFragment.create(categories[position].id)
        } else {
            ChatEmojiFragment.create(categories[position].id)
        }
    }

    override fun getCount(): Int {
        return categories.size
    }
}