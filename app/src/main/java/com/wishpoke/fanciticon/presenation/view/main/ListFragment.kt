package com.wishpoke.fanciticon.presenation.view.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.Emoticon
import com.wishpoke.fanciticon.data.model.emoji.Emoji
import com.wishpoke.fanciticon.data.model.eventbus.ScrollTop
import com.wishpoke.fanciticon.presenation.base.InssaFragment
import com.wishpoke.fanciticon.util.Util
import kotlinx.android.synthetic.main.fragment_list.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.koin.android.viewmodel.ext.android.viewModel


class ListFragment : InssaFragment(), EmoticonAdapter.Interaction {

    private lateinit var emoticonAdapter: EmoticonAdapter
    private var categoryId: String? = null

    private val emojiViewModel: EmojiViewModel by viewModel()

    companion object {
        fun create(categoryId: String?): ListFragment {
            val fragment = ListFragment()
            val bundle = Bundle(1)
            bundle.putString("categoryId", categoryId)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_list, container, false)
        categoryId = arguments?.getString("categoryId")
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        subscribeObservers()
        loadEmoticons()
    }

    private fun init() {
        swipeRefreshLayout.isEnabled = false
        rv_emoji.apply {
            layoutManager = LinearLayoutManager(activity)
            emoticonAdapter = EmoticonAdapter(this@ListFragment)
            adapter = emoticonAdapter

            addOnScrollListener(object: RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    val lm = layoutManager as? LinearLayoutManager
                    val position = lm?.findFirstVisibleItemPosition() ?: 0
                    if (position > 1) {
                        (activity as? MainActivity)?.hideTitle()
                    } else {
                        (activity as? MainActivity)?.showTitle()
                    }
                }
            })
        }
    }

    private fun subscribeObservers() {
        emojiViewModel.favoriteEmojis.observe(viewLifecycleOwner, Observer { favoriteList ->
            if (isAdded) {
                val newList = ArrayList<Emoticon>()
                for (item in emoticonAdapter.getList()) {
                    val emoticon = Emoticon(item.text)
                    emoticon.isFavorite = favoriteList.contains(Emoji(item.strip()))
                    newList.add(emoticon)
                }
                emoticonAdapter.submitList(newList)
            }
        })
    }

    private fun loadEmoticons() {
        val resId = resources.getIdentifier(categoryId, "array", activity?.packageName)
        val emojiList = resources.getStringArray(resId)
        val emoticonList = ArrayList<Emoticon>()
        for (emoji in emojiList) {
            emoticonList.add(Emoticon(emoji))
        }
        emoticonAdapter.submitList(emoticonList)
    }

    override fun scrollTop() {
        if (isAdded) {
            rv_emoji.smoothScrollToPosition(0)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onScrollTop(scrollTop: ScrollTop) {
        if (scrollTop.categoryId == categoryId) {
            scrollTop()
        }
    }

    override fun onItemSelected(position: Int, item: Emoticon, viewId: Int) {
        when (viewId) {
            R.id.btn_favorite -> {
                if (item.isFavorite) {
                    emojiViewModel.removeFavoriteEmoji(item.strip())
                } else {
                    emojiViewModel.addFavoriteEmoji(item.strip())
                }

            }
            else -> {
                Util.copyToClipboard(context, item.strip())
                emojiViewModel.addRecentEmoji(item.strip())
                (activity as? MainActivity)?.showButtonUseEmoji(item.strip() ?: "")
            }
        }
    }
}