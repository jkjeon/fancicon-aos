package com.wishpoke.fanciticon.presenation.view.chat

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.chat.ChatRoom
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.util.*
import com.wishpoke.fanciticon.util.extention.createDialog
import com.wishpoke.fanciticon.util.listener.ItemClick
import com.wishpoke.fanciticon.util.listener.SocketStateListener
import kotlinx.android.synthetic.main.activity_chat_member.*
import org.koin.android.viewmodel.ext.android.viewModel

class ChatMemberActivity : BaseActivity() {

    @BindView(R.id.tv_room_title)
    lateinit var tvRoomTitle: TextView
    @BindView(R.id.tv_count_member)
    lateinit var tvCountMember: TextView
    @BindView(R.id.rv_member)
    lateinit var rvMember: RecyclerView

    companion object {
        const val REQUEST_EXIT = 2022
    }
    val mChatViewModel: ChatViewModel by viewModel()
    private lateinit var memberAdapter: ChatMemberAdapter
    private var chatRoom: ChatRoom? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_member)
        ButterKnife.bind(this)
        adLoader = FacebookAdLoader(this, banner_container, ll_isn, wv_edge)
        chatRoom = EventBusManager.getInstance().clearAccept(ChatRoom::class.java) as? ChatRoom
        init()
        subscribeObservers()
        mChatViewModel.getChatRoomMembers(chatRoom?.roomId)
    }

    private fun subscribeObservers() {
        mChatViewModel.chatRoomMembers.observe(this, Observer {
            when (it.status) {
                LoadingState.Status.LOADING -> {
                    mDialog = DialogManager.getInstance().createLoadingDialog(mContext)
                    mDialog?.show()
                }
                LoadingState.Status.SUCCESS -> {
                    mDialog?.dismiss()
                    memberAdapter.setList(it.data!!)
                }
                LoadingState.Status.FAILED -> {
                    mDialog?.dismiss()
                    Toaster.show(mContext, R.string.error_unknown, it.throwable?.code ?: 0)
                }
            }
        })
    }

    private fun init() {
        memberAdapter = ChatMemberAdapter(this)
        memberAdapter.itemClick = ItemClick { v, position ->
            val chatMember = memberAdapter.getItemAt(position)
            when (v.id) {
                R.id.iv_delete_member -> {
                    if (chatMember.userId == storage.userId) {
                        // 퇴장하기
                        val isChatMaster = chatRoom?.roomHost == storage.userId
                        val message = if (isChatMaster) {
                            R.string.dialog_chat_exit_master
                        } else {
                            R.string.dialog_chat_exit
                        }
                        mDialog = createDialog(
                                message = message,
                                positiveLabel = R.string.chat_kick_positive,
                                negativeLabel = R.string.chat_kick_negative,
                                positiveBlock = {
                                    // 퇴장처리
                                    if (isChatMaster) {
                                        mChatViewModel.closeChatRoom(chatRoom?.roomId)
                                        chatRoom?.roomId?.let {
                                            ChatRoomAdapter.deletedRoomIds.add(it)
                                        }
                                    } else {
                                        mChatViewModel.exitChatRoom(chatRoom?.roomId, chatRoom?.stageName)
                                    }
                                    setResult(Activity.RESULT_OK)
                                    finish()
                                }
                        ).apply { show() }
                    } else {
                        // 강퇴퇴하기
                        val message = String.format(getString(R.string.dialog_chat_kick), chatMember.stageName)
                        mDialog = createDialog(
                                message = message,
                                positiveLabel = R.string.chat_kick_positive,
                                negativeLabel = R.string.chat_kick_negative,
                                positiveBlock = {
                                    mChatViewModel.kickChatMember(chatRoom?.roomId, chatMember)
                                    memberAdapter.removeItem(position)
                                }
                        ).apply { show() }
                    }

                }
            }
        }
        rvMember.setHasFixedSize(true)
        rvMember.itemAnimator?.changeDuration = 0
        rvMember.layoutManager = LinearLayoutManager(this)
        rvMember.adapter = memberAdapter

        tvRoomTitle.text = chatRoom?.title
        tvCountMember.text = String.format("(%d/20)", chatRoom?.memberCnt)

        mChatViewModel.connectSocket(object: SocketStateListener {
            override fun onSocketOpened() {}
        })
    }

    @OnClick(R.id.btn_back)
    fun onClick(v: View) {
        when (v.id) {
            R.id.btn_back -> {
                onBackPressed()
                Animator.slideDown(this)
            }
        }
    }
}