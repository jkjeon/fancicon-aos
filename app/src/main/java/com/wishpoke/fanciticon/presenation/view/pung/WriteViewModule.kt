package com.wishpoke.fanciticon.presenation.view.pung

import android.content.Context
import android.view.View
import android.widget.EditText
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import com.wishpoke.fanciticon.util.Animator
import com.wishpoke.fanciticon.util.Toaster
import com.wishpoke.fanciticon.util.extention.isVisibleWhen
import java.util.*

class WriteViewModule(private val context: Context,
                      private val etUserName: EditText,
                      private val etComment: EditText,
                      private val etPassword: EditText,
                      private val ivClear: View) {

    var userName: String? = null
    var comment: String? = null
    var movieName: String? = null
    var password: String? = null
    val storage = AppStorage(context)

    var lastToastShow = 0L

    private var badWords: Array<String> = context.resources.getStringArray(R.array.bad_words)

    init {
        etUserName.setOnClickListener {
            if (!storage.isLoggedIn) {
                it.startAnimation(Animator.getClickAnimation(context))
                Toaster.show(context.applicationContext, R.string.error_cannot_change_alias)
            }
        }
    }

    fun setNickNameView(nickname: String? = null) {
        /*
         * [회원] 닉네임 언제든지 변경가능, 하지만 중복은 안됨
         * [비회원] 닉네임 최초등록 후 변경 불가. 최초 등록시에도 중복은 안됨
         */
        if (storage.isLoggedIn) {
            etUserName.setText(storage.nickname)
        } else {
            etUserName.setText(nickname)
        }
        etUserName.setSelection(etUserName.text.length)
        val isEditable = storage.isLoggedIn || nickname.isNullOrEmpty()
        etUserName.isFocusable = isEditable
        ivClear.isVisibleWhen(isEditable)
    }

    fun isValid(showMessage: Boolean = false, boardNo: Int = -1): Boolean {
        userName = etUserName.text.toString().trim()
        comment = etComment.text.toString().trim()
        password = etPassword.text.toString().trim()
        // 공백이 아니라면 유효함
        var valid = !comment?.trim().isNullOrEmpty() && !userName?.trim().isNullOrEmpty()
        valid = valid && checkBadWord()
        // 카테고리를 선택해야함
        valid = valid && boardNo != 0
        // 비밀번호를 4자리 입력 / 또는 아예 비워둬야함
        if (etPassword.visibility == View.VISIBLE) {
            valid = valid && password?.length == 4
            if (showMessage && !valid) {
                Toaster.show(context.applicationContext, R.string.toast_input_post)
            }
        }
        return valid
    }

    private fun checkBadWord(): Boolean {
        for (name in badWords) {
            if (userName?.contains(name) == true || comment?.contains(name) == true) {
                setBadWordView(true)
                return false
            }
        }
        setBadWordView(false)
        return true
    }

    private fun setBadWordView(flag: Boolean) {
        val now = Date().time/1000
        if (flag && now - lastToastShow > 5) {
            lastToastShow = now
            Toaster.show(context, "욕설과 폭력적인 글은 안돼요 ㅠㅠ")
        }
    }
}