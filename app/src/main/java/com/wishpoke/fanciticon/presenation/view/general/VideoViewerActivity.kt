package com.wishpoke.fanciticon.presenation.view.general

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.View
import butterknife.BindView
import butterknife.ButterKnife
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.util.YoutubeUtils

class VideoViewerActivity : BaseActivity() {

    @BindView(R.id.youtube_player_view)
    lateinit var youTubePlayerView: YouTubePlayerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_viewer)
        ButterKnife.bind(this)

        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        actionBar?.hide()

        val url = intent.getStringExtra("url")
        val vId = YoutubeUtils.getVideoId(url)
        lifecycle.addObserver(youTubePlayerView)

        youTubePlayerView.initialize(object: YouTubePlayerListener {
            override fun onApiChange(youTubePlayer: YouTubePlayer) {
                
            }

            override fun onCurrentSecond(youTubePlayer: YouTubePlayer, second: Float) {
                
            }

            override fun onError(youTubePlayer: YouTubePlayer, error: PlayerConstants.PlayerError) {
                
            }

            override fun onPlaybackQualityChange(youTubePlayer: YouTubePlayer, playbackQuality: PlayerConstants.PlaybackQuality) {
                
            }

            override fun onPlaybackRateChange(youTubePlayer: YouTubePlayer, playbackRate: PlayerConstants.PlaybackRate) {
                
            }

            override fun onReady(youTubePlayer: YouTubePlayer) {
                if (vId.isNotEmpty()) {
                    youTubePlayer.loadVideo(vId, 0F)
                    youTubePlayerView.enterFullScreen()
                    youTubePlayer.play()
                } else {
                    finish()
                }
            }

            override fun onStateChange(youTubePlayer: YouTubePlayer, state: PlayerConstants.PlayerState) {
                requestedOrientation = if (state == PlayerConstants.PlayerState.PLAYING) {
                    ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
                } else {
                    ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                }
            }

            override fun onVideoDuration(youTubePlayer: YouTubePlayer, duration: Float) {
                
            }

            override fun onVideoId(youTubePlayer: YouTubePlayer, videoId: String) {
                
            }

            override fun onVideoLoadedFraction(youTubePlayer: YouTubePlayer, loadedFraction: Float) {
                
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        youTubePlayerView.release()
    }
}