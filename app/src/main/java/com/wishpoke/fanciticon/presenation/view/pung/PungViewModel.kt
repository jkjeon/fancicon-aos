package com.wishpoke.fanciticon.presenation.view.pung

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.pung.Post
import com.wishpoke.fanciticon.domain.usecase.file.DeleteFileUseCase
import com.wishpoke.fanciticon.domain.usecase.file.UploadFileUseCase
import com.wishpoke.fanciticon.domain.usecase.pung.*
import com.wishpoke.fanciticon.presenation.base.BaseViewModel
import com.wishpoke.fanciticon.presenation.resource.ResponseHandler
import com.wishpoke.fanciticon.util.HiddenContentModule
import com.wishpoke.fanciticon.util.Logger
import com.wishpoke.fanciticon.util.UDTextUtils
import com.wishpoke.fanciticon.util.constants.PrefKeys
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File


class PungViewModel(application: Application,
                    private val getPopularPostNoUseCase: GetPopularPostNoUseCase,
                    private val getPostListUseCase: GetPostListUseCase,
                    private val likeCountUpUseCase: LikeCountUpUseCase,
                    private val shareCountUpUseCase: ShareCountUpUseCase,
                    private val deletePostUseCase: DeletePostUseCase,
                    private val getPostDetailUseCase: GetPostDetailUseCase,
                    private val reportPostUseCase: ReportPostUseCase,
                    private val uploadPostUseCase: UploadPostUseCase,
                    private val uploadFileUseCase: UploadFileUseCase,
                    private val deleteFileUseCase: DeleteFileUseCase,
                    private val validatePostUseCase: ValidatePostUseCase,
                    private val updatePostUseCase: UpdatePostUseCase,
                    private val getNicknameUseCase: GetNicknameUseCase) : BaseViewModel(application) {

    private val _popularPostNo = MutableLiveData<LoadingState<List<Int>>>()
    val popularPostNo: LiveData<LoadingState<List<Int>>>
        get() = _popularPostNo

    private val _postList = MutableLiveData<LoadingState<List<Post>>>()
    val postList: LiveData<LoadingState<List<Post>>>
        get() = _postList

    private val _deletePost = MutableLiveData<LoadingState<Any>>()
    val deletePost: LiveData<LoadingState<Any>>
        get() = _deletePost

    private val _uploadPost = MutableLiveData<LoadingState<Any>>()
    val uploadPost: LiveData<LoadingState<Any>>
        get() = _uploadPost

    private val _updatePost = MutableLiveData<LoadingState<String?>>()
    val updatePost: LiveData<LoadingState<String?>>
        get() = _updatePost

    private val _post = MutableLiveData<LoadingState<Post>>()
    val post: LiveData<LoadingState<Post>>
        get() = _post

    private val _validatePost = MutableLiveData<LoadingState<Any>>()
    val validatePost: LiveData<LoadingState<Any>>
        get() = _validatePost

    private val _nickname = MutableLiveData<LoadingState<String>>()
    val nickname: LiveData<LoadingState<String>>
        get() = _nickname

    fun getPopularPostNo(boardNo: Int?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _popularPostNo.updateValue(
                        getPopularPostNoUseCase(storage.userId, boardNo)
                )
            } catch (e: Exception) {
                _popularPostNo.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun getPostList(orderType: Int?, boardNo: Int?, requestPage: Int?) {
        // viewModelScope contributes to structured concurrency by adding an extension property to the ViewModel class that automatically cancels its child coroutines when the ViewModel is destroyed.
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _postList.postValue(LoadingState.loading())
                val response = getPostListUseCase(storage.userId, orderType, boardNo, requestPage)
                var loadingState = ResponseHandler.handle(response)
                if (loadingState.isSuccessful()) {
                    loadingState.data?.let {
                        for (post in it) {
                            post.userName = UDTextUtils.decode(post.userName)
                            post.comment = UDTextUtils.decode(post.comment)
                            post.linkUrl = UDTextUtils.decode(post.linkUrl)
                        }
                        // 숨긴 콘텐츠 처리
                        val list = HiddenContentModule
                                .processHideContent(getApplication(), HiddenContentModule.ContentType.POST, it)
                        loadingState = LoadingState.success(list)
                    }

                }
                _postList.postValue(loadingState)
            } catch (e: Exception) {
                _postList.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun likeCountUp(postNo: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                likeCountUpUseCase(postNo)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun shareCountUp(postNo: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                shareCountUpUseCase(postNo)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun reportPost(postNo: Int, comment: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                HiddenContentModule.addHiddenContent(getApplication(),
                        HiddenContentModule.ContentType.POST, postNo)
                reportPostUseCase(storage.userId, postNo, comment)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun uploadPost(boardNo: Int, username: String?, comment: String?, file: File?, linkUrl: String?, movieName: String?, password: String?) {
        val fcmToken = storage.getString(PrefKeys.FCM_TOKEN, "")
        var fileName: String? = null
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _uploadPost.postValue(LoadingState.loading())
                if (file != null && movieName == null) {
                    // 파일 업로드
                    val fileResponse = uploadFileUseCase(file)
                    if (fileResponse.isSuccessful) {
                        fileName = fileResponse.body()
                    }
                    Logger.e("Test", "fileName1 = ${fileResponse.body().toString()}")
                    Logger.e("Test", "fileName2 = ${fileResponse.errorBody().toString()}")
                }
                val response = uploadPostUseCase(
                        storage.userId, fcmToken, getUUID(), password, username, boardNo, comment, fileName, movieName, linkUrl
                )
                _uploadPost.postValue(ResponseHandler.handle(response))
            } catch (e: Exception) {
                if (fileName != null) {
                    try { deleteFileUseCase(fileName) } catch (e: Exception) {}
                }
                _uploadPost.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun deletePost(postNo: Int?, password: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _deletePost.updateValue(
                        deletePostUseCase(storage.userId, postNo ?: 0, getUUID(), password)
                )
            } catch (e: Exception) {
                _deletePost.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun getPostDetail(postNo: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _post.updateValue(getPostDetailUseCase(storage.userId, postNo))
            } catch (e: Exception) {
                _post.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun validatePost(postNo: Int, password: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _validatePost.postValue(LoadingState.loading())
                val response = validatePostUseCase(storage.userId, postNo, getUUID(), password)
                _validatePost.postValue(ResponseHandler.handle(response))
            } catch (e: Exception) {
                _validatePost.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun updatePost(postNo: Int, password: String?, comment: String?, file: File?, fileName: String?, movieName: String?, linkUrl: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            var newFileName: String? = null
            var fileName = fileName
            try {
                _updatePost.postValue(LoadingState.loading())
                if (movieName != null) {
                    fileName = null
                } else {
                    if (file != null) {
                        // 파일 업로드
                        val fileResponse = uploadFileUseCase(file)
                        if (fileResponse.isSuccessful) {
                            newFileName = fileResponse.body()
                        }
                    }
                }
                val response = updatePostUseCase(storage.userId, postNo, getUUID(), password, comment,
                                newFileName ?: fileName, movieName, linkUrl)
                val loadingState = ResponseHandler.handle(response)
                if (loadingState.isSuccessful()) {
                    _updatePost.postValue(LoadingState.success(newFileName ?: fileName))
                } else {
                    _updatePost.postValue(LoadingState.error(loadingState.throwable))
                }
            } catch (e: Exception) {
                if (newFileName != null && file != null) {
                    try { deleteFileUseCase(fileName) } catch (e: Exception) {}
                }
                _updatePost.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun getNickname() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _nickname.postValue(LoadingState.loading())
                val response = getNicknameUseCase(getUUID())
                val loadingState = ResponseHandler.handle(response)
                _nickname.postValue(LoadingState.success(UDTextUtils.decode(loadingState.data?.userName)))
            } catch (e: Exception) {
                _nickname.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun hidePost(postNo: Int?) {
        HiddenContentModule.addHiddenContent(getApplication(), HiddenContentModule.ContentType.POST, postNo)
    }

}