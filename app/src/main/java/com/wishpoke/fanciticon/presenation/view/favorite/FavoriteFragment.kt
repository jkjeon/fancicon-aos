package com.wishpoke.fanciticon.presenation.view.favorite

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.google.gson.Gson
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.Banner
import com.wishpoke.fanciticon.data.model.eventbus.ScrollTop
import com.wishpoke.fanciticon.data.model.response.ResultResponse
import com.wishpoke.fanciticon.presenation.base.InssaFragment
import com.wishpoke.fanciticon.presenation.resource.SingleResponse
import com.wishpoke.fanciticon.presenation.view.general.WebViewActivity
import com.wishpoke.fanciticon.presenation.view.main.EmojiViewModel
import com.wishpoke.fanciticon.util.Animator
import com.wishpoke.fanciticon.util.Logger
import com.wishpoke.fanciticon.util.constants.Constants
import com.wishpoke.fanciticon.util.listener.ItemClick
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

class FavoriteFragment : InssaFragment() {

    @BindView(R.id.rv_list)
    lateinit var rvList: RecyclerView

    lateinit var adapter: FavoriteAdapter
    private val emojiViewModel: EmojiViewModel by viewModel()

    companion object {
        @JvmStatic fun create(): FavoriteFragment {
            return FavoriteFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view: View = inflater.inflate(R.layout.fragment_favorite_test, container, false)
        unbinder = ButterKnife.bind(this, view)
        init()
        subscribeObservers()

        mMainViewModel.recent.observe(viewLifecycleOwner) {
            adapter.submitRecentList(it)
        }

        emojiViewModel.favoriteEmojis.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            adapter.submitFavoriteList(it)
        })

        mMainViewModel.getBanners()
        return view
    }

    private fun init() {
        adapter = FavoriteAdapter(mContext)
        adapter.itemClick = ItemClick { view, position ->
            when (view.id) {
                R.id.btn_favorite -> {
                    view.startAnimation(Animator.getCopiedClickAnimation(mContext))
                    val emoji = adapter.getItemAt(position)
                    if (!adapter.isFavorite(emoji)) {
                        emojiViewModel.addFavoriteEmoji(emoji)
                    } else {
                        emojiViewModel.removeFavoriteEmoji(emoji)
                    }
                }
            }
        }
        adapter.footerItemClick = ItemClick { view, _ ->
            // 버튼: 어떻게 사용하나요?
            view.startAnimation(Animator.getClickAnimation(mContext))
            val intent = Intent(mActivity, WebViewActivity::class.java)
            if (Locale.getDefault().language == "ko") {
                intent.putExtra("url", Constants.Url.YOUTUBE)
            } else {
                intent.putExtra("url", Constants.Url.YOUTUBE_EN)
            }
            startActivity(intent)
        }
        rvList.layoutManager = LinearLayoutManager(mContext)
        rvList.itemAnimator?.changeDuration = 0
        rvList.adapter = adapter
    }

    private fun subscribeObservers() {
        mMainViewModel.banners.observe(viewLifecycleOwner, Observer {
            if (it.isSuccessful()) {
                adapter.bannerList = it.data!!
            }
        })
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onScrollTop(scrollTop: ScrollTop) {
        if (scrollTop.categoryId == "favorite") {
            scrollTop()
        }
    }
}