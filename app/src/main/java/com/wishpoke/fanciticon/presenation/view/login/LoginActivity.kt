package com.wishpoke.fanciticon.presenation.view.login

import android.os.Bundle
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.Observer
import butterknife.ButterKnife
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.presenation.resource.SingleResponse
import com.wishpoke.fanciticon.util.DialogManager
import com.wishpoke.fanciticon.util.Toaster
import com.wishpoke.fanciticon.util.Util
import com.wishpoke.fanciticon.util.constants.Errors
import com.wishpoke.fanciticon.util.constants.PrefKeys
import com.wishpoke.fanciticon.util.extention.open
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.viewmodel.ext.android.viewModel

class LoginActivity : BaseActivity() {

    private val loginViewModel: LoginViewModel by viewModel()
    private var loginTrial = 0
    private var userId = ""
    private var password = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        ButterKnife.bind(this)
        init()
        setOnClickListeners()
        subscribeObservers()
    }

    private fun init() {
        // 비밀번호 작성 후 Done 버튼 클릭시 로그인
        et_pw.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                btn_find_id.performClick()
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
        et_pw.setPasswordVisibleButton(iv_visible)
    }


    private fun setOnClickListeners() {
        btn_back.setOnClickListener { onBackPressed() }
        btn_login.setOnClickListener {
            // 로그인
            userId = et_id.text.toString().trim()
            password = et_pw.text.toString().trim()
            if (this.userId != userId) {
                this.userId = userId
                loginTrial = 0
            }
            if (userId.isNotEmpty() && password.isNotEmpty()) {
                val fcmToken = storage.getString(PrefKeys.FCM_TOKEN, "")
                loginViewModel.login(userId, password, fcmToken)
            }
        }
        btn_find_id.setOnClickListener { open<FindIdActivity>() }
        btn_reset_pw.setOnClickListener { open<ResetPasswordStep1Activity>() }
        btn_sign_up.setOnClickListener { open<SignUpActivity>() }
    }

    private fun subscribeObservers() {
        loginViewModel.login.observe(this, Observer {
            when (it.status) {
                LoadingState.Status.LOADING -> {
                    mDialog = DialogManager.getInstance().createLoadingDialog(this)
                    mDialog?.show()
                }
                LoadingState.Status.SUCCESS -> {
                    mDialog?.dismiss()
                    val user = it.data!!
                    loginTrial = 0
                    storage.put(PrefKeys.USER, user)
                    if (!user.authorization.isNullOrEmpty()) {
                        storage.put(PrefKeys.AUTHORIZATION, user.authorization)
                    }
                    storage.put(PrefKeys.PASSWORD, password)
                    // 로그인 후 이동
                    Toaster.show(mContext, String.format(getString(R.string.toast_greetings),
                            user.userId))
                    finish()
                }
                LoadingState.Status.FAILED -> {
                    when (it.throwable?.code ?: 0) {
                        // 입력 정보가 잘못 된 경우
                        Errors.NOT_FOUND_USER_ID,
                        Errors.NOT_FOUND_USER_PW,
                        Errors.PASSWORDS_DO_NOT_MATCH,
                        Errors.NON_EXIST_USER -> {
                            mDialog?.dismiss()
                            Toaster.show(mContext, R.string.error_login)
                        }
                        // 토근이 만료되었거나 없는 경우
                        Errors.TOKEN_IS_NOT_VALID,
                        Errors.TOKEN_EXPIRED,
                        Errors.AUTHORIZATION_NOT_FOUND -> {
                            if (loginTrial < 1) {
                                refreshToken(userId, password)
                            } else {
                                // 토큰을 새로 받아와서 로그인 시도했는데 안됨.
                                mDialog?.dismiss()
                                loginTrial = 0
                                Toaster.show(mContext, R.string.error_login)
                            }
                        }
                        else -> {
                            mDialog?.dismiss()
                            Toaster.show(mContext, R.string.error_unknown, it.throwable?.code)
                        }
                    }
                }
            }
        })
    }

    // 토큰 재발급
    private fun refreshToken(userId: String, password: String) {
        loginViewModel.getAuthToken(userId, password, object: SingleResponse<Any>() {
            override fun onSuccess(t: Any) {
                mDialog?.dismiss()
                val token = t as String
                storage.put(PrefKeys.AUTHORIZATION, token)
                btn_login.performClick() // 다시 로그인
                loginTrial++
            }

            override fun onFailure(e: Throwable, code: Int) {
                loginTrial = 0
                mDialog?.dismiss()
                when (code) {
                    // 입력 정보가 잘못 된 경우
                    Errors.NOT_FOUND_USER_ID,
                    Errors.NOT_FOUND_USER_PW,
                    Errors.PASSWORDS_DO_NOT_MATCH,
                    Errors.NON_EXIST_USER -> {
                        Toaster.show(mContext, R.string.error_login)
                    }
                    else -> {
                        Toaster.show(mContext, R.string.error_unknown, e)
                    }
                }
            }
        })
    }

}