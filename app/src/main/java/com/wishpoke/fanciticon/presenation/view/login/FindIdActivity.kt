package com.wishpoke.fanciticon.presenation.view.login

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.presenation.controller.PhoneController
import com.wishpoke.fanciticon.presenation.resource.SingleResponse
import com.wishpoke.fanciticon.presenation.resource.ui.FormEditText
import com.wishpoke.fanciticon.util.Toaster
import com.wishpoke.fanciticon.util.constants.Errors
import com.wishpoke.fanciticon.util.extention.createDialog
import com.wishpoke.fanciticon.util.extention.open
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

class FindIdActivity : BaseActivity() {

    @BindView(R.id.et_phone)
    lateinit var etPhone: EditText
    @BindView(R.id.et_code)
    lateinit var etCode: FormEditText
    @BindView(R.id.btn_send_code)
    lateinit var btnSendCode: TextView
    @BindView(R.id.btn_verify_code)
    lateinit var btnVerifyCode: TextView
    @BindView(R.id.tv_timer)
    lateinit var tvTimer: TextView
    @BindView(R.id.tv_verification_desc)
    lateinit var tvVerificationDesc: TextView
    @BindView(R.id.btn_submit)
    lateinit var btnSubmit: View

    private lateinit var phoneController: PhoneController
    private var timer: Timer? = null

    private val loginViewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_find_id)
        ButterKnife.bind(this)
        init()
    }

    private fun init() {
        phoneController = PhoneController(this, etPhone, etCode, btnSendCode, btnVerifyCode, tvTimer, tvVerificationDesc)
    }

    override fun onResume() {
        super.onResume()
        try {
            if (timer == null) {
                timer = Timer()
            }
            timer?.scheduleAtFixedRate(object : TimerTask() {
                override fun run() {
                    runOnUiThread {
                        btnSubmit.isEnabled = phoneController.phoneInfo.isVerified
                    }
                }
            }, 0, 1000)
        } catch (e: Exception) {
            btnSubmit.isEnabled = true
        }
    }

    @OnClick(R.id.btn_back, R.id.btn_submit)
    fun onClick(v: View) {
        when (v.id) {
            R.id.btn_back -> onBackPressed()
            R.id.btn_submit -> {
                val phoneInfo = phoneController.phoneInfo
                if (phoneInfo.isReliable()) {
                    loginViewModel.findId(phoneInfo, object: SingleResponse<Any>() {
                        override fun onSuccess(t: Any) {
                            // UserId 알려주기
                            val userId = t as String
                            val label = String.format(getString(R.string.dialog_find_id_user_found), userId)
                            mDialog = mContext.createDialog(
                                    message = label,
                                    positiveBlock = { finish() }
                            ).apply { show() }
                        }

                        override fun onFailure(e: Throwable, code: Int) {
                            if (code == Errors.NON_EXIST_USER) {
                                // 가입정보 없음, 회원가입 페이지로 유도
                                mDialog = mContext.createDialog(
                                        message = R.string.dialog_find_id_user_not_found,
                                        negativeLabel = R.string.cancel,
                                        positiveBlock = {
                                            open<SignUpActivity>()
                                            finish()
                                        }
                                ).apply { show() }
                            } else {
                                Toaster.show(applicationContext, R.string.error_unknown, code)
                            }
                        }

                    })
                } else {
                    Toaster.show(applicationContext, R.string.toast_check_phone_verification)
                }
            }
        }
    }

    override fun onStop() {
        timer?.cancel()
        super.onStop()
    }
}