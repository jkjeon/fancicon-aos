package com.wishpoke.fanciticon.presenation.view.chat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.BindView
import butterknife.ButterKnife
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.emoji.Emoji
import com.wishpoke.fanciticon.presenation.base.BaseFragment
import com.wishpoke.fanciticon.presenation.view.custom_emoji.CustomEmojiViewModel
import com.wishpoke.fanciticon.presenation.view.main.EmojiAdapter
import com.wishpoke.fanciticon.presenation.view.main.EmojiViewModel
import com.wishpoke.fanciticon.util.listener.ItemClick
import org.greenrobot.eventbus.EventBus
import org.koin.android.viewmodel.ext.android.viewModel

class ChatEmojiFragment : BaseFragment() {

    @BindView(R.id.rv_emoji)
    lateinit var rvEmoji: RecyclerView
    @BindView(R.id.swipeRefreshLayout)
    @JvmField var swipeRefreshLayout: SwipeRefreshLayout? = null

    private val emojiViewModel: EmojiViewModel by viewModel()
    private val customEmojiViewModel: CustomEmojiViewModel by viewModel()
    private var customEmojiList: MutableList<String> = ArrayList()
    private lateinit var emojiAdapter: EmojiAdapter
    private var categoryId: String? = null

    companion object {
        fun create(categoryId: String): ChatEmojiFragment {
            val fragment = ChatEmojiFragment()
            val bundle = Bundle()
            bundle.putString("categoryId", categoryId)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)
        unbinder = ButterKnife.bind(this, view)
        categoryId = arguments?.getString("categoryId")
        init()

        emojiViewModel.favoriteEmojis.observe(viewLifecycleOwner, Observer {
            emojiAdapter.favoriteEmojiList = it
        })
        return view
    }

    private fun init() {
        swipeRefreshLayout?.isEnabled = false

        emojiAdapter = EmojiAdapter(mContext)
        emojiAdapter.setChatMode(true)
        emojiAdapter.itemClick = ItemClick { view, position ->
            val emoji = emojiAdapter.getItemAt(position)
            when (view.id) {
                R.id.btn_favorite -> {
                    if (!emojiAdapter.isFavorite(emoji)) {
                        emojiViewModel.addFavoriteEmoji(emoji)
                    } else {
                        emojiViewModel.removeFavoriteEmoji(emoji)
                    }
                }
                R.id.rl_item -> {
                    EventBus.getDefault().post(Emoji(emoji))
                    if (!emojiAdapter.isRecentAdapter) {
                        emojiViewModel.addRecentEmoji(emoji)
                    }
                }
            }
        }
        rvEmoji.adapter = emojiAdapter
        rvEmoji.setHasFixedSize(true)
        rvEmoji.layoutManager = LinearLayoutManager(mContext)

        if (emojiAdapter.isFavoriteAdapter()) {
            emojiAdapter.setFavoriteAdapter(false)
        }
        if (categoryId == "custom") {
            customEmojiViewModel.customEmojiList.observe(viewLifecycleOwner, Observer {
                customEmojiList.clear()
                for (emoji in it) {
                    customEmojiList.add(emoji.emoji)
                }
                emojiAdapter.setList(customEmojiList)
            })
        } else if (!categoryId.isNullOrEmpty()) {
            val emojiList = mMainViewModel.getEmojiList(categoryId!!)
            emojiAdapter.setList(emojiList)
        }
    }

}
