package com.wishpoke.fanciticon.presenation.base

import org.greenrobot.eventbus.EventBus

open class InssaFragment : BaseFragment() {
    open fun scrollTop() {}

    override fun onStart() {
        EventBus.getDefault().register(this)
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }
}