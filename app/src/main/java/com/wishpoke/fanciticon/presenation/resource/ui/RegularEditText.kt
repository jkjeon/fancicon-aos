package com.wishpoke.fanciticon.presenation.resource.ui

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import com.wishpoke.fanciticon.util.extention.isVisibleWhen


class RegularEditText : androidx.appcompat.widget.AppCompatEditText {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    fun setClearButton(btnClear: View) {
        btnClear.isVisibleWhen(text.toString().isNotEmpty())
        addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                btnClear.isVisibleWhen(text?.isNotEmpty() == true)
            }
        })
        btnClear.setOnClickListener {
            clear()
        }
        invalidate()
    }

    fun clear() {
        setText("")
        clearFocus()
    }
}