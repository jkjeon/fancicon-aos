package com.wishpoke.fanciticon.presenation.base

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.User
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import com.wishpoke.fanciticon.presenation.view.general.ImageViewerActivity
import com.wishpoke.fanciticon.presenation.view.general.VideoViewerActivity
import com.wishpoke.fanciticon.presenation.view.general.WebViewActivity
import com.wishpoke.fanciticon.presenation.view.main.MainViewModel
import com.wishpoke.fanciticon.util.AppLifeCycleHandler
import com.wishpoke.fanciticon.util.AppLifeCycleHandler.AppLifeCycleCallback
import com.wishpoke.fanciticon.util.FacebookAdLoader
import com.wishpoke.fanciticon.util.ISNCounter
import com.wishpoke.fanciticon.util.ServiceManager
import com.wishpoke.fanciticon.util.constants.PrefKeys
import com.wishpoke.fanciticon.util.constants.RequestCodes
import com.wishpoke.fanciticon.util.listener.PermissionDoneListener
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*


abstract class BaseActivity : AppCompatActivity(), AppLifeCycleCallback {

    lateinit var mActivity: Activity
    lateinit var mContext: Context
    val mMainViewModel: MainViewModel by viewModel()
    val storage: AppStorage by inject()

    @JvmField var adLoader: FacebookAdLoader? = null
    @JvmField var mLastClickTime = 0
    @JvmField var mDialog: AlertDialog? = null

    private var mPermissionDoneListener: PermissionDoneListener? = null
    private val appLifeCycleHandler = AppLifeCycleHandler(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        mContext = this

        registerComponentCallbacks(appLifeCycleHandler)
    }


    open fun goTo(activity: Activity, target: Class<*>, isFinish: Boolean = false, isClear: Boolean = false) {
        val intent = Intent(activity, target)
        if (isClear) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        startActivity(intent)
        if (isFinish) {
            activity.finish()
        }
    }

    open fun openImageViewer(fileName: String) {
        val intent = Intent(this, ImageViewerActivity::class.java)
        val items = ArrayList<String>()
        items.add(fileName)
        intent.putStringArrayListExtra(ImageViewerActivity.KEY, items)
        startActivity(intent)
    }

    open fun openVideoViewer(url: String?) {
        val intent = Intent(this, VideoViewerActivity::class.java)
        intent.putExtra("url", url)
        startActivity(intent)
    }

    open fun openWebViewer(url: String?) {
        val intent = Intent(this, WebViewActivity::class.java)
        intent.putExtra("url", url)
        startActivity(intent)
    }

    open fun updateLoginStateView(btnLogin: TextView) { // 액션바 상단 로그인 버튼
        val user = storage.getObject(PrefKeys.USER, User::class.java)
        if (user == null) {
            btnLogin.background = ContextCompat.getDrawable(this, R.drawable.background_purple_button)
            btnLogin.setTextColor(ContextCompat.getColor(this, R.color.white))
            btnLogin.setText(R.string.settings_login)
        } else {
            btnLogin.background = ContextCompat.getDrawable(this, R.drawable.background_purple_hollow_button)
            btnLogin.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
            btnLogin.text = String.format(getString(R.string.settings_username), user.userId)
        }
    }

    open fun onRequiredPermissionChecked(includeCamera: Boolean, onCompleteListener: PermissionDoneListener?) {
        if (onCompleteListener != null) {
            mPermissionDoneListener = onCompleteListener
        }
        val readPermission = ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE)
        val writePermission = ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val cameraPermission = ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA)
        val permissions = ArrayList<String>()
        if (readPermission == PackageManager.PERMISSION_DENIED) {
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
        if (writePermission == PackageManager.PERMISSION_DENIED) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (includeCamera && cameraPermission == PackageManager.PERMISSION_DENIED) {
            permissions.add(Manifest.permission.CAMERA)
        }
        if (permissions.size > 0) {
            ActivityCompat.requestPermissions(
                    mActivity,
                    permissions.toTypedArray(),
                    RequestCodes.MEDIA_PERMISSION
            )
        } else {
            if (mPermissionDoneListener != null) {
                mPermissionDoneListener?.onDone(true)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == RequestCodes.MEDIA_PERMISSION) {
            // 카메라 / READ / WRITE 스토리지 하나라도 체크 안됬다면 false
            var granted = true
            for (i in permissions.indices) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    granted = false
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        val showRationale = shouldShowRequestPermissionRationale(permissions[i])
                        when {
                            !showRationale -> {
                                val snackbar: Snackbar = Snackbar.make(
                                        findViewById<View>(android.R.id.content),
                                        R.string.toast_set_permission_here,
                                        Snackbar.LENGTH_LONG)
                                snackbar.setAction(R.string.settings) {
                                    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                    val uri = Uri.fromParts("package", packageName, null)
                                    intent.data = uri
                                    startActivity(intent)
                                }
                                snackbar.setActionTextColor(resources.getColor(R.color.purple))
                                val snackbarView = snackbar.view
                                val textView = snackbarView.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
                                textView.maxLines = 5
                                snackbar.show()
                            }
                            Manifest.permission.READ_EXTERNAL_STORAGE == permissions[i] || Manifest.permission.WRITE_EXTERNAL_STORAGE == permissions[i] -> {
                                // 왜 저장공간 권한이 필요한지 알려주기
                                val snackbar: Snackbar = Snackbar.make(
                                        findViewById<View>(android.R.id.content),
                                        R.string.error_permission_required_storage,
                                        Snackbar.LENGTH_LONG)
                                val snackbarView = snackbar.view
                                val textView = snackbarView.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
                                textView.maxLines = 5
                                snackbar.show()
                            }
                            Manifest.permission.CAMERA == permissions[i] -> {
                                // 왜 카메라 권한이 필요한지 알려주기
                                val snackbar: Snackbar = Snackbar.make(
                                        findViewById<View>(android.R.id.content),
                                        R.string.error_permission_required_camera,
                                        Snackbar.LENGTH_LONG)
                                val snackbarView = snackbar.view
                                val textView = snackbarView.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
                                textView.maxLines = 5
                                snackbar.show()
                            }
                        }
                    }
                }
            }
            mPermissionDoneListener?.onDone(granted)
        }
    }

    override fun onResume() {
        super.onResume()
        if (storage.isReconnection) {
            ISNCounter.onReconnection()
        }
        try {
            ServiceManager.stopService(applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onPause() {
        storage.setQuitAt()
        super.onPause()
    }

    override fun onStop() {
        mDialog?.dismiss()
        super.onStop()
    }

    override fun onDestroy() {
        mDialog?.dismiss()
        adLoader?.onDestroy()
        unregisterComponentCallbacks(appLifeCycleHandler)
        super.onDestroy()
    }

    override fun onAppForeground() {

    }

    override fun onAppBackground() {

    }

}