package com.wishpoke.fanciticon.presenation.view.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.wishpoke.fanciticon.R;
import com.wishpoke.fanciticon.data.model.Banner;
import com.wishpoke.fanciticon.util.ImageResolver;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BannerAdapter extends RecyclerView.Adapter<BannerAdapter.ViewHolder> {

    private Context context;
    private List<Banner> banners = new ArrayList<>();
    private View.OnClickListener clickListener;

    public BannerAdapter(Context context, View.OnClickListener clickListener) {
        this.clickListener = clickListener;
        this.context = context;
    }

    public void setBanners(List<Banner> banners) {
        this.banners = banners;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_banner, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.view.setOnClickListener(clickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Banner banner = banners.get(position);

        Glide.with(context).load(banner.getImageUrl())
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                .into(holder.ivImage);

        holder.view.setTag(banner.getLinkUrl());

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        int dp30 = (int) ImageResolver.convertDpToPixel(30, context);
        int dp20 = (int) ImageResolver.convertDpToPixel(20, context);
        if (position == 0) {
            params.setMargins(dp30, 0, 0, 0);
        } else if (position == banners.size() - 1) {
            params.setMargins(0, 0, dp20, 0);
        }

        holder.view.setLayoutParams(params);
    }

    @Override
    public int getItemCount() {
        return banners.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_image)
        ImageView ivImage;
        View view;

        ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            ButterKnife.bind(this, itemView);
        }
    }
}
