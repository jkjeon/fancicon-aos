package com.wishpoke.fanciticon.presenation.view.chat

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.chat.ChatMember
import com.wishpoke.fanciticon.presenation.base.BaseRecyclerViewAdapter
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import com.wishpoke.fanciticon.util.extention.isVisibleWhen

class ChatMemberAdapter(val context: Context) : BaseRecyclerViewAdapter<ChatMemberAdapter.ViewHolder, ChatMember>() {

    private val hexColors = context.resources.getStringArray(R.array.chat_colors)
    private val storage = AppStorage(context)
    private var hostUserId: String = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.item_chat_member, null)
        itemView.layoutParams = params
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val chatMember = getItemAt(position)
        if (hostUserId.isEmpty() && chatMember.isHost == 1) {
            hostUserId = chatMember.userId ?: ""
        }
        with(holder) {
            tvName.text = chatMember.stageName
            ivDeleteMember.isVisibleWhen(storage.userId == hostUserId)
            tvMaster.isVisibleWhen(chatMember.isHost == 1)

            // 컬러 적용 (colorCode가 #HEX로 내려올때)
            chatMember.fontColorType = if (chatMember.fontColorType >= hexColors.size)
                0 else chatMember.fontColorType // ArrayOutOfIndex 방지
            val color = Color.parseColor(hexColors[chatMember.fontColorType])
            ivImage.setColorFilter(color)
            ivDeleteMember.setOnClickListener { itemClick?.onItemClick(it, position) }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.view_online)
        lateinit var viewOnline: View
        @BindView(R.id.tv_name)
        lateinit var tvName: TextView
        @BindView(R.id.iv_image)
        lateinit var ivImage: ImageView
        @BindView(R.id.tv_master)
        lateinit var tvMaster: TextView
        @BindView(R.id.iv_delete_member)
        lateinit var ivDeleteMember: View

        init {
            ButterKnife.bind(this, itemView)
        }
    }
}