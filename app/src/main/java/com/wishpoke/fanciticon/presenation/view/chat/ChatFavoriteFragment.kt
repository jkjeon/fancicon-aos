package com.wishpoke.fanciticon.presenation.view.chat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.BindView
import butterknife.ButterKnife
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.emoji.Emoji
import com.wishpoke.fanciticon.presenation.base.BaseFragment
import com.wishpoke.fanciticon.presenation.view.main.EmojiViewModel
import com.wishpoke.fanciticon.util.listener.ItemClick
import org.greenrobot.eventbus.EventBus
import org.koin.android.viewmodel.ext.android.viewModel

class ChatFavoriteFragment : BaseFragment() {

    @BindView(R.id.rv_emoji)
    lateinit var rvEmoji: RecyclerView
    @BindView(R.id.swipeRefreshLayout)
    @JvmField var swipeRefreshLayout: SwipeRefreshLayout? = null

    private val emojiViewModel: EmojiViewModel by viewModel()
    private lateinit var chatEmojiFavoriteAdapter: ChatEmojiFavoriteAdapter

    companion object {
        fun create(categoryId: String): ChatFavoriteFragment {
            val fragment = ChatFavoriteFragment()
            val bundle = Bundle()
            bundle.putString("categoryId", categoryId)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)
        unbinder = ButterKnife.bind(this, view)
        init()
        return view
    }

    private fun init() {
        swipeRefreshLayout?.isEnabled = false

        // DB에서 즐겨찾기 불러오기
        emojiViewModel.favoriteEmojis.observe(viewLifecycleOwner, Observer {
            chatEmojiFavoriteAdapter.submitFavoriteList(it)
        })

        chatEmojiFavoriteAdapter = ChatEmojiFavoriteAdapter(mActivity)
        chatEmojiFavoriteAdapter.itemClick = ItemClick { v, position ->
            val emoji = chatEmojiFavoriteAdapter.getItemAt(position)
            when (v.id) {
                R.id.btn_favorite -> {
                    if (!chatEmojiFavoriteAdapter.isFavorite(emoji)) {
                        emojiViewModel.addFavoriteEmoji(emoji)
                    } else {
                        emojiViewModel.removeFavoriteEmoji(emoji)
                    }
                }
                else -> {
                    EventBus.getDefault().post(Emoji(emoji))
                    if (!chatEmojiFavoriteAdapter.isRecentPosition(position)) {
                        emojiViewModel.addRecentEmoji(emoji)
                    }
                }
            }
        }

        val recentEmojis = emojiViewModel.getRecentEmojis()
        chatEmojiFavoriteAdapter.submitRecentList(recentEmojis)

        rvEmoji.setHasFixedSize(true)
        rvEmoji.layoutManager = LinearLayoutManager(mActivity)
        rvEmoji.adapter = chatEmojiFavoriteAdapter
    }
}