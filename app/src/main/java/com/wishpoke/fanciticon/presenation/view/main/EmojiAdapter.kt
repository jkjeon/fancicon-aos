package com.wishpoke.fanciticon.presenation.view.main

import android.content.Context
import android.content.pm.PackageManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.emoji.Emoji
import com.wishpoke.fanciticon.presenation.base.BaseRecyclerViewAdapter
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import com.wishpoke.fanciticon.util.Animator
import com.wishpoke.fanciticon.util.Util
import com.wishpoke.fanciticon.util.constants.Constants
import com.wishpoke.fanciticon.util.extention.isVisibleWhen
import com.wishpoke.fanciticon.util.listener.ItemClick
import java.util.*

class EmojiAdapter(val context: Context) : BaseRecyclerViewAdapter<EmojiAdapter.ViewHolder, String>() {

    var favoriteEmojiList: MutableList<Emoji> = ArrayList()
    private var isFavoriteAdapter = false
    var isRecentAdapter = false
    private var isCustomAdapter = false
    private val storage: AppStorage = AppStorage(context)
    private var versionCode = 0
    private var isChatMode = false

    fun isFavoriteAdapter(): Boolean {
        return isFavoriteAdapter
    }

    fun setChatMode(isChatMode: Boolean) {
        this.isChatMode = isChatMode
        notifyDataSetChanged()
    }
    
    fun removeFavorite(position: Int, emoji: String?) {
        favoriteEmojiList.remove(Emoji(emoji))
        notifyItemChanged(position)
    }

    fun addFavorite(position: Int, emoji: String?) {
        favoriteEmojiList.add(0, Emoji(emoji))
        notifyItemChanged(position)
    }

    fun setFavoriteAdapter(flag: Boolean) {
        isFavoriteAdapter = flag
        notifyDataSetChanged()
    }

    fun setCustomAdapter(flag: Boolean) {
        isCustomAdapter = flag
        notifyDataSetChanged()
    }

    override fun getItemAt(position: Int): String {
        return super.getItemAt(position).replace(Constants.NewLabel, "")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_emoji, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val emoji = list[position]
        holder.bind(emoji,
                isFavorite(emoji), isCustomAdapter,
                isRecentAdapter, isChatMode, itemClick)
    }

    fun isFavorite(emoji: String): Boolean {
        return favoriteEmojiList.contains(Emoji(emoji))
    }

    fun updateFavoriteEmojiList(favoriteEmojiList: MutableList<Emoji>) {
        if (isFavoriteAdapter) {
            list.clear()
            favoriteEmojiList.reverse()
            for (emoji in favoriteEmojiList) {
                list.add(emoji.emoji)
            }
        }
        this.favoriteEmojiList = favoriteEmojiList
        notifyDataSetChanged()
    }

    class ViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        
        @BindView(R.id.btn_favorite)
        lateinit var btnFavorite: ImageView
        @BindView(R.id.btn_remove)
        lateinit var btnRemove: ImageView
        @BindView(R.id.tv_emoji)
        lateinit var tvEmoji: TextView
        @BindView(R.id.rl_item)
        lateinit var rlItem: RelativeLayout
        @BindView(R.id.view_new)
        lateinit var viewNew: LinearLayout

        private var context = view.context
        private var storage = AppStorage(context)
        private var versionCode = 0

        init {
            ButterKnife.bind(this, view)
            try {
                val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
                versionCode = pInfo.versionCode
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }
        }

        fun bind(emoji: String, isFavorite: Boolean, isCustomAdapter: Boolean, isRecentAdapter: Boolean, isChatMode: Boolean, itemClick: ItemClick?) {
            val originalEmoji = emoji
            val emoji = emoji.replace(Constants.NewLabel, "")

            Glide.with(context).load(
                    if (isFavorite) {
                        R.drawable.ic_star_purple
                    } else {
                        R.drawable.ic_star_gray
                    }).into(btnFavorite)

            val isNewEmoji = originalEmoji.contains(Constants.NewLabel) && !storage.checkClickedEmoji(versionCode, emoji)
            viewNew.isVisibleWhen(isNewEmoji)
            btnRemove.isVisibleWhen(isCustomAdapter)

            tvEmoji.text = emoji
            btnRemove.setTag(R.string.tag_emoji, emoji)
            btnFavorite.setTag(R.string.tag_emoji, emoji)
            btnFavorite.setTag(R.string.tag_is_favorite, isFavorite)
            btnFavorite.setTag(R.string.tag_position, position)
            rlItem.tag = isRecentAdapter

            btnFavorite.setOnClickListener {
                val isFavorite = btnFavorite.getTag(R.string.tag_is_favorite) as Boolean
                Glide.with(context).load(
                        if (isFavorite) {
                            R.drawable.ic_star_gray
                        } else {
                            R.drawable.ic_star_purple
                        }).into(btnFavorite)
                itemClick?.onItemClick(it, position)
            }
            rlItem.setOnClickListener {
                // Copied! 애니메이션
                if (!isChatMode) {
                    val tvCopied = view.findViewById<View>(R.id.tv_copied) as TextView
                    tvCopied.clearAnimation()
                    tvCopied.startAnimation(Animator.getCopiedClickAnimation(context))
                    // Copy to Clipboard
                    Util.copyToClipboard(context, emoji)
                }
                // 배경 색상 변경 애니메이션
                val itemClickAnimation = Animator.getItemClickAnimation(context, view, R.color.white, R.color.colorItemClick, 1200)
                itemClickAnimation.start()

                // New 이모티콘 딱지 떼기
                storage.saveClickedEmoji(versionCode, emoji)
                (it.findViewById(R.id.view_new) as LinearLayout).visibility = View.INVISIBLE

                itemClick?.onItemClick(it, position)
            }
            if (isCustomAdapter) {
                btnRemove.setOnClickListener {
                    itemClick?.onItemClick(it, position)
                }
            }
        }
    }

    init {
        try {
            val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
            versionCode = pInfo.versionCode
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }
}