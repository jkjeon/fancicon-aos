package com.wishpoke.fanciticon.presenation.view.pung

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.ActivityResultEvent
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.MetaData
import com.wishpoke.fanciticon.data.model.pung.Post
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.presenation.controller.SingleImageController
import com.wishpoke.fanciticon.util.*
import com.wishpoke.fanciticon.util.YoutubeUtils.Companion.getThumbnail
import com.wishpoke.fanciticon.util.constants.Errors
import com.wishpoke.fanciticon.util.constants.PrefKeys
import com.wishpoke.fanciticon.util.extention.isVisibleWhen
import com.wishpoke.fanciticon.util.extention.setOnSingleClickListener
import com.wishpoke.fanciticon.util.listener.DialogClickListener
import com.wishpoke.fanciticon.util.listener.PermissionDoneListener
import com.wishpoke.fanciticon.util.listener.RequestListener
import kotlinx.android.synthetic.main.activity_write_pung.*
import kotlinx.coroutines.*
import org.jsoup.Jsoup
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File

open class WritePungActivity : BaseActivity(), View.OnClickListener {

    private var post: Post? = null
    private var boardNo = 0
    private var linkUrl: String? = null

    private var metaDataJob: Job? = null
    private lateinit var singleImageController: SingleImageController
    private lateinit var writeViewModule: WriteViewModule

    private val pungViewModel: PungViewModel by viewModel()

    private val requestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)

    companion object {
        const val REQUEST_UPDATE_POST = 2119
    }
    
    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_write_pung)
        adLoader = FacebookAdLoader(this, banner_container, ll_isn, wv_edge)
        post = EventBusManager.getInstance().clearAccept(Post::class.java) as? Post
        init()
        writeViewModule.setNickNameView()
        subscribeObservers()
        setClickListeners()

        // 로그인 안했다면 닉네임 호출
        if (!storage.isLoggedIn) {
            pungViewModel.getNickname()
        }
    }

    private fun init() {
        writeViewModule = WriteViewModule(this, et_username, et_comment, et_password, iv_clear)
        singleImageController = SingleImageController(this, object : RequestListener<File?> {
            override fun onSuccess(result: File?) {
                Glide.with(mActivity).load(result)
                        .apply(requestOptions)
                        .into(iv_upload)
                writeViewModule.movieName = null
            }

            override fun onFailure() { // 파일 첨부 실패
                Toaster.show(mContext, R.string.error_failed_to_upload_image)
            }
        }, 1, 1)
        adLoader = FacebookAdLoader(this, banner_container, ll_isn, wv_edge)

        et_password.isVisibleWhen(!storage.isLoggedIn)
        password_divider.isVisibleWhen(!storage.isLoggedIn)

        // 문자 길이에 따라 올리기 버튼 활성화
        val textWatcher: TextWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                iv_clear.isVisibleWhen(et_username.isFocusable && et_username.text.toString().isNotEmpty())
                setSubmitEnabled(writeViewModule.isValid(false, boardNo))
                checkUrl()
            }
            override fun afterTextChanged(s: Editable) {}
        }
        et_username.addTextChangedListener(textWatcher)
        et_comment.addTextChangedListener(textWatcher)
        setSubmitEnabled(writeViewModule.isValid(false, boardNo))
        tv_category_1.setOnClickListener(this)
        tv_category_2.setOnClickListener(this)
        tv_category_3.setOnClickListener(this)
        tv_category_4.setOnClickListener(this)
        tv_category_5.setOnClickListener(this)
        tv_category_6.setOnClickListener(this)
        activateCategory(1)

        post?.let {
            et_username.setText(it.userName)
            et_comment.setText(it.comment)
            et_password.setText(it.password)
            if (it.boardNo > 0) {
                activateCategory(it.boardNo)
            }
            if (!it.fileName.isNullOrEmpty()) {
                Glide.with(mActivity).load(Util.getImageDownloadUrl(it.fileName))
                        .apply(requestOptions)
                        .into(iv_upload)
            }
            if (!it.movieName.isNullOrEmpty()) {
                Glide.with(mActivity).load(YoutubeUtils.getThumbnail(it.movieName))
                        .apply(requestOptions)
                        .into(iv_upload)
            }
            writeViewModule.movieName = post?.movieName
        }
    }

    private fun subscribeObservers() {
        pungViewModel.nickname.observe(this, Observer {
            if (it.status == LoadingState.Status.SUCCESS) {
                writeViewModule.setNickNameView(it?.data)
            }
        })
        pungViewModel.uploadPost.observe(this, androidx.lifecycle.Observer {
            when (it.status) {
                LoadingState.Status.SUCCESS -> {
                    mDialog?.dismiss()
                    storage.nickname = writeViewModule.userName
                    Toaster.showCustomWhite(applicationContext, R.string.pung_complete)

                    adLoader?.showInterstitial(object: FacebookAdLoader.AdCallback {
                        override fun onNext() {
                            tv_submit.isEnabled = true
                            finish()
                        }
                    })
                }
                LoadingState.Status.FAILED -> {
                    mDialog?.dismiss()
                    tv_submit.isEnabled = true
                    handleErrors(it.throwable?.code)
                }
                LoadingState.Status.LOADING -> {
                    mDialog = DialogManager.getInstance().createLoadingDialog(mContext)
                    mDialog?.show()
                    tv_submit.isEnabled = false
                }
            }
        })
        pungViewModel.updatePost.observe(this, Observer {
            when (it.status) {
                LoadingState.Status.SUCCESS -> {
                    mDialog?.dismiss()
                    tv_submit.isEnabled = true
                    Toaster.showCustomWhite(applicationContext, R.string.pung_update_complete)

                    post?.boardNo = boardNo
                    post?.comment = writeViewModule.comment
                    post?.password = writeViewModule.password
                    post?.linkUrl = linkUrl
                    post?.movieName = writeViewModule.movieName
                    post?.fileName = it?.data

                    val data = Intent()
                    data.putExtra("update_post", Gson().toJson(post))
                    setResult(Activity.RESULT_OK, data)
                    finish()
                }
                LoadingState.Status.FAILED -> {
                    mDialog?.dismiss()
                    tv_submit.isEnabled = true
                    handleErrors(it.throwable?.code)
                }
                LoadingState.Status.LOADING -> {
                    mDialog = DialogManager.getInstance().createLoadingDialog(mContext)
                    mDialog?.show()
                    tv_submit.isEnabled = false
                }
            }
        })
    }

    private fun handleErrors(errorCode: Int?) {
        when (errorCode ?: 0) {
            Errors.PERMISSION_DENIED,
            Errors.REGISTERED_ALIAS -> {
                Toaster.show(applicationContext, R.string.error_registered_alias)
            }
            Errors.NON_EXIST_USER -> {
                Toaster.show(applicationContext, R.string.error_user_not_found)
            }
            Errors.PASSWORD_REQUIRED -> {
                Toaster.show(applicationContext, R.string.error_input_4digit_password)
            }
            else -> {
                Toaster.show(applicationContext, R.string.error_unknown, errorCode)
            }
        }
    }


    private fun setClickListeners() {
        btn_back.setOnClickListener {
            it.startAnimation(Animator.getClickAnimation(mContext))
            onBackPressed()
        }
        // 메타 데이터 지우기
        iv_remove_metadata.setOnClickListener {
            it.startAnimation(Animator.getClickAnimation(this))
            if (singleImageController.file == null) {
                // 유튜브 업로드를 클릭해서 썸네일을 채웠다면 Clear
                iv_upload.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_image))
            }
            rl_metadata.visibility = View.GONE
            linkUrl = null
        }
        // 닉네임 지우기
        iv_clear.setOnClickListener {
            et_username.setText("")
        }
        // 주의 사항 클릭
        tv_warnings.setOnClickListener {
            it.startAnimation(Animator.getClickAnimation(mContext))
            mDialog = DialogManager.getInstance().createPungWarningDialog(mContext)
            mDialog?.show()
        }
        // 이미지 업로드
        iv_upload.setOnClickListener {
            clickImage()
        }
        // 글 업로드
        tv_submit.setOnSingleClickListener {
            it.startAnimation(Animator.getClickAnimation(mContext))
            if (writeViewModule.isValid(true, boardNo)) {
                if (post == null) {
                    if (storage.getBoolean(PrefKeys.SAW_PUNG_GUIDE, false)) {
                        mDialog = DialogManager.getInstance().createPungGuideDialog(mContext, object : DialogClickListener<View> {
                            override fun onClick(view: View) {
                                if (view.id == R.id.tv_skip) {
                                    storage.put(PrefKeys.SAW_PUNG_GUIDE, true)
                                }
                                uploadPost()
                            }
                        })
                        mDialog?.show()
                    } else {
                        uploadPost()
                    }
                } else {
                    writeViewModule.run {
                        pungViewModel.updatePost(post!!.postNo, password, comment,
                                singleImageController.file, post!!.fileName, movieName, linkUrl)
                    }
                }
            }
        }
    }

    private fun uploadPost() {
        writeViewModule.run {
            pungViewModel.uploadPost(boardNo, userName, comment,
                    singleImageController.file, linkUrl, movieName, password)
        }
    }

    protected open fun checkUrl() {
        val content = et_comment.text.toString()
        val url = Util.extractUrlFromText(content)
        if (!url.isNullOrEmpty()) {
            metaDataJob = CoroutineScope(Dispatchers.IO).launch {
                val metaData = fetchMetadata(url)
                withContext(Dispatchers.Main) {
                    if (!metaData.isEmpty()) {
                        linkUrl = url
                        Glide.with(mContext)
                                .load(metaData.image)
                                .placeholder(R.mipmap.image_placeholder)
                                .into(iv_metadata_image)
                        tv_metdata_title.text = Util.shorten(metaData.title)
                        tv_metdata_url.text = Util.shorten(metaData.domain)
                        rl_metadata.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun fetchMetadata(url: String): MetaData {
        val metaData = MetaData()
        metaData.url = url
        metaData.domain = Validator.getDomainName(url)
        try {
            val conn = Jsoup.connect(url)
            val doc = conn.get()
            val ogTags = doc.select("meta[property^=og:]")

            if (ogTags.isNotEmpty()) {
                for (tag in ogTags) {
                    when (val text = tag.attr("property")) {
                        "og:title" -> metaData.title = tag.attr("content")
                        "og:description" -> metaData.desc = tag.attr("content")
                        "og:image" -> metaData.image = tag.attr("content")
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return metaData
    }


    protected open fun setSubmitEnabled(flag: Boolean) {
        val color = if (flag) R.color.colorPrimary else R.color.aaaaaa
        tv_submit.setTextColor(resources.getColor(color))
        tv_submit.isEnabled = flag
    }

    // 이미지 클릭
    private fun clickImage() {
        val buttonLabels = arrayOf(getString(R.string.camera), getString(R.string.gallery), getString(R.string.youtube_link))
        mDialog = DialogManager.getInstance().createNChoiceDialog(mContext, buttonLabels, object : DialogClickListener<Int> {
            override fun onClick(value: Int) {
                mDialog?.dismiss()
                when (value) {
                    R.id.btn_1 ->
                        // 카메라
                        onRequiredPermissionChecked(true, object : PermissionDoneListener {
                            override fun onDone(granted: Boolean) {
                                if (granted) {
                                    singleImageController.setDefaultImage(false)
                                    singleImageController.takeCamera()
                                }
                            }
                        })
                    R.id.btn_2 ->
                        // 갤러리
                        onRequiredPermissionChecked(false, object : PermissionDoneListener {
                            override fun onDone(granted: Boolean) {
                                if (granted) {
                                    singleImageController.setDefaultImage(false)
                                    singleImageController.takeGallery()
                                }
                            }
                        })
                    R.id.btn_3 -> {
                        // 유튜브
                        mDialog = DialogManager.getInstance().createYoutubeLinkDialog(mContext, object : DialogClickListener<String> {
                            override fun onClick(url: String) {
                                val thumbnail = getThumbnail(url)
                                Glide.with(mActivity).load(thumbnail)
                                        .apply(requestOptions)
                                        .into(iv_upload)
                                singleImageController.file = null
                                writeViewModule.movieName = url
                            }
                        })
                        if (!isFinishing) mDialog?.show()
                    }
                }
            }
        })
        if (!isFinishing) {
            mDialog?.show()
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        singleImageController.onActivityResult(ActivityResultEvent(requestCode, resultCode, data))
    }

    override fun onClick(v: View) {
        v.startAnimation(Animator.getClickAnimation(mContext))
        when (v.id) {
            R.id.tv_category_1 -> activateCategory(1)
            R.id.tv_category_2 -> activateCategory(2)
            R.id.tv_category_3 -> activateCategory(3)
            R.id.tv_category_4 -> activateCategory(4)
            R.id.tv_category_5 -> activateCategory(5)
            R.id.tv_category_6 -> activateCategory(6)
        }
    }

    private fun activateCategory(number: Int) {
        boardNo = number
        val NUM_OF_CATEGORY = 6
        for (i in 0 until NUM_OF_CATEGORY) {
            val resId = resources.getIdentifier("tv_category_" + (i + 1), "id", packageName)
            val textView = findViewById<TextView>(resId)
            if (i == number - 1) {
                textView.setTextColor(resources.getColor(R.color.white))
                textView.background = resources.getDrawable(R.drawable.background_post_category_active)
            } else {
                textView.setTextColor(resources.getColor(R.color.textGray))
                textView.background = resources.getDrawable(R.drawable.background_post_category)
            }
        }
        setSubmitEnabled(writeViewModule.isValid(false, boardNo))
    }

    override fun onStop() {
        metaDataJob?.cancel()
        super.onStop()
    }

}