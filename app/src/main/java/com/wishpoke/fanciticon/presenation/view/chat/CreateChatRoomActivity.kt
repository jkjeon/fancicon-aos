package com.wishpoke.fanciticon.presenation.view.chat

import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.view.View
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.ImageView
import android.widget.Switch
import androidx.core.content.ContextCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnCheckedChanged
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.ActivityResultEvent
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.chat.ChatRoom
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.presenation.controller.SingleImageController
import com.wishpoke.fanciticon.presenation.resource.SingleResponse
import com.wishpoke.fanciticon.presenation.resource.ui.RegularEditText
import com.wishpoke.fanciticon.util.*
import com.wishpoke.fanciticon.util.constants.PrefKeys
import com.wishpoke.fanciticon.util.listener.DialogClickListener
import com.wishpoke.fanciticon.util.listener.PermissionDoneListener
import com.wishpoke.fanciticon.util.listener.RequestListener
import kotlinx.android.synthetic.main.activity_create_chat_room.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File
import java.util.*

class CreateChatRoomActivity : BaseActivity() {

    @BindView(R.id.iv_upload)
    lateinit var ivUpload: ImageView
    @BindView(R.id.et_master_nickname)
    lateinit var etMasterNickname: RegularEditText
    @BindView(R.id.et_room_name)
    lateinit var etRoomName: RegularEditText
    @BindView(R.id.iv_clear_room_name)
    lateinit var ivClearRoomName: View
    @BindView(R.id.iv_clear_master_nickname)
    lateinit var ivClearMasterNickname: View
    @BindView(R.id.et_password)
    lateinit var etPassword: EditText

    private var board = 0

    val chatRoomViewModel: ChatRoomViewModel by viewModel()
    val chatViewModel: ChatViewModel by viewModel()

    private lateinit var singleImageController: SingleImageController
    private var chatRoom: ChatRoom = ChatRoom()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_chat_room)
        ButterKnife.bind(this)
        adLoader = FacebookAdLoader(this, banner_container, ll_isn, wv_edge)
        init()
        subscribeObservers()
        chatRoomViewModel.createChatRoom.observe(this, androidx.lifecycle.Observer {
            when (it.status) {
                LoadingState.Status.FAILED -> {
                    Toaster.show(applicationContext, R.string.error_unknown, it.throwable?.code ?: 0)
                }
                LoadingState.Status.SUCCESS -> {
                    val response = it.get<ChatRoom>()
                    chatRoom.roomId = response.roomId
                    chatRoom.roomHost = response.roomHost

                    // Open ChatActivity
                    chatRoom.tempStageName = chatRoom.stageName
                    chatRoom.createTime = Date().time / 1000
                    chatRoom.memberCnt = 1
                    EventBusManager.getInstance().cast(chatRoom)
                    val intent = Intent(mActivity, ChatActivity::class.java)
                    intent.putExtra("created", true)
                    startActivity(intent)
                    finish()
                }
            }
        })
    }

    private fun init() {
        etMasterNickname.setClearButton(ivClearMasterNickname)
        etRoomName.setClearButton(ivClearRoomName)

        for (i in 0..5) {
            val switchRes = resources.getIdentifier("sw_subject_$i", "id", packageName)
            val switch = findViewById<Switch>(switchRes)
            switch.setOnCheckedChangeListener { compoundButton, isChecked ->
                switch.isEnabled = !isChecked
                switch.startAnimation(Animator.getClickAnimation(mContext))
                val textColorRes = if (isChecked) R.color.white else R.color.textGray
                val textBackgroundRes = if (isChecked) R.drawable.background_post_category_active else R.drawable.background_post_category
                switch.setTextColor(resources.getColor(textColorRes))
                switch.background = ContextCompat.getDrawable(this, textBackgroundRes)
                if (isChecked) {
                    if (board != i && board > -1) {
                        val prevSwitchRes = resources.getIdentifier("sw_subject_$board", "id", packageName)
                        val prevSwitch = findViewById<Switch>(prevSwitchRes)
                        prevSwitch.isChecked = false
                    }
                    board = i
                }
            }
        }

        singleImageController = SingleImageController(this, object: RequestListener<File?> {
            override fun onSuccess(result: File?) {
                val requestOptions = RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                Glide.with(mActivity).load(result)
                        .apply(requestOptions)
                        .into(ivUpload)
            }

            override fun onFailure() {
                // 파일 첨부 실패
                Toaster.show(applicationContext, R.string.error_failed_to_upload_image)
            }

        }, 1, 1)
    }

    private fun subscribeObservers() {
        chatViewModel.file.observe(this, androidx.lifecycle.Observer {
            if (it.isSuccessful()) {
                chatRoom.fileName = it.data!!
                createChatRoom()
            }
            if (it.isFailed()) {
                Toaster.show(applicationContext, R.string.error_failed_to_upload_image_to_server, it.throwable?.code)
            }
        })
    }

    @OnCheckedChanged(R.id.cb_private)
    fun onCheckedChanged(v: CompoundButton, isChecked: Boolean) {
        if (isChecked) {
            chatRoom.isPrivate = 1
        } else {
            chatRoom.isPrivate = 0
            etPassword.setText("")
        }
        etPassword.isEnabled = isChecked
    }

    @OnClick(R.id.btn_back, R.id.iv_upload, R.id.tv_warnings, R.id.tv_submit)
    fun onClick(v: View) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 800) {
            return
        }
        mLastClickTime = SystemClock.elapsedRealtime().toInt()
        when (v.id) {
            R.id.btn_back -> onBackPressed()
            R.id.iv_upload -> {
                // 사진 업로드
                val buttonLabels = arrayOf(getString(R.string.camera), getString(R.string.gallery))
                mDialog = DialogManager.getInstance().createNChoiceDialog(mContext, buttonLabels, object: DialogClickListener<Int> {
                    override fun onClick(value: Int) {
                        when (value) {
                            R.id.btn_1 -> {
                                // 카메라
                                onRequiredPermissionChecked(true, object: PermissionDoneListener {
                                    override fun onDone(granted: Boolean) {
                                        if (granted) {
                                            singleImageController.setDefaultImage(false)
                                            singleImageController.takeCamera()
                                        }
                                    }
                                })
                            }
                            else -> {
                                // 갤러리
                                onRequiredPermissionChecked(false, object: PermissionDoneListener {
                                    override fun onDone(granted: Boolean) {
                                        if (granted) {
                                            singleImageController.setDefaultImage(false)
                                            singleImageController.takeGallery()
                                        }
                                    }
                                })
                            }
                        }
                    }
                })
                mDialog?.show()
            }
            R.id.tv_warnings -> {
                // 주의사항 보기
                v.startAnimation(Animator.getClickAnimation(mContext))
                mDialog = DialogManager.getInstance().createChatWarningDialog(mContext)
                mDialog?.show()
            }
            R.id.tv_submit -> {
                // 채팅방 생성하기
                v.startAnimation(Animator.getClickAnimation(mContext))
                parseChatRoom()
                if (isValid(chatRoom)) {
                    if (singleImageController.file != null) {
                        chatViewModel.uploadFile(singleImageController.file!!)
                    } else {
                        createChatRoom()
                    }
                }
            }
        }
    }

    private fun parseChatRoom() {
        chatRoom.title = etRoomName.text.toString()
        if (chatRoom.isPrivate == 1) {
            chatRoom.password = etPassword.text.toString()
        }
        chatRoom.stageName = etMasterNickname.text.toString()
        chatRoom.boardNo = board
        // 방장 컬러 랜덤 배정
        val hexColorList = resources.getStringArray(R.array.chat_colors)
        chatRoom.fontColorType = (Math.random() * hexColorList.size).toInt()
    }

    private fun isValid(chatRoom: ChatRoom): Boolean {
        return when {
            UDTextUtils.isEmpty(chatRoom.title) -> {
                // 제목 확인
                Toaster.show(applicationContext, R.string.toast_check_title)
                false
            }
            UDTextUtils.isEmpty(chatRoom.stageName) -> {
                // 톡짱 닉네임 확인
                Toaster.show(applicationContext, R.string.toast_input_nickname)
                false
            }
            chatRoom.isPrivate == 1 && chatRoom.password?.length != 4 -> {
                // 비번 확인
                Toaster.show(applicationContext, R.string.toast_check_password)
                false
            }
            else -> true
        }
    }

    private fun createChatRoom() {
        // 채팅 가이드 보여주기
        val fcmToken = storage.getString(PrefKeys.FCM_TOKEN, "")
        val sawChatGuide = storage.getBoolean(PrefKeys.SAW_CHAT_GUIDE, false)
        if (sawChatGuide) {
            chatRoomViewModel.createChatRoom(chatRoom, fcmToken)
        } else {
            // 채팅 가이드 보여주기
            mDialog = DialogManager.getInstance().createChatGuideDialog(mContext, object: DialogClickListener<View> {
                override fun onClick(value: View) {
                    if (value.id == R.id.tv_skip) {
                        storage.put(PrefKeys.SAW_CHAT_GUIDE, true)
                    }
                    chatRoomViewModel.createChatRoom(chatRoom, fcmToken)
                }
            })
            mDialog?.show()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        singleImageController.onActivityResult(ActivityResultEvent(requestCode, resultCode, data))
    }
}