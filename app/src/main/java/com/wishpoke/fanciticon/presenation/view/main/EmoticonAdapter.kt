package com.wishpoke.fanciticon.presenation.view.main

import android.content.pm.PackageManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.Emoticon
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import com.wishpoke.fanciticon.util.Animator
import com.wishpoke.fanciticon.util.extention.isVisibleWhen
import kotlinx.android.synthetic.main.item_emoji.view.*

class EmoticonAdapter(private val interaction: Interaction? = null) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Emoticon>() {
        override fun areItemsTheSame(oldItem: Emoticon, newItem: Emoticon): Boolean {
            return oldItem.strip() == newItem.strip()
        }

        override fun areContentsTheSame(oldItem: Emoticon, newItem: Emoticon): Boolean {
            return oldItem == newItem
        }
    }
    private val differ = AsyncListDiffer(this, DIFF_CALLBACK)

    fun getList(): MutableList<Emoticon> {
        return differ.currentList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                        R.layout.item_emoji,
                        parent,
                        false
                ),
                interaction
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ViewHolder -> {
                holder.bind(differ.currentList[position])
            }
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    fun submitList(list: List<Emoticon>) {
        differ.submitList(list)
    }

    class ViewHolder
    constructor(
            itemView: View,
            private val interaction: Interaction?
    ) : RecyclerView.ViewHolder(itemView) {

        private val storage = AppStorage(itemView.context)
        private var versionCode = 0

        init {
            try {
                val pInfo = itemView.context.packageManager.getPackageInfo(itemView.context.packageName, 0)
                versionCode = pInfo.versionCode
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }
        }

        fun bind(item: Emoticon) = with(itemView) {
            itemView.setOnClickListener {
                // Copied 글자 애니메이션
                tv_copied.clearAnimation()
                tv_copied.startAnimation(Animator.getCopiedClickAnimation(itemView.context))

                // 배경 색상 변경 애니메이션
                val itemClickAnimation = Animator.getItemClickAnimation(context, itemView, R.color.white, R.color.colorItemClick, 1200)
                itemClickAnimation.start()

                // New 이모티콘 딱지 떼기
                storage.saveClickedEmoji(versionCode, item.strip())
                view_new.visibility = View.GONE

                interaction?.onItemSelected(adapterPosition, item, it.id)
            }
            btn_favorite.setOnClickListener {
                it.startAnimation(Animator.getClickAnimation(itemView.context))
                interaction?.onItemSelected(adapterPosition, item, it.id)
            }

            val isNew = item.strip() != item.text && storage.checkClickedEmoji(versionCode, item.strip())
            view_new.isVisibleWhen(isNew)
            tv_emoji.text = item.strip()
            Glide.with(context).load(
            if (item.isFavorite) {
                R.drawable.ic_star_purple
            } else {
                R.drawable.ic_star_gray
            }).into(btn_favorite)
        }
    }

    interface Interaction {
        fun onItemSelected(position: Int, item: Emoticon, viewId: Int)
    }
}
