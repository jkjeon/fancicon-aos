package com.wishpoke.fanciticon.presenation.view.pung

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.BindView
import butterknife.ButterKnife
import com.google.gson.Gson
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.eventbus.ScrollTop
import com.wishpoke.fanciticon.data.model.pung.Post
import com.wishpoke.fanciticon.presenation.base.InssaFragment
import com.wishpoke.fanciticon.presenation.view.general.WebViewActivity
import com.wishpoke.fanciticon.presenation.view.main.MainActivity
import com.wishpoke.fanciticon.util.*
import com.wishpoke.fanciticon.util.extention.createDialog
import com.wishpoke.fanciticon.util.listener.ItemClick
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.koin.android.viewmodel.ext.android.viewModel

class PungFragment : InssaFragment() {

    @BindView(R.id.rv_reply)
    lateinit var rvReply: RecyclerView
    @BindView(R.id.swipeRefreshLayout)
    @JvmField var swipeRefreshLayout: SwipeRefreshLayout? = null


    private val pungViewModel: PungViewModel by viewModel()
    private lateinit var pungAdapter: PungAdapter
    private var requestPage = 0

    companion object {
        @JvmStatic fun create(): PungFragment {
            return PungFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view: View = inflater.inflate(R.layout.fragment_pung, container, false)
        unbinder = ButterKnife.bind(this, view)
        init()
        requestPage = 0
        subscribeObservers()
        loadPostsAsync()
        pungViewModel.getPopularPostNo(pungAdapter.boardNo)

        return view
    }



    private fun init() {
        rvReply.layoutManager = LinearLayoutManager(mActivity)
        rvReply.itemAnimator?.changeDuration = 0
        rvReply.setItemViewCacheSize(20)

        pungAdapter = PungAdapter(mContext)
        pungAdapter.headerItemClick = ItemClick { view, _ ->
            view.startAnimation(Animator.getClickAnimation(mContext))
            when (view.id) {
                R.id.btn_info -> {
                    // 인싸펑이 뭔가요?
                    mDialog = mContext.createDialog(
                            message = "24시간만 공개되는 인싸펑 게시판!\n작성된지 24시간이 되면 게시글은 펑! 하고 사라집니다 (๑°ㅁ°๑)",
                            negativeLabel = 0
                    ).apply { show() }
                }
                R.id.btn_board -> {
                    // 카테고리 설정
                    mDialog = DialogManager.getInstance().createSingleChoiceDialog(context, pungAdapter.boards, pungAdapter.boardNo) {
                        _, i ->
                        mDialog?.dismiss()
                        pungAdapter.boardNo = i
                        pungAdapter.notifyItemChanged(0)
                        requestPage = 0
                        pungAdapter.clear()
                        loadPostsAsync()
                        pungViewModel.getPopularPostNo(pungAdapter.boardNo)
                    }
                    mDialog?.show()
                }
                R.id.btn_order -> {
                    // 정렬 설정
                    mDialog = DialogManager.getInstance().createSingleChoiceDialog(context, pungAdapter.orders, pungAdapter.orderType - 1) {
                        _, i ->
                        mDialog?.dismiss()
                        pungAdapter.orderType = i + 1
                        pungAdapter.notifyItemChanged(0)
                        requestPage = 0
                        pungAdapter.clear()
                        loadPostsAsync()
                    }
                    mDialog?.show()
                }
                R.id.btn_write -> {
                    // 작성
                    startActivity(Intent(context, WritePungActivity::class.java))
                }
            }
        }
        pungAdapter.itemClick = ItemClick { view, position ->
            val post = pungAdapter.getItemAt(position)
            when (view.id) {
                R.id.ll_like -> {
                    // 좋아요
                    pungViewModel.likeCountUp(post.postNo)
                    if (storage.countPungListLike()) {
                        (mActivity as MainActivity).showInterstitial()
                    }
                }
                R.id.ll_share -> {
                    // 공유하기
                    val intent = Intent()
                    intent.action = Intent.ACTION_SEND
                    if (!post.fileName.isNullOrEmpty()) {
                        intent.type = "*/*"
                        val imageView = (view.parent.parent.parent as ViewGroup).findViewById<View>(R.id.iv_image) as ImageView
                        intent.putExtra(Intent.EXTRA_STREAM,
                                ImageResolver.getLocalBitmapUri(context, ImageResolver.getBitmapFromImageView(imageView)))
                    } else {
                        intent.type = "text/plain"
                    }
                    intent.putExtra(Intent.EXTRA_TEXT, post.comment + "\n#인싸티콘")
                    val chooser = Intent.createChooser(intent, getString(R.string.app_name))
                    startActivity(chooser)

                    pungViewModel.shareCountUp(post.postNo)
                }
                R.id.rl_metadata -> {
                    // 메타데이터
                    val intent = Intent(context, WebViewActivity::class.java)
                    intent.putExtra("url", post.linkUrl)
                    startActivity(intent)
                }
                R.id.iv_more -> {
                    // 더 보기 메뉴
                    val isMine = post.userId == storage.userId
                    val isAnonymous = post.userId.isNullOrEmpty()
                    // TODO: 더보기 메뉴 처리

                }
                else -> {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) return@ItemClick
                    mLastClickTime = SystemClock.elapsedRealtime().toInt()
                    // 상세보기
                    val intent = Intent(context, PungActivity::class.java)
                    intent.putExtra("post", Gson().toJson(post))
                    intent.putExtra("position", position)
                    startActivityForResult(intent, PungActivity.REQUEST_VIEW)
                }
            }
        }
        rvReply.adapter = pungAdapter

        swipeRefreshLayout?.setOnRefreshListener {
            swipeRefreshLayout?.isRefreshing = true
            requestPage = 0
            pungAdapter.clear()
            loadPostsAsync()
        }
        swipeRefreshLayout?.setColorSchemeColors(resources.getColor(R.color.colorPrimary))

        rvReply.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val layoutManager = rvReply.layoutManager as LinearLayoutManager
                val totalItemCount = layoutManager.itemCount
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()

                if (isAdded) {
                    if (totalItemCount <= (lastVisibleItem + 2)) {
                        loadPostsAsync()
                    }
                    if (newState == SCROLL_STATE_IDLE) {
                        if (layoutManager.findFirstVisibleItemPosition() > 1) {
                            (activity as? MainActivity)?.hideTitle()
                        } else {
                            (activity as? MainActivity)?.showTitle()
                        }
                    }
                }
            }
        })

    }

    private fun subscribeObservers() {
        // 순위 데이터 변동
        pungViewModel.popularPostNo.observe(viewLifecycleOwner, Observer {
            if (it.isSuccessful()) {
                it.data?.let { list ->
                    pungAdapter.setPopularPostNo(list)
                }
            }
        })
        // 게시물 데이터 변동
        pungViewModel.postList.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                LoadingState.Status.SUCCESS -> {
                    pungAdapter.setLoading(false)
                    swipeRefreshLayout?.isRefreshing = false

                    it.data?.let { list ->
                        if (list.isNotEmpty()) requestPage++ else requestPage = -1
                        pungAdapter.addList(list)
                    }
                }
                LoadingState.Status.FAILED -> {
                    Logger.e("Test", "error occurred: ${it.throwable?.customMessage} ")
                    if (TestUtil.isDebugMode()) {
                        Toaster.show(mContext.applicationContext, it.throwable?.customMessage ?: "x", it.throwable?.code ?: 0)
                    }
                    pungAdapter.setLoading(false)
                    swipeRefreshLayout?.isRefreshing = false
                }
                LoadingState.Status.LOADING -> {
                    pungAdapter.setLoading(true)
                    if (requestPage == 0) swipeRefreshLayout?.isRefreshing = true
                }
            }
        })
    }

    override fun onDestroyView() {
        rvReply.adapter = null
        super.onDestroyView()
    }

    override fun scrollTop() {
        if (isAdded) {
            rvReply.scrollToPosition(0)
        }
    }

    private fun loadPostsAsync() {
        if (!pungAdapter.isLoading() && requestPage > -1) {
            pungViewModel.getPostList(pungAdapter.orderType, pungAdapter.boardNo, requestPage)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PungActivity.REQUEST_VIEW && resultCode == RESULT_OK) {
            val deletePostNo = data?.getIntExtra("deletePost", 0) ?: 0
            if (deletePostNo > 0) {
                val deleteIndex = pungAdapter.getAdapterPosition(pungAdapter.list.indexOfFirst { it.postNo == deletePostNo })
                if (deleteIndex > -1) pungAdapter.removeItem(deleteIndex)
            } else {
                val postRaw = data?.getStringExtra("post")
                val position = data?.getIntExtra("position", -1) ?: -1
                if (position > 0) {
                    try {
                        val post = Gson().fromJson(postRaw, Post::class.java)
                        pungAdapter.setItem(position, post)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onScrollTop(scrollTop: ScrollTop) {
        if (scrollTop.categoryId == "pung") {
            scrollTop()
        }
    }
}