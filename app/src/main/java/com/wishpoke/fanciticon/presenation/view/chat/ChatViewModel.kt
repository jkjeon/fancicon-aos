package com.wishpoke.fanciticon.presenation.view.chat

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.chat.Chat
import com.wishpoke.fanciticon.data.model.chat.ChatMember
import com.wishpoke.fanciticon.data.model.chat.ChatRoom
import com.wishpoke.fanciticon.data.model.response.DefaultResponse
import com.wishpoke.fanciticon.domain.usecase.chat.*
import com.wishpoke.fanciticon.domain.usecase.file.UploadFileUseCase
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import com.wishpoke.fanciticon.presenation.resource.ResponseHandler
import com.wishpoke.fanciticon.presenation.resource.SingleResponse
import com.wishpoke.fanciticon.util.listener.SocketStateListener
import com.wishpoke.fanciticon.util.listener.ValueCallback
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ua.naiksoftware.stomp.dto.StompMessage
import java.io.File

class ChatViewModel(application: Application,
                    private val connectSocketUseCase: ConnectSocketUseCase,
                    private val enterChatRoomUseCase: EnterChatRoomUseCase,
                    private val joinChatRoomUseCase: JoinChatRoomUseCase,
                    private val sendMessageUseCase: SendMessageUseCase,
                    private val likeChatRoomUseCase: LikeChatRoomUseCase,
                    private val shareChatRoomUseCase: ShareChatRoomUseCase,
                    private val closeChatRoomUseCase: CloseChatRoomUseCase,
                    private val exitChatRoomUseCase: ExitChatRoomUseCase,
                    private val kickChatMemberUseCase: KickChatMemberUseCase,
                    private val getChatRoomMembersUseCase: GetChatRoomMembersUseCase,
                    private val togglePushUseCase: TogglePushUseCase,
                    private val loadMessagesUseCase: LoadMessagesUseCase,
                    private val uploadFileUseCase: UploadFileUseCase,
                    private val registerFcmTokenUseCase: RegisterFcmTokenUseCase): AndroidViewModel(application) {

    val storage: AppStorage = AppStorage(application)

    private val _chatRoomMembers = MutableLiveData<LoadingState<List<ChatMember>>>()
    val chatRoomMembers: LiveData<LoadingState<List<ChatMember>>>
        get() = _chatRoomMembers

    private val _togglePush = MutableLiveData<LoadingState<Boolean>>()
    val togglePush: LiveData<LoadingState<Boolean>>
        get() = _togglePush

    private val _messages = MutableLiveData<LoadingState<List<Chat>>>()
    val messages: LiveData<LoadingState<List<Chat>>>
        get() = _messages

    private val _file = MutableLiveData<LoadingState<String>>()
    val file: LiveData<LoadingState<String>>
        get() = _file

    private val compositeDisposable = CompositeDisposable()

    fun connectSocket(socketStateListener: SocketStateListener) {
        val disposable = connectSocketUseCase(socketStateListener)
        if (disposable != null) {
            compositeDisposable.add(disposable)
        }
    }

    fun enterChatRoom(roomId: String?, valueCallback: ValueCallback<StompMessage>) {
        enterChatRoomUseCase(roomId ?: "", valueCallback)
    }

    fun joinChatRoom(chatRoom: ChatRoom?, fcmToken: String?) {
        val disposable = joinChatRoomUseCase(
                storage.userId,
                chatRoom?.roomId ?: "",
                chatRoom?.stageName ?: "",
                chatRoom?.fontColorType ?: 0,
                fcmToken ?: "")
        if (disposable != null) {
            compositeDisposable.add(disposable)
        }
    }

    fun sendMessage(chatRoom: ChatRoom?, message: String?, fileName: String?) {
        val disposable = sendMessageUseCase(storage.userId,
                chatRoom?.roomId ?: "",
                chatRoom?.stageName ?: "",
                chatRoom?.fontColorType ?: 0,
                message ?: "",
                fileName ?: "")
        if (disposable != null) {
            compositeDisposable.add(disposable)
        }
    }

    fun likeChatRoom(roomId: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                likeChatRoomUseCase(roomId ?: "")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun shareChatRoom(roomId: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                shareChatRoomUseCase(roomId ?: "")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun closeChatRoom(roomId: String?) {
        val disposable = closeChatRoomUseCase(roomId ?: "")
        if (disposable != null) {
            compositeDisposable.add(disposable)
        }
    }

    fun exitChatRoom(roomId: String?, stageName: String?) {
        val disposable = exitChatRoomUseCase(storage.userId,
                roomId ?: "", stageName ?: "")
        if (disposable != null) {
            compositeDisposable.add(disposable)
        }
    }

    fun kickChatMember(roomId: String?, chatMember: ChatMember?) {
        val disposable = kickChatMemberUseCase(roomId ?: "",
                chatMember?.userId ?: "", chatMember?.stageName ?: "", chatMember?.userNo ?: 0)
        if (disposable != null) {
            compositeDisposable.add(disposable)
        }
    }

    fun getChatRoomMembers(roomId: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _chatRoomMembers.postValue(LoadingState.loading())
                val result = getChatRoomMembersUseCase(roomId ?: "")
                _chatRoomMembers.postValue(ResponseHandler.handle(result))
            } catch (e: Exception) {
                _chatRoomMembers.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun togglePush(roomId: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _togglePush.postValue(LoadingState.loading())
                val result = togglePushUseCase(storage.userId, roomId ?: "")
                _togglePush.postValue(ResponseHandler.handle(result))
            } catch (e: Exception) {
                _togglePush.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun loadMessages(roomId: String?, messageNo: Int?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _messages.postValue(LoadingState.loading())
                val result = loadMessagesUseCase(
                        storage.userId, roomId ?: "", messageNo ?: 0
                )
                _messages.postValue(ResponseHandler.handle(result))
            } catch (e: Exception) {
                _messages.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun uploadFile(file: File) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _file.postValue(LoadingState.loading())
                val fileResponse = uploadFileUseCase(file)
                if (fileResponse.isSuccessful) {
                    _file.postValue(LoadingState.success(fileResponse.body()))
                } else {
                    _file.postValue(LoadingState.error())
                }
            } catch (e: Exception) {
                _file.postValue(LoadingState.error(e))
            }
        }

    }

    fun registerFcmToken(userId: String?, fcmToken: String?) {
        val disposable = registerFcmTokenUseCase(userId ?: "", fcmToken ?: "",
                object: SingleResponse<DefaultResponse>() {})
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}