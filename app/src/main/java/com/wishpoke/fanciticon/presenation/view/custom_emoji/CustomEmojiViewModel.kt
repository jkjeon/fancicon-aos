package com.wishpoke.fanciticon.presenation.view.custom_emoji

import android.app.Application
import android.content.Context
import androidx.lifecycle.viewModelScope
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.CustomCategory
import com.wishpoke.fanciticon.data.model.emoji.CustomEmoji
import com.wishpoke.fanciticon.database.dao.CustomEmojiDao
import com.wishpoke.fanciticon.database.dao.OldCustomEmojiDao
import com.wishpoke.fanciticon.presenation.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

class CustomEmojiViewModel(
        application: Application,
        private val customEmojiDao: CustomEmojiDao,
        private val oldCustomEmojiDao: OldCustomEmojiDao
) : BaseViewModel(application) {

    val customEmojiList = customEmojiDao.customEmojiList
    val oldCustomEmojiList = oldCustomEmojiDao.customEmojiList

    private val context: Context = application
    private val numberOfColumn = 4

    fun create(emoji: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            if (!emoji.isNullOrEmpty()) {
                customEmojiDao.insert(CustomEmoji(emoji))
            }
        }
    }

    fun delete(emoji: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            if (!emoji.isNullOrEmpty()) {
                customEmojiDao.delete(CustomEmoji(emoji))
            }
        }
    }

    fun getCustomLetters(): ArrayList<String> {
        val letters = java.util.ArrayList<String>()
        // get custom categories
        val categoryIds = context.resources.getStringArray(R.array.custom_categories)
        for (id in categoryIds) {
            val resId = context.resources.getIdentifier(id, "array", context.packageName)
            letters.addAll(listOf(*context.resources.getStringArray(resId)))
            // 하나의 카테고리가 추가될때마다 공백 1줄 추가
            var mod = letters.size % numberOfColumn
            mod = if (mod == 0) numberOfColumn else numberOfColumn * 2 - mod
            for (i in 0 until mod) {
                letters.add("")
            }
        }
        return letters
    }

    fun getCustomCategories(): ArrayList<CustomCategory> {
        var index = 0
        val customCategories = java.util.ArrayList<CustomCategory>()
        // get custom categories
        val categoryIds = context.resources.getStringArray(R.array.custom_categories)
        for (id in categoryIds) {
            val category = CustomCategory()
            category.index = index
            category.id = id
            val nameResId = context.resources.getIdentifier(id, "string", context.packageName)
            category.name = context.getString(nameResId)
            val arrayResId = context.resources.getIdentifier(id, "array", context.packageName)
            index += context.resources.getStringArray(arrayResId).size
            // 하나의 카테고리가 추가될때마다 공백 1줄 추가
            var mod = index % numberOfColumn
            mod = if (mod == 0) numberOfColumn else numberOfColumn * 2 - mod
            index += mod
            customCategories.add(category)
        }
        return customCategories
    }
}