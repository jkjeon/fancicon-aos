package com.wishpoke.fanciticon.presenation.view.general

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.ads.MobileAds
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.iapxk.sdvem.nkzspq.op.ISN
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.presenation.view.main.MainActivity
import com.wishpoke.fanciticon.util.*
import com.wishpoke.fanciticon.util.VersionHelper.getVersion
import com.wishpoke.fanciticon.util.VersionHelper.isUpdateRequired
import com.wishpoke.fanciticon.util.constants.Constants
import com.wishpoke.fanciticon.util.constants.PrefKeys
import com.wishpoke.fanciticon.util.extention.createDialog
import com.wishpoke.fanciticon.util.extention.open
import java.util.*


class SplashActivity : BaseActivity() {

    private val REQUEST_PERMISSION = 999
    private var pushMessageRaw: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        FirebaseApp.initializeApp(this)
        pushMessageRaw = intent.getStringExtra("pushMessage")
//
        // FCM Token 저장
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {
            if (it.isSuccessful) {
                storage.put(PrefKeys.FCM_TOKEN, it.result?.token ?: "")
            }
        }

        if (storage.pushNotification) {
            FirebaseMessaging.getInstance().subscribeToTopic(Constants.FcmTopic)
        }
        // Stop running services
        ServiceManager.stopService(applicationContext)
        // ISN 설정
        ISNCounter.onReconnection()

        mMainViewModel.versionInfo.observe(this, androidx.lifecycle.Observer {
            when (it.status) {
                LoadingState.Status.SUCCESS -> {
                    val versionInfo = it.data!!
                    if (versionInfo.android.required == 1) {
                        if (!isFinishing && isUpdateRequired(mContext, versionInfo) && !TestUtil.isDebugMode()) {
                            val message = String.format(getString(R.string.dialog_update_required),
                                    getVersion(mContext),
                                    versionInfo.android.version)
                            mDialog = mContext.createDialog(
                                    message = message,
                                    positiveBlock = {
                                        Util.openGooglePlay(mActivity)
                                        finish()
                                    },
                                    cancelable = false
                            ).apply { show() }
                        } else {
                            next()
                        }
                    } else {
                        next()
                    }
                }
                LoadingState.Status.FAILED -> next()
            }
        })
        checkPermission()


        MobileAds.initialize(this)
    }

    private fun checkPermission() {
        val locale = Locale.getDefault().language
        if (locale == "ko") {
            Logger.e("Test","permissionChecked")
            val camPerm = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
            val readPerm = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            if (camPerm == PackageManager.PERMISSION_DENIED || readPerm == PackageManager.PERMISSION_DENIED) {
                mDialog = DialogManager.getInstance().showPermissionDialog(this) {
                    mDialog!!.dismiss()
                    try {
                        val permissions: MutableList<String> = ArrayList()
                        if (camPerm == PackageManager.PERMISSION_DENIED) permissions.add(Manifest.permission.CAMERA)
                        if (readPerm == PackageManager.PERMISSION_DENIED) permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE)
                        val permissionArray = arrayOfNulls<String>(permissions.size)
                        for (i in permissions.indices) {
                            permissionArray[i] = permissions[i]
                        }
                        ActivityCompat.requestPermissions(
                                this@SplashActivity,
                                permissionArray,
                                REQUEST_PERMISSION
                        )
                    } catch (e: Exception) {
                        mMainViewModel.getVersion()
                    }
                }
                if (!isFinishing) {
                    mDialog?.show()
                } else {
                    checkISNPermission()
                }
            } else {
                checkISNPermission()
            }
        } else {
            checkISNPermission()
        }
    }

    private fun checkISNPermission() {
        try {
            if (ISN.checkISN_Permission(this)) {
                mMainViewModel.getVersion()
            }
        } catch (e: Exception) {
            mMainViewModel.getVersion()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ISN.OVERLAY_CODE || requestCode == ISN.IGNORE_CODE) {
            if (ISN.checkISN_Permission(this)) {
                mMainViewModel.getVersion()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        checkPermission()
    }

    private operator fun next() {
        if (storage.isFirstLaunch) {
            storage.pushNotification = true
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this@SplashActivity)) {
                open<PermissionActivity>()
                finish()
            } else {
                open<MainActivity> {
                    putExtra("pushMessage", pushMessageRaw)
                }
                finish()
            }
        } else {
            open<MainActivity> {
                putExtra("pushMessage", pushMessageRaw)
            }
            finish()
        }
    }
}