package com.wishpoke.fanciticon.presenation.resource.ui

import android.content.Context
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.wishpoke.fanciticon.R

class FormEditText : androidx.appcompat.widget.AppCompatEditText {
    var visible = false
    // isValid 상태를 바꾸면 빨간 테두리로 변경
    var isValid: Boolean = true
        set(flag) {
            background = ContextCompat.getDrawable(context, if (flag) {
                R.drawable.background_form_input
            } else {
                R.drawable.background_form_input_marked
            })
            invalidate()
            field = flag
        }
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    fun setPasswordVisibleButton(btnVisible: ImageView) {
        btnVisible.setOnClickListener {
            visible = !visible
            transformationMethod = if (visible) {
                btnVisible.setColorFilter(context.resources.getColor(R.color.aaaaaa))
                null
            } else {
                btnVisible.setColorFilter(context.resources.getColor(R.color.dddddd))
                PasswordTransformationMethod()
            }
            invalidate()
            setSelection(text?.length ?: 0)
        }
    }

}