package com.wishpoke.fanciticon.presenation.base

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.airbnb.lottie.LottieAnimationView
import com.wishpoke.fanciticon.R

abstract class LoadingRecyclerViewAdapter<VH : RecyclerView.ViewHolder, T: Any> : BaseRecyclerViewAdapter<VH, T>() {
    override fun getItemViewType(position: Int): Int {
        return when {
            headerItemClick != null && position == 0 -> TypeHeader
            position == itemCount - 1 -> TypeLoading
            else -> TypeItem
        }
    }

    override fun getItemCount(): Int {
        return if (headerItemClick != null) list.size + 2 else list.size + 1
    }

    fun onBindLoadingViewHolder(holder: RecyclerView.ViewHolder) {
        val h = holder as LoadingViewHolder
        if (isLoading()) {
            h.llLoading.visibility = View.VISIBLE
        } else {
            h.llLoading.visibility = View.GONE
        }
    }

    open fun onCreateLoadingViewHolder(context: Context): LoadingViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_loading, null)
        view.layoutParams = params
        return LoadingViewHolder(view)
    }

    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.ll_loading)
        lateinit var llLoading: View
        @BindView(R.id.av_loading)
        lateinit var avLoading: LottieAnimationView

        init {
            ButterKnife.bind(this, itemView)
        }
    }
}