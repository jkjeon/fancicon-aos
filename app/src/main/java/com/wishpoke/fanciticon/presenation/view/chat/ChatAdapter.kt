package com.wishpoke.fanciticon.presenation.view.chat

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.MetaData
import com.wishpoke.fanciticon.data.model.chat.Chat
import com.wishpoke.fanciticon.presenation.base.BaseRecyclerViewAdapter
import com.wishpoke.fanciticon.presenation.resource.ui.OnSwipeTouchListener
import com.wishpoke.fanciticon.util.Moment
import com.wishpoke.fanciticon.util.Toaster
import com.wishpoke.fanciticon.util.Util
import com.wishpoke.fanciticon.util.Validator
import com.wishpoke.fanciticon.util.constants.ChatActionTypes
import com.wishpoke.fanciticon.util.constants.Constants
import com.wishpoke.fanciticon.util.extention.dp2px
import com.wishpoke.fanciticon.util.extention.isVisibleWhen
import com.wishpoke.fanciticon.util.listener.ItemClick
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jsoup.Jsoup
import java.util.*
import kotlin.collections.ArrayList

class ChatAdapter(val context: Context, val createTime: Long = 0) : BaseRecyclerViewAdapter<RecyclerView.ViewHolder, Chat>() {

    private val uiScope = CoroutineScope(Dispatchers.Main)
    private val hexColors = context.resources.getStringArray(R.array.chat_colors)
    var haveNoMessagesToLoad = false
    var views: List<ViewWrapper> = ArrayList()

    companion object {
        const val TypeInfo = 4
    }

    data class ViewWrapper(val position: Int, val view: View) {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other == null || javaClass != other.javaClass) return false
            val that = other as ViewWrapper
            ?
            return position == that?.position
        }
    }

    init {
        // 제대로 작동하기 위해 빈 HeaderItemClick 생성
        headerItemClick = ItemClick { view, position ->

        }
    }

    fun getFirstMessageNo(): Int {
        return if (list.isNullOrEmpty()) {
            0
        } else {
            list[0].messageNo
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TypeHeader -> {
                val view = LayoutInflater.from(context).inflate(R.layout.item_chat_header, null)
                view.layoutParams = params
                HeaderViewHolder(view)
            }
            TypeInfo -> {
                val view = LayoutInflater.from(context).inflate(R.layout.item_chat_info, null)
                view.layoutParams = params
                InfoViewHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(context).inflate(R.layout.item_chat, null)
                view.layoutParams = params
                ChatViewHolder(view)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        with(holder) {
            when (this) {
                is HeaderViewHolder -> {
                    tvTimer.text = Moment.getPungTime(Date(createTime * 1000))
                }
                is InfoViewHolder -> {
                    // 안내용 메시지
                    val chat = getItemAt(position)
                    tvInfo.text = when (chat.actionType) {
                        ChatActionTypes.ENTER -> {
                            String.format(context.getString(R.string.chat_entered), chat.stageName)
                        }
                        ChatActionTypes.KICK,
                        ChatActionTypes.EXIT -> {
                            String.format(context.getString(R.string.chat_exit), chat.stageName)
                        }
                        else -> chat.msg
                    }
                }
                is ChatViewHolder -> {
                    // 채팅내용
                    val chat = getItemAt(position)
                    // Url이 포함된 경우 메타데이터 보여주기
                    val url = Validator.extractUrl(chat.msg)
                    if (url.isNotEmpty()) {
                        uiScope.launch {
                            fetchMetadata(url, tvMetadataTitle, tvMetadataDesc,
                                    tvMetadataDomain, ivMetadataImageView)
                        }
                    }
                    // 이미지가 포함된 경우 보여주기
                    if (!chat.fileName.isNullOrEmpty()) {
                        ivImage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.image_placeholder))
                        Glide.with(context).asBitmap()
                                .load("${Constants.Url.BASE}/fileApi/getFile?downloadFileName=" + chat.fileName)
                                .into(object: CustomTarget<Bitmap>() {
                                    override fun onLoadCleared(placeholder: Drawable?) {
                                    }
                                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                        ivImage.setImageBitmap(resource)
                                    }
                                })
                    }
                    tvMessage.text = chat.msg
                    tvName.text = chat.stageName
                    tvName.isVisibleWhen(!shouldHideNickname(position))
                    tvMessage.isVisibleWhen(!chat.msg.isNullOrEmpty())
                    ivImage.isVisibleWhen(!chat.fileName.isNullOrEmpty())
                    llMetadata.isVisibleWhen(url.isNotEmpty())

                    // 컬러 적용 (colorCode가 #HEX로 내려올때)
                    chat.fontColorType = if (chat.fontColorType >= hexColors.size)
                        0 else chat.fontColorType // ArrayOutOfIndex 방지
                    val color = Color.parseColor(hexColors[chat.fontColorType])
                    viewChat.setBackgroundColor(color)
                    tvName.setTextColor(color)
                    ivImage.setOnClickListener { itemClick?.onItemClick(it, position) }
                    llMetadata.setOnClickListener { itemClick?.onItemClick(it, position) }

                    var isDifferent = true
                    val time = Moment.format(chat.createTime, "HH:mm")
                    if (getRealPosition(position - 1) > 0) {
                        val prevChat = getItemAt(position - 1)
                        isDifferent = prevChat.actionType != chat.actionType || prevChat.userId != chat.userId || Moment.format(prevChat.createTime, "HH:mm") != time
                    }
                    tvTime.text = if (isDifferent) time else ""

                    val imageTouchListener = object: OnSwipeTouchListener(context) {
                        override fun onCustomActionStart(offset: Float) {
                            super.onCustomActionStart(offset)
                            for (viewWrapper in views) {
                                viewWrapper.view.x = offset
                            }
                        }

                        override fun onCustomActionEnd() {
                            super.onCustomActionEnd()
                            for (viewWrapper in views) {
                                viewWrapper.view.animate().x(0F).setDuration(100).start()
                            }
                        }
                    }

                    val touchListener = object: OnSwipeTouchListener(context) {
                        override fun onCustomActionStart(offset: Float) {
                            super.onCustomActionStart(offset)
                            for (viewWrapper in views) {
                                viewWrapper.view.x = offset
                            }
                        }

                        override fun onCustomActionEnd() {
                            super.onCustomActionEnd()
                            for (viewWrapper in views) {
                                viewWrapper.view.animate().x(0F).setDuration(100).start()
                            }
                        }

                        override fun onLongPressed() {
                            super.onLongPressed()
                            showPopupWindow(tvMessage, chat)
                        }
                    }
                    llChatWrapper.setOnTouchListener(touchListener)
                    ivImage.setOnTouchListener(imageTouchListener)
                    llMetadata.setOnTouchListener(touchListener)

                    val viewWrapper = ViewWrapper(position, llChatWrapper)
                    views += viewWrapper
                }
                else -> {}
            }
        }
    }

    private fun showPopupWindow(parentView: View, chat: Chat) {
        if (!chat.msg.isNullOrEmpty()) {

            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val layout = inflater.inflate(R.layout.view_popup_chat, null)

            val ivCopy = layout.findViewById(R.id.iv_copy) as ImageView
            val ivShare = layout.findViewById(R.id.iv_share) as ImageView

            val width = context.dp2px((32 + 10 + 80).toFloat())
            val height = context.dp2px(40F)
            val popup = PopupWindow(layout, width, height, true)

            ivCopy.setOnClickListener {
                Util.copyToClipboard(context, chat.msg)
                Toaster.show(context, R.string.toast_copied)
                popup.dismiss()
            }

            ivShare.setOnClickListener {
                val intent = Intent()
                intent.action = Intent.ACTION_SEND
                intent.type = "text/plain"
                intent.putExtra(Intent.EXTRA_TEXT, chat.msg)
                val chooser = Intent.createChooser(intent, context.getString(R.string.app_name))
                context.startActivity(chooser)
                popup.dismiss()
            }

            popup.showAsDropDown(parentView, parentView.x.toInt(), parentView.y.toInt())

            parentView.performClick()
        }
    }

    private fun shouldHideNickname(position: Int): Boolean {
        // 이전 채팅이 본인의 채팅일 경우
        val currChat = getItemAt(position)
        val prevPosition = getRealPosition(position) - 1
        return if (prevPosition > -1) {
            val prevChat = getItemAt(position - 1)
            !prevChat.isEntered() && prevChat.userId == currChat.userId
        } else {
            false
        }
    }

    override fun getItemViewType(position: Int): Int {
        var chat: Chat? = null
        if (position > 0) {
            chat = getItemAt(position)
        }
        return when {
            position == 0 -> TypeHeader
            chat?.actionType == ChatActionTypes.MESSAGE -> TypeItem
            else -> TypeInfo
        }
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)
        if (getItemViewType(holder.adapterPosition) == TypeItem) {
            val viewWrapper = ViewWrapper(holder.adapterPosition, (holder as ChatViewHolder).llChatWrapper)
            views += viewWrapper
        }
    }

    fun updateVisiblePosition(firstVisiblePosition: Int, lastVisiblePosition: Int) {
        views = views.filter { it.position in firstVisiblePosition..lastVisiblePosition }
    }

    class ChatViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.tv_name)
        lateinit var tvName: TextView
        @BindView(R.id.tv_message)
        lateinit var tvMessage: TextView
        @BindView(R.id.ll_metadata)
        lateinit var llMetadata: View
        @BindView(R.id.iv_metadata_image)
        lateinit var ivMetadataImageView: ImageView
        @BindView(R.id.tv_metadata_title)
        lateinit var tvMetadataTitle: TextView
        @BindView(R.id.tv_metadata_desc)
        lateinit var tvMetadataDesc: TextView
        @BindView(R.id.tv_metadata_domain)
        lateinit var tvMetadataDomain: TextView
        @BindView(R.id.view_chat)
        lateinit var viewChat: View
        @BindView(R.id.iv_image)
        lateinit var ivImage: ImageView
        @BindView(R.id.tv_time)
        lateinit var tvTime: TextView
        @BindView(R.id.ll_chat_wrapper)
        lateinit var llChatWrapper: LinearLayout

        init {
            ButterKnife.bind(this, itemView)
        }
    }

    class InfoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.tv_info)
        lateinit var tvInfo: TextView
        init {
            ButterKnife.bind(this, itemView)
        }
    }

    class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.tv_timer)
        lateinit var tvTimer: TextView
        init {
            ButterKnife.bind(this, itemView)
        }
    }

    private suspend fun fetchMetadata(url: String, tvTitle: TextView, tvDesc: TextView, tvDomain: TextView, ivImage: ImageView) {
        withContext(Dispatchers.IO) {
            val metaData = MetaData()
            metaData.domain = Validator.getDomainName(url)
            try {
                val conn = Jsoup.connect(url)
                val doc = conn.get()
                val ogTags = doc.select("meta[property^=og:]")

                if (ogTags.isNotEmpty()) {
                    for (tag in ogTags) {
                        val content = tag.attr("content")
                        if (!content.isNullOrEmpty()) {
                            when (val text = tag.attr("property")) {
                                "og:title" -> metaData.title = content
                                "og:description" -> metaData.desc = content
                                "og:image" -> metaData.image = content
                            }
                        }
                    }
                }
                if (metaData.title.isNullOrEmpty()) {
                    val titleTags = doc.select("title")
                    metaData.title = if (!titleTags.text().isNullOrEmpty()) {
                        titleTags.text()
                    } else {
                        metaData.domain
                    }
                }
                if (metaData.image.isNullOrEmpty()) {
                    val imageTag = doc.selectFirst("img")
                    metaData.image = imageTag.attr("src")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            withContext(Dispatchers.Main) {
                if (metaData.desc.isNullOrEmpty()) {
                    metaData.desc = context.getString(R.string.empty_state_check_link)
                }
                tvTitle.text = metaData.title
                tvDesc.text = metaData.desc
                tvDomain.text = metaData.domain
                try {
                    Glide.with(context).load(metaData.image)
                            .error(R.mipmap.image_placeholder)
                            .placeholder(R.mipmap.image_placeholder).into(ivImage)
                } catch (e: java.lang.Exception) {
                }
            }
        }
    }
}