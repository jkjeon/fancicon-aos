package com.wishpoke.fanciticon.presenation.view.custom_emoji;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wishpoke.fanciticon.R;
import com.wishpoke.fanciticon.data.model.CustomCategory;
import com.wishpoke.fanciticon.presenation.view.main.CategoryAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomCategoryAdapter extends RecyclerView.Adapter<CustomCategoryAdapter.ViewHolder> {

    private Context context;
    private int active = 0;
    private List<CustomCategory> customCategories;
    private CategoryAdapter.OnClickCategoryListener listener;

    public CustomCategoryAdapter(Context context, CategoryAdapter.OnClickCategoryListener listener) {
        this.context = context;
        this.listener = listener;
    }

    public void setCategories(List<CustomCategory> customCategories) {
        this.customCategories = customCategories;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_custom_category, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.view.setOnClickListener(listener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CustomCategory category = customCategories.get(position);
        holder.tvName.setText(category.getName());
        if (position == active) {
            holder.tvName.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.viewLine.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        } else {
            holder.tvName.setTextColor(context.getResources().getColor(R.color.textGray));
            holder.viewLine.setBackgroundColor(context.getResources().getColor(R.color.customCategory));
        }
        holder.view.setTag(position);
    }

    @Override
    public int getItemCount() {
        return customCategories.size();
    }

    public void setCurrentItem(int position) {
        active = position;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.view_line)
        View viewLine;
        View view;

        ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            ButterKnife.bind(this, itemView);
        }
    }
}
