package com.wishpoke.fanciticon.presenation.resource

import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.response.ObjectResponse
import com.wishpoke.fanciticon.util.Logger
import com.wishpoke.fanciticon.util.constants.Errors
import retrofit2.Response
import java.net.UnknownHostException

object ResponseHandler {
    fun <T>handle(data: Response<ObjectResponse<T>>): LoadingState<T> {
        return try {
            return if (data.isSuccessful) {
                val body = data.body() as ObjectResponse<*>
                if (body.status.code == 0) {
                    LoadingState.success(body.`object` as T)
                } else {
                    Logger.e("[ResponseHandler] e[code]: ${body.status.code} ${body.status.msg}")
                    LoadingState.error(body.status.code, body.status.msg)
                }
            } else {
                Logger.e("[ResponseHandler] e[data]: $data")
                LoadingState.error(data.code(), data.message())
            }
        } catch (e: Exception) {
            Logger.e("[ResponseHandler] e: ${e.message} ${e.javaClass.name}")
            LoadingState.error(Errors.NETWORK, e.message)
        }
    }

    fun <T>handle(e: Exception): LoadingState<T> {
        return if (e is UnknownHostException) {
            LoadingState.error(Errors.NETWORK, "")
        } else {
            LoadingState.error(Errors.NETWORK, e.message)
        }
    }
}