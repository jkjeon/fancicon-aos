package com.wishpoke.fanciticon.presenation.resource

import android.app.Activity
import android.view.View
import android.widget.TextView
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.util.Util
import com.wishpoke.fanciticon.util.extention.isVisibleWhen
import java.lang.ref.WeakReference
import java.util.*

class MissionBannerModule(val activity: Activity, llMissionBannerRef: WeakReference<View>, tvMissionBannerRef: WeakReference<TextView>) {
    private var llMissionBanner = llMissionBannerRef.get()
    private var tvMissionBanner = tvMissionBannerRef.get()
    private var bannerTexts = activity.resources.getStringArray(R.array.mission_banner_texts)
    private var timer: Timer? = null
    private var index: Int = 0

    var visibility: Boolean = false
        set(value) {
            this.llMissionBanner?.isVisibleWhen(Util.isKorean() && value)
            field = value
        }

    init {
        this.visibility = false
    }


    fun onResume() {
        timer = Timer()
        timer?.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                activity.runOnUiThread {
                    index %= bannerTexts.size
                    tvMissionBanner?.text = bannerTexts[index]
                    index++
                }
            }
        }, 0, 3000)
    }

    fun onStop() {
        timer?.cancel()
    }
}