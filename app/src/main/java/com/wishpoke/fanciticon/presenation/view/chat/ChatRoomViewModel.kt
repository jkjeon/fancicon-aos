package com.wishpoke.fanciticon.presenation.view.chat

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.chat.ChatRoom
import com.wishpoke.fanciticon.domain.usecase.chat.CanJoinToChatRoomUseCase
import com.wishpoke.fanciticon.domain.usecase.chat.CreateChatRoomUseCase
import com.wishpoke.fanciticon.domain.usecase.chat.GetChatRoomListUseCase
import com.wishpoke.fanciticon.domain.usecase.chat.GetPopularChatRoomIdsUseCase
import com.wishpoke.fanciticon.presenation.base.BaseViewModel
import com.wishpoke.fanciticon.presenation.resource.ResponseHandler
import com.wishpoke.fanciticon.util.Logger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ChatRoomViewModel(application: Application,
                        private val getChatRoomListUseCase: GetChatRoomListUseCase,
                        private val createChatRoomUseCase: CreateChatRoomUseCase,
                        private val getPopularChatRoomIdsUseCase: GetPopularChatRoomIdsUseCase,
                        private val canJoinToChatRoomUseCase: CanJoinToChatRoomUseCase): BaseViewModel(application) {

    private val _chatRoomList = MutableLiveData<LoadingState<List<ChatRoom>>>()
    val chatRoomList: LiveData<LoadingState<List<ChatRoom>>>
        get() = _chatRoomList

    private val _createChatRoom = MutableLiveData<LoadingState<ChatRoom>>()
    val createChatRoom: LiveData<LoadingState<ChatRoom>>
        get() = _createChatRoom

    private val _popularChatRoomIds = MutableLiveData<LoadingState<List<String>>>()
    val popularChatRoomIds: LiveData<LoadingState<List<String>>>
        get() = _popularChatRoomIds

    private val _canJoinToChatRoom = MutableLiveData<LoadingState<Any>>()
    val canJoinToChatRoom: LiveData<LoadingState<Any>>
        get() = _canJoinToChatRoom

    fun getChatRoomList(userId: String?, requestPage: Int?, boardNo: Int?, orderType: Int?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _chatRoomList.updateValue(
                        getChatRoomListUseCase(
                                userId,
                                requestPage ?: 0,
                                boardNo ?: 0,
                                orderType ?: 0
                        )
                )
            } catch (e: Exception) {
                _chatRoomList.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun createChatRoom(chatRoom: ChatRoom, fcmToken: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _createChatRoom.updateValue(
                        createChatRoomUseCase(storage.userId, chatRoom, fcmToken ?: "")
                )
            } catch (e: Exception) {
                _createChatRoom.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun canJoinToChatRoom(chatRoom: ChatRoom?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _canJoinToChatRoom.updateValue(
                        canJoinToChatRoomUseCase(storage.userId, chatRoom?.roomId, chatRoom?.password, chatRoom?.tempStageName)
                )
            } catch (e: Exception) {
                _canJoinToChatRoom.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun getPopularChatRoomIds(boardNo: Int?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _popularChatRoomIds.updateValue(getPopularChatRoomIdsUseCase(boardNo ?: 0))
            } catch (e: Exception) {
                _popularChatRoomIds.postValue(ResponseHandler.handle(e))
            }
        }
    }

}