package com.wishpoke.fanciticon.presenation.view.login

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.os.SystemClock
import android.text.Editable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.core.widget.NestedScrollView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.PhoneInfo
import com.wishpoke.fanciticon.data.model.User
import com.wishpoke.fanciticon.data.model.response.DefaultResponse
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.presenation.controller.PhoneController
import com.wishpoke.fanciticon.presenation.resource.SingleResponse
import com.wishpoke.fanciticon.presenation.resource.ui.FormEditText
import com.wishpoke.fanciticon.presenation.view.general.WebViewActivity
import com.wishpoke.fanciticon.presenation.view.main.MainActivity
import com.wishpoke.fanciticon.util.DialogManager
import com.wishpoke.fanciticon.util.Toaster
import com.wishpoke.fanciticon.util.UDTextUtils
import com.wishpoke.fanciticon.util.Validator
import com.wishpoke.fanciticon.util.constants.Constants
import com.wishpoke.fanciticon.util.constants.Errors
import com.wishpoke.fanciticon.util.constants.PrefKeys
import com.wishpoke.fanciticon.util.extention.createDialog
import com.wishpoke.fanciticon.util.extention.isVisibleWhen
import org.koin.android.viewmodel.ext.android.viewModel

class SignUpActivity : BaseActivity() {

    @BindView(R.id.et_id)
    lateinit var etId: FormEditText
    @BindView(R.id.et_pw)
    lateinit var etPw: FormEditText
    @BindView(R.id.et_confirm_pw)
    lateinit var etConfirmPw: FormEditText
    @BindView(R.id.et_phone)
    lateinit var etPhone: EditText
    @BindView(R.id.et_code)
    lateinit var etCode: FormEditText
    @BindView(R.id.btn_send_code)
    lateinit var btnSendCode: TextView
    @BindView(R.id.btn_verify_code)
    lateinit var btnVerifyCode: TextView
    @BindView(R.id.tv_terms)
    lateinit var tvTerms: TextView
    @BindView(R.id.tv_timer)
    lateinit var tvTimer: TextView
    @BindView(R.id.tv_verification_desc)
    lateinit var tvVerificationDesc: TextView
    @BindView(R.id.btn_id_check)
    lateinit var btnIdCheck: TextView
    @BindView(R.id.tv_id_message)
    lateinit var tvIdMessage: TextView
    @BindView(R.id.tv_pw_message)
    lateinit var tvPwMessage: TextView
    @BindView(R.id.tv_confirm_pw_message)
    lateinit var tvConfirmPwMessage: TextView
    @BindView(R.id.et_referral_code)
    lateinit var etReferralCode: FormEditText
    @BindView(R.id.nestedScrollView)
    lateinit var nestedScrollView: NestedScrollView

    private var checkedId = false
    private lateinit var phoneController: PhoneController
    private val loginViewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        ButterKnife.bind(this)
        init()
    }

    private fun init() {
        phoneController = PhoneController(this, etPhone, etCode, btnSendCode, btnVerifyCode, tvTimer, tvVerificationDesc)

        val sentence = getString(R.string.sign_up_terms)
        val strong = getString(R.string.sign_up_terms_strong)
        val spannable = SpannableString(sentence)

        spannable.setSpan(StyleSpan(Typeface.BOLD),
                UDTextUtils.getStartOf(sentence, strong),
                UDTextUtils.getEndOf(sentence, strong),
                SpannableString.SPAN_EXCLUSIVE_INCLUSIVE)
        spannable.setSpan(UnderlineSpan(),
                UDTextUtils.getStartOf(sentence, strong),
                UDTextUtils.getEndOf(sentence, strong),
                SpannableString.SPAN_EXCLUSIVE_INCLUSIVE)
        tvTerms.text = spannable

        etId.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(userId: CharSequence?, p1: Int, p2: Int, p3: Int) {
                checkedId = false
                updateCheckIdView()
            }
        })

        etPw.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(password: CharSequence?, p1: Int, p2: Int, p3: Int) {
                etPw.isValid = Validator.isValidPassword(password.toString())
                if (etPw.isValid) {
                    etConfirmPw.isValid = etConfirmPw.text.toString() == password.toString()
                }
            }
        })

        etConfirmPw.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(password: CharSequence?, p1: Int, p2: Int, p3: Int) {
                etConfirmPw.isValid = etPw.text.toString() == password.toString()
            }
        })
    }

    private fun updateCheckIdView() {
        btnIdCheck.isEnabled = !checkedId
        btnIdCheck.text = getString(if (checkedId) {
            R.string.sign_up_id_check_completed
        } else {
            R.string.sign_up_id_check
        })
    }

    private fun isValid(): Boolean {
        val password = etPw.text?.trim().toString()
        val phoneInfo = phoneController.phoneInfo
        tvIdMessage.isVisibleWhen(!checkedId)
        tvPwMessage.isVisibleWhen(!Validator.isValidPassword(password))
        tvConfirmPwMessage.isVisibleWhen(!etConfirmPw.isValid || etConfirmPw.text.isNullOrEmpty())
        return when {
            !checkedId -> {
                // 중복검사
                nestedScrollView.scrollTo(0, 0)
                false
            }
            !Validator.isValidPassword(password) -> {
                // 비밀번호 검사
                nestedScrollView.scrollTo(0, 0)
                false
            }
            !etConfirmPw.isValid || etConfirmPw.text.isNullOrEmpty() -> false // 비밀번호 확인 검사
            !phoneInfo.isReliable() -> false
            else -> true
        }
    }

    @OnClick(R.id.btn_back, R.id.btn_id_check, R.id.btn_submit, R.id.tv_terms)
    fun onClick(v: View) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) return
        mLastClickTime = SystemClock.elapsedRealtime().toInt()
        when (v.id) {
            R.id.btn_back -> onBackPressed()
            R.id.btn_id_check -> {
                // 아이디 중복 체크
                val userId = etId.text?.trim().toString()
                if (Validator.isValidUserId(userId)) {
                        loginViewModel.checkId(userId, "", object: SingleResponse<DefaultResponse>() {
                            override fun onSuccess(t: DefaultResponse) {
                                mDialog = mContext.createDialog(
                                        message = R.string.dialog_id_available
                                ).apply { show() }
                                checkedId = true
                                updateCheckIdView()
                            }

                            override fun onFailure(e: Throwable, code: Int) {
                                if (code != Errors.UNKNOWN_EXCEPTION) {
                                    mDialog = mContext.createDialog(
                                            message = R.string.dialog_id_duplicate
                                    ).apply { show() }
                                } else {
                                    Toaster.show(mContext, R.string.error_unknown, code)
                                }
                            }
                        })
                } else {
                    Toaster.show(mContext, R.string.toast_validate_id)
                }
            }
            R.id.btn_submit -> {
                // 회원가입
                val userId = etId.text?.trim().toString()
                val password = etPw.text?.trim().toString()
                val referralCode = etReferralCode.text?.trim().toString()
                var phoneInfo = phoneController.phoneInfo
                etReferralCode.isValid = true
                if (isValid()) {
                    mDialog = DialogManager.getInstance().createLoadingDialog(this)
                    mDialog?.show()
                    if (referralCode.isNotEmpty()) {
                        loginViewModel.checkId(userId, "", object: SingleResponse<DefaultResponse>() {
                            override fun onSuccess(t: DefaultResponse) {
                                signUp(userId, password, phoneInfo, referralCode)
                            }

                            override fun onFailure(e: Throwable, code: Int) {
                                mDialog?.dismiss()
                                if (code == Errors.INVALID_REFERRAL_CODE) {
                                    Toaster.show(mContext, R.string.error_invalid_referral_code, code)
                                    etReferralCode.isValid = false
                                }
                            }
                        })
                    } else {
                        signUp(userId, password, phoneInfo, referralCode)
                    }
                } else {
                    Toaster.show(mContext, R.string.toast_check_sign_up_form)
                }
            }
            R.id.tv_terms -> {
                // 이용약관
                val intent = Intent(this, WebViewActivity::class.java)
                intent.putExtra("url", Constants.Url.TERMS)
                startActivity(intent)
            }
        }
    }

    fun signUp(userId: String, password: String, phoneInfo: PhoneInfo, referralCode: String) {
        loginViewModel.signUp(userId, password, phoneInfo, referralCode, object: SingleResponse<User>() {
            override fun onSuccess(user: User) {
                // 가입 완료
                mDialog?.dismiss()
                storage.put(PrefKeys.USER, user)
                storage.put(PrefKeys.PASSWORD, password)
                if (!user.authorization.isNullOrEmpty()) {
                    storage.put(PrefKeys.AUTHORIZATION, user.authorization)
                }
                Toaster.show(mContext, R.string.toast_sign_up_complete)
                goTo(mActivity, MainActivity::class.java, true, true)
            }

            override fun onFailure(e: Throwable, code: Int) {
                mDialog?.dismiss()
                if (code == Errors.REGISTERED_PHONE) {
                    // 해당 휴대폰 번호로 이미 가입된 계정일 경우
                    mDialog = createDialog(message = R.string.error_duplicate_phone).apply { show() }
                } else {
                    Toaster.show(mContext, R.string.error_unknown, code)
                }
            }
        })
    }

    override fun onDestroy() {
        phoneController.onDestroy()
        super.onDestroy()
    }
}