package com.wishpoke.fanciticon.presenation.view.general

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.PagerAdapter
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.otaliastudios.zoom.ZoomImageView
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.util.constants.Constants
import com.wishpoke.fanciticon.util.listener.ItemClick


class ImageViewerAdapter(val context: Context) : PagerAdapter() {

    var itemClick: ItemClick? = null
    private var items = ArrayList<String>()

    override fun isViewFromObject(view: View, any: Any): Boolean {
        return view === any
    }

    override fun getCount(): Int {
        return items.size
    }

    override fun destroyItem(container: ViewGroup, position: Int, any: Any) {
        container.removeView(any as View?)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(context).inflate(R.layout.view_image_viewer, null)
        val holder = ViewHolder(view)
        val item = items[position]

        with (holder) {
            if (item.startsWith("http://") || item.startsWith("https://")) {
                Glide.with(context).load(item).into(ivImage)
            } else {
                ivImage.zoomTo(1.0F, false)
                ivImage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.image_placeholder))
                Glide.with(context).asBitmap()
                        .load("${Constants.Url.BASE}/fileApi/getFile?downloadFileName=$item")
                        .into(object: CustomTarget<Bitmap>() {
                            override fun onLoadCleared(placeholder: Drawable?) {
                            }
                            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                ivImage.setImageBitmap(resource)
                            }
                        })
            }
            ivImage.setOnClickListener {
                itemClick?.onItemClick(it, position)
            }
        }
        container.addView(view)
        return view
    }

    fun setList(items: ArrayList<String>) {
        this.items = items
        notifyDataSetChanged()
    }

    fun getItemAt(position: Int): String {
        return items[position]
    }

    class ViewHolder(itemView: View) {
        @BindView(R.id.iv_image)
        lateinit var ivImage: ZoomImageView

        init {
            ButterKnife.bind(this, itemView)
        }
    }
}