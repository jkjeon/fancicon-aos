package com.wishpoke.fanciticon.presenation.view.custom_emoji

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.AbsListView
import android.widget.AdapterView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import butterknife.OnClick
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.CustomCategory
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.presenation.view.main.CategoryAdapter.OnClickCategoryListener
import com.wishpoke.fanciticon.presenation.view.main.EmojiViewModel
import com.wishpoke.fanciticon.util.Animator
import com.wishpoke.fanciticon.util.DialogManager
import com.wishpoke.fanciticon.util.FacebookAdLoader
import com.wishpoke.fanciticon.util.Toaster.show
import kotlinx.android.synthetic.main.activity_custom_emoji.*
import org.koin.android.viewmodel.ext.android.viewModel

class CustomEmojiActivity : BaseActivity() {

    private var customCategories: ArrayList<CustomCategory> = ArrayList()
    private var gridAdpter: GridAdapter? = null
    private var visibleItems = 0
    private var previousScrollTo = 0
    private var clickCategory = false

    private lateinit var categoryAdapter: CustomCategoryAdapter
    private val emojiViewModel: EmojiViewModel by viewModel()
    private val customEmojiViewModel: CustomEmojiViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_emoji)
        ButterKnife.bind(this)

        customCategories = customEmojiViewModel.getCustomCategories()

        adLoader = FacebookAdLoader(this, banner_container, ll_isn, wv_edge)

        // 카테고리
        categoryAdapter = CustomCategoryAdapter(this, categoryListener)
        categoryAdapter.setCategories(customCategories)
        val horizontalLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rv_custom_category.layoutManager = horizontalLayoutManager
        rv_custom_category.adapter = categoryAdapter

        // 문자 목록
        gridAdpter = GridAdapter(this)
        gridAdpter?.setLetters(customEmojiViewModel.getCustomLetters())

        gv_letter.adapter = gridAdpter
        gv_letter.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val letter = (view.findViewById<View>(R.id.tv_letter) as TextView).tag as String
            val cursorIndex = if (et_emoji.selectionEnd >= 0) et_emoji.selectionEnd else 0
            val builder = StringBuilder(et_emoji.text.toString())
            builder.insert(cursorIndex, letter)
            et_emoji.setText(builder.toString())
            et_emoji.setSelection(cursorIndex + letter.length)
        }
        gv_letter.postDelayed({
            if (!isFinishing) {
                val firstVisiblePosition = gv_letter.firstVisiblePosition
                val lastVisiblePosition = gv_letter.lastVisiblePosition
                visibleItems = lastVisiblePosition - firstVisiblePosition
            }
        }, 50)
        gv_letter.setOnScrollListener(object : AbsListView.OnScrollListener {
            var previousCategoryPosition = 0
            var previousScrollState = 0
            var scrolling = false
            override fun onScrollStateChanged(view: AbsListView, scrollState: Int) {
                // 카테고리 클릭해서 자동으로 스크롤 이동중
                if (clickCategory && scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    scrolling = false
                } else if (clickCategory && previousScrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING && scrollState == 0) {
                    scrolling = false
                    clickCategory = false
                } else if (!clickCategory && previousScrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && scrollState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    scrolling = true
                } else if (!clickCategory && previousScrollState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL && scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    scrolling = true
                } else if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    scrolling = false
                }
                previousScrollState = scrollState
            }

            override fun onScroll(view: AbsListView, firstVisibleItem: Int, visibleItemCount: Int, totalItemCount: Int) {
                if (scrolling) {
                    val firstVisiblePosition = gv_letter.firstVisiblePosition + 12
                    var categoryPosition = customCategories.size - 1
                    for (i in 0 until customCategories.size - 1) {
                        val currentCategoryStart = customCategories[i].index
                        val currentCategoryEnd = customCategories[i + 1].index - 4
                        if (firstVisiblePosition in currentCategoryStart..currentCategoryEnd) {
                            categoryPosition = i
                        }
                    }
                    if (categoryPosition != previousCategoryPosition) {
                        val position = customCategories.indexOf(customCategories[categoryPosition])
                        categoryAdapter.setCurrentItem(position)
                        rv_custom_category.scrollToPosition(position)
                        previousCategoryPosition = categoryPosition
                    }
                }
            }
        })
    }

    private val categoryListener: OnClickCategoryListener = object : OnClickCategoryListener {
        override fun onClick(v: View) {
            clickCategory = true
            val position = v.tag as Int
            categoryAdapter.setCurrentItem(position)
            val handler = Handler()
            handler.postDelayed({
                val scrollPosition = customCategories[position].index
                var scrollTo = if (scrollPosition == 0) 0 else scrollPosition + visibleItems - 4
                if (scrollPosition != 0 && previousScrollTo > scrollTo) {
                    scrollTo = scrollPosition
                }
                gv_letter.smoothScrollToPosition(scrollTo)
                previousScrollTo = scrollTo
            }, 50)
        }
    }

    @OnClick(R.id.btn_detail)
    fun onClickDetail(v: View?) {
        val emoji = et_emoji.text.toString()
        mDialog = DialogManager.getInstance().createCustomEmojiDetailDialog(this, emoji)
        mDialog?.show()
    }

    @OnClick(R.id.btn_save)
    fun onClickSave(v: View) {
        v.startAnimation(Animator.getClickAnimation(this))
        val emoji = et_emoji.text.toString()
        if (emoji.isNotEmpty()) {
            mDialog = DialogManager.getInstance().createLoadingDialog(mActivity)
            mDialog?.show()
            adLoader?.showInterstitial(object: FacebookAdLoader.AdCallback {
                override fun onNext() {
                    mDialog?.dismiss()
                    customEmojiViewModel.create(emoji)
                    show(applicationContext, R.string.toast_custom_emoji_saved)
                    finish()
                }
            })
        }
    }

    @OnClick(R.id.btn_back)
    fun onClickBack(v: View) {
        v.startAnimation(Animator.getClickAnimation(this))
        onBackPressed()
    }
}