package com.wishpoke.fanciticon.presenation.controller

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import androidx.core.content.FileProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.nipunru.nsfwdetector.NSFWDetector
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.ActivityResultEvent
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.presenation.base.BaseFragment
import com.wishpoke.fanciticon.util.FileManager
import com.wishpoke.fanciticon.util.Logger
import com.wishpoke.fanciticon.util.Toaster
import com.wishpoke.fanciticon.util.constants.RequestCodes
import com.wishpoke.fanciticon.util.listener.RequestListener
import com.yalantis.ucrop.UCrop
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import pl.aprilapps.easyphotopicker.MediaFile
import pl.aprilapps.easyphotopicker.MediaSource
import java.io.File

open class SingleImageController {
    protected lateinit var activity: BaseActivity
    private var uploadListener: RequestListener<File?>? = null
    var file: File? = null
    var fileName: String? = null
    protected var onSaved = false
    protected var aspectX = 0
    protected var aspectY = 0
    protected lateinit var easyImage: EasyImage
    private var useHighQualityImage = false
    private var useDefaultImage = false

    protected var lastRequestCode = 0

    constructor(activity: BaseActivity, uploadListener: RequestListener<File?>?, aspectX: Int, aspectY: Int) {
        this.uploadListener = uploadListener
        initialize(activity, aspectX, aspectY)
    }

    constructor(activity: BaseActivity, uploadListener: RequestListener<File?>?, aspectX: Int, aspectY: Int,
                useHighQualityImage: Boolean) : this(activity, uploadListener, aspectX, aspectY) {
        this.uploadListener = uploadListener
        this.useHighQualityImage = useHighQualityImage
        initialize(activity, aspectX, aspectY)
    }

    constructor(fragment: BaseFragment, uploadListener: RequestListener<File?>?, aspectX: Int, aspectY: Int,
                useHighQualityImage: Boolean) {
        activity = fragment.mActivity as BaseActivity
        this.uploadListener = uploadListener
        initialize(activity, aspectX, aspectY)
    }

    constructor()

    internal fun initialize(activity: BaseActivity, aspectX: Int, aspectY: Int) {
        easyImage = EasyImage.Builder(activity)
                .setCopyImagesToPublicGalleryFolder(false)
                .allowMultiple(false)
                .build()
        this.activity = activity
        this.aspectX = aspectX
        this.aspectY = aspectY
    }

    fun setDefaultImage(useDefaultImage: Boolean) {
        this.useDefaultImage = useDefaultImage
    }

    fun takeCamera() {
        lastRequestCode = RequestCodes.CAMERA
        easyImage.openCameraForImage(activity)
    }

    fun takeGallery() {
        lastRequestCode = RequestCodes.GALLERY
        easyImage.openGallery(activity)
    }

    // 마지막에 했었던 방법으로 다시 요청
    fun requestAgain() {
        when (lastRequestCode) {
            RequestCodes.CAMERA -> takeCamera()
            RequestCodes.GALLERY -> takeGallery()
        }
    }

    open fun onActivityResult(activityResultEvent: ActivityResultEvent) {
        val requestCode = activityResultEvent.requestCode
        val resultCode = activityResultEvent.resultCode
        val data = activityResultEvent.data
        onSaved = false
        easyImage.handleActivityResult(requestCode, resultCode, data, activity, object : DefaultCallback() {
            override fun onMediaFilesPicked(mediaFiles: Array<MediaFile>, mediaSource: MediaSource) {
                val mediaFile: MediaFile? = mediaFiles[0]
                mediaFile?.file?.let {
                    val sourceUri = if (Build.VERSION.SDK_INT >= 23) {
                        FileProvider.getUriForFile(activity,
                                activity.packageName + ".provider",
                                it)
                    } else {
                        Uri.fromFile(it)
                    }
                    val tempFile = File(FileManager.getCroppedImagePath())
                    val mainColor = activity.resources.getColor(R.color.purple)
                    val options = UCrop.Options()
                    options.setActiveControlsWidgetColor(mainColor)
                    options.setCompressionQuality(85)

                    val uCrop = UCrop.of(sourceUri, Uri.fromFile(tempFile))
                            .withOptions(options)
                            .withMaxResultSize(1024, 9999)
                    if (aspectX > 0 || aspectY > 0) {
                        uCrop.withAspectRatio(aspectX.toFloat(), aspectY.toFloat())
                    }
                    uCrop.start(activity)
                }
            }

            override fun onImagePickerError(error: Throwable, source: MediaSource) {
                super.onImagePickerError(error, source)
                Logger.e("Test", "image error: ${error.message}")
                uploadListener?.onFailure()
            }
        })
        if (requestCode == UCrop.REQUEST_CROP) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = UCrop.getOutput(data)
                file = FileManager.getFileFromUri(activity, uri)
                // 크롭 성공시
                val requestOptions = RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                Glide.with(activity)
                        .asBitmap()
                        .apply(requestOptions)
                        .load(file)
                        .into(object: CustomTarget<Bitmap>() {
                            override fun onLoadCleared(placeholder: Drawable?) {}

                            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                val confidenceThreshold = 0.8F
                                NSFWDetector.isNSFW(resource, confidenceThreshold) { isNude, confidence, image ->
                                    if (isNude) {
                                        // 누드다!
                                        Toaster.show(activity, R.string.toast_nude)
                                    } else {
                                        uploadListener?.onSuccess(file)
                                    }
                                }
                            }
                        })
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // 크롭을 하려다 취소 했을때, 다시 이전의 화면을 띄워준다.
                requestAgain()
            } else {
                if (resultCode == UCrop.RESULT_ERROR) {
                    val cropError = UCrop.getError(data)
                    cropError?.message?.let { Toaster.show(activity, it) }
                } else {
                    Toaster.show(activity, R.string.error_unknown, resultCode)
                }
            }
        }
    }
}