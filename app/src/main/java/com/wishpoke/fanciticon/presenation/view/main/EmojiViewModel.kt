package com.wishpoke.fanciticon.presenation.view.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.Category
import com.wishpoke.fanciticon.data.model.emoji.Emoji
import com.wishpoke.fanciticon.database.dao.EmojiDao
import com.wishpoke.fanciticon.domain.usecase.emoji.AddRecentEmojiUseCase
import com.wishpoke.fanciticon.domain.usecase.emoji.GetCategoriesUseCase
import com.wishpoke.fanciticon.domain.usecase.emoji.GetRecentEmojisUseCase
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class EmojiViewModel(
        application: Application,
        private val storage: AppStorage,
        val getRecentEmojisUseCase: GetRecentEmojisUseCase,
        val addRecentEmojiUseCase: AddRecentEmojiUseCase,
        val getCategoriesUseCase: GetCategoriesUseCase,
        val emojiDao: EmojiDao
) : AndroidViewModel(application) {

    val favoriteEmojis = emojiDao.getFavoriteEmojiList()

    var emojiCategoryStartPosition: Int = 0
    private val _categories = MutableLiveData<List<Category>>()
    val categories: LiveData<List<Category>>
        get() = _categories

    private val context = application

    fun addFavoriteEmoji(text: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            if (text != null) {
                emojiDao.insert(Emoji(text))
            }
        }
    }

    fun removeFavoriteEmoji(text: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            if (text != null) {
                emojiDao.delete(Emoji(text))
            }
        }
    }

    fun getRecentEmojis() = getRecentEmojisUseCase()

    fun addRecentEmoji(text: String?) = addRecentEmojiUseCase(text)

    fun getCategories() {
        viewModelScope.launch(Dispatchers.Default) {
            val firstScreen = storage.getString(AppStorage.FIRST_SCREEN_CATEGORY_ID, "favorite")
            val categoryHashMap: HashMap<String, String> = HashMap()

            val categoryIdsArray = context.resources.getStringArray(R.array.categories)
            for (categoryId in categoryIdsArray) {
                val categoryResId = context.resources.getIdentifier(categoryId, "string", context.packageName)
                val categoryName = context.resources.getString(categoryResId)

                categoryHashMap[categoryId] = categoryName
            }
            val response = getCategoriesUseCase(firstScreen, categoryHashMap)
            emojiCategoryStartPosition = response.emojiCategoryStartPosition
            _categories.postValue(response.categories)
        }
    }




}