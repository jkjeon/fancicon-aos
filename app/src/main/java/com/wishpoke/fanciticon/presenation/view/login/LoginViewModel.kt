package com.wishpoke.fanciticon.presenation.view.login

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.PhoneInfo
import com.wishpoke.fanciticon.data.model.Registered
import com.wishpoke.fanciticon.data.model.User
import com.wishpoke.fanciticon.data.model.response.DefaultResponse
import com.wishpoke.fanciticon.data.model.response.ObjectResponse
import com.wishpoke.fanciticon.data.repository.LoginRepository
import com.wishpoke.fanciticon.domain.usecase.login.*
import com.wishpoke.fanciticon.presenation.base.BaseViewModel
import com.wishpoke.fanciticon.presenation.resource.ResponseHandler
import com.wishpoke.fanciticon.presenation.resource.SingleResponse
import com.wishpoke.fanciticon.util.constants.PrefKeys
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

class LoginViewModel(application: Application,
                     private val checkIdUseCase: CheckIdUseCase,
                     private val findIdUseCase: FindIdUseCase,
                     private val getAuthTokenUseCase: GetAuthTokenUseCase,
                     private val getUserDetailUseCase: GetUserDetailUseCase,
                     private val leaveUseCase: LeaveUseCase,
                     private val logInUseCase: LogInUseCase,
                     private val logOutUseCase: LogOutUseCase,
                     private val signUpUseCase: SignUpUseCase,
                     private val updatePasswordUseCase: UpdatePasswordUseCase,
                     private val updateUserPointUseCase: UpdateUserPointUseCase,
                     private val repository: LoginRepository
) : BaseViewModel(application) {

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    private val _login = MutableLiveData<LoadingState<User>>()
    val login: LiveData<LoadingState<User>>
        get() = _login

    private val _logout = MutableLiveData<LoadingState<Any>>()
    val logout: LiveData<LoadingState<Any>>
        get() = _logout

    private val _updateProfile = MutableLiveData<LoadingState<Any>>()
    val updateProfile: LiveData<LoadingState<Any>>
        get() = _updateProfile

    private val _checkNickname = MutableLiveData<LoadingState<Registered>>()
    val checkNickname: LiveData<LoadingState<Registered>>
        get() = _checkNickname

    private fun getUser(): User {
        return User().apply {
            userId = storage.userId
            password = storage.getString(PrefKeys.PASSWORD)
            createTime = storage.getLong(PrefKeys.STAMP_TIME)
            authorization = storage.getString(PrefKeys.AUTHORIZATION)
        }
    }

    fun checkId(userId: String?, referralCode: String?, singleResponse: SingleResponse<DefaultResponse>)  {
        val disposable = checkIdUseCase(userId ?: "", referralCode ?: "", singleResponse)
        compositeDisposable.add(disposable)
    }

    fun leave(password: String?, singleResponse: SingleResponse<DefaultResponse>){
        val user = getUser().apply {
            this.password = password
        }
        val disposable = leaveUseCase(user, singleResponse)
        compositeDisposable.add(disposable)
    }

    fun signUp(userId: String?, password: String?, phoneInfo: PhoneInfo, referralCode: String?, singleResponse: SingleResponse<User>) {
        val stampTime = Date().time / 1000
        storage.put(PrefKeys.STAMP_TIME, stampTime)

        val user = User().apply {
            this.userId = userId
            this.password = password
            this.nacd = phoneInfo.nacd
            this.phone = phoneInfo.phone
            this.referralCode = referralCode
            this.createTime = stampTime
        }


        val disposable = signUpUseCase(user, object: SingleResponse<ObjectResponse<User>>() {
            override fun onSuccess(t: ObjectResponse<User>) {
                super.onSuccess(t)
                singleResponse.onSuccess(t.get())
            }
            override fun onFailure(throwable: Throwable, code: Int) {
                super.onFailure(throwable, code)
                singleResponse.onFailure(throwable, code)
            }
        })
        compositeDisposable.add(disposable)
    }

    fun login(userId: String?, password: String?, fcmToken: String?) {
        val stampTime = storage.getLong(PrefKeys.STAMP_TIME)

        val user = User().apply {
            this.userId = userId
            this.password = password
            createTime = stampTime
            authorization = storage.getString(PrefKeys.AUTHORIZATION)
        }

        viewModelScope.launch(Dispatchers.IO) {
            try {
                _login.updateValue(logInUseCase(user, fcmToken ?: ""))
            } catch (e: Exception) {
                _login.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun logout(userId: String?, fcmToken: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _logout.updateValue(logOutUseCase(userId, fcmToken ?: ""))
            } catch (e: java.lang.Exception) {
                _logout.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun getUserDetail(singleResponse: SingleResponse<User>) {
        val disposable = getUserDetailUseCase(
                storage.userId, object: SingleResponse<ObjectResponse<User>>() {
            override fun onSuccess(t: ObjectResponse<User>) {
                super.onSuccess(t)
                singleResponse.onSuccess(t.get())
            }
            override fun onFailure(throwable: Throwable, code: Int) {
                super.onFailure(throwable, code)
                singleResponse.onFailure(throwable, code)
            }
        })
        compositeDisposable.add(disposable)
    }

    fun findId(phoneInfo: PhoneInfo, singleResponse: SingleResponse<Any>) {
        val disposable = findIdUseCase(phoneInfo, object: SingleResponse<ObjectResponse<Any>>() {
            override fun onSuccess(t: ObjectResponse<Any>) {
                super.onSuccess(t)
                singleResponse.onSuccess(t.get())
            }

            override fun onFailure(throwable: Throwable, code: Int) {
                super.onFailure(throwable, code)
                singleResponse.onFailure(throwable, code)
            }
        })
        compositeDisposable.add(disposable)
    }

    fun updatePassword(phoneInfo: PhoneInfo, password: String, singleResponse: SingleResponse<DefaultResponse>) {
        val disposable = updatePasswordUseCase(phoneInfo, password, singleResponse)
        compositeDisposable.add(disposable)
    }

    fun updateUserPoint(amount: Int, provider: String, singleResponse: SingleResponse<DefaultResponse>) {
        val disposable = updateUserPointUseCase(
                getUser(),
                amount, provider, singleResponse)
        compositeDisposable.add(disposable)
    }

    fun getAuthToken(userId: String, password: String, singleResponse: SingleResponse<Any>) {
        val stampTime = Date().time / 1000
        storage.put(PrefKeys.STAMP_TIME, stampTime)

        val user = User().apply {
            this.userId = userId
            this.password = password
            createTime = stampTime
        }

        val disposable = getAuthTokenUseCase(user, object: SingleResponse<ObjectResponse<Any>>() {
            override fun onSuccess(t: ObjectResponse<Any>) {
                super.onSuccess(t)
                singleResponse.onSuccess(t.get())
            }

            override fun onFailure(throwable: Throwable, code: Int) {
                super.onFailure(throwable, code)
                singleResponse.onFailure(throwable, code)
            }
        })

        compositeDisposable.add(disposable)
    }

    fun updateProfile(updateId: String?, updatePassword: String?, updateUsername: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val user = getUser()
                val password = updatePassword ?: user.password
                _updateProfile.updateValue(repository.updateProfile(
                        user, updateId, password, updateUsername
                ))
            } catch (e: Exception) {
                _updateProfile.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun checkNickname(userName: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _checkNickname.updateValue(
                        repository.checkNickname(userName)
                )
            } catch (e: Exception) {
                _checkNickname.postValue(ResponseHandler.handle(e))
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
    }
}