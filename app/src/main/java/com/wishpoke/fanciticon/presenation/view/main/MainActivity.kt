package com.wishpoke.fanciticon.presenation.view.main

import android.animation.ValueAnimator
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager.SimpleOnPageChangeListener
import com.google.gson.Gson
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.Category
import com.wishpoke.fanciticon.data.model.LanguageFontSizes
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.PushMessage
import com.wishpoke.fanciticon.data.model.chat.ChatRoom
import com.wishpoke.fanciticon.data.model.eventbus.ScrollTop
import com.wishpoke.fanciticon.data.model.eventbus.TabChange
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.presenation.base.InssaFragment
import com.wishpoke.fanciticon.presenation.controller.AppLinkController.handle
import com.wishpoke.fanciticon.presenation.resource.MissionBannerModule
import com.wishpoke.fanciticon.presenation.resource.SingleResponse
import com.wishpoke.fanciticon.presenation.view.chat.ChatActivity
import com.wishpoke.fanciticon.presenation.view.chat.EnterChatActivity
import com.wishpoke.fanciticon.presenation.view.general.SettingsActivity
import com.wishpoke.fanciticon.presenation.view.login.LoginActivity
import com.wishpoke.fanciticon.presenation.view.login.LoginViewModel
import com.wishpoke.fanciticon.presenation.view.main.CategoryAdapter.OnClickCategoryListener
import com.wishpoke.fanciticon.presenation.view.pung.PungActivity
import com.wishpoke.fanciticon.presenation.view.pung.PungViewModel
import com.wishpoke.fanciticon.util.*
import com.wishpoke.fanciticon.util.UDTextUtils.Companion.isEmpty
import com.wishpoke.fanciticon.util.constants.FcmActionTypes
import com.wishpoke.fanciticon.util.constants.PrefKeys
import com.wishpoke.fanciticon.util.extention.afterMeasured
import com.wishpoke.fanciticon.util.extention.createDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.include_action_bar.*
import org.greenrobot.eventbus.EventBus
import org.koin.android.viewmodel.ext.android.viewModel
import java.lang.ref.WeakReference
import java.util.*

class MainActivity : BaseActivity() {

    var chatRoomId = ""
    var useButtonTime = 0
    var backPressFlag = false

    private lateinit var adapter: CategoryAdapter
    private lateinit var pagerAdapter: ViewPagerAdapter
    private lateinit var billingModule: BillingModule

    private var copiedEmoji: String = ""
    private var shouldServiceStart = true
    private var pagerScrolled = false
    private var language = ""
    private var scrollState: Int = 0
    private var lastTitleVisible = 0L
    private var titleHeight: Int = 0
    private var initialTabPosition = 0
    private var chatIndex = -1
    private var isCallFromPushMessage = false
    private var isCallFromAppLink = false
    private var isTitleVisible = false

    private val pungViewModel: PungViewModel by viewModel()
    private val loginViewModel: LoginViewModel by viewModel()

    companion object {
        var isChatTab = false
        var isMissionTabVisible = false
        const val MISSION_TAB_POSITION = 4
        const val MARKET_TAB_POSITION = 5
    }


    private class EmojiUseHandler(activity: MainActivity) : Handler() {
        private val activityRef: WeakReference<MainActivity> = WeakReference(activity)
        override fun handleMessage(msg: Message) {
            val activity = activityRef.get()
            if (activity != null) {
            }
        }

    }

    private class BackPressHandler(activity: MainActivity) : Handler() {
        private val activityRef: WeakReference<MainActivity> = WeakReference(activity)
        override fun handleMessage(msg: Message) {
            val activity = activityRef.get()
            if (activity != null) {
                if (msg.what == 0) {
                    activity.backPressFlag = false
                }
            }
        }
    }

    class EmojiUseRunnable(activity: MainActivity) : Runnable {
        private val activityRef: WeakReference<MainActivity> = WeakReference(activity)
        override fun run() {
            val activity = activityRef.get()
            if (activity != null) {
                val isVisible = activity.btn_use_emoji.tag as Boolean
                if (activity.useButtonTime > 0) {
                    activity.useButtonTime -= 1000
                    if (!isVisible) activity.showButtonUseEmoji()
                    activity.emojiUseHandler.postDelayed(this, 1000)
                } else {
                    activity.hideButtonUseEmoji()
                    activity.emojiUseHandler.removeCallbacks(activity.emojiUseRunnable)
                }
            }
        }
    }

    var emojiUseRunnable = EmojiUseRunnable(this)
    private val emojiUseHandler = EmojiUseHandler(this)
    private val backPressHandler = BackPressHandler(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        billingModule = BillingModule(this)
        adLoader = FacebookAdLoader(this, banner_container, ll_isn, wv_edge)
        init()

        initialTabPosition = intent.getIntExtra("initialTabPosition", 0)
        mMainViewModel.parseUpdateCategories()

        var shouldUpdateLanguage = false
        if (savedInstanceState != null) {
            language = savedInstanceState.getString("language")!!
            if (language != Locale.getDefault().language) {
                shouldUpdateLanguage = true
            }
        }

        setActionBarTitle()
        setCategoryTabView()
        setViewPager()

        hideButtonUseEmoji()
        subscribeObservers()
        iv_top.isEnabled = false

        if (shouldUpdateLanguage) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }

        if (TestUtil.isDebugMode()) {
            Logger.e("Test", "userId: " + storage.userId)
            Logger.e("Test", "Password: " + storage.getString(PrefKeys.PASSWORD))
            Logger.e("Test", "Stamp: " + storage.getLong(PrefKeys.STAMP_TIME))
            Logger.e("Test", "Auth: " + storage.getString(PrefKeys.AUTHORIZATION))
        }


        chatRoomId = handle(intent) ?: ""
        if (!isEmpty(chatRoomId)) {
            val position = mMainViewModel.getCategories().indexOf(Category("chat"))
            if (position != -1) {
                pager.currentItem = position
                mMainViewModel.getChatRoomDetail(chatRoomId)
                isCallFromAppLink = true
            }
        }

        // 푸시 메세지 받기
        if (TestUtil.isDebugMode()) {
            val fcmToken = storage.getString(PrefKeys.FCM_TOKEN, "")
            Logger.e("Test", "fcm: $fcmToken")
        }

        chatIndex = mMainViewModel.getCategories().indexOf(Category("chat"))
        val pushMessageRaw = intent.getStringExtra("pushMessage")
        if (!isEmpty(pushMessageRaw)) {
            val pushMessage = Gson().fromJson(pushMessageRaw, PushMessage::class.java)
            if (pushMessage.actionType == FcmActionTypes.PUNG_REPLY) {
                val postNo = Util.parseInt(pushMessage.execution)
                // 인싸펑 상세 열기
                pungViewModel.getPostDetail(postNo)
            } else {
                // 채팅 관련
                pushMessage.execution?.let { roomId ->
                    mMainViewModel.getChatRoomDetail(roomId)
                    isCallFromPushMessage = true
                }
            }

        }

        // 로그인 되어있다면 토큰 갱신
        if (storage.isLoggedIn) {
            loginViewModel.getAuthToken(
                    storage.userId,
                    storage.getString(PrefKeys.PASSWORD) ?: "",
                    object: SingleResponse<Any>() {
                        override fun onSuccess(t: Any) {
                            val token = t as String
                            storage.put(PrefKeys.AUTHORIZATION, token)
                        }
                    }
            )
        }

        setClickListeners()
    }

    private fun setClickListeners() {
        iv_top.setOnClickListener {
            // 위로 올리기
            val position = pager.currentItem
            it.startAnimation(Animator.getClickAnimation(mActivity))
            val category = mMainViewModel.getCategories()[position]
            EventBus.getDefault().post(ScrollTop(category.id))
        }

        btn_menu.setOnClickListener {
            // 플로팅 버튼
            val intent = Intent(this, MenuActivity::class.java)
            intent.putExtra("position", pager.currentItem)
            startActivityForResult(intent, MenuActivity.REQ_MENU)
            Animator.slideUp(this)
        }

        btn_settings.setOnClickListener {
            // 세팅
            it.startAnimation(Animator.getClickAnimation(mActivity))
            startActivity(Intent(mActivity, SettingsActivity::class.java))
        }

        btn_use_emoji.setOnClickListener {
            // 이모티콘 바로쓰기 공유
            it.startAnimation(Animator.getClickAnimation(mActivity))
            hideButtonUseEmoji()
            // Share
            intent = Intent()
            intent.action = Intent.ACTION_SEND
            intent.type = "text/plain"

            intent.putExtra(Intent.EXTRA_TEXT, copiedEmoji)
            val chooser = Intent.createChooser(intent, getString(R.string.app_name))
            startActivity(chooser)
        }
    }

    private fun subscribeObservers() {
        mMainViewModel.chatRoom.observe(this, Observer {
            if (it.status == LoadingState.Status.SUCCESS) {
                val chatRoom: ChatRoom = it.get()
                if (isCallFromAppLink) {
                    isCallFromAppLink = false
                    val pungTime = Moment.getPungTimeToLong(chatRoom.createTime)
                    if (pungTime > 0) {
                        // 이미 회원이거나 방장일경우 바로 입장
                        if (storage.isLoggedIn) {
                            EventBusManager.getInstance().cast(chatRoom)
                            if (chatRoom.joined()) {
                                goTo(mActivity, ChatActivity::class.java, false, false)
                            } else {
                                goTo(mActivity, EnterChatActivity::class.java, false, false)
                            }
                        } else {
                            goTo(mActivity, LoginActivity::class.java, false, false)
                        }
                    } else {
                        mDialog = createDialog(message = R.string.dialog_chat_room_expired).apply { show() }
                    }
                }

                if (isCallFromPushMessage) {
                    isCallFromPushMessage = false
                    if (chatRoom.joined()) {
                        EventBusManager.getInstance().cast(chatRoom)
                        goTo(mActivity, ChatActivity::class.java, false, false)
                    } else {
                        if (chatIndex != -1) {
                            pager.currentItem = chatIndex
                        }
                    }
                }
            }
        })
        pungViewModel.post.observe(this, Observer {
            if (it.isSuccessful()) {
                val intent = Intent(mActivity, PungActivity::class.java)
                intent.putExtra("post", Gson().toJson(it.data!!))
                startActivityForResult(intent, PungActivity.REQUEST_VIEW)
            }
        })
    }

    private fun setActionBarTitle() {
        // 액션바 글자 크기변경
        val fontSizes = LanguageFontSizes()
        language = Locale.getDefault().language
        var fontSize = 28
        if (fontSizes.containsKey(language)) {
            fontSize = fontSizes[language]!!
        }
        tv_actionbar_title.textSize = fontSize.toFloat()

        tv_actionbar_title.afterMeasured {
            val titleWidth: Int = tv_actionbar_title.width
            val dp30 = ImageResolver.convertDpToPixel(30f, mContext)
            if (titleWidth + dp30 > ImageResolver.getDisplaySize(mActivity).x - dp30 * 2 && language == "ko") {
                tv_actionbar_title.text = getString(R.string.intro_subtitle_new_line)
            }

            // 헤더 길이 재기
            rl_title.afterMeasured {
                titleHeight = rl_title.height + ImageResolver.convertDpToPixel(30f, mContext)
            }
        }
    }

    private fun setCategoryTabView() {
        val horizontalLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rv_category.layoutManager = horizontalLayoutManager
        rv_category.setHasFixedSize(true)
        rv_category.itemAnimator?.changeDuration = 0
        adapter = CategoryAdapter(mActivity, object : OnClickCategoryListener {
            override fun onClick(v: View) {
                val position = v.tag as Int
                if (storage.isClickedUpdateCategory(mMainViewModel.getCategories()[position].id)) {
                    adapter.update()
                }
                if (pagerAdapter.getItem(position) is InssaFragment) {
                    (pagerAdapter.getItem(position) as InssaFragment).scrollTop()
                }
                pager.setCurrentItem(position, true)
                // 카테고리 N번 클릭시 광고 노출
                if (position >= mMainViewModel.emojiCategoryStart) {
                    if (storage.countTab()) {
                        adLoader?.showInterstitial()
                    }
                }
            }
        })
        adapter.setList(mMainViewModel.getCategories())
        rv_category.adapter = adapter
    }

    private fun setViewPager() {
        pagerAdapter = ViewPagerAdapter(supportFragmentManager, mMainViewModel.getCategories())
        pager.adapter = pagerAdapter
        pager.offscreenPageLimit = 1
        pager.addOnPageChangeListener(object : SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                isMissionTabVisible = Util.isKorean() && position == MISSION_TAB_POSITION
                EventBus.getDefault().post(TabChange(position))
                adapter.setCurrentItem(position)
                rv_category.smoothScrollToPosition(position)
                isChatTab = chatIndex > -1 && position == chatIndex
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels)
            }

            override fun onPageScrollStateChanged(state: Int) {
                super.onPageScrollStateChanged(state)
                // 초기화
                if (state == 0) {
                    scrollState = 0
                } else {
                    scrollState += state
                    pagerScrolled = scrollState == 3
                }
                // - 스크롤: 1-2-0
                // - 클릭: 2-0
            }
        })

        val categoryIndex = intent.getIntExtra("categoryIndex", 0)
        if (categoryIndex > 0) {
            pager.currentItem = categoryIndex
        }

        // Intent에서 받은 원하는 탭으로 이동
        if (initialTabPosition != 0) {
            pager.currentItem = initialTabPosition
        }
    }

    private fun init() {

    }

    fun showButtonUseEmoji(emoji: String) {
        this.copiedEmoji = emoji
        useButtonTime += 3000
        emojiUseHandler.postDelayed(emojiUseRunnable, 100)
    }

    private fun showButtonUseEmoji() {
        btn_use_emoji.tag = true
        btn_use_emoji.animate().translationY(ImageResolver.convertDpToPixel(125f, this) * -1.toFloat()).setInterpolator(AccelerateInterpolator()).setDuration(400).start()
    }

    private fun hideButtonUseEmoji() {
        btn_use_emoji.tag = false
        btn_use_emoji.animate().translationY(ImageResolver.convertDpToPixel(125f, this).toFloat()).setInterpolator(AccelerateInterpolator()).setDuration(400).start()
        val params = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        params.setMargins(ImageResolver.convertDpToPixel(24f, this), 0,
                ImageResolver.convertDpToPixel(94f, this), ImageResolver.convertDpToPixel(110f, this) * -1)
        params.addRule(RelativeLayout.ABOVE, R.id.ll_ad)
        btn_use_emoji.layoutParams = params
    }

    fun showTitle() {
        if (!isTitleVisible) {
            val current = Date().time
            if (current - lastTitleVisible > 1000) {
                lastTitleVisible = current
                isTitleVisible = true
                val params = action_bar.layoutParams as RelativeLayout.LayoutParams
                val animator = ValueAnimator.ofInt(params.topMargin, 0)
                animator.addUpdateListener { valueAnimator ->
                    params.topMargin = (valueAnimator.animatedValue as Int)
                    action_bar.requestLayout()
                }
                animator.duration = 300
                animator.start()
                iv_top.animate().alpha(0f).duration = 300
                iv_top.visibility = View.GONE
                iv_top.isEnabled = false
            }
        }
    }

    fun hideTitle() {
        if (isTitleVisible) {
            val current = Date().time
            if (current - lastTitleVisible > 1000) {
                isTitleVisible = false
                val params = action_bar.layoutParams as RelativeLayout.LayoutParams
                val animator = ValueAnimator.ofInt(params.topMargin, -titleHeight)
                animator.addUpdateListener { valueAnimator: ValueAnimator ->
                    params.topMargin = (valueAnimator.animatedValue as Int)
                    action_bar.requestLayout()
                }
                animator.duration = 300
                animator.start()
                iv_top.animate().alpha(1f).duration = 300
                iv_top.visibility = View.VISIBLE
                iv_top.isEnabled = true
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("language", language)
    }

    override fun onBackPressed() {
        // 백 키를 터치한 경우
        if (!backPressFlag) {
            backPressFlag = true
            backPressHandler.sendEmptyMessageDelayed(0, 2000) // 2초 내로 터치시
            pager.setCurrentItem(0, true)
        } else {
            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MenuActivity.REQ_MENU && resultCode == Activity.RESULT_OK) {
            val position = data?.getIntExtra("position", 0) ?: 0
            pager.currentItem = position
            // 카테고리 N번 클릭시 광고 노출
            if (position >= mMainViewModel.emojiCategoryStart) {
                if (storage.countTab()) {
                    adLoader?.showInterstitial()
                }
            }
        }
    }

    override fun onResume() {
        shouldServiceStart = true
        billingModule.onResume()
        adLoader?.update()
        super.onResume()
    }

    override fun onAppBackground() {
        super.onAppBackground()
        if (shouldServiceStart) {
            ServiceManager.startService(this, pager.currentItem)
            shouldServiceStart = false
        }
    }

    fun showInterstitial() = adLoader?.showInterstitial()
}