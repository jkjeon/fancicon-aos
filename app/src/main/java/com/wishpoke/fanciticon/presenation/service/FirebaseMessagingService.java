package com.wishpoke.fanciticon.presenation.service;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.wishpoke.fanciticon.R;
import com.wishpoke.fanciticon.data.model.PushMessage;
import com.wishpoke.fanciticon.presenation.resource.AppStorage;
import com.wishpoke.fanciticon.presenation.view.general.SplashActivity;
import com.wishpoke.fanciticon.presenation.view.main.MainActivity;
import com.wishpoke.fanciticon.util.Logger;
import com.wishpoke.fanciticon.util.TestUtil;
import com.wishpoke.fanciticon.util.Util;

import java.util.List;

public class FirebaseMessagingService  extends com.google.firebase.messaging.FirebaseMessagingService {
    private static final String TAG = "FirebaseMsgService";

    private String msg;
    private String title;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        AppStorage storage = new AppStorage(getApplicationContext());
        boolean isChatActivityForeground = storage.getBoolean("ChatActivity");

        title = remoteMessage.getData().get("title");
        msg = remoteMessage.getData().get("body");

        if (TestUtil.isDebugMode()) {
            Logger.e("Test", "PushMessage: " + isChatActivityForeground + " " + new Gson().toJson(remoteMessage.getData()));
        }

        String execution = remoteMessage.getData().get("execution");
        int badge = Util.parseInt(remoteMessage.getData().get("badge"));
        int actionType = Util.parseInt(remoteMessage.getData().get("actionType"));

        PushMessage pushMessage = new PushMessage(execution, badge, actionType, title, msg);


        Intent intent = new Intent(this,
                applicationInForeground() ? MainActivity.class
                : SplashActivity.class);
        intent.putExtra("pushMessage", new Gson().toJson(pushMessage));

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        Uri soundUri = Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.zin);

        if (!isChatActivityForeground) {
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentTitle(title)
                    .setContentText(msg)
                    .setAutoCancel(true)
                    .setSound(soundUri)
                    .setNumber(badge)
                    .setContentIntent(contentIntent)
                    .setChannelId(getString(R.string.fcm_default_channel_id))
                    .setDefaults(0);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
                AudioAttributes audioAttributes = new AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .setUsage(AudioAttributes.USAGE_ALARM)
                        .build();

                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new
                        NotificationChannel(getString(R.string.fcm_default_channel_id),
                        getString(R.string.app_name), importance);

                mChannel.enableLights(true);
                mChannel.setLightColor(ContextCompat.getColor
                        (getApplicationContext(), R.color.colorPrimary));
                mChannel.setSound(soundUri, audioAttributes);
                notificationManager.createNotificationChannel(mChannel);
            }

            notificationManager.notify(0, mBuilder.build());
        }

        /*
        if (!UDTextUtils.isEmpty(execution) && MainActivity.isForeground && !MainActivity.isChatTab) {
            Intent intent = new Intent("chat");
            intent.putExtra("pushMessage", new Gson().toJson(pushMessage));
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        }
        */
    }

    private boolean applicationInForeground() {
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> services = activityManager.getRunningAppProcesses();
        boolean isActivityFound = false;

        if (services.get(0).processName
                .equalsIgnoreCase(getPackageName()) && services.get(0).importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
            isActivityFound = true;
        }
        return isActivityFound;
    }
}
