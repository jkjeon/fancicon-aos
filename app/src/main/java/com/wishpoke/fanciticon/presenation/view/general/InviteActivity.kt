package com.wishpoke.fanciticon.presenation.view.general

import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.view.View
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.User
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.util.UDTextUtils
import com.wishpoke.fanciticon.util.constants.PrefKeys

class InviteActivity : BaseActivity() {

    @BindView(R.id.tv_share)
    lateinit var tvShare: TextView
    @BindView(R.id.tv_sub_desc)
    lateinit var tvSubDesc: TextView
    @BindView(R.id.btn_login)
    lateinit var btnLogin: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invite)
        ButterKnife.bind(this)

        init()
        updateLoginStateView(btnLogin)
    }

    private fun init() {
        val sentence = getString(R.string.invite_desc)
        val strong = getString(R.string.app_name)

        val spannable = SpannableString(sentence)
        spannable.setSpan(ForegroundColorSpan(resources.getColor(R.color.purple)),
                UDTextUtils.getStartOf(sentence, strong),
                UDTextUtils.getEndOf(sentence, strong), SpannableString.SPAN_EXCLUSIVE_INCLUSIVE)

        spannable.setSpan(UnderlineSpan(),
                UDTextUtils.getStartOf(sentence, strong),
                UDTextUtils.getEndOf(sentence, strong), SpannableString.SPAN_EXCLUSIVE_INCLUSIVE)

        tvSubDesc.text = spannable

        val user: User? = storage.getObject(PrefKeys.USER, User::class.java)
        tvShare.text = user?.referralCode
    }

    @OnClick(R.id.btn_share)
    public fun onClick(v: View) {
        when (v.id) {
            R.id.btn_share -> {
                val referralCode = tvShare.text?.trim().toString()
                val shareText = String.format(getString(R.string.share_referral), referralCode)

                val intent = Intent()
                intent.action = Intent.ACTION_SEND
                intent.type = "text/plain"
                intent.putExtra(Intent.EXTRA_TEXT, shareText)
                val chooser = Intent.createChooser(intent, getString(R.string.app_name))
                startActivity(chooser)
            }
        }
    }
}