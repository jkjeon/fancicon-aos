package com.wishpoke.fanciticon.presenation.resource;

import com.wishpoke.fanciticon.data.model.AppThrowable;
import com.wishpoke.fanciticon.data.model.response.ObjectResponse;
import com.wishpoke.fanciticon.util.Logger;
import com.wishpoke.fanciticon.util.constants.Errors;

import io.reactivex.observers.DisposableObserver;

public abstract class CallbackWrapper<T> extends DisposableObserver<T> {
    public CallbackWrapper() {

    }

    protected abstract void onSuccess(T t);

    protected abstract void onFailure(Throwable e, int code);

    @Override
    public void onNext(T t) {
        if (t instanceof ObjectResponse) {
            ObjectResponse objectResponse = (ObjectResponse) t;
            if (objectResponse.isSuccessful()) {
                onSuccess(t);
            } else {
                onError(new AppThrowable(objectResponse.status.code, objectResponse.status.msg));
            }
        } else {
            onSuccess(t);
        }
    }

    @Override
    public void onError(Throwable e) {
        int errorCode = Errors.NETWORK;
        Logger.e("Test", "[CallbackWrapper] Failed: " + e.getMessage());
        if (e instanceof AppThrowable) {
            errorCode = ((AppThrowable) e).getCode();
        }
        onFailure(e, errorCode);
    }

    @Override
    public void onComplete() {

    }
}
