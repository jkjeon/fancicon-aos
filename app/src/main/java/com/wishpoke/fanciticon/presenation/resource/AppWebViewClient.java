package com.wishpoke.fanciticon.presenation.resource;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.wishpoke.fanciticon.util.UDTextUtils;

import java.net.URISyntaxException;

public class AppWebViewClient extends WebViewClient {
    private Context context;
    private String initialUrl;

    public AppWebViewClient(Context context) {
        this.context = context;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        if (initialUrl == null) {
            initialUrl = url;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager.getInstance().sync();
        }
        super.onPageFinished(view, url);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.startsWith("https://") || url.startsWith("http://")) {
            return false;
        } else if (url.startsWith("tel:")) {
            Intent tel = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
            context.startActivity(tel);
            return true;
        } else if (url.startsWith("sms:")) {
            Intent i = new Intent(Intent.ACTION_SENDTO, Uri.parse(url));
            context.startActivity(i);
            return true;
        } else if (url.startsWith("market://")) {
            try {
                Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                if (intent != null) {
                    context.startActivity(intent);
                }
                return true;
            } catch (URISyntaxException e) {
                e.printStackTrace();
                android.util.Log.d("test", "err: " + e.getMessage());
            }
        } else if (url.startsWith("intent:") || url.contains("://")) {
            try {
                Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                String fallbackUrl = intent.getStringExtra("browser_fallback_url");
                Intent existPackage = context.getPackageManager().getLaunchIntentForPackage(intent.getPackage());
                if (!UDTextUtils.isEmpty(fallbackUrl)) {
                    view.loadUrl(fallbackUrl);
                } else if (existPackage != null) {
                    context.startActivity(intent);
                } else {
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW);
                    marketIntent.setData(Uri.parse("market://details?id=" + intent.getPackage()));
                    context.startActivity(marketIntent);
                }
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                android.util.Log.d("test", "err: " + e.getMessage());
            }
        }
        return false;
    }
}
