package com.wishpoke.fanciticon.presenation.view.login

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.response.DefaultResponse
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.presenation.resource.SingleResponse
import com.wishpoke.fanciticon.presenation.resource.ui.FormEditText
import com.wishpoke.fanciticon.util.DialogManager
import com.wishpoke.fanciticon.util.Toaster
import com.wishpoke.fanciticon.util.Validator
import com.wishpoke.fanciticon.util.constants.Errors
import com.wishpoke.fanciticon.util.constants.PrefKeys
import com.wishpoke.fanciticon.util.extention.createDialog
import org.koin.android.viewmodel.ext.android.viewModel

class LeaveActivity : BaseActivity() {

    @BindView(R.id.et_pw)
    lateinit var etPw: FormEditText
    @BindView(R.id.iv_visible)
    lateinit var ivVisible: ImageView

    private val loginViewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leave)
        ButterKnife.bind(this)
        init()
    }

    private fun init() {
        etPw.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(password: CharSequence?, p1: Int, p2: Int, p3: Int) {
                etPw.isValid = Validator.isValidPassword(password.toString())
            }
        })
        etPw.setPasswordVisibleButton(ivVisible)
    }

    @OnClick(R.id.btn_back, R.id.btn_submit)
    fun onClick(v: View) {
        when (v.id) {
            R.id.btn_back -> onBackPressed()
            R.id.btn_submit -> {
                mDialog = createDialog(
                        message = R.string.dialog_leave,
                        negativeLabel = R.string.cancel,
                        positiveBlock = {
                            // 탈퇴 진행
                            mDialog = DialogManager.getInstance().createLoadingDialog(mContext)
                            mDialog?.show()
                            val password = etPw.text?.trim().toString()
                            loginViewModel.leave(password, object: SingleResponse<DefaultResponse>() {
                                override fun onSuccess(t: DefaultResponse) {
                                    // 탈퇴 완료
                                    mDialog?.dismiss()
                                    storage.put(PrefKeys.USER, null)
                                    Toaster.show(mContext, R.string.toast_process_complete)
                                    finish()
                                }

                                override fun onFailure(e: Throwable, code: Int) {
                                    mDialog?.dismiss()
                                    when (code) {
                                        Errors.PASSWORDS_DO_NOT_MATCH -> Toaster.show(mContext, R.string.error_password)
                                        else -> Toaster.show(mContext, R.string.error_unknown, code)
                                    }

                                }
                            })
                        }
                ).apply { show() }
            }
        }
    }
}