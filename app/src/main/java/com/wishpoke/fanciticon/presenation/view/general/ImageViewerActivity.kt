package com.wishpoke.fanciticon.presenation.view.general

import android.media.MediaScannerConnection
import android.os.Bundle
import android.os.Environment
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.presenation.resource.SingleResponse
import com.wishpoke.fanciticon.util.DialogManager
import com.wishpoke.fanciticon.util.Toaster
import kotlinx.android.synthetic.main.activity_image_viewer.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList


class ImageViewerActivity : BaseActivity() {
    private lateinit var pagerAdapter: ImageViewerAdapter
    private var items: java.util.ArrayList<String> = ArrayList()
    private var index = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_viewer)

        items = intent.getStringArrayListExtra(KEY)
        index = intent.getIntExtra(INITIAL_INDEX, 0)
        init()
        setClickListeners()
    }

    private fun init() {
        pagerAdapter = ImageViewerAdapter(mActivity)
        pagerAdapter.setList(items)
        pager.adapter = pagerAdapter
        pager.setOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                updateCounter()
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
        pager.currentItem = index
        updateCounter()
    }

    private fun updateCounter() {
        val label = (pager.currentItem + 1).toString() + " / " + items.size
        tv_count.text = label
    }

    private fun setClickListeners() {
        btn_back.setOnClickListener { onBackPressed() }
        btn_download.setOnClickListener {
            mDialog = DialogManager.getInstance().createLoadingDialog(mContext)
            mDialog?.show()
            val fileName = pagerAdapter.getItemAt(pager.currentItem)
            val saveFileName = "inssaticon_${(Date().time / 1000)}"
            mMainViewModel.downloadFile(fileName, saveFileName, object: SingleResponse<File>() {
                override fun onSuccess(t: File) {
                    super.onSuccess(t)
                    mDialog?.dismiss()
                    val savePath = t.path.replace(Environment.getExternalStorageDirectory().toString(),  "")
                    // 갤러리 뜰수있도록 저장되었다는걸 알림
                    MediaScannerConnection.scanFile(applicationContext, arrayOf(t.path), arrayOf("images/*"), null)
                    Toaster.show(mContext, String.format(getString(R.string.toast_file_save_completed), savePath))
                }

                override fun onFailure(throwable: Throwable, code: Int) {
                    super.onFailure(throwable, code)
                    mDialog?.dismiss()
                    Toaster.show(mContext, R.string.error_file_save_failed, code)
                }
            })
        }
    }

    companion object {
        const val KEY = "images"
        const val INITIAL_INDEX = "initial_index"
    }
}