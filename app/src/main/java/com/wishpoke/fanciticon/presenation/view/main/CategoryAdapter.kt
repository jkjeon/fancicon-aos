package com.wishpoke.fanciticon.presenation.view.main

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.Category
import com.wishpoke.fanciticon.presenation.base.BaseRecyclerViewAdapter
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import com.wishpoke.fanciticon.util.Animator
import com.wishpoke.fanciticon.util.ImageResolver
import com.wishpoke.fanciticon.util.TestUtil
import com.wishpoke.fanciticon.util.Util
import com.wishpoke.fanciticon.util.extention.dp2px

class CategoryAdapter(private val context: Context, private val listener: OnClickCategoryListener) : BaseRecyclerViewAdapter<CategoryAdapter.ViewHolder, Category>() {
    private var isMenu = false
    private val storage: AppStorage = AppStorage(context)
    private var updateCategories: List<String>? = null
    private var active = 0 // AdapterPosition 기준

    interface OnClickCategoryListener : View.OnClickListener

    fun update() {
        updateCategories = storage.updateCategories
        notifyDataSetChanged()
    }

    fun isMenu(flag: Boolean) {
        isMenu = flag
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_category, parent, false)
        val holder = ViewHolder(itemView)
        holder.view.setOnClickListener(listener)
        return holder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var isImageCategory = true
        val category = getItemAt(position)
        if (updateCategories?.contains(category.id) == true) {
            holder.viewNew.visibility = View.VISIBLE
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.viewNew.outlineProvider = null
            }
        } else {
            holder.viewNew.visibility = View.INVISIBLE
        }
        when (val categoryId = category.id) {
            "favorite" -> {
                setImageCategory(holder)
                holder.ivCategory.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_white))
            }
            "pung", "chat" -> {
                val resId = context.resources.getIdentifier("ic_$categoryId", "mipmap", context.packageName)
                setImageCategory(holder)
                holder.ivCategory.setImageDrawable(ContextCompat.getDrawable(context, resId))
            }
            "custom" -> {
                setImageCategory(holder)
                holder.ivCategory.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_add))
            }
            else -> {
                isImageCategory = false
                setTextCategory(holder)
                holder.tvCategory.text = category.name
            }
        }
        val params = holder.ivCategory.layoutParams as FrameLayout.LayoutParams
        params.width = context.dp2px(17f)
        params.height = context.dp2px(17f)
        holder.ivCategory.layoutParams = params

        val backgroundDrawable: Int
        val activeColor: Int
        if (position == active) {
            holder.btnCategory.startAnimation(Animator.getClickAnimation(context))
            backgroundDrawable = if (isImageCategory) R.drawable.button_category_active else R.drawable.button_outline_category_active
            activeColor = if (isImageCategory) R.color.white else R.color.purple
        } else {
            backgroundDrawable = if (isImageCategory) R.drawable.button_category else R.drawable.button_outline_category
            activeColor = if (isImageCategory) R.color.white else R.color.lightPurple
        }
        holder.btnCategory.background = ContextCompat.getDrawable(context, backgroundDrawable)
        holder.tvCategory.setTextColor(ContextCompat.getColor(context, activeColor))
        holder.ivCategory.setColorFilter(ContextCompat.getColor(context, activeColor))
        holder.btnCategory.layoutParams = getItemLayoutParams(position)
        holder.view.tag = getRealPosition(position)
        val padding = if (isMenu) ImageResolver.convertDpToPixel(4f, context) else 0
        holder.wrapper.setPadding(padding, padding, padding, padding)
    }

    private fun setImageCategory(holder: ViewHolder) {
        holder.tvCategory.text = ""
        holder.ivCategory.visibility = View.VISIBLE
        holder.tvCategory.visibility = View.INVISIBLE
    }

    private fun setTextCategory(holder: ViewHolder) {
        holder.tvCategory.visibility = View.VISIBLE
        holder.ivCategory.visibility = View.GONE
    }

    // 리스트 간격 설정
    private fun getItemLayoutParams(position: Int): RelativeLayout.LayoutParams {
        val params = RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT)
        val dp5 = ImageResolver.convertDpToPixel(5f, context)
        val dp4 = ImageResolver.convertDpToPixel(4f, context)
        if (!isMenu) {
            if (position == 0) {
                params.setMargins(dp5 * 6, dp4, dp5, dp4)
            } else if (position == itemCount - 1) {
                params.setMargins(dp5, dp4, dp5 * 6, dp4)
            } else {
                params.setMargins(dp5, dp4, dp5, dp4)
            }
        } else {
            params.setMargins(0, dp4, dp4, 0)
        }
        return params
    }

    // 탭 활성화
    fun setCurrentItem(position: Int) {
        active = getAdapterPosition(position)
        notifyDataSetChanged()
    }

    inner class ViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        @BindView(R.id.btn_category)
        lateinit var btnCategory: CardView
        @BindView(R.id.tv_category)
        lateinit var tvCategory: TextView
        @BindView(R.id.iv_category)
        lateinit var ivCategory: ImageView
        @BindView(R.id.wrapper)
        lateinit var wrapper: RelativeLayout
        @BindView(R.id.view_new)
        lateinit var viewNew: LinearLayout

        init {
            ButterKnife.bind(this, view)
        }
    }

    init {
        updateCategories = storage.updateCategories
    }
}