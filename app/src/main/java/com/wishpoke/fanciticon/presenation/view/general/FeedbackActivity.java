package com.wishpoke.fanciticon.presenation.view.general;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.wishpoke.fanciticon.R;
import com.wishpoke.fanciticon.presenation.base.BaseActivity;
import com.wishpoke.fanciticon.util.Animator;
import com.wishpoke.fanciticon.util.FacebookAdLoader;
import com.wishpoke.fanciticon.util.constants.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedbackActivity extends BaseActivity {

    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.btn_back)
    ImageView btnBack;
    @BindView(R.id.ll_isn)
    LinearLayout isnContainer;
    @BindView(R.id.wv_edge)
    WebView isnWebView;
    @BindView(R.id.banner_container)
    LinearLayout bannerContainer;

    private String url;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        ButterKnife.bind(this);
        url = Constants.Url.FEEDBACK;
        adLoader = new FacebookAdLoader(this, bannerContainer, isnContainer, isnWebView);
        setWebView();

        btnBack.setOnClickListener(v -> {
            v.startAnimation(Animator.getClickAnimation(mContext));
            onBackPressed();
        });
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setWebView() {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(url);
    }
}

