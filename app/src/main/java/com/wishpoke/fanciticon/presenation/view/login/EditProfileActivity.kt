package com.wishpoke.fanciticon.presenation.view.login

import android.os.Bundle
import androidx.lifecycle.Observer
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.User
import com.wishpoke.fanciticon.data.model.response.DefaultResponse
import com.wishpoke.fanciticon.databinding.ActivityEditProfileBinding
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.presenation.resource.SingleResponse
import com.wishpoke.fanciticon.util.DialogManager
import com.wishpoke.fanciticon.util.FacebookAdLoader
import com.wishpoke.fanciticon.util.Toaster
import com.wishpoke.fanciticon.util.Validator
import com.wishpoke.fanciticon.util.constants.Errors
import com.wishpoke.fanciticon.util.constants.PrefKeys
import com.wishpoke.fanciticon.util.extention.onTextChanged
import kotlinx.android.synthetic.main.activity_edit_profile.*
import org.koin.android.viewmodel.ext.android.viewModel

class EditProfileActivity : BaseActivity() {

    private val loginViewModel: LoginViewModel by viewModel()
    private var user: User? = null
    private lateinit var binding: ActivityEditProfileBinding

    private var updateId: String? = null
    private var updatePassword: String? = null
    private var updateNickname: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditProfileBinding.bind(layoutInflater.inflate(R.layout.activity_edit_profile, null))
        setContentView(binding.root)

        adLoader = FacebookAdLoader(this, banner_container, ll_isn, wv_edge)

        user = storage.getObject(PrefKeys.USER, User::class.java)

        init()
        setClickListeners()
        subscribeObservers()
    }

    private fun init() {
        binding.etId.setText(user?.userId)
        binding.etNickname.setText(storage.nickname)

        binding.etId.onTextChanged { text, start, count, after ->
            binding.btnIdCheck.isEnabled = text != user?.userId
        }
        binding.etNickname.onTextChanged { text, start, count, after ->
            binding.btnNicknameCheck.isEnabled = text != storage.nickname
        }
    }

    private fun setClickListeners() {
        // ID 중복확인
        binding.btnIdCheck.setOnClickListener {
            val userId = binding.etId.text.toString()
            loginViewModel.checkId(userId, "", object: SingleResponse<DefaultResponse>() {
                override fun onSuccess(t: DefaultResponse) {
                    Toaster.show(mContext, R.string.toast_available_id)
                    binding.btnIdCheck.isEnabled = false
                }

                override fun onFailure(throwable: Throwable, code: Int) {
                    if (code == Errors.EXIST_USER) {
                        binding.btnIdCheck.isEnabled = true
                    }
                }
            })
        }
        // 닉네임 중복확인
        binding.btnNicknameCheck.setOnClickListener {
            val nickname = binding.etNickname.text.toString()
            loginViewModel.checkNickname(nickname)
        }
        // 변경사항 저장
        binding.btnSubmit.setOnClickListener {
            // 닉네임 변경 / 아이디 변경 / 비밀번호 변경
            updateId = binding.etId.text?.trim().toString()
            updateNickname = binding.etNickname.text?.trim().toString()
            updatePassword = binding.etNewPassword.text?.trim().toString()

            if (validate()) {
                loginViewModel.updateProfile(updateId, updatePassword, updateNickname)
            }
        }
    }

    private fun validate(): Boolean {
        if (!Validator.isValidUserId(updateId)) {
            Toaster.show(mContext, R.string.toast_validate_id)
            return false
        }
        if (binding.btnIdCheck.isEnabled) {
            Toaster.show(mContext, R.string.toast_check_id)
            return false
        }
        if (updateNickname?.length ?: 0 < 2) {
            Toaster.show(mContext, R.string.toast_invalid_nickname)
            return false
        }
        if (binding.btnNicknameCheck.isEnabled) {
            Toaster.show(mContext, R.string.toast_check_nickname)
            return false
        }
        if (!Validator.isValidPassword(updatePassword)) {
            Toaster.show(mContext, R.string.error_invalid_password)
            return false
        }
        val originalPassword = storage.getString(PrefKeys.PASSWORD)
        val currentPassword = binding.etCurrentPassword.text?.trim().toString()
        if (originalPassword != currentPassword) {
            Toaster.show(mContext, R.string.toast_check_prev_pw)
            return false
        }
        val newPasswordConfirm = binding.etNewPasswordConfirm.text?.trim().toString()
        if (updatePassword != newPasswordConfirm) {
            Toaster.show(mContext, R.string.toast_confirm_password)
            return false
        }
        return true
    }

    private fun subscribeObservers() {
        loginViewModel.checkNickname.observe(this, Observer {
            if (it.isSuccessful()) {
                binding.btnNicknameCheck.isEnabled = it.data!!.isRegistered == 1
                if (binding.btnNicknameCheck.isEnabled) {
                    Toaster.show(mContext, R.string.error_registered_alias)
                } else {
                    Toaster.show(mContext, R.string.toast_available_nickname)
                }
            }
        })

        loginViewModel.updateProfile.observe(this, Observer {
            when (it.status) {
                LoadingState.Status.LOADING -> {
                    mDialog = DialogManager.getInstance().createLoadingDialog(mContext)
                    mDialog?.show()
                }
                LoadingState.Status.FAILED -> {
                    mDialog?.dismiss()
                    Toaster.show(mContext, R.string.error_unknown, it.throwable?.code)
                }
                LoadingState.Status.SUCCESS -> {
                    mDialog?.dismiss()
                    // save to storage
                    storage.nickname = updateNickname
                    user?.userId = updateId
                    user?.password = updatePassword
                    storage.put(PrefKeys.USER, user)
                    Toaster.show(mContext, R.string.toast_complete)
                    finish()
                }
            }
        })
    }
}