package com.wishpoke.fanciticon.presenation.view.chat

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Vibrator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.airbnb.lottie.LottieAnimationView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.chat.ChatRoom
import com.wishpoke.fanciticon.presenation.base.LoadingRecyclerViewAdapter
import com.wishpoke.fanciticon.util.Animator
import com.wishpoke.fanciticon.util.Logger
import com.wishpoke.fanciticon.util.Moment
import com.wishpoke.fanciticon.util.UDTextUtils
import com.wishpoke.fanciticon.util.constants.Constants
import com.wishpoke.fanciticon.util.extention.isVisibleWhen
import com.wishpoke.fanciticon.util.listener.ItemClick
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

class ChatRoomAdapter(val context: Context, headerItemClick: ItemClick) : LoadingRecyclerViewAdapter<RecyclerView.ViewHolder, ChatRoom>() {

    var orderType = 2
        set(value) {
            this.notifyItemChanged(0)
            field = value
        }
    var boardNo = 0
        set(value) {
            this.notifyItemChanged(0)
            field = value
        }
    companion object {
        var deletedRoomIds: MutableList<String> = ArrayList()
    }

    val orders = arrayOf(
            context.getString(R.string.order_latest),
            context.getString(R.string.order_popular),
            context.getString(R.string.order_count)
    )
    val boards = context.resources.getStringArray(R.array.pung_categories)

    private var popularRoomIds: List<String> = java.util.ArrayList()

    init {
        this.headerItemClick = headerItemClick
    }

    fun setPopularRoomIds(ids: List<String>) {
        this.popularRoomIds = ids
        notifyDataSetChanged()
    }

    override fun addList(list: List<ChatRoom>) {
        var countAdded = 0
        for (item in list) {
            if (!deletedRoomIds.contains(item.roomId)) {
                this.list.add(item)
                countAdded++
            }
        }
        notifyItemRangeInserted(this.list.size, countAdded)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TypeHeader -> {
                val itemView = LayoutInflater.from(context).inflate(R.layout.item_header_chat_room, parent, false)
                HeaderViewHolder(itemView)
            }
            TypeLoading -> onCreateLoadingViewHolder(context)
            else -> {
                val itemView = LayoutInflater.from(context).inflate(R.layout.item_chat_room, parent, false)
                ViewHolder(itemView)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        with(holder) {
            when (this) {
                is HeaderViewHolder -> {
                    tvOrder.text = orders[orderType - 1]
                    tvBoard.text = boards[boardNo]

                    val clickListener = View.OnClickListener {
                        headerItemClick?.onItemClick(it, position)
                    }

                    btnWrite.setOnClickListener(clickListener)
                    btnInfo.setOnClickListener(clickListener)
                    btnBoard.setOnClickListener(clickListener)
                    btnOrder.setOnClickListener(clickListener)
                }
                is LoadingViewHolder -> {
                    onBindLoadingViewHolder(holder)
                }
                is ViewHolder -> {
                    val numberFormat = NumberFormat.getInstance()
                    val chatRoom = getItemAt(position)

                    ivImage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.ic_chat_room_placeholder))
                    Glide.with(context).asBitmap()
                            .load("${Constants.Url.BASE}/fileApi/getFile?downloadFileName=" + chatRoom.fileName)
                            .into(object: CustomTarget<Bitmap>() {
                                override fun onLoadCleared(placeholder: Drawable?) {
                                }
                                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                    ivImage.setImageBitmap(resource)
                                }
                            })
                    tvMaster.text = UDTextUtils.decode(chatRoom.hostStageName)
                    tvCountShare.text = numberFormat.format(chatRoom.shareCnt)
                    tvCountLike.text = numberFormat.format(chatRoom.likeCnt)
                    tvCountMember.text = chatRoom.memberCnt.toString()
                    tvDate.text = Moment.getPungTime(Date(chatRoom.createTime * 1000))
                    tvTitle.text = UDTextUtils.decode(chatRoom.title)
                    ivLocked.isVisibleWhen(chatRoom.isPrivate == 1)
                    tvJoined.isVisibleWhen(chatRoom.joined())

                    // 금/은/동싸 설정
                    val tierImages = arrayOf(R.drawable.ic_gold, R.drawable.ic_silver, R.drawable.ic_bronze)
                    if (!popularRoomIds.isNullOrEmpty()) {
                        val index = popularRoomIds.indexOf(chatRoom.roomId)
                        if (index > -1 && index < tierImages.size && chatRoom.likeCnt > 0) {
                            chatRoom.rank = index + 1
                            Glide.with(context).load(tierImages[index]).into(ivTier)
                        }
                    }
                    ivTier.isVisibleWhen(chatRoom.rank > 0)

                    val clickListener = View.OnClickListener {
                        itemClick?.onItemClick(it, position)
                    }

                    llLike.setOnClickListener { v ->
                        val vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                        vibrator.vibrate(Constants.VibrateTime)
                        avLike.playAnimation()
                        chatRoom.likeCnt += 1
                        tvCountLike.text = numberFormat.format(chatRoom.likeCnt)
                        itemClick?.onItemClick(v, position)
                    }

                    llShare.setOnClickListener { v ->
                        v.startAnimation(Animator.getFavoriteClickAnimation(context))
                        chatRoom.shareCnt += 1
                        tvCountShare.text = numberFormat.format(chatRoom.shareCnt)
                        itemClick?.onItemClick(v, position)
                    }

                    itemView.setOnClickListener(clickListener)
                }
            }
        }
    }

    class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.btn_info)
        lateinit var btnInfo: View
        @BindView(R.id.btn_board)
        lateinit var btnBoard: View
        @BindView(R.id.tv_board)
        lateinit var tvBoard: TextView
        @BindView(R.id.btn_order)
        lateinit var btnOrder: View
        @BindView(R.id.tv_order)
        lateinit var tvOrder: TextView
        @BindView(R.id.btn_write)
        lateinit var btnWrite: View

        init {
            ButterKnife.bind(this, itemView)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.tv_joined)
        lateinit var tvJoined: View
        @BindView(R.id.iv_image)
        lateinit var ivImage: ImageView
        @BindView(R.id.tv_master)
        lateinit var tvMaster: TextView
        @BindView(R.id.tv_date)
        lateinit var tvDate: TextView
        @BindView(R.id.iv_tier)
        lateinit var ivTier: ImageView
        @BindView(R.id.tv_title)
        lateinit var tvTitle: TextView
        @BindView(R.id.tv_count_share)
        lateinit var tvCountShare: TextView
        @BindView(R.id.ll_share)
        lateinit var llShare: View
        @BindView(R.id.ll_like)
        lateinit var llLike: View
        @BindView(R.id.tv_count_member)
        lateinit var tvCountMember: TextView
        @BindView(R.id.iv_locked)
        lateinit var ivLocked: View
        @BindView(R.id.av_like)
        lateinit var avLike: LottieAnimationView
        @BindView(R.id.tv_count_like)
        lateinit var tvCountLike: TextView

        init {
            ButterKnife.bind(this, itemView)
        }
    }
}