package com.wishpoke.fanciticon.presenation.view.chat

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.Category
import com.wishpoke.fanciticon.presenation.base.BaseRecyclerViewAdapter
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import com.wishpoke.fanciticon.util.extention.isVisibleWhen

class ChatEmojiCategoryAdapter(val context: Context) : BaseRecyclerViewAdapter<ChatEmojiCategoryAdapter.ViewHolder, Category>() {

    var activePosition = 0
    private val storage = AppStorage(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.item_chat_emoji_category, null)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with (holder) {
            val activeCategory = getItemAt(activePosition)
            val category = getItemAt(position)

            val isImageCategory = position < 2

            ivCategory.isVisibleWhen(isImageCategory)
            ivCategory.setImageDrawable(ContextCompat.getDrawable(context, if (position == 0) {
                R.drawable.ic_star_white
            } else {
                R.drawable.ic_add
            }))
            tvCategory.isVisibleWhen(!isImageCategory)
            llCategory.background = ContextCompat.getDrawable(context,
                    if (activeCategory.id == category.id) {
                        R.drawable.background_category_active
                    } else {
                        R.drawable.background_category
                    })
            tvCategory.text = category.name
            llCategory.setOnClickListener {
                itemClick?.onItemClick(it, position)
            }
        }
    }

    // 마지막에 사용한 카테고리를 열기
    fun loadRecentActivePosition() {
        val categoryPosition = storage.getInt("ChatCategoryPosition", 0)
        if (categoryPosition > 0 && categoryPosition < list.size) {
            activePosition = categoryPosition
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.iv_category)
        lateinit var ivCategory: ImageView
        @BindView(R.id.tv_category)
        lateinit var tvCategory: TextView
        @BindView(R.id.ll_category)
        lateinit var llCategory: View

        init {
            ButterKnife.bind(this, itemView)
        }
    }
}