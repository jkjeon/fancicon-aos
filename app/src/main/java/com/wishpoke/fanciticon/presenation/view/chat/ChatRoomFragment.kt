package com.wishpoke.fanciticon.presenation.view.chat

import android.app.NotificationManager
import android.content.Context
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ShareCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.BindView
import butterknife.ButterKnife
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.chat.ChatRoom
import com.wishpoke.fanciticon.data.model.eventbus.ScrollTop
import com.wishpoke.fanciticon.data.model.eventbus.TabChange
import com.wishpoke.fanciticon.presenation.base.InssaFragment
import com.wishpoke.fanciticon.presenation.view.login.LoginActivity
import com.wishpoke.fanciticon.presenation.view.main.MainActivity
import com.wishpoke.fanciticon.util.*
import com.wishpoke.fanciticon.util.constants.Constants
import com.wishpoke.fanciticon.util.extention.createDialog
import com.wishpoke.fanciticon.util.extention.open
import com.wishpoke.fanciticon.util.listener.ItemClick
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.koin.android.viewmodel.ext.android.viewModel

class ChatRoomFragment : InssaFragment() {

    @BindView(R.id.rv_emoji)
    lateinit var rvList: RecyclerView
    @BindView(R.id.swipeRefreshLayout)
    @JvmField var swipeRefreshLayout: SwipeRefreshLayout? = null

    private lateinit var chatRoomAdapter: ChatRoomAdapter
    private var requestPage = 0

    private val chatRoomViewModel: ChatRoomViewModel by viewModel()
    private val chatViewModel: ChatViewModel by viewModel()

    companion object {
        @JvmStatic fun create(): ChatRoomFragment {
            return ChatRoomFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view: View = inflater.inflate(R.layout.fragment_chat_room, container, false)
        unbinder = ButterKnife.bind(this, view)
        init()
        subscribeObservers()
        swipeRefreshLayout?.setColorSchemeColors(resources.getColor(R.color.purple))
        swipeRefreshLayout?.setOnRefreshListener {
            swipeRefreshLayout?.isRefreshing = true
            requestPage = 0
            loadChatRooms()
        }
        requestPage = 0
        loadChatRooms()
        return view
    }

    private fun subscribeObservers() {
        chatRoomViewModel.chatRoomList.observe(viewLifecycleOwner, Observer {
            chatRoomAdapter.setLoading(it.isLoading())
            swipeRefreshLayout?.isRefreshing = it.isLoading()
            when (it.status) {
                LoadingState.Status.SUCCESS -> {
                    if (requestPage == 0) {
                        chatRoomAdapter.clear()
                    }
                    chatRoomAdapter.addListDistinct(it.data!!)
                    if (it.data.isNotEmpty()) {
                        requestPage++
                    } else {
                        requestPage = -1
                    }
                }
                LoadingState.Status.FAILED -> {
                    Logger.e("Test", "error message: ${it.throwable?.customMessage}")
                    if (TestUtil.isDebugMode()) {
                        Toaster.show(mContext.applicationContext, it.throwable?.customMessage ?: "x", it.throwable?.code ?: 0)
                    }
                }
            }
        })

        chatRoomViewModel.popularChatRoomIds.observe(viewLifecycleOwner, Observer {
            if (it.isSuccessful()) {
                chatRoomAdapter.setPopularRoomIds(it.data!!)
            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        chatRoomViewModel.getPopularChatRoomIds(chatRoomAdapter.boardNo)
    }

    private fun init() {
        chatRoomAdapter = ChatRoomAdapter(mContext, ItemClick { view, position ->
            when (view.id) {
                R.id.btn_write -> {
                    // 채팅방 생성
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 800) {
                        return@ItemClick
                    }
                    mLastClickTime = SystemClock.elapsedRealtime().toInt()
                    if (storage.isLoggedIn) {
                        goTo(mActivity, CreateChatRoomActivity::class.java)
                    } else {
                        mDialog = mContext.createDialog(
                                message = R.string.dialog_sign_up_to_join,
                                positiveLabel = R.string.sign_up_to_join,
                                negativeLabel = R.string.cancel,
                                positiveBlock = { goTo(mActivity, LoginActivity::class.java) }
                        ).apply { show() }
                    }
                }
                R.id.btn_info -> {
                    // 인싸챗이란?
                    mDialog = mContext.createDialog(
                            message = R.string.dialog_chat_meaming
                    ).apply { show() }
                }
                R.id.btn_board -> {
                    // 보드 카테고리
                    mDialog = DialogManager.getInstance().createSingleChoiceDialog(context, chatRoomAdapter.boards, chatRoomAdapter.boardNo) { _, i ->
                        mDialog?.dismiss()
                        chatRoomAdapter.boardNo = i
                        chatRoomAdapter.notifyItemChanged(0)
                        // 카테고리에 따라 다시 로드
                        requestPage = 0
                        loadChatRooms()
                        chatRoomViewModel.getPopularChatRoomIds(chatRoomAdapter.boardNo)
                    }
                    mDialog?.show()
                }
                R.id.btn_order -> {
                    // 오더
                    mDialog = DialogManager.getInstance().createSingleChoiceDialog(context, chatRoomAdapter.orders, chatRoomAdapter.orderType - 1) { _, i ->
                        mDialog?.dismiss()
                        chatRoomAdapter.orderType = i + 1
                        chatRoomAdapter.notifyItemChanged(0)
                        // 오더에따라 다시 로드
                        requestPage = 0
                        loadChatRooms()
                    }
                    mDialog?.show()
                }
            }
        })
        chatRoomAdapter.itemClick = ItemClick { v, position ->
            val chatRoom = chatRoomAdapter.getItemAt(position)
            when (v.id) {
                R.id.ll_like -> {
                    chatViewModel.likeChatRoom(chatRoom.roomId)
                    if (storage.countChatRoomListLike()) {
                        (mActivity as MainActivity).showInterstitial()
                    }
                }
                R.id.ll_share -> {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) return@ItemClick
                    mLastClickTime = SystemClock.elapsedRealtime().toInt()
                    chatViewModel.shareChatRoom(chatRoom.roomId)
                    val url = "${Constants.Url.CHAT_SHARE_PREFIX}${chatRoom?.roomId}"
                    val shareText = String.format(getString(R.string.chat_invite),
                            "찐톡 유저", chatRoom.title ?: "", url)
                    ShareCompat.IntentBuilder.from(mActivity)
                            .setType("text/plain")
                            .setChooserTitle(getString(R.string.chat_menu_invite))
                            .setText(shareText)
                            .startChooser()
                }
                else -> {
                    // 이미 회원이거나 방장일경우 바로 입장
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) return@ItemClick
                    mLastClickTime = SystemClock.elapsedRealtime().toInt()
                    if (storage.isLoggedIn) {
                        EventBusManager.getInstance().cast(chatRoom)
                        if (chatRoom.joined()) {
                            goTo(mActivity, ChatActivity::class.java)
                        } else {
                            goTo(mActivity, EnterChatActivity::class.java)
                        }
                    } else {
                        mDialog = mContext.createDialog(
                                message = R.string.dialog_sign_up_to_join,
                                positiveLabel = R.string.sign_up_to_join,
                                negativeLabel = R.string.cancel,
                                positiveBlock = { mActivity.open<LoginActivity>() }
                        ).apply { show() }
                    }
                }
            }
        }

        val layoutManager = LinearLayoutManager(mContext)
        rvList.setHasFixedSize(true)
        rvList.layoutManager = layoutManager
        rvList.adapter = chatRoomAdapter
        rvList.itemAnimator?.changeDuration = 0
        rvList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                // End of Scroll
                val layoutManager = rvList.layoutManager as LinearLayoutManager
                if (layoutManager.findLastVisibleItemPosition() + 2 > chatRoomAdapter.itemCount) {
                    loadChatRooms()
                }
                val scrollPosition: Int = layoutManager.findFirstVisibleItemPosition()
                if (scrollPosition > 1) {
                    (mContext as MainActivity).hideTitle()
                } else {
                    (mContext as MainActivity).showTitle()
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        if (isAdded) {
            // 뱃지 사라지게 하기
            val notificationManager = context?.getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager
            notificationManager?.cancel(0)

            // 다시 로딩
            requestPage = 0
            loadChatRooms()
        }
    }

    private fun loadChatRooms() {
        if (!chatRoomAdapter.isLoading() && requestPage > -1) {
            chatRoomViewModel.getChatRoomList(
                    storage.userId,
                    requestPage,
                    chatRoomAdapter.boardNo,
                    chatRoomAdapter.orderType)
            chatRoomViewModel.getPopularChatRoomIds(chatRoomAdapter.boardNo)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onTabPositionChanged(tabChange: TabChange) {
        // 찐톡 진입시 뱃지 제거
        val currentCategoryId = mMainViewModel.getCategories()[tabChange.position].id
        if (currentCategoryId == "chat") {
            val notificationManager = context?.getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager
            notificationManager?.cancel(0)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onScrollTop(scrollTop: ScrollTop) {
        if (scrollTop.categoryId == "chat") {
            rvList.smoothScrollToPosition(0)
        }
    }
}