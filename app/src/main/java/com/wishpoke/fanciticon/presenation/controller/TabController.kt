package com.wishpoke.fanciticon.presenation.controller

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.material.tabs.TabLayout
import com.wishpoke.fanciticon.R

class TabController(val context: Context,
                    private val tabLayout: TabLayout,
                    private val labels: Array<String>,
                    private val selectedListener: OnTabSelectedListener?) {

    interface OnTabSelectedListener {
        fun onSelected(position: Int)
    }

    init {
        tabLayout.tabMode = TabLayout.MODE_FIXED

        tabLayout.removeAllTabs()
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL
        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(context, R.color.transparent))


        for ((index, label) in labels.withIndex()) {
            val tab  = tabLayout.newTab()
            val view = LayoutInflater.from(context).inflate(R.layout.view_tab, null)
            val tvTitle = view.findViewById<TextView>(R.id.tv_title)
            tvTitle.text = label
            updateTabView(index == 0, view)
            tab.customView = view
            tabLayout.addTab(tab)
        }


        tabLayout.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                updateTabView(false, tab?.customView)
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                updateTabView(true, tab?.customView)
                selectedListener?.onSelected(tab?.position ?: 0)
            }
        })

        tabLayout.scrollX = 0
    }

    private fun updateTabView(isActive: Boolean, view: View?) {
        val tvTitle = view?.findViewById<TextView>(R.id.tv_title)
        val viewLine = view?.findViewById<View>(R.id.view_line)
        val textColorRes = if (isActive) R.color.colorPrimary else R.color.textGray
        val lineColorRes = if (isActive) R.color.colorPrimary else R.color.customCategory
        tvTitle?.setTextColor(ContextCompat.getColor(context, textColorRes))
        viewLine?.setBackgroundColor(ContextCompat.getColor(context, lineColorRes))
    }
}