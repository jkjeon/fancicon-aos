package com.wishpoke.fanciticon.presenation.view.pung


import android.content.Context
import android.content.Intent
import android.os.Vibrator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.NO_POSITION
import butterknife.BindView
import butterknife.ButterKnife
import coil.load
import coil.size.Scale
import coil.transform.RoundedCornersTransformation
import com.airbnb.lottie.LottieAnimationView
import com.bumptech.glide.Glide
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.MetaData
import com.wishpoke.fanciticon.data.model.pung.Post
import com.wishpoke.fanciticon.presenation.base.LoadingRecyclerViewAdapter
import com.wishpoke.fanciticon.presenation.view.general.VideoViewerActivity
import com.wishpoke.fanciticon.util.Animator
import com.wishpoke.fanciticon.util.Moment
import com.wishpoke.fanciticon.util.Util
import com.wishpoke.fanciticon.util.Validator
import com.wishpoke.fanciticon.util.YoutubeUtils.Companion.getThumbnail
import com.wishpoke.fanciticon.util.YoutubeUtils.Companion.isYoutube
import com.wishpoke.fanciticon.util.constants.Constants
import com.wishpoke.fanciticon.util.extention.dp2px
import com.wishpoke.fanciticon.util.extention.isVisibleWhen
import kotlinx.android.synthetic.main.item_reply.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jsoup.Jsoup

class PungAdapter(val context: Context) : LoadingRecyclerViewAdapter<RecyclerView.ViewHolder, Post>() {

    var orderType = 2
    var boardNo = 0

    val orders = arrayOf(context.getString(R.string.order_latest), context.getString(R.string.order_popular))
    val boards = context.resources.getStringArray(R.array.pung_categories)

    private var popularPostNo: List<Int> = java.util.ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TypeHeader -> {
                val itemView = LayoutInflater.from(context).inflate(R.layout.item_header_pung, parent, false)
                itemView.layoutParams = params
                HeaderViewHolder(itemView)
            }
            TypeLoading -> onCreateLoadingViewHolder(context)
            else -> {
                val itemView = LayoutInflater.from(context).inflate(R.layout.item_reply, parent, false)
                ViewHolder(itemView)
            }
        }
    }

    fun setPopularPostNo(ids: List<Int>) {
        this.popularPostNo = ids
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        with(holder) {
            when (this) {
                is HeaderViewHolder -> {
                    val clickListener = View.OnClickListener {
                        headerItemClick?.onItemClick(it, position)
                    }
                    btnInfo.setOnClickListener(clickListener)
                    btnBoard.setOnClickListener(clickListener)
                    btnOrder.setOnClickListener(clickListener)
                    btnWrite.setOnClickListener(clickListener)
                    tvOrder.text = orders[orderType - 1]
                    tvBoard.text = boards[boardNo]
                }
                is LoadingViewHolder -> onBindLoadingViewHolder(holder)
                is ViewHolder -> bind(getItemAt(position))
                else -> {}
            }
        }
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)
        if (holder is ViewHolder) {
            holder.avLike.cancelAnimation()
        }
    }

    override fun getItemId(position: Int): Long = when (getItemViewType(position)) {
        TypeHeader -> -1
        TypeLoading -> -2
        else -> getItemAt(position).postNo.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            position == 0 -> TypeHeader
            position == list.size + 1 -> TypeLoading
            else -> TypeItem
        }
    }

    override fun getRealPosition(position: Int): Int {
        return position - 1
    }

    override fun getAdapterPosition(position: Int): Int {
        return position + 1
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.tv_username)
        lateinit var tvUsername: TextView
        @BindView(R.id.tv_time)
        lateinit var tvTime: TextView
        @BindView(R.id.tv_comment)
        lateinit var tvComment: TextView
        @BindView(R.id.tv_count_comment)
        lateinit var tvCountComment: TextView
        @BindView(R.id.tv_count_share)
        lateinit var tvCountShare: TextView
        @BindView(R.id.tv_score)
        lateinit var tvScore: TextView
        @BindView(R.id.ll_like)
        lateinit var llLike: LinearLayout
        @BindView(R.id.ll_share)
        lateinit var llShare: LinearLayout
        @BindView(R.id.iv_share)
        lateinit var ivShare: ImageView
        @BindView(R.id.iv_image)
        lateinit var ivImage: ImageView
        @BindView(R.id.iv_rank)
        lateinit var ivRank: ImageView
        @BindView(R.id.av_like)
        lateinit var avLike: LottieAnimationView
        @BindView(R.id.rl_metadata)
        lateinit var rlMetadata: RelativeLayout
        @BindView(R.id.iv_metadata_image)
        lateinit var ivMetadataImage: ImageView
        @BindView(R.id.tv_metdata_url)
        lateinit var tvMetadataUrl: TextView
        @BindView(R.id.tv_metdata_title)
        lateinit var tvMetadataTitle: TextView
        @BindView(R.id.iv_play_video)
        lateinit var ivPlayVideo: View
        @BindView(R.id.iv_video)
        lateinit var ivVideo: ImageView

        private var context = itemView.context

        init {
            ButterKnife.bind(this, itemView)

            rlMetadata.setOnClickListener {
                if (adapterPosition != NO_POSITION) itemClick?.onItemClick(it, adapterPosition)
            }
            itemView.setOnClickListener {
                if (adapterPosition != NO_POSITION) itemClick?.onItemClick(it, adapterPosition)
            }
            llLike.setOnClickListener {
                if (adapterPosition != NO_POSITION) {
                    val post = getItemAt(adapterPosition)
                    val vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                    vibrator.vibrate(Constants.VibrateTime)
                    avLike.playAnimation()
                    post.likeCnt += 1
                    tvScore.text = Util.formatLikeCount(post.likeCnt)
                    itemClick?.onItemClick(it, position)
                }
            }
            llShare.setOnClickListener {
                if (adapterPosition != NO_POSITION) {
                    val post = getItemAt(adapterPosition)
                    ivShare.startAnimation(Animator.getFavoriteClickAnimation(context))
                    post.shareCnt += 1
                    tvCountShare.text = Util.formatLikeCount(post.shareCnt)
                    itemClick?.onItemClick(it, position)
                }
            }
            ivPlayVideo.setOnClickListener {
                if (adapterPosition != NO_POSITION) {
                    val post = getItemAt(adapterPosition)
                    it.startAnimation(Animator.getClickAnimation(context))
                    val intent = Intent(context, VideoViewerActivity::class.java)
                    intent.putExtra("url", post.youtubeUrl)
                    context.startActivity(intent)
                }
            }

            with (itemView) {
                iv_more.setOnClickListener {
                    if (adapterPosition != NO_POSITION) {
                        itemClick?.onItemClick(it, adapterPosition)
                    }
                }
            }
        }

        fun bind(post: Post) {
            avLike.pauseAnimation()

            with (itemView) {
                iv_more.visibility = View.GONE
                // TODO: 더보기 메뉴 처리
            }

            tvUsername.text = post.userName
            tvComment.text = post.comment
            tvScore.text = Util.formatLikeCount(post.likeCnt)
            tvTime.text = Moment.getPungTime(post.createTime)
            tvCountShare.text = Util.formatLikeCount(post.shareCnt)
            tvCountComment.text = Util.formatLikeCount(post.replyCnt)

            // 금/은/동싸 설정
            val rankImages = arrayOf(R.drawable.ic_gold, R.drawable.ic_silver, R.drawable.ic_bronze)
            if (orderType == 2) {
                val index = getRealPosition(position)
                if (index < rankImages.size) {
                    post.rank = index + 1
                    Glide.with(context).load(rankImages[index]).into(ivRank)
                }
            } else {
                if (!popularPostNo.isNullOrEmpty()) {
                    val index = popularPostNo.indexOf(post.postNo)
                    if (index > -1 && index < rankImages.size) {
                        post.rank = index + 1
                        Glide.with(context).load(rankImages[index]).into(ivRank)
                    }
                }
            }

            ivRank.isVisibleWhen(post.rank > 0)

            if (!post.fileName.isNullOrEmpty()) {
                ivImage.load(Util.getImageDownloadUrl(post.fileName)) {
                    crossfade(true)
                    size(500, 500)
                    placeholder(R.mipmap.image_placeholder)
                    transformations(RoundedCornersTransformation(context.dp2px(10F).toFloat()))
                }
            }
            ivImage.isVisibleWhen(!post.fileName.isNullOrEmpty())

            // 메타 데이터 설정
            val hasUrl = !post.metaDataUrl.isNullOrEmpty() && !isYoutube(post.metaDataUrl)
            if (hasUrl) {
                CoroutineScope(Dispatchers.Main).launch {
                    try {
                        val metaData = fetchMetadata(post.metaDataUrl!!)
                        if (metaData.url == post.metaDataUrl) {
                            val radius = context.dp2px(4F).toFloat()
                            ivMetadataImage.load(metaData.image) {
                                crossfade(true)
                                scale(Scale.FILL)
                                placeholder(R.mipmap.image_placeholder)
                                transformations(RoundedCornersTransformation(radius))
                            }
                            tvMetadataTitle.text = Util.shorten(metaData.title)
                            tvMetadataUrl.text = Util.shorten(metaData.domain)
                            rlMetadata.visibility = View.VISIBLE
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
            rlMetadata.isVisibleWhen(hasUrl)
            // 유튜브 비디오 설정
            ivVideo.isVisibleWhen(!post.youtubeUrl.isNullOrEmpty())
            ivPlayVideo.isVisibleWhen(!post.youtubeUrl.isNullOrEmpty())
            if (post.youtubeUrl != null) {
                ivVideo.load(getThumbnail(post.youtubeUrl!!)) {
                    crossfade(true)
                    scale(Scale.FILL)
                    placeholder(R.mipmap.image_placeholder)
                    transformations(RoundedCornersTransformation(context.dp2px(2F).toFloat()))
                }
            }
        }

        private suspend fun fetchMetadata(url: String): MetaData {
            val metaData = MetaData()
            metaData.url = url
            try {
                withContext(Dispatchers.IO) {
                    metaData.domain = Validator.getDomainName(url)
                    val conn = Jsoup.connect(url).followRedirects(true)
                    val doc = conn.get()
                    val ogTags = doc.select("meta[property^=og:]")

                    if (ogTags.isNotEmpty()) {
                        for (tag in ogTags) {
                            when (val text = tag.attr("property")) {
                                "og:title" -> metaData.title = tag.attr("content")
                                "og:description" -> metaData.desc = tag.attr("content")
                                "og:image" -> metaData.image = tag.attr("content")
                            }
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return metaData
        }
    }

    internal class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.btn_info)
        lateinit var btnInfo: View
        @BindView(R.id.btn_board)
        lateinit var btnBoard: View
        @BindView(R.id.tv_board)
        lateinit var tvBoard: TextView
        @BindView(R.id.btn_order)
        lateinit var btnOrder: View
        @BindView(R.id.tv_order)
        lateinit var tvOrder: TextView
        @BindView(R.id.btn_write)
        lateinit var btnWrite: View

        init {
            ButterKnife.bind(this, itemView)
        }
    }


}

