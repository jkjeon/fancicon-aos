package com.wishpoke.fanciticon.presenation.resource.ui;

public class BounceInterpolator implements android.view.animation.Interpolator {
    private double mAmplitude = 0.1F;
    private double mFrequency = 4;

    public BounceInterpolator() {

    }

    public BounceInterpolator(double amplitude, double frequency) {
        mAmplitude = amplitude;
        mFrequency = frequency;
    }
    public float getInterpolation(float time) {
        return (float) (-1 * Math.pow(Math.E, -time/ mAmplitude) *
                Math.cos(mFrequency * time) + 1);
    }
}
