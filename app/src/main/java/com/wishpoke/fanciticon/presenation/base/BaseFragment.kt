package com.wishpoke.fanciticon.presenation.base

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import butterknife.Unbinder
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import com.wishpoke.fanciticon.presenation.view.general.ImageViewerActivity
import com.wishpoke.fanciticon.presenation.view.main.MainViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

abstract class BaseFragment : Fragment() {

    lateinit var mActivity: Activity
    lateinit var mContext: Context
    val storage: AppStorage by inject()
    val mMainViewModel: MainViewModel by viewModel()

    @JvmField var unbinder: Unbinder? = null
    @JvmField var mDialog: AlertDialog? = null
    @JvmField var mLastClickTime = 0
    @JvmField var mIsVisible = false

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = activity as Activity
        mContext = context
    }

    override fun onDestroyView() {
        mDialog?.dismiss()
        unbinder?.unbind()
        super.onDestroyView()
    }

    override fun setMenuVisibility(visible: Boolean) {
        super.setMenuVisibility(visible)
        mIsVisible = visible
    }

    open fun goTo(activity: Activity, target: Class<*>, isFinish: Boolean = false, isClear: Boolean = false) {
        val intent = Intent(activity, target)
        if (isClear) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        startActivity(intent)
        if (isFinish) {
            activity.finish()
        }
    }

    open fun openImageViewer(fileName: String) {
        val intent = Intent(mActivity, ImageViewerActivity::class.java)
        val items = ArrayList<String>()
        items.add(fileName)
        intent.putStringArrayListExtra(ImageViewerActivity.KEY, items)
        startActivity(intent)
    }
}