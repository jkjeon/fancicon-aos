package com.wishpoke.fanciticon.presenation.view.general;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.wishpoke.fanciticon.R;
import com.wishpoke.fanciticon.presenation.view.main.MainActivity;
import com.wishpoke.fanciticon.util.Animator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PermissionActivity extends AppCompatActivity {

    @BindView(R.id.btn_positive)
    CardView btnPositive;
    @BindView(R.id.btn_negative)
    CardView btnNegative;

    private static final int DRAW_OVER_OTHER_APP_PERMISSION = 1232;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission);
        ButterKnife.bind(this);

        btnNegative.setBackground(ContextCompat.getDrawable(this, R.drawable.button_category));
        btnPositive.setBackground(ContextCompat.getDrawable(this, R.drawable.button_category_active));
    }

    @OnClick(R.id.btn_positive)
    public void clickPositive(View v) {
        v.startAnimation(Animator.getClickAnimation(this));
        askForSystemOverlayPermission();
    }

    @OnClick(R.id.btn_negative)
    public void clickNegative(View v) {
        v.startAnimation(Animator.getClickAnimation(this));
        startActivity(new Intent(PermissionActivity.this, MainActivity.class));
        finish();
    }

    // Widget 권한 묻기
    private void askForSystemOverlayPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, DRAW_OVER_OTHER_APP_PERMISSION);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DRAW_OVER_OTHER_APP_PERMISSION) {
            //Check if the permission is granted or not.
            startActivity(new Intent(PermissionActivity.this, MainActivity.class));
            finish();
        }
    }
}
