package com.wishpoke.fanciticon.presenation.resource;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.Nullable;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.wishpoke.fanciticon.data.model.User;
import com.wishpoke.fanciticon.util.UDTextUtils;
import com.wishpoke.fanciticon.util.constants.AdTriggerNumbers;
import com.wishpoke.fanciticon.util.constants.Constants;
import com.wishpoke.fanciticon.util.constants.PrefKeys;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AppStorage {
    private SharedPreferences pref;

    public static final String FIRST_SCREEN_CATEGORY_ID = "firstScreenIndex";

    private String RECENT = "recent";
    private String PURCHASED_REMOVE_ADS = "remove_ads";
    private String QUIT_AT = "quit_at"; // 종료시 기록
    private String CLICKED_NEW_EMOJIS = "clicked_new_emojis"; // 새로운 이모티콘 중 클릭한 수
    private String NICKNAME = "nickname";
    // 신규카테고리
    private String UPDATE_CATEGORY_VERSION_CODE = "updateCategory_versionCode";
    private String UPDATE_CATEGORY= "updateCategory";

    private final int MAX_RECENT = 3; // 최근 이모티콘 수
    private final int RECONNECTION_MINUTES = 2; // 재실행 시간 (분)
    private final String divider = "m789m";

    public AppStorage(Context context) {
        pref = context.getSharedPreferences("recent_storage", Context.MODE_PRIVATE);
    }

    public void put(String key, String value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getUserId() {
        User user = getObject(PrefKeys.USER, User.class);
        if (user != null) {
            return user.getUserId();
        } else {
            return "";
        }
    }

    public boolean isLoggedIn() {
        return !UDTextUtils.isEmpty(getUserId());
    }

    public <T> void put(String key, T object) {
        put(key, new Gson().toJson(object));
    }

    public <T> T getObject(String key, Class<T> classType) {
        String raw = getString(key);
        try {
            return new Gson().fromJson(raw, classType);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public @Nullable  String getString(String key) {
        return pref.getString(key, null);
    }

    public String getString(String key, String defaultValue) {
        return pref.getString(key, defaultValue);
    }

    public void put(String key, long value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public long getLong(String key) {
        return pref.getLong(key, 0);
    }

    public void put(String key, int value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public int getInt(String key) {
        return pref.getInt(key, 0);
    }

    public int getInt(String key, int defaultValue) {
        return pref.getInt(key, defaultValue);
    }

    public void put(String key, boolean value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public Boolean getBoolean(String key) {
        return pref.getBoolean(key, false);
    }

    public Boolean getBoolean(String key, boolean defaultValue) {
        return pref.getBoolean(key, defaultValue);
    }

    public void setPushNotification(boolean on) {
        if (on) {
            FirebaseMessaging.getInstance().subscribeToTopic(Constants.FcmTopic);
        } else {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(Constants.FcmTopic);
        }
        put("push", on);
    }

    public boolean getPushNotification() {
        return pref.getBoolean("push", true);
    }

    public boolean isFirstLaunch() {
        return pref.getBoolean("is_first", true);
    }

    public void markLaunch() {
        put("is_first", false);
    }

    public boolean shouldShowAd() {
        return !this.purchasedRemoveAds() || !isFirstLaunch();
    }

    public void setQuitAt() {
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong(QUIT_AT, (System.currentTimeMillis() / 1000));
        editor.apply();
    }

    public boolean isReconnection() {
        boolean isReconnection = false;
        long now = System.currentTimeMillis() / 1000;
        long quitAt = pref.getLong(QUIT_AT, (System.currentTimeMillis() / 1000));
        if (now - quitAt > RECONNECTION_MINUTES * 60) {
            isReconnection = true;
        }
        return isReconnection;
    }

    // 유저가 이모티콘 클릭했는지 검사
    public boolean checkClickedEmoji(int versionCode, String emoji) {
        int currentVersionCode = pref.getInt(UPDATE_CATEGORY_VERSION_CODE, 0);
        if (versionCode > currentVersionCode) {
            // 버전 코드 업데이트시 리셋
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt(UPDATE_CATEGORY_VERSION_CODE, versionCode);
            editor.putString(CLICKED_NEW_EMOJIS, null);
            editor.apply();
        }
        ArrayList<String> clickedEmojis = new ArrayList<>();
        String clickedEmojisRaw = pref.getString(CLICKED_NEW_EMOJIS, null);
        if (clickedEmojisRaw != null) {
            clickedEmojis = new ArrayList<>(Arrays.asList(clickedEmojisRaw.split(",")));
        }
        for (String e : clickedEmojis) {
            if (e.equals(emoji)) {
                return true;
            }
        }
        return false;
    }

    // 유저가 클릭한 이모티콘 저장 (버전코드별)
    public void saveClickedEmoji(int versionCode, String emoji) {
        if (!this.checkClickedEmoji(versionCode, emoji)) {
            ArrayList<String> clickedEmojis = new ArrayList<>();
            String clickedEmojisRaw = pref.getString(CLICKED_NEW_EMOJIS, null);

            if (clickedEmojisRaw != null) {
                clickedEmojis = new ArrayList<>(Arrays.asList(clickedEmojisRaw.split(",")));
            }
            // 추가 후 저장
            clickedEmojis.add(emoji);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(CLICKED_NEW_EMOJIS, convertListToString(clickedEmojis));
            editor.apply();
        }
    }

    // 유저가 클릭하지 않은 신규 카테고리 저장 (버전코드별)
    public void saveUpdateCategories(int versionCode, String[] updateCategories) {
        int currentVersionCode = pref.getInt(UPDATE_CATEGORY_VERSION_CODE, 0);
        if (versionCode > currentVersionCode) {
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt(UPDATE_CATEGORY_VERSION_CODE, versionCode);
            editor.putString(UPDATE_CATEGORY, convertListToString(Arrays.asList(updateCategories)));
            editor.apply();
        }
    }

    public List<String> getUpdateCategories() {
        String categoriesRaw = pref.getString(UPDATE_CATEGORY, null);
        if (categoriesRaw != null) {
            return Arrays.asList(categoriesRaw.split(","));
        }
        return null;
    }

    // 유저 클릭 처리 후 저장
    public boolean isClickedUpdateCategory(String categoryId) {
        boolean isClickedUpdateCategory = false;
        String categoriesRaw = pref.getString(UPDATE_CATEGORY, null);
        if (categoriesRaw != null) {
            // 이미 클릭했는지 체크
            ArrayList<String> categories = new ArrayList<>(Arrays.asList(categoriesRaw.split(",")));
            isClickedUpdateCategory = categories.contains(categoryId);
            // 클릭했다면 리스트에서 제거후 저장
            if (isClickedUpdateCategory) {
                categories.remove(categoryId);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString(UPDATE_CATEGORY, convertListToString(categories));
                editor.apply();
            }
        }
        return isClickedUpdateCategory;
    }

    private String convertListToString(List<String> array) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < array.size(); i++) {
            sb.append(array.get(i)).append(",");
        }
        return sb.toString();
    }

    // 카테고리 탭 N번 클릭시마다 광고
    public boolean countTab() {
        int n = AdTriggerNumbers.TAB_EMOTICON_CATEGORY;
        int count = (pref.getInt("count_tab", 0) + 1) % n;
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("count_tab", count);
        editor.apply();
        return count == 0;
    }

    public boolean countLaunch() {
        int count = (pref.getInt("count_launch", 0) + 1);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("count_launch", count);
        editor.apply();
        return count == 100 || count == 30; // true 일경우 리뷰 팝업 보여줌
    }

    public boolean countChatRoomLike() {
        int n = AdTriggerNumbers.LIKE_IN_CHAT_ROOM;
        int countChat = pref.getInt("count_chat_room_like", 0);
        int count = (countChat + 1);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("count_chat_room_like", count % n);
        editor.apply();
        return count > 0 && count % n == 0; // true 일경우 광고 보여줌
    }

    public boolean countChatRoomListLike() {
        int n = AdTriggerNumbers.LIKE_IN_CHAT_LIST;
        int count = (pref.getInt("count_chat_room_list_like", 0) + 1) % n;
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("count_chat_room_list_like", count);
        editor.apply();
        return count == 0;
    }

    public boolean countPungLike() {
        int n = AdTriggerNumbers.LIKE_IN_PUNG_POST;
        int count = (pref.getInt("count_pung_like", 0) + 1) % n;
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("count_pung_like", count);
        editor.apply();
        return count == 0;
    }

    public boolean countPungListLike() {
        int n = AdTriggerNumbers.LIKE_IN_PUNG_LIST;
        int count = (pref.getInt("count_pung_list_like", 0) + 1) % n;
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("count_pung_list_like", count);
        editor.apply();
        return count == 0;
    }

    public boolean purchasedRemoveAds() {
        // 배포버전에서만 광고 보임
        return pref.getBoolean(PURCHASED_REMOVE_ADS, false);
    }

    public void setPurchasedRemoveAds(boolean flag) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(PURCHASED_REMOVE_ADS, flag);
        editor.apply();
    }

    public ArrayList<String> getRecentArrayList() {
        String rawString = pref.getString(RECENT, null);
        ArrayList<String> arrayList = new ArrayList<>();
        if (rawString != null) {
            String[] stringArr = rawString.split(divider);
            arrayList.addAll(Arrays.asList(stringArr));
        }
        return arrayList;
    }

    public ArrayList<String> addRecent(String emoji) {
        ArrayList<String> arrayList = getRecentArrayList();
        // 중복이 아닌경우 추가
        if (!arrayList.contains(emoji)) {
            arrayList.add(0, emoji);
            if (arrayList.size() > MAX_RECENT) {
                arrayList.remove(MAX_RECENT);
            }
        }
        // ArrayList -> String
        String rawString = "";
        for (int i = 0; i < arrayList.size(); i++) {
            rawString += arrayList.get(i);
            if (i != arrayList.size() - 1) {
                rawString += divider;
            }
        }
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(RECENT, rawString);
        editor.apply();

        return arrayList;
    }

    public ArrayList<String> removeRecent(String emoji) {
        ArrayList<String> arrayList = getRecentArrayList();
        arrayList.remove(emoji);
        
        // ArrayList -> String
        String rawString = "";
        for (int i = 0; i < arrayList.size(); i++) {
            rawString += arrayList.get(i);
            if (i != arrayList.size() - 1) {
                rawString += divider;
            }
        }

        SharedPreferences.Editor editor = pref.edit();
        editor.putString(RECENT, rawString);
        editor.apply();

        return arrayList;
    }

    public void setNickname(String nickname) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(NICKNAME, nickname);
        editor.apply();
    }

    public String getNickname() {
        return pref.getString(NICKNAME, null);
    }
}
