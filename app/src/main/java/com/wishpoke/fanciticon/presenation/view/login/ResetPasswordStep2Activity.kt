package com.wishpoke.fanciticon.presenation.view.login

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.gson.Gson
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.PhoneInfo
import com.wishpoke.fanciticon.data.model.response.DefaultResponse
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.presenation.resource.SingleResponse
import com.wishpoke.fanciticon.presenation.resource.ui.FormEditText
import com.wishpoke.fanciticon.util.Toaster
import com.wishpoke.fanciticon.util.Validator
import com.wishpoke.fanciticon.util.constants.PrefKeys
import com.wishpoke.fanciticon.util.extention.isVisibleWhen
import org.koin.android.viewmodel.ext.android.viewModel

class ResetPasswordStep2Activity : BaseActivity() {

    @BindView(R.id.et_pw)
    lateinit var etPw: FormEditText
    @BindView(R.id.et_confirm_pw)
    lateinit var etConfirmPw: FormEditText
    @BindView(R.id.tv_pw_message)
    lateinit var tvPwMessage: TextView
    @BindView(R.id.tv_confirm_pw_message)
    lateinit var tvConfirmPwMessage: TextView
    @BindView(R.id.iv_confirm_visible)
    lateinit var ivConfirmVisible: ImageView
    @BindView(R.id.iv_visible)
    lateinit var ivVisible: ImageView
    @BindView(R.id.tv_title)
    lateinit var tvTitle: TextView
    @BindView(R.id.btn_submit)
    lateinit var btnSubmit: TextView
    @BindView(R.id.ll_prev_pw)
    lateinit var llPrevPw: View
    @BindView(R.id.cl_prev_pw)
    lateinit var clPrevPw: View
    @BindView(R.id.tv_title_pw)
    lateinit var tvTitlePw: TextView
    @BindView(R.id.tv_title_confirm_pw)
    lateinit var tvTitleConfirmPw: TextView
    @BindView(R.id.et_prev_pw)
    lateinit var etPrevPw: FormEditText
    @BindView(R.id.iv_prev_visible)
    lateinit var ivPrevVisible: ImageView

    private val loginViewModel: LoginViewModel by viewModel()

    private var phoneInfo: PhoneInfo? = null
    private var isChangePassword: Boolean = false // 로그인 - 비밀번호 변경인지 여부

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password_step2)
        ButterKnife.bind(this)
        phoneInfo = Gson().fromJson(intent.getStringExtra("phoneInfo"), PhoneInfo::class.java)
        isChangePassword = intent.getBooleanExtra("isChangePassword", false)
        init()
    }

    private fun init() {
        // 비밀번호 변경이냐 재설정이냐에 따라 분기 처리
        if (isChangePassword) {
            tvTitle.setText(R.string.change_password_confirm)
            btnSubmit.setText(R.string.change_password_confirm)
            tvTitlePw.setText(R.string.change_password_new_pw)
            tvTitleConfirmPw.setText(R.string.change_password_new_confirm_pw)
        } else {
            tvTitle.setText(R.string.reset_password_title)
            btnSubmit.setText(R.string.next)
            tvTitlePw.setText(R.string.pw)
            tvTitleConfirmPw.setText(R.string.confirm_pw)
        }
        clPrevPw.isVisibleWhen(isChangePassword)
        llPrevPw.isVisibleWhen(isChangePassword)


        etPrevPw.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(password: CharSequence?, p1: Int, p2: Int, p3: Int) {
                etPrevPw.isValid = Validator.isValidPassword(password.toString())
            }
        })

        etPw.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(password: CharSequence?, p1: Int, p2: Int, p3: Int) {
                etPw.isValid = Validator.isValidPassword(password.toString())
                if (etPw.isValid) {
                    etConfirmPw.isValid = etConfirmPw.text.toString() == password.toString()
                }
            }
        })

        etConfirmPw.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(password: CharSequence?, p1: Int, p2: Int, p3: Int) {
                etConfirmPw.isValid = etPw.text.toString() == password.toString()
            }
        })

        etPw.setPasswordVisibleButton(ivVisible)
        etConfirmPw.setPasswordVisibleButton(ivConfirmVisible)
        etPrevPw.setPasswordVisibleButton(ivPrevVisible)
    }

    private fun isValid(): Boolean {
        val password = etPw.text?.trim().toString()
        val prevPassword = etPrevPw.text?.trim().toString()
        tvPwMessage.isVisibleWhen(!Validator.isValidPassword(password))
        tvConfirmPwMessage.isVisibleWhen(!etConfirmPw.isValid || etConfirmPw.text.isNullOrEmpty())
        return when {
            !Validator.isValidPassword(password) -> false
            !etConfirmPw.isValid || etConfirmPw.text.isNullOrEmpty() -> false // 비밀번호 확인 검사
            phoneInfo == null -> {
                Toaster.show(applicationContext, R.string.error_phone_info_not_found)
                false
            }
            isChangePassword && prevPassword != storage.getString(PrefKeys.PASSWORD) -> {
                // 이전 비밀번호가 일치하지 않을때
                Toaster.show(applicationContext, R.string.toast_check_prev_pw)
                false
            }
            else -> true
        }
    }

    @OnClick(R.id.btn_back, R.id.btn_submit)
    fun onClick(v: View) {
        when (v.id) {
            R.id.btn_back -> onBackPressed()
            R.id.btn_submit -> {
                val password = etPw.text?.trim().toString()
                if (isValid()) {
                    loginViewModel.updatePassword(phoneInfo!!, password, object: SingleResponse<DefaultResponse>() {
                        override fun onSuccess(t: DefaultResponse) {
                            // 비밀번호 저장
                            storage.put(PrefKeys.PASSWORD, password)
                            Toaster.show(applicationContext, if (isChangePassword) {
                                R.string.toast_change_pw_complete
                            } else {
                                R.string.toast_reset_pw_complete
                            })
                            finish()
                        }

                        override fun onFailure(e: Throwable, code: Int) {
                            Toaster.show(applicationContext, R.string.error_unknown, code)
                        }
                    })
                }
            }
        }
    }
}