package com.wishpoke.fanciticon.presenation.view.general;

import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.wishpoke.fanciticon.R;
import com.wishpoke.fanciticon.presenation.base.BaseActivity;
import com.wishpoke.fanciticon.presenation.resource.AppWebViewClient;
import com.wishpoke.fanciticon.util.Animator;
import com.wishpoke.fanciticon.util.FacebookAdLoader;
import com.wishpoke.fanciticon.util.Validator;
import com.wishpoke.fanciticon.util.constants.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WebViewActivity extends BaseActivity {
    @BindView(R.id.btn_cancel)
    ImageView btnCancel;
    @BindView(R.id.btn_back)
    ImageView btnBack;
    @BindView(R.id.btn_forward)
    ImageView btnForward;
    @BindView(R.id.et_url)
    EditText etUrl;
    @BindView(R.id.pb_loading)
    ProgressBar pbLoading;
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.ll_tools)
    LinearLayout llTools;
    @BindView(R.id.tv_cancel)
    TextView tvCancel;
    @BindView(R.id.tv_share)
    TextView tvShare;

    private String url;
    private String cacheUrl;

    private View mCustomView;
    private RelativeLayout mContentView;
    private FrameLayout mCustomViewContainer;
    private WebChromeClient.CustomViewCallback mCustomViewCallback;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        ButterKnife.bind(this);

        adLoader = new FacebookAdLoader(this, null, null, null);

        url = getIntent().getStringExtra("url");
        etUrl.setText(url);

        setWebView();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(Animator.getClickAnimation(mContext));
                finish();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(Animator.getClickAnimation(mContext));
                if (webView.canGoBack()) {
                    webView.goBack();
                }
            }
        });

        btnForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(Animator.getClickAnimation(mContext));
                if (webView.canGoForward()) {
                    webView.goForward();
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etUrl.clearFocus();
                etUrl.setText(cacheUrl);
                viewRevealTools();
            }
        });

        etUrl.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    viewRevealCancel();
                    cacheUrl = etUrl.getText().toString();
                } else {
                    viewRevealTools();
                }
            }
        });
        etUrl.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    url = etUrl.getText().toString();
                    loadUrl();
                    return true;
                }
                return false;
            }
        });

        tvShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");

                intent.putExtra(Intent.EXTRA_TEXT, webView.getUrl());
                Intent chooser = Intent.createChooser(intent, getString(R.string.app_name));
                startActivity(chooser);
            }
        });
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setWebView() {
        webView.setWebChromeClient(chromeClient);
        webView.setWebViewClient(webViewClient);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setAllowFileAccess(true);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            cookieManager.setAcceptThirdPartyCookies(webView, true);
        }

        CookieSyncManager.createInstance(webView.getContext());
        CookieSyncManager.getInstance().startSync();

        loadUrl();
    }

    private WebChromeClient chromeClient = new WebChromeClient() {
        FrameLayout.LayoutParams LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            mContentView = (RelativeLayout) findViewById(R.id.content_view);
            mContentView.setVisibility(View.GONE);
            mCustomViewContainer = new FrameLayout(WebViewActivity.this);
            mCustomViewContainer.setLayoutParams(LayoutParameters);
            mCustomViewContainer.setBackgroundResource(android.R.color.black);
            view.setLayoutParams(LayoutParameters);
            mCustomViewContainer.addView(view);
            mCustomView = view;
            mCustomViewCallback = callback;
            mCustomViewContainer.setVisibility(View.VISIBLE);
            setContentView(mCustomViewContainer);
        }

        @Override
        public void onHideCustomView() {
            if (mCustomView == null) {
                return;
            } else {
                // Hide the custom view.
                mCustomView.setVisibility(View.GONE);
                // Remove the custom view from its container.
                mCustomViewContainer.removeView(mCustomView);
                mCustomView = null;
                mCustomViewContainer.setVisibility(View.GONE);
                mCustomViewCallback.onCustomViewHidden();
                // Show the content view.
                mContentView.setVisibility(View.VISIBLE);
                setContentView(mContentView);
            }
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            if (pbLoading.getAlpha() == 0) {
                pbLoading.animate().alpha(1).setDuration(300).start();
            }
            pbLoading.setProgress(newProgress);
        }
    };

    private WebViewClient webViewClient = new AppWebViewClient(this) {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.equals(Constants.Url.TERMS) || url.equals(Constants.Url.POLICY)) {
                view.loadUrl(url);
                return false;
            } else {
                return super.shouldOverrideUrlLoading(view, url);
            }
        }

        @Override
        public void onPageFinished(WebView view, final String url) {
            super.onPageFinished(view, url);
            String script = "document.getElementsByClassName('TQUPK')[0] != null ? 1 : 0";
            webView.evaluateJavascript(script, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String value) {
                    if (url.contains("instagram") && value.equals("1") && CookieManager.getInstance().getCookie(url).contains("ds_user_id")) {
                        webView.reload();
                    }
                }
            });
            if (pbLoading.getAlpha() == 1) {
                pbLoading.animate().alpha(0).setDuration(300).start();
                pbLoading.setProgress(0);
            }
        }
    };

    private void loadUrl() {
        url = Validator.validateUrl(url);
        etUrl.setText(url);
        webView.loadUrl(url);
    }

    private void viewRevealTools() {
        llTools.clearAnimation();
        tvCancel.clearAnimation();
        tvShare.clearAnimation();

        llTools.setAlpha(0f);
        tvShare.setAlpha(0f);
        llTools.setVisibility(View.VISIBLE);
        tvShare.setVisibility(View.VISIBLE);
        tvShare.animate().alpha(1f).setDuration(500).start();
        llTools.animate().alpha(1f).setDuration(500).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(android.animation.Animator animation) {
                super.onAnimationEnd(animation);
                llTools.setVisibility(View.VISIBLE);
                tvShare.setVisibility(View.VISIBLE);
            }
        }).start();

        tvCancel.setAlpha(1f);
        tvCancel.animate().alpha(0f).setDuration(500).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(android.animation.Animator animation) {
                super.onAnimationEnd(animation);
                tvCancel.setVisibility(View.GONE);
            }
        }).start();
    }

    private void viewRevealCancel() {
        llTools.clearAnimation();
        tvCancel.clearAnimation();
        tvShare.clearAnimation();

        tvCancel.setAlpha(0f);
        tvCancel.setVisibility(View.VISIBLE);
        tvCancel.animate().alpha(1f).setDuration(500).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(android.animation.Animator animation) {
                super.onAnimationEnd(animation);
                tvCancel.setVisibility(View.VISIBLE);
            }
        }).start();

        llTools.setAlpha(1f);
        tvShare.setAlpha(1f);
        tvShare.animate().alpha(0f).setDuration(500).start();
        llTools.animate().alpha(0f).setDuration(500).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(android.animation.Animator animation) {
                super.onAnimationEnd(animation);
                llTools.setVisibility(View.GONE);
                tvShare.setVisibility(View.GONE);
            }
        }).start();
    }

}
