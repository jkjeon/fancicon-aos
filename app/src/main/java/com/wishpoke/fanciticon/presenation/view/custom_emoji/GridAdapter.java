package com.wishpoke.fanciticon.presenation.view.custom_emoji;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import com.wishpoke.fanciticon.R;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter {
    private ArrayList<String> letters = new ArrayList<>();
    private LayoutInflater inflater;
    private Context context;

    public GridAdapter(Context context) {
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setLetters(ArrayList<String> letters) {
        this.letters = letters;
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return letters.size();
    }

    @Override
    public Object getItem(int position) {
        return letters.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String rawLetter = letters.get(position);
        boolean shouldUseFont = rawLetter.contains("$font$");
        String letter = rawLetter.replace("$font$", "");
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_letter, parent, false);
            holder = new ViewHolder();
            holder.tvLetter = convertView.findViewById(R.id.tv_letter);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (shouldUseFont) {
            holder.tvLetter.setTypeface(ResourcesCompat.getFont(context, R.font.abydosr));
        }
        holder.tvLetter.setText(letter);
        holder.tvLetter.setTag(letter);
        return convertView;
    }


    public class ViewHolder {
        public TextView tvLetter;
    }
}
