package com.wishpoke.fanciticon.presenation.resource.ui;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MarginItemDecoration extends RecyclerView.ItemDecoration {
    int left, top, right, bottom;
    public MarginItemDecoration(int left, int top, int right, int bottom) {
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        // parent.getChildAdapterPosition(view) : 포지션 값 얻을 수 있음
        outRect.top = top;
        outRect.right = right;
        outRect.left = left;
        outRect.bottom = bottom;

        // 오른쪽 마진만 줄경우 마지막 아이템 마진은 0
        if (top == 0 && left == 0 && bottom == 0 && right != 0) {
            if (parent.getChildAdapterPosition(view) == parent.getAdapter().getItemCount() - 1) {
                outRect.right = 0;
            }
        }

        if (top == 0 && left == 0 && bottom != 0 && right == 0) {
            if (parent.getChildAdapterPosition(view) == parent.getAdapter().getItemCount() - 1) {
                outRect.bottom = 0;
            }
        }
    }
}

