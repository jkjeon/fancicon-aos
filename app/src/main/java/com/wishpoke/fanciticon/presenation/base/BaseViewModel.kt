package com.wishpoke.fanciticon.presenation.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.response.ObjectResponse
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import com.wishpoke.fanciticon.presenation.resource.ResponseHandler
import com.wishpoke.fanciticon.util.Util
import retrofit2.Response


open class BaseViewModel(application: Application) : AndroidViewModel(application) {
    open val storage = AppStorage(application)

    protected fun getUUID(): String? {
        var uuid: String? = null
        if (!storage.isLoggedIn) {
            uuid = Util.getUUID(getApplication())
        }
        return uuid
    }

    fun <T> MutableLiveData<LoadingState<T>>.updateValue(response: Response<ObjectResponse<T>>) {
        try {
            this.postValue(LoadingState.loading())
            this.postValue(ResponseHandler.handle(response))
        } catch (e: Exception) {
            this.postValue(ResponseHandler.handle(e))
        }
    }

}