package com.wishpoke.fanciticon.presenation.controller

import android.app.Activity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.PhoneInfo
import com.wishpoke.fanciticon.presenation.resource.ui.FormEditText
import com.wishpoke.fanciticon.util.Moment
import com.wishpoke.fanciticon.util.PhoneUtils
import com.wishpoke.fanciticon.util.Toaster
import com.wishpoke.fanciticon.util.constants.Errors
import com.wishpoke.fanciticon.util.extention.createDialog
import java.util.*

class PhoneController(val activity: Activity,
                      val etPhone: EditText,
                      val etCode: FormEditText,
                      val btnSendCode: TextView,
                      val btnVerifyCode: TextView,
                      val tvTimer: TextView,
                      val tvVerificationDesc: TextView) {

    interface TimerCallback {
        fun onTick(sec: Int)
    }

    var timerCallback: TimerCallback? = null
    private var timer: Timer? = null
    private var timerRemainSec = 0
    private var mDialog: AlertDialog? = null
    lateinit var phoneInfo: PhoneInfo

    init {
        clear()

        etPhone.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                // 11글자를 입력시 인증번호 받기 활성
                btnSendCode.isEnabled = text?.length == 11
            }
        })

        val clickListener = View.OnClickListener {
            when (it.id) {
                btnSendCode.id -> {
                    if (btnSendCode.text == activity.getString(R.string.phone_verify_another)) {
                        // 다른번호 인증
                        clear()
                    } else {
                        // 인증번호 보내기
                        phoneInfo.phone = etPhone.text.trim().toString()
                        PhoneUtils.sendCode(activity, phoneInfo, object: PhoneUtils.CodeSentCallback {
                            override fun onVerificationCompleted() {
                                // 인증성공
                                phoneInfo.isVerified = true
                                setVerificationCompleteView()
                                stopTimer()
                                // 인증이 완료되었습니다 팝업
                                mDialog = activity.createDialog(
                                        message = R.string.dialog_verified_code
                                ).apply { show() }
                            }

                            override fun onVerificationFailed(e: FirebaseException) {
                                // 이 메서드는 요청에 잘못된 전화번호 또는 인증 코드가 지정된 경우와 같이 잘못된 인증 요청에 대한 응답으로 호출됩니다.
                                when (e) {
                                    is FirebaseAuthInvalidCredentialsException -> {
                                        if (e.message?.contains("expired") == true) {
                                            // 코드 만료
                                            Toaster.show(activity.applicationContext, R.string.error_verification_code_expired)
                                        } else {
                                            // 인증정보가 잘못됨
                                            Toaster.show(activity.applicationContext, R.string.error_firebase_invalid_credentials)
                                        }
                                    }
                                    is FirebaseTooManyRequestsException -> {
                                        // 너무 많은 요청
                                        Toaster.show(activity.applicationContext, R.string.error_firebase_too_many_request)
                                    }
                                    else -> {
                                        // 알 수 없는 오류
                                        Toaster.show(activity.applicationContext, R.string.error_unknown, Errors.FIREBASE_VERIFICATION_REQUEST_FAILED)
                                    }
                                }
                            }

                            override fun onCodeSent(verificationId: String) {
                                // 코드 전송 완료, 타이머 시작
                                phoneInfo.verificationId = verificationId
                                etCode.isEnabled = true
                                btnVerifyCode.isEnabled = true
                                btnVerifyCode.setTextColor(ContextCompat.getColor(activity, R.color.purple))
                                tvVerificationDesc.setText(R.string.phone_desc_check_required)
                                tvVerificationDesc.setTextColor(ContextCompat.getColor(activity, R.color.textEmoji))
                                //  인증번호 발송 완료 다이얼로그 표시
                                mDialog = activity.createDialog(
                                        message = R.string.dialog_sent_code
                                ).apply { show() }
                                // 타이머 시작, 타이머 표시
                                startTimer()
                                etCode.requestFocus()
                            }
                        })
                    }
                }
                btnVerifyCode.id -> {
                    // 인증번호 확인
                    phoneInfo.code = etCode.text?.trim().toString()
                    PhoneUtils.verifyCode(phoneInfo, object: PhoneUtils.CodeVerifyCallback {
                        override fun onSucceed() {
                            // 인증성공
                            etCode.isValid = true
                            phoneInfo.isVerified = true
                            setVerificationCompleteView()
                            stopTimer()
                            // 인증이 완료되었습니다 팝업
                            mDialog = activity.createDialog(
                                    message = R.string.dialog_verified_code
                            ).apply { show() }
                        }

                        override fun onFailed(e: Exception?) {
                            // 올바르지 않은 인증 번호
                            etCode.isValid = false
                            Toaster.show(activity.applicationContext, R.string.error_verification_code_invalid)
                        }
                   })
                }
            }
        }
        btnSendCode.setOnClickListener(clickListener)
        btnVerifyCode.setOnClickListener(clickListener)

        timerCallback = object: TimerCallback {
            override fun onTick(sec: Int) {
                activity.runOnUiThread {
                    // 1초마다 타이머 뷰 업데이트
                    tvTimer.visibility = View.VISIBLE
                    tvTimer.text = Moment.formatTimer(sec)
                    val textColor = if (sec == 0) R.color.cccccc else R.color.pink
                    tvTimer.setTextColor(ContextCompat.getColor(activity, textColor))
                }
            }
        }
    }

    fun clear() {
        phoneInfo = PhoneInfo()
        phoneInfo.nacd = PhoneUtils.getDefaultCountryCode()

        // 폼 초기화
        tvTimer.text = ""
        etPhone.setText("")
        etCode.setText("")
        etCode.isValid = true
        etPhone.isEnabled = true
        etCode.isEnabled = false
        btnVerifyCode.isEnabled = false
        btnVerifyCode.setTextColor(ContextCompat.getColor(activity, R.color.dddddd))
        btnSendCode.setText(R.string.phone_send_code)
        tvVerificationDesc.text = ""
    }

    private fun setVerificationCompleteView() {
        btnSendCode.setText(R.string.phone_verify_another)
        btnSendCode.isEnabled = true

        btnVerifyCode.isEnabled = false
        btnVerifyCode.setTextColor(ContextCompat.getColor(activity, R.color.dddddd))
        etPhone.isEnabled = false
        etCode.isEnabled = false

        tvTimer.text = ""
        tvVerificationDesc.setText(R.string.phone_desc_verified)
        tvVerificationDesc.setTextColor(ContextCompat.getColor(activity, R.color.purple))
    }

    private fun startTimer() {
        tvTimer.visibility = View.GONE
        timer?.cancel()
        timer = Timer()
        timerRemainSec = 60 * 2 // 2분
        timer?.scheduleAtFixedRate(object: TimerTask() {
            override fun run() {
                timerCallback?.onTick(timerRemainSec)
                if (timerRemainSec > 0) {
                    timerRemainSec--
                } else {
                    timer?.cancel()
                }
            }
        }, 0, 1000)
    }

    private fun stopTimer() {
        tvTimer.visibility = View.GONE
        timer?.cancel()
    }

    fun onDestroy() {
        stopTimer()
        mDialog?.dismiss()
    }
}