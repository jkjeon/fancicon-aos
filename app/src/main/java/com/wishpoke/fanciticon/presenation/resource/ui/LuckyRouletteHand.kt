package com.wishpoke.fanciticon.presenation.resource.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

class LuckyRouletteHand : View {

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        paint.color = Color.parseColor("#813af9") // 보라색
        paint.strokeWidth = 12F
        paint.strokeCap = Paint.Cap.ROUND

        canvas?.run {
            val centerX: Float = (width / 2).toFloat() // 중심좌표
            val centerY: Float = (height / 2).toFloat()
            val r = centerX // 반지름

            // 바늘 그리기
            val margin = 50F
            drawLine(centerX, centerY, centerX, margin, paint)
        }
    }
}