package com.wishpoke.fanciticon.presenation.view.favorite

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.Banner
import com.wishpoke.fanciticon.data.model.emoji.Emoji
import com.wishpoke.fanciticon.presenation.view.chat.ChatEmojiFavoriteAdapter
import com.wishpoke.fanciticon.presenation.view.general.WebViewActivity
import com.wishpoke.fanciticon.presenation.view.main.BannerAdapter
import com.wishpoke.fanciticon.presenation.view.main.EmojiAdapter
import com.wishpoke.fanciticon.util.Animator
import com.wishpoke.fanciticon.util.Toaster
import com.wishpoke.fanciticon.util.listener.ItemClick

class FavoriteAdapter(context: Context) : ChatEmojiFavoriteAdapter(context) {

    companion object {
        const val TypeBanner = 5
        const val TypeFooter = 6
    }
    var bannerList: List<Banner> = ArrayList()
        set(value) {
            field = value
            notifyItemChanged(0)
        }
    var footerItemClick: ItemClick? = null

    override fun submitRecentList(list: MutableList<String>) {
        recentList = list
        notifyDataSetChanged()
    }

    override fun submitFavoriteList(list: MutableList<Emoji>) {
        favoriteList = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TypeBanner -> {
                val itemView = LayoutInflater.from(context).inflate(R.layout.item_favorite_banner, parent, false)
                BannerViewHolder(itemView)
            }
            TypeHeader -> {
                val itemView = LayoutInflater.from(context).inflate(R.layout.item_favorite_section, parent, false)
                HeaderViewHolder(itemView)
            }
            TypeFooter -> {
                val itemView = LayoutInflater.from(context).inflate(R.layout.item_favorite_footer, parent, false)
                itemView.setOnClickListener {
                    footerItemClick?.onItemClick(it, 0)
                }
                FooterViewHolder(itemView)
            }
            else -> {
                val itemView = LayoutInflater.from(context).inflate(R.layout.item_emoji, parent, false)
                EmojiAdapter.ViewHolder(itemView)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is BannerViewHolder -> holder.bind(bannerList)
            is HeaderViewHolder -> holder.bind(if (isRecentPosition(position)) {
                "Recent"
            } else {
                "Favorite"
            })
            is EmojiAdapter.ViewHolder -> {
                val emoji = getItemAt(position)
                holder.bind(emoji,
                        isFavorite(emoji), false,
                        isRecentPosition(position), false, itemClick)
            }
        }
    }

    override fun isRecentPosition(position: Int): Boolean {
        return position > 0 && position < recentList.size + 2
    }

    override fun getRealPosition(position: Int): Int {
        return if (isRecentPosition(position)) {
            // 최근
            position - 2
        } else {
            // Fav
            position - 3 - recentList.size
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> TypeBanner
            1, recentList.size + 2 -> TypeHeader
            itemCount - 1 -> TypeFooter
            else -> TypeItem

        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + 2 // for 배너, 푸터
    }

    class BannerViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.banner)
        lateinit var banner: View
        @BindView(R.id.btn_banner_desc)
        lateinit var btnBannerDesc: View
        @BindView(R.id.rv_banner)
        lateinit var rvBanner: RecyclerView
        private var context = itemView.context

        init {
            ButterKnife.bind(this, itemView)
            // 인싸티콘 소식이란?
            btnBannerDesc.setOnClickListener {
                it.startAnimation(Animator.getClickAnimation(context))
                Toaster.show(context.applicationContext, R.string.banner_desc)
            }
        }

        fun bind(bannerList: List<Banner>) {
            if (bannerList.isNotEmpty()) {
                val adapter = BannerAdapter(context, View.OnClickListener {
                    // 배너 클릭
                    it.startAnimation(Animator.getClickAnimation(context))
                    val intent = Intent(context, WebViewActivity::class.java)
                    intent.putExtra("url", it.tag.toString())
                    context.startActivity(intent);
                })
                adapter.setBanners(bannerList)
                val horizontalLayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                rvBanner.layoutManager = horizontalLayoutManager
                rvBanner.adapter = adapter
                banner.visibility = View.VISIBLE
                val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                banner.layoutParams = params
            } else {
                banner.visibility = View.INVISIBLE
                val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0)
                banner.layoutParams = params
            }
        }
    }

    class HeaderViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.tv_label)
        lateinit var tvLabel: TextView
        init {
            ButterKnife.bind(this, itemView)
        }
        fun bind(label: String) {
            tvLabel.text = label
        }
    }

    class FooterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.btn_how_to_use)
        lateinit var btnHowToUse: View
        init {
            ButterKnife.bind(this, itemView)
        }
    }
}