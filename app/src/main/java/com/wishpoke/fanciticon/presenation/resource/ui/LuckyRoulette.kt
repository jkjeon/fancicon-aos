package com.wishpoke.fanciticon.presenation.resource.ui

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.wishpoke.fanciticon.data.model.LuckReward
import kotlin.math.cos
import kotlin.math.sin

class LuckyRoulette : View {

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val textPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val pathPaint = Paint(Paint.ANTI_ALIAS_FLAG)

    private var centerX = 0F
    private var centerY = 0F

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        paint.apply {
            color = Color.parseColor("#813af9") // 보라색
            strokeWidth = 12F
            strokeCap = Paint.Cap.ROUND
        }

        textPaint.apply {
            color = Color.parseColor("#ed3093")
            textSize = 35F
            typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
        }

        canvas?.run {
            centerX = (width / 2).toFloat() // 중심좌표
            centerY = (height / 2).toFloat()
            val r = centerX // 반지름
            val handGap = -0.7F // 바늘이 딱 맞는 순간 각도차이

            // 200 포인트 눈금 그리기
            val tickLength = 40 // 눈금 길이
            var degree = Math.toRadians((LuckReward.REWARD200_DEGREE - (90F + handGap)).toDouble()) // 각도를 라디안으로
            var startX: Float = getCircleX(r - tickLength, degree)
            var startY: Float = getCircleY(r - tickLength, degree)
            var stopX: Float = getCircleX(r, degree)
            var stopY: Float = getCircleY(r, degree)
            drawLine(startX, startY, stopX, stopY, paint)
            save()

            // 200 포인트 텍스트 그리기
            var textGap = 10F // 텍스트가 눈금과 떨어져야하는 거리
            var label = "200"
            var textWidth = textPaint.measureText(label)
            var pathStartX = getCircleX(r - (textWidth + tickLength + textGap), degree)
            var pathStartY = getCircleY(r - (textWidth + tickLength + textGap), degree)

            val path = Path()
            path.moveTo(pathStartX, pathStartY + (textPaint.textSize / 2)) // text를 적어야하므로 약간 아래로 내림
            path.lineTo(startX, startY + (textPaint.textSize / 2))
            drawTextOnPath(label, path, 0F, 0F, textPaint)
            save()

            // 4,000 포인트 눈금 그리기
            degree = Math.toRadians((LuckReward.REWARD4000_DEGREE - (90F - handGap)).toDouble()) // 각도를 라디안으로, -90F 에서 바늘이 딱 맞는 순간 각도
            startX = getCircleX(r - tickLength, degree)
            startY = getCircleY(r - tickLength, degree)
            stopX = getCircleX(r, degree)
            stopY = getCircleY(r, degree)
            drawLine(startX, startY, stopX, stopY, paint)
            save()

            // 4,000 포인트 텍스트 그리기
            textGap = 36F
            label = "4,000"
            textWidth = textPaint.measureText(label)

            pathStartX = getCircleX(r - (textWidth + tickLength + textGap), degree)
            pathStartY = getCircleY(r - (textWidth + tickLength + textGap), degree)

            path.reset()
            path.moveTo(pathStartX, pathStartY + (textPaint.textSize / 2)) // text를 적어야하므로 약간 아래로 내림
            path.lineTo(startX, startY + (textPaint.textSize / 2))
            drawTextOnPath(label, path, 0F, 0F, textPaint)
            save()
        }
    }


    // radius만큼의 반지름을 가진 원의 degree 각도에서의 외곽 X좌표
    private fun getCircleX(radius: Float, degree: Double): Float {
        return centerX + (radius - 15) * cos(degree).toFloat()
    }

    // radius만큼의 반지름을 가진 원의 degree 각도에서의 외곽 Y좌표
    private fun getCircleY(radius: Float, degree: Double): Float {
        return centerY + (radius - 5) * sin(degree).toFloat()
    }
}