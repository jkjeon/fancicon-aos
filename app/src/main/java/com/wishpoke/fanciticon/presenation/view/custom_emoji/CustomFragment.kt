package com.wishpoke.fanciticon.presenation.view.custom_emoji

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.eventbus.ScrollTop
import com.wishpoke.fanciticon.presenation.base.InssaFragment
import com.wishpoke.fanciticon.presenation.view.general.WebViewActivity
import com.wishpoke.fanciticon.presenation.view.main.EmojiAdapter
import com.wishpoke.fanciticon.presenation.view.main.EmojiViewModel
import com.wishpoke.fanciticon.presenation.view.main.MainActivity
import com.wishpoke.fanciticon.util.Animator
import com.wishpoke.fanciticon.util.ImageResolver
import com.wishpoke.fanciticon.util.constants.Constants
import com.wishpoke.fanciticon.util.listener.ItemClick
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

class CustomFragment : InssaFragment() {
    @BindView(R.id.rv_emoji)
    lateinit var rvEmoji: RecyclerView
    @BindView(R.id.btn_create_emoji)
    lateinit var btnCreateEmoji: TextView
    @BindView(R.id.btn_how_to_create)
    lateinit var btnHowToCreate: LinearLayout
    @BindView(R.id.nestedScrollView)
    lateinit var nestedScrollView: NestedScrollView

    private var adapter: EmojiAdapter? = null

    private val emojiViewModel: EmojiViewModel by viewModel()
    private val customEmojiViewModel: CustomEmojiViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_custom, container, false)
        unbinder = ButterKnife.bind(this, view)

        subscribeObservers()

        adapter = EmojiAdapter(mContext)
        adapter?.itemClick = ItemClick { view, position ->
            val emoji = adapter?.getItemAt(position)
            when (view.id) {
                R.id.btn_favorite -> if (adapter?.isFavorite(emoji ?: "") == false) {
                    emojiViewModel.addFavoriteEmoji(emoji)
                } else {
                    emojiViewModel.removeFavoriteEmoji(emoji)
                }
                R.id.rl_item -> {
                    if (adapter?.isRecentAdapter == false) {
                        emojiViewModel.addRecentEmoji(emoji)
                    }
                    try {
                        (activity as MainActivity?)?.showButtonUseEmoji(emoji ?: "")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                R.id.btn_remove -> {
                    view.startAnimation(Animator.getFavoriteClickAnimation(mContext))
                    val dialog = AlertDialog.Builder(mContext)
                            .setTitle(R.string.title_delete_my_emojis)
                            .setMessage(R.string.content_delete_my_emojis)
                            .setPositiveButton(R.string.ok) { dialog, which ->
                                customEmojiViewModel.delete(emoji)
                                emojiViewModel.removeFavoriteEmoji(emoji)
                            }
                            .setNegativeButton(R.string.cancel) { dialog, which -> dialog.dismiss() }
                            .show()
                }
            }
        }
        adapter?.setCustomAdapter(true)
        rvEmoji.layoutManager = LinearLayoutManager(mContext)
        rvEmoji.setHasFixedSize(true)
        rvEmoji.isNestedScrollingEnabled = false
        rvEmoji.adapter = adapter
        btnHowToCreate.setOnClickListener { v ->
            v.startAnimation(Animator.getClickAnimation(mContext))
            val intent = Intent(mActivity, WebViewActivity::class.java)
            if (Locale.getDefault().language == "ko") {
                intent.putExtra("url", Constants.Url.YOUTUBE)
            } else {
                intent.putExtra("url", Constants.Url.YOUTUBE_EN)
            }
            startActivity(intent)
        }
        btnCreateEmoji.setOnClickListener { startActivity(Intent(mContext, CustomEmojiActivity::class.java)) }

        val scrollListener = ViewTreeObserver.OnScrollChangedListener {
            if (mIsVisible) {
                val scrollY = nestedScrollView.scrollY
                val criteria = ImageResolver.convertDpToPixel(Constants.ScrollOffset.toFloat(), mContext)
                if (scrollY >= criteria) {
                    (mContext as MainActivity).hideTitle()
                }
                if (scrollY < criteria) {
                    (mContext as MainActivity).showTitle()
                }
            }
        }
        nestedScrollView.viewTreeObserver.addOnScrollChangedListener(scrollListener)
        nestedScrollView.addOnAttachStateChangeListener(object: View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View?) {

            }
            override fun onViewDetachedFromWindow(v: View?) {
                nestedScrollView.viewTreeObserver.removeOnScrollChangedListener(scrollListener)
            }
        })

        return view
    }

    private fun subscribeObservers() {
        emojiViewModel.favoriteEmojis.observe(viewLifecycleOwner, Observer { favoriteEmojiList ->
            if (isAdded && favoriteEmojiList != null) {
                adapter?.favoriteEmojiList = favoriteEmojiList
            }
        })
        customEmojiViewModel.customEmojiList.observe(viewLifecycleOwner, Observer { customEmojis ->
            val customEmojiList: ArrayList<String> = ArrayList()
            for (emoji in customEmojis) {
                customEmojiList.add(emoji.emoji)
            }
            adapter?.list = customEmojiList
            adapter?.notifyDataSetChanged()
        })

        val restored = storage.getBoolean("restore_custom_emojis", false)
        if (!restored) {
            customEmojiViewModel.oldCustomEmojiList.observe(viewLifecycleOwner, Observer { oldCustomEmojis ->
                try {
                    for (emoji in oldCustomEmojis) {
                        customEmojiViewModel.create(emoji.emoji)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                storage.put("restore_custom_emojis", true)
            })
        }

    }

    override fun scrollTop() {
        try {
            nestedScrollView.fling(0)
            nestedScrollView.fullScroll(View.FOCUS_UP)
            nestedScrollView.scrollTo(0, 0)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onScrollTop(scrollTop: ScrollTop) {
        if (scrollTop.categoryId == "custom") {
            scrollTop()
        }
    }

    companion object {
        @JvmStatic
        fun create(): CustomFragment {
            return CustomFragment()
        }
    }
}