package com.wishpoke.fanciticon.presenation.view.chat

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Vibrator
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.AbsListView.OnScrollListener.SCROLL_STATE_FLING
import android.widget.AbsListView.OnScrollListener.SCROLL_STATE_IDLE
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ShareCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.OnFocusChange
import com.airbnb.lottie.LottieAnimationView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.ActivityResultEvent
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.chat.Chat
import com.wishpoke.fanciticon.data.model.chat.ChatRoom
import com.wishpoke.fanciticon.data.model.emoji.Emoji
import com.wishpoke.fanciticon.data.model.response.ObjectResponse
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.presenation.controller.SingleImageController
import com.wishpoke.fanciticon.presenation.resource.SingleResponse
import com.wishpoke.fanciticon.presenation.resource.ui.MarginItemDecoration
import com.wishpoke.fanciticon.presenation.view.general.WebViewActivity
import com.wishpoke.fanciticon.util.*
import com.wishpoke.fanciticon.util.constants.ChatActionTypes
import com.wishpoke.fanciticon.util.constants.Constants
import com.wishpoke.fanciticon.util.constants.PrefKeys
import com.wishpoke.fanciticon.util.extention.createDialog
import com.wishpoke.fanciticon.util.extention.dp2px
import com.wishpoke.fanciticon.util.extention.isVisibleWhen
import com.wishpoke.fanciticon.util.listener.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.koin.android.viewmodel.ext.android.viewModel
import ua.naiksoftware.stomp.dto.StompMessage
import java.io.File
import java.util.*


class ChatActivity : BaseActivity() {

    @BindView(R.id.tv_member)
    lateinit var tvMember: TextView
    @BindView(R.id.tv_room_title)
    lateinit var tvRoomTitle: TextView
    @BindView(R.id.av_like)
    lateinit var avLike: LottieAnimationView
    @BindView(R.id.et_message)
    lateinit var etMessage: EditText
    @BindView(R.id.rv_chat)
    lateinit var rvChat: RecyclerView
    @BindView(R.id.pager)
    lateinit var pager: ViewPager
    @BindView(R.id.rv_emoji_category)
    lateinit var rvEmojiCategory: RecyclerView
    @BindView(R.id.ll_send)
    lateinit var llSend: View
    @BindView(R.id.ll_photo)
    lateinit var llPhoto: View
    @BindView(R.id.iv_photo)
    lateinit var ivPhoto: View
    @BindView(R.id.iv_emoji)
    lateinit var ivEmoji: View
    @BindView(R.id.iv_thumbnail)
    lateinit var ivThumbnail: ImageView

    @BindView(R.id.ll_preview_container)
    lateinit var llPreviewContainer: View
    @BindView(R.id.iv_preview_thumbnail)
    lateinit var ivPreviewThumbnail: ImageView
    @BindView(R.id.tv_preview_name)
    lateinit var tvPreviewName: TextView
    @BindView(R.id.tv_preview_message)
    lateinit var tvPreviewMessage: TextView

    private var startTime: Long = 0
    private var MAX_DURATION = 300
    private var autoScrolled = false
    private var isKeyboardOpen = false
    private var isKicked = false // 강퇴선고 받았을때 메세지 수신 못하게 하기위해
    private var isFirstLoad = true // 처음에 예전메세지를 불러왔을때 스크롤을 아래에 고정하기 위해
    private var created = false // 방장 만들고 채팅 시작하기 위함
    private var oldState = 0

    private lateinit var emojiCategoryAdapter: ChatEmojiCategoryAdapter
    private lateinit var chatAdapter: ChatAdapter
    private lateinit var singleImageController: SingleImageController
    private lateinit var badWords: Array<String>
    private lateinit var hexColors: Array<String>

    private lateinit var pagerAdapter: ChatEmojiPagerAdapter

    val mChatViewModel: ChatViewModel by viewModel()

    private var chatRoom: ChatRoom? = null
    private var timer: Timer? = null

    inline fun <reified T> Gson.fromJson(json: String) = fromJson<T>(json, object: TypeToken<T>() {}.type)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        ButterKnife.bind(this)
        created = intent.getBooleanExtra("created", false)
        badWords = resources.getStringArray(R.array.bad_words)

        chatRoom = EventBusManager.getInstance().clearAccept(ChatRoom::class.java) as? ChatRoom
        Logger.e("Test", "chatRoom: ${Gson().toJson(chatRoom)}")
        if (chatRoom == null) finish()

        init()
        loadPreviousChat()
        mChatViewModel.connectSocket(object: SocketStateListener {
            override fun onSocketOpened() {
                subscribeChatRoom()
            }
        })

        // fontColor 찾기
        mChatViewModel.getChatRoomMembers(chatRoom?.roomId)

        subscribeObservers()
    }

    private fun subscribeObservers() {
        mChatViewModel.file.observe(this, androidx.lifecycle.Observer {
            if (it.isSuccessful()) {
                singleImageController.fileName = it.data!!
                ivThumbnail.visibility = View.VISIBLE
                ivPhoto.visibility = View.GONE
            }
            if (it.isFailed()) {
                Toaster.show(mContext, R.string.error_failed_to_upload_image_to_server, it.throwable?.code)
            }
        })
        mChatViewModel.chatRoomMembers.observe(this, androidx.lifecycle.Observer {
            if (it.isSuccessful()) {
                for (chatMember in it.data!!) {
                    if (chatMember.userId == storage.userId) {
                        chatRoom?.fontColorType = chatMember.fontColorType
                    }
                }
            }
        })
        mChatViewModel.messages.observe(this, androidx.lifecycle.Observer {
            when (it.status) {
                LoadingState.Status.SUCCESS -> {
                    it.data?.let { messages ->
                        chatAdapter.setLoading(false)
                        chatAdapter.addList(0, messages)
                        chatAdapter.haveNoMessagesToLoad = messages.isEmpty()
                        if (isFirstLoad) {
                            scrollToBottom()
                            isFirstLoad = false
                        }
                    }
                }
                LoadingState.Status.FAILED -> {
                    chatAdapter.setLoading(false)
                }
            }
        })

        mChatViewModel.togglePush.observe(this, androidx.lifecycle.Observer {
            when (it.status) {
                LoadingState.Status.SUCCESS -> {
                    chatRoom?.pushOn = if (it.data ?: false) 1 else 0
                    Toaster.show(mContext, R.string.toast_applied_changes)
                }
                LoadingState.Status.FAILED -> {
                    chatRoom?.pushOn = if (chatRoom?.pushOn == 0) 1 else 0
                }
            }
        })
    }

    private fun loadPreviousChat() {
        if (!chatAdapter.isLoading() && !chatAdapter.haveNoMessagesToLoad && !created) {
            chatAdapter.setLoading(true)
            mChatViewModel.loadMessages(chatRoom?.roomId, chatAdapter.getFirstMessageNo())
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun init() {
        hexColors = resources.getStringArray(R.array.chat_colors)
        tvRoomTitle.text = chatRoom?.title
        adLoader = FacebookAdLoader(this)

        singleImageController = SingleImageController(this, object: RequestListener<File?> {
            override fun onSuccess(result: File?) {
                // 파일 업로드
                if (result != null) {
                    val requestOptions = RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)

                    Glide.with(mActivity).load(singleImageController.file)
                            .apply(requestOptions)
                            .into(ivThumbnail)

                    mChatViewModel.uploadFile(result)
                }

            }

            override fun onFailure() {
                // 파일 첨부 실패
                Toaster.show(mContext, R.string.error_failed_to_upload_image)
            }
        }, 1, 1)
        tvMember.text = (chatRoom?.memberCnt ?: 0).toString()

        chatAdapter = ChatAdapter(mContext, chatRoom?.createTime
                ?: 0)
        chatAdapter?.itemClick = ItemClick { v, position ->
            val chat = chatAdapter.getItemAt(position)
            when (v.id) {
                R.id.iv_image -> {
                    chat.fileName?. let {
                        openImageViewer(it)
                    }
                }
                R.id.ll_metadata -> {
                    val url = Validator.extractUrl(chat.msg)
                    val intent = Intent(mContext, WebViewActivity::class.java)
                    intent.putExtra("url", url)
                    mContext.startActivity(intent)
                }
            }
        }
        val layoutManager = LinearLayoutManager(this)
        rvChat.layoutManager = layoutManager
        rvChat.itemAnimator?.changeDuration = 0
        rvChat.adapter = chatAdapter
        rvChat.setOnTouchListener { _, ev ->
            if (ev.action == MotionEvent.ACTION_UP) {
                startTime = System.currentTimeMillis()
            } else if (ev.action == MotionEvent.ACTION_DOWN) {
                if (System.currentTimeMillis() - startTime <= MAX_DURATION) {
                    //DOUBLE TAP
                    hideEmojiKeyboard()
                    hideKeyboard()
                    return@setOnTouchListener true
                }
            }
            return@setOnTouchListener false
        }
        rvChat.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == SCROLL_STATE_IDLE && (oldState != SCROLL_STATE_FLING || chatAdapter.views.size > 100)) {
                    if (chatAdapter.list.size > 0) {
                        chatAdapter.updateVisiblePosition(layoutManager.findFirstVisibleItemPosition() - 6,
                                layoutManager.findLastVisibleItemPosition() + 6)
                    }
                }
                oldState = newState
            }
        })

        rvChat.addItemDecoration(MarginItemDecoration(0, mContext.dp2px(-2F), 0, 0))

        rvChat.itemAnimator?.changeDuration = 0
        rvChat.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(-1)) {
                    // 이전 채팅 불러오기
                    loadPreviousChat()
                }
                if (!recyclerView.canScrollVertically(1)) {
                    hidePreviewContainer()
                }
            }
        })

        // 이모티콘 화면
        pagerAdapter = ChatEmojiPagerAdapter(supportFragmentManager)
        pagerAdapter.categories = mMainViewModel.categoriesForChat
        pager.adapter = pagerAdapter
        pager.addOnPageChangeListener(object: ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                emojiCategoryAdapter.activePosition = position
                rvEmojiCategory.smoothScrollToPosition(position)
                emojiCategoryAdapter.notifyDataSetChanged()

                storage.put("ChatCategoryPosition", position)
            }
        })

        // 이모티콘 카테고리
        emojiCategoryAdapter = ChatEmojiCategoryAdapter(this)
        emojiCategoryAdapter.itemClick = ItemClick { v, position ->
            v.startAnimation(Animator.getClickAnimation(mContext))
            pager.currentItem = position
        }
        // 최근에 사용한 카테고리를 기본으로 열기
        emojiCategoryAdapter.setList(mMainViewModel.categoriesForChat)
        emojiCategoryAdapter.loadRecentActivePosition()
        pager.currentItem = emojiCategoryAdapter.activePosition


        val side = mActivity.dp2px(6F)
        val topBottom = mActivity.dp2px(8F)
        val decoration = MarginItemDecoration(side, topBottom, side, topBottom)
        rvEmojiCategory.addItemDecoration(decoration)
        rvEmojiCategory.setHasFixedSize(true)
        rvEmojiCategory.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rvEmojiCategory.itemAnimator?.changeDuration = 0
        rvEmojiCategory.adapter = emojiCategoryAdapter

        etMessage.imeOptions = EditorInfo.IME_ACTION_SEND
        etMessage.setRawInputType(InputType.TYPE_CLASS_TEXT)
        etMessage.setOnEditorActionListener { v, actionId, event ->
            return@setOnEditorActionListener if (actionId == EditorInfo.IME_ACTION_SEND) {
                // 메세지 보내기
                sendMessage()
                true
            } else {
                false
            }
        }
        etMessage.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                val message = p0.toString()
                var isBad = false
                for (badWord in badWords) {
                    if (message.contains(badWord)) {
                        isBad = true
                        break
                    }
                }
                etMessage.setTextColor(resources.getColor(if (isBad) {
                    R.color.pink
                } else {
                    R.color.textBlack
                }))
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
        KeyboardUtils.addKeyboardToggleListener(this) { isVisible ->
            isKeyboardOpen = rvEmojiCategory.visibility != View.GONE || isVisible }

        // 이모티콘 창 숨기기
        hideEmojiKeyboard()


    }

    override fun onResume() {
        storage.put("ChatActivity", true)
        super.onResume()
        try {
            timer = Timer()
            chatAdapter.notifyItemChanged(0)
            timer?.scheduleAtFixedRate(object: TimerTask() {
                override fun run() {
                    if (chatAdapter != null) {
                        runOnUiThread {
                            chatAdapter.notifyItemChanged(0)
                        }

                    }
                }
            }, 0, 1000 * 60)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onPause() {
        Logger.e("Test", "-- onPause");
        storage.put("ChatActivity", false)
        super.onPause()
    }

    override fun onStart() {
        super.onStart()
        Logger.e("Test", "-- onStart");
        storage.put("ChatActivity", true)
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        timer?.cancel()
        EventBus.getDefault().unregister(this)
    }

    private fun subscribeChatRoom() {
        /*
          (enterChatRoom(conenctSocket)) + joinChatRoom
         */
        chatRoom?. let {
            // 구독하기
            mChatViewModel.enterChatRoom(it.roomId, object: ValueCallback<StompMessage> {
                override fun onDone(message: StompMessage) {
                    if (TestUtil.isDebugMode()) {
                        Logger.e("Test", "Message: ${message.payload}")
                    }
                    // 메세지 표시하기
                    val response = Gson().fromJson<ObjectResponse<Chat>>(message.payload)
                    val chat = response.get()
                    if (!isKicked && chat.actionType != ChatActionTypes.KICK && chat.isNotEmpty()) {
                        chatAdapter.addItem(chat)

                        val linearLayoutManager = rvChat.layoutManager as LinearLayoutManager
                        val lastVisiblePosition = linearLayoutManager.findLastCompletelyVisibleItemPosition()

                        if (chatAdapter.itemCount - lastVisiblePosition < 5 || chat.userId == storage.userId) {
                            scrollToBottom()
                        } else {
                            showPreviewContainer(chat)
                        }
                    }
                    when (chat.actionType) {
                        ChatActionTypes.KICK,
                        ChatActionTypes.EXIT -> {
                            it.memberCnt -= 1
                            tvMember.text = (chatRoom?.memberCnt ?: 0).toString()
                            if (chat.userId == storage.userId && chat.actionType == ChatActionTypes.KICK) {
                                // 내가 퇴장 당했을땐 다이얼로그를 띄운다.
                                isKicked = true
                                mDialog = mActivity.createDialog(
                                        message = R.string.dialog_chat_kicked,
                                        positiveBlock = {
                                            finish()
                                        },
                                        cancelable = false
                                ).apply { show() }
                            }
                        }
                        ChatActionTypes.ENTER -> {
                            it.memberCnt += 1
                            tvMember.text = (chatRoom?.memberCnt ?: 0).toString()
                        }
                    }
                }

            })
            // 아직 입장하지 않은상태라면 입장처리
            if (chatRoom?.joined() == false || created) {
                chatRoom?.stageName = chatRoom?.tempStageName

                val fcmToken = storage.getString(PrefKeys.FCM_TOKEN, "")
                mChatViewModel.joinChatRoom(chatRoom, fcmToken)
            }

        }
    }

    @OnFocusChange(R.id.et_message)
    fun onFocusChange(v: View, isFocused: Boolean) {
        if (isFocused) {
            hideEmojiKeyboard()
        } else {
            showEmojiKeyboard()
        }
    }

    @OnClick(R.id.btn_back, R.id.iv_member, R.id.tv_member, R.id.ll_send, R.id.btn_more, R.id.av_like,
            R.id.ll_photo, R.id.iv_emoji, R.id.et_message, R.id.ll_preview_container)
    fun onClick(v: View) {
        when (v.id) {
            R.id.btn_back -> onBackPressed()
            R.id.iv_member,
            R.id.tv_member -> {
                // 채팅 유저 목록
                EventBusManager.getInstance().cast(chatRoom)
                val intent = Intent(this, ChatMemberActivity::class.java)
                startActivityForResult(intent, ChatMemberActivity.REQUEST_EXIT)
            }
            R.id.ll_send -> {
                v.startAnimation(Animator.getClickAnimation(mContext))
                sendMessage()
            }
            R.id.btn_more -> {
                // 더보기
                val buttonLabels = arrayOf(
                        getString(R.string.report),
                        getString(R.string.chat_menu_invite),
                        getString(R.string.chat_menu_exit),
                        getString(if (chatRoom?.pushOn == 1) {
                            R.string.chat_menu_push_off
                        } else {
                            R.string.chat_menu_push_on
                        }))
                mDialog = DialogManager.getInstance().createNChoiceDialog(mContext, buttonLabels, object: DialogClickListener<Int> {
                    override fun onClick(viewId: Int) {
                        mDialog?.dismiss()
                        when (viewId) {
                            R.id.btn_1 -> {
                                // TODO: 신고하기
                            }
                            R.id.btn_2 -> {
                                // 친구 초대하기
                                /*
                                    # TestURL: http://welcometo.inssaticon.com/20200517TLUAGQkeXqRatCgjA1j6
                                    # TestChromeIntent: intent://chat#Intent;scheme=inssaticon;action=android.intent.action.VIEW;action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;package=com.wishpoke.fanciticon;S.roomId=20200517TLUAGQkeXqRatCgjA1j6;end;
                                    # TestCustomSchemeURL: inssaticon://chat?roomId=2020050696OZW00p5nzq9OC40a77
                                 */
                                val url = "${Constants.Url.CHAT_SHARE_PREFIX}${chatRoom?.roomId}"
                                val shareText = String.format(getString(R.string.chat_invite),
                                        chatRoom?.stageName ?: "", chatRoom?.title ?: "", url)
                                ShareCompat.IntentBuilder.from(mActivity)
                                        .setType("text/plain")
                                        .setChooserTitle(getString(R.string.chat_menu_invite))
                                        .setText(shareText)
                                        .startChooser()
                                mChatViewModel.shareChatRoom(chatRoom?.roomId)
                            }
                            R.id.btn_3 -> {
                                // 톡방에서 나가기
                                val isChatMaster = chatRoom?.roomHost == storage.userId
                                val message = if (isChatMaster) {
                                    R.string.dialog_chat_exit_master
                                } else {
                                    R.string.dialog_chat_exit
                                }
                                mDialog = mActivity.createDialog(
                                        message = message,
                                        positiveLabel = R.string.chat_kick_positive,
                                        negativeLabel = R.string.chat_kick_negative,
                                        positiveBlock = {
                                            // 퇴장처리
                                            if (isChatMaster) {
                                                mChatViewModel.closeChatRoom(chatRoom?.roomId)
                                                chatRoom?.roomId?.let {
                                                    ChatRoomAdapter.deletedRoomIds.add(it)
                                                }
                                            } else {
                                                mChatViewModel.exitChatRoom(chatRoom?.roomId, chatRoom?.stageName)
                                            }
                                            finish()
                                        }
                                ).apply { show() }

                            }
                            R.id.btn_4 -> {
                                // 알림 ON/OFF
                                mChatViewModel.togglePush(chatRoom?.roomId)
                            }
                        }
                    }
                })
                mDialog?.show()
            }
            R.id.iv_emoji -> {
                val imm: InputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)

                etMessage.postDelayed({
                    val isVisibleNow = rvEmojiCategory.visibility == View.VISIBLE
                    if (isVisibleNow) {
                        hideEmojiKeyboard()
                    } else {
                        showEmojiKeyboard()
                    }
                }, 200)
            }
            R.id.et_message -> {
                showKeyboard()
            }
            R.id.ll_photo -> {
                // 사진 첨부
                if (ivPhoto.visibility == View.VISIBLE) {
                    ivPhoto.startAnimation(Animator.getClickAnimation(mContext))
                } else {
                    ivThumbnail.startAnimation(Animator.getClickAnimation(mContext))
                }
                val buttonLabels = if (ivThumbnail.visibility == View.VISIBLE) {
                    arrayOf(getString(R.string.camera), getString(R.string.gallery), getString(R.string.delete))
                } else {
                    arrayOf(getString(R.string.camera), getString(R.string.gallery))
                }
                mDialog = DialogManager.getInstance().createNChoiceDialog(mContext, buttonLabels, object: DialogClickListener<Int> {
                    override fun onClick(value: Int) {
                        when (value) {
                            R.id.btn_1 -> {
                                // 카메라
                                onRequiredPermissionChecked(true, object: PermissionDoneListener {
                                    override fun onDone(granted: Boolean) {
                                        if (granted) {
                                            singleImageController.setDefaultImage(false)
                                            singleImageController.takeCamera()
                                        }
                                    }
                                })
                            }
                            R.id.btn_2 -> {
                                // 갤러리
                                onRequiredPermissionChecked(false, object: PermissionDoneListener {
                                    override fun onDone(granted: Boolean) {
                                        if (granted) {
                                            singleImageController.setDefaultImage(false)
                                            singleImageController.takeGallery()
                                        }
                                    }
                                })
                            }
                            R.id.btn_3 -> {
                                // 삭제하기
                                mDialog?.dismiss()
                                singleImageController.file = null
                                singleImageController.fileName = null
                                ivThumbnail.visibility = View.GONE
                                ivPhoto.visibility = View.VISIBLE
                            }
                        }
                    }
                })
                mDialog?.show()
            }
            R.id.av_like -> {
                // 좋아요
                val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                vibrator.vibrate(Constants.VibrateTime)
                if (storage.countChatRoomLike()) {
                    adLoader?.showInterstitial()
                }
                avLike.playAnimation()
                chatRoom?. let {
                    it.likeCnt += 1
                }
                mChatViewModel.likeChatRoom(chatRoom?.roomId)
            }
            R.id.ll_preview_container -> scrollToBottom()
        }
    }

    private fun showPreviewContainer(chat: Chat) {
        llPreviewContainer.startAnimation(Animator.getClickSlightAnimation(mContext))
        llPreviewContainer.visibility = View.VISIBLE
        tvPreviewName.text = chat.stageName
        tvPreviewMessage.text = chat.msg

        chat.fontColorType = if (chat.fontColorType >= hexColors.size)
            0 else chat.fontColorType // ArrayOutOfIndex 방지
        val color = Color.parseColor(hexColors[chat.fontColorType])
        tvPreviewName.setTextColor(color)

        ivPreviewThumbnail.isVisibleWhen(!chat.fileName.isNullOrEmpty())
        if (!chat.fileName.isNullOrEmpty()) {
            Glide.with(this).asBitmap()
                    .load("${Constants.Url.BASE}/fileApi/getFile?downloadFileName=" + chat.fileName)
                    .into(object: CustomTarget<Bitmap>() {
                        override fun onLoadCleared(placeholder: Drawable?) {
                        }
                        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                            ivPreviewThumbnail.setImageBitmap(resource)
                        }
                    })
        }
    }

    private fun hidePreviewContainer() {
        llPreviewContainer.visibility = View.GONE
    }

    override fun onBackPressed() {
        if (isKeyboardOpen) {
            hideEmojiKeyboard()
            hideKeyboard()
        } else {
            super.onBackPressed()
        }
    }

    private fun scrollToBottom() {
        // 끝으로 스크롤 이동
        autoScrolled = true
        val positionEnd = chatAdapter.itemCount ?: 0 - 1
        if (positionEnd > -1) {
            val layoutManager = rvChat.layoutManager as LinearLayoutManager
            layoutManager.smoothScrollToPosition(rvChat, null, positionEnd)
        }
        hidePreviewContainer()
    }

    private fun showSendButton() {
        if (llSend.visibility == View.GONE) {
            llSend.alpha = 0F
            ivEmoji.visibility = View.GONE
            llPhoto.visibility = View.GONE
            llSend.visibility = View.VISIBLE
            llSend.animate().alpha(0F).alphaBy(1F).setDuration(200).start()
            llSend.startAnimation(Animator.getClickAnimation(mContext))
        }
    }

    private fun hideSendButton() {
        if (llSend.visibility == View.VISIBLE) {
            llSend.visibility = View.GONE
            llSend.alpha = 0F
            ivEmoji.visibility = View.VISIBLE
            llPhoto.visibility = View.VISIBLE
        }
    }

    private fun showEmojiKeyboard() {
        rvEmojiCategory.visibility = View.VISIBLE
        pager.visibility = View.VISIBLE

        isKeyboardOpen = true
    }

    private fun hideEmojiKeyboard() {
        rvEmojiCategory.visibility = View.GONE
        pager.visibility = View.GONE

        isKeyboardOpen = false
    }

    private fun showKeyboard() {
        // 이모티콘 창 숨기기
        hideEmojiKeyboard()

        // 키보드 보이기
        val imm: InputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(etMessage, 0)

        isKeyboardOpen = true
    }

    private fun hideKeyboard() {
        val imm: InputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(etMessage.windowToken, 0)

        isKeyboardOpen = false
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onClickEmoji(emoji: Emoji) {
        showSendButton()
        etMessage.text.append(emoji.emoji)
    }

    private fun sendMessage() {
        if (etMessage.currentTextColor != resources.getColor(R.color.pink)) {
            hideSendButton() // 메세지 보내고나서 보내기 버튼 숨기고 원래대로..
            val message = etMessage.text.toString()

            if (!message.isNullOrEmpty() || !singleImageController.fileName.isNullOrEmpty()) {
                mChatViewModel.sendMessage(chatRoom, message, singleImageController.fileName)
                etMessage.setText("")
                singleImageController.fileName = ""
                ivPhoto.visibility = View.VISIBLE
                ivThumbnail.visibility = View.GONE
                scrollToBottom()
            }
        } else {
            Toaster.show(mContext, R.string.toast_chat_bad_word)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        singleImageController.onActivityResult(ActivityResultEvent(requestCode, resultCode, data))
        if (requestCode == ChatMemberActivity.REQUEST_EXIT && resultCode == Activity.RESULT_OK) {
            finish()
        }
    }
}