package com.wishpoke.fanciticon.presenation.view.pung.reply

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import coil.load
import coil.size.Scale
import coil.transform.RoundedCornersTransformation
import com.airbnb.lottie.LottieAnimationView
import com.bumptech.glide.Glide
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.MetaData
import com.wishpoke.fanciticon.data.model.pung.Post
import com.wishpoke.fanciticon.data.model.pung.Post.Reply
import com.wishpoke.fanciticon.presenation.base.BaseRecyclerViewAdapter
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import com.wishpoke.fanciticon.util.*
import com.wishpoke.fanciticon.util.YoutubeUtils.Companion.getThumbnail
import com.wishpoke.fanciticon.util.YoutubeUtils.Companion.isYoutube
import com.wishpoke.fanciticon.util.extention.dp2px
import com.wishpoke.fanciticon.util.extention.isVisibleWhen
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jsoup.Jsoup
import java.util.*

class ReplyAdapter(private val activity: Activity) : BaseRecyclerViewAdapter<RecyclerView.ViewHolder, Reply>() {

    private val uiScope = CoroutineScope(Dispatchers.Main)
    private var post: Post? = null
    private val storage: AppStorage = AppStorage(activity)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TypeHeader -> {
                val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_reply, parent, false)
                PostViewHolder(itemView)
            }
            else -> {
                val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_reply2, parent, false)
                ViewHolder(itemView)
            }
        }
    }

    fun update(reply: Reply) {
        val index = list.indexOfFirst { it.replyNo == reply.replyNo }
        if (index != -1) {
            list[index] = reply
            notifyDataSetChanged()
        }
    }

    fun delete(replyNo: Int) {
        val index = list.indexOfFirst { it.replyNo == replyNo }
        if (index != -1) {
            list.removeAt(index)
            notifyDataSetChanged()
        }
    }

    fun setPost(post: Post?) {
        this.post = post
        post?.userName = UDTextUtils.decode(post?.userName)
        post?.comment = UDTextUtils.decode(post?.comment)
        post?.linkUrl = UDTextUtils.decode(post?.linkUrl)
        notifyDataSetChanged()
    }

    fun getPost(): Post? {
        return this.post
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        with (holder) {
            when (this) {
                is PostViewHolder -> bind(post ?: Post())
                is ViewHolder -> {
                    val reply = getItemAt(position)
                    reply.userName = UDTextUtils.decode(reply.userName)
                    reply.comment = UDTextUtils.decode(reply.comment)

                    tvUsername.text = UDTextUtils.decode(reply.userName)
                    tvComment.text = UDTextUtils.decode(reply.comment)
                    tvScore.text = Util.formatLikeCount(reply.likeCnt ?: 0)
                    tvTime.text = Moment.getWriteTime(Date(reply.createTime * 1000))

                    // 이미지 설정
                    if (!reply.fileName.isNullOrEmpty()) {
                        ivImage.load(Util.getImageDownloadUrl(reply.fileName)) {
                            crossfade(true)
                            scale(Scale.FILL)
                            placeholder(R.mipmap.image_placeholder)
                            transformations(RoundedCornersTransformation(activity.dp2px(10F).toFloat()))
                        }
                    } else {
                        ivImage.setImageDrawable(null)
                    }
                    ivImage.isVisibleWhen(!reply.fileName.isNullOrEmpty())

                    // 메타 데이터 설정
                    val linkUrl = Util.extractUrlFromText(reply.comment)
                    if (!linkUrl.isNullOrEmpty()) {
                        if (isYoutube(linkUrl)) {
                            Glide.with(activity).load(getThumbnail(linkUrl ?: "")).into(ivVideo)
                        } else {
                            uiScope.launch {
                                val radius = activity.dp2px(4F).toFloat()
                                fetchMetadata(radius, linkUrl ?: "", ivMetadataImage, tvMetadataTitle, tvMetadataUrl, rlMetadata)
                            }
                        }
                    }
                    rlMetadata.isVisibleWhen(!linkUrl.isNullOrEmpty() && !isYoutube(linkUrl))

                    // 유튜브 비디오 설정
                    val youtubeUrl = if (isYoutube(linkUrl)) linkUrl else reply.movieName
                    clVideo.isVisibleWhen(!youtubeUrl.isNullOrEmpty())
                    ivVideo.isVisibleWhen(!youtubeUrl.isNullOrEmpty())
                    ivPlayVideo.isVisibleWhen(!youtubeUrl.isNullOrEmpty())
                    youtubeUrl?.let {
                        ivVideo.load(getThumbnail(it)) {
                            crossfade(true)
                            scale(Scale.FILL)
                            placeholder(R.mipmap.image_placeholder)
                            transformations(RoundedCornersTransformation(activity.dp2px(2F).toFloat()))
                        }
                    }

                    val clickListener = View.OnClickListener {
                        itemClick?.onItemClick(it, position)
                    }
                    avLike.setOnClickListener {
                        like(position)
                        itemClick?.onItemClick(it, position)
                    }
                    rlMetadata.setOnClickListener(clickListener)
                    ivPlayVideo.setOnClickListener(clickListener)
                    ivMore.setOnClickListener(clickListener)
                }
                else -> {}
            }
        }
        
        
    }

    private fun like(position: Int) {
        getItemAt(position).likeCnt = getItemAt(position).likeCnt.plus(1)
        notifyItemChanged(position)
    }

    inner class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.tv_username)
        lateinit var tvUsername: TextView
        @BindView(R.id.tv_time)
        lateinit var tvTime: TextView
        @BindView(R.id.tv_comment)
        lateinit var tvComment: TextView
        @BindView(R.id.tv_count_comment)
        lateinit var tvCountComment: TextView
        @BindView(R.id.tv_count_share)
        lateinit var tvCountShare: TextView
        @BindView(R.id.tv_score)
        lateinit var tvScore: TextView
        @BindView(R.id.ll_like)
        lateinit var llLike: LinearLayout
        @BindView(R.id.ll_share)
        lateinit var llShare: LinearLayout
        @BindView(R.id.iv_share)
        lateinit var ivShare: ImageView
        @BindView(R.id.iv_image)
        lateinit var ivImage: ImageView
        @BindView(R.id.iv_rank)
        lateinit var ivRank: ImageView
        @BindView(R.id.av_like)
        lateinit var avLike: LottieAnimationView
        @BindView(R.id.rl_metadata)
        lateinit var rlMetadata: RelativeLayout
        @BindView(R.id.iv_metadata_image)
        lateinit var ivMetadataImage: ImageView
        @BindView(R.id.tv_metdata_url)
        lateinit var tvMetadataUrl: TextView
        @BindView(R.id.tv_metdata_title)
        lateinit var tvMetadataTitle: TextView
        @BindView(R.id.iv_play_video)
        lateinit var ivPlayVideo: View
        @BindView(R.id.iv_video)
        lateinit var ivVideo: ImageView

        init {
            ButterKnife.bind(this, itemView)
        }

        fun bind(post: Post) {
            tvUsername.text = post.userName
            tvComment.text = post.comment
            tvScore.text = Util.formatLikeCount(post.likeCnt)
            tvTime.text = Moment.getPungTime(post.createTime)
            tvCountComment.text = Util.formatLikeCount(post.replyCnt)
            tvCountShare.text = Util.formatLikeCount(post.shareCnt)

            when (post?.rank) {
                1 -> ivRank.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_gold))
                2 -> ivRank.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_silver))
                3 -> ivRank.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_bronze))
                else -> ivRank.visibility = View.GONE
            }

            // 사진 설정
            if (!post.fileName.isNullOrEmpty()) {
                ivImage.load(Util.getImageDownloadUrl(post.fileName)) {
                    crossfade(true)
                    scale(Scale.FILL)
                    placeholder(R.mipmap.image_placeholder)
                    transformations(RoundedCornersTransformation(activity.dp2px(10F).toFloat()))
                }
            } else {
                ivImage.setImageDrawable(null)
            }
            ivImage.isVisibleWhen(!post.fileName.isNullOrEmpty())

            // 메타 데이터 설정
            val hasUrl = !post.metaDataUrl.isNullOrEmpty() && !isYoutube(post.metaDataUrl)
            if (hasUrl) {
                uiScope.launch {
                    val radius = activity.dp2px(4F).toFloat()
                    fetchMetadata(radius, post.metaDataUrl!!, ivMetadataImage, tvMetadataTitle, tvMetadataUrl, rlMetadata)
                }
            }
            rlMetadata.isVisibleWhen(hasUrl)
            // 유튜브 비디오 설정
            ivVideo.isVisibleWhen(!post.youtubeUrl.isNullOrEmpty())
            ivPlayVideo.isVisibleWhen(!post.youtubeUrl.isNullOrEmpty())
            if (post.youtubeUrl != null) {
                ivVideo.load(getThumbnail(post.youtubeUrl!!)) {
                    crossfade(true)
                    scale(Scale.FILL)
                    placeholder(R.mipmap.image_placeholder)
                    transformations(RoundedCornersTransformation(activity.dp2px(2F).toFloat()))
                }
            }
            val clickListener = View.OnClickListener {
                headerItemClick?.onItemClick(it, position)
            }
            llShare.setOnClickListener {
                it.startAnimation(Animator.getFavoriteClickAnimation(activity))
                val count = tvCountShare.text.toString().toInt() + 1
                tvCountShare.text = count.toString()
                // 게시글 공유
                val intent = Intent()
                intent.action = Intent.ACTION_SEND
                if (!post.fileName.isNullOrEmpty()) {
                    intent.type = "*/*"
                    intent.putExtra(Intent.EXTRA_STREAM,
                            ImageResolver.getLocalBitmapUri(activity, ImageResolver.getBitmapFromImageView(ivImage)))
                } else {
                    intent.type = "text/plain"
                }
                intent.putExtra(Intent.EXTRA_TEXT, post?.comment + "\n#인싸티콘")
                val chooser = Intent.createChooser(intent, activity.getString(R.string.app_name))
                activity.startActivity(chooser)
                post.shareCnt = count
                headerItemClick?.onItemClick(it, position)
            }
            llLike.setOnClickListener {
                val score = Util.parseInt(tvScore.text.toString())
                tvScore.text = (score + 1).toString()
                post.likeCnt = score + 1
                avLike.playAnimation()
                headerItemClick?.onItemClick(it, position)
            }
            rlMetadata.setOnClickListener(clickListener)
            ivPlayVideo.setOnClickListener(clickListener)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> TypeHeader
            else -> TypeItem
        }
    }

    override fun getRealPosition(position: Int): Int {
        return position - 1
    }

    override fun getAdapterPosition(position: Int): Int {
        return position + 1
    }

    override fun getItemCount(): Int {
        return list.size + 1
    }

    inner class ViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

        @BindView(R.id.tv_username)
        lateinit var tvUsername: TextView
        @BindView(R.id.tv_time)
        lateinit var tvTime: TextView
        @BindView(R.id.tv_comment)
        lateinit var tvComment: TextView
        @BindView(R.id.tv_score)
        lateinit var tvScore: TextView
        @BindView(R.id.av_like)
        lateinit var avLike: LottieAnimationView
        @BindView(R.id.iv_image)
        lateinit var ivImage: ImageView
        @BindView(R.id.rl_metadata)
        lateinit var rlMetadata: RelativeLayout
        @BindView(R.id.iv_metadata_image)
        lateinit var ivMetadataImage: ImageView
        @BindView(R.id.tv_metdata_url)
        lateinit var tvMetadataUrl: TextView
        @BindView(R.id.tv_metdata_title)
        lateinit var tvMetadataTitle: TextView
        @BindView(R.id.cl_video)
        lateinit var clVideo: View
        @BindView(R.id.iv_play_video)
        lateinit var ivPlayVideo: View
        @BindView(R.id.iv_video)
        lateinit var ivVideo: ImageView
        @BindView(R.id.iv_more)
        lateinit var ivMore: View

        init {
            ButterKnife.bind(this, view)
        }
    }

    private suspend fun fetchMetadata(radius: Float, url: String, ivMetadataImage: ImageView, tvMetadataTitle: TextView, tvMetadataUrl: TextView, rlMetadata: View) {
        withContext(Dispatchers.IO) {
            val metaData = MetaData()
            metaData.domain = Validator.getDomainName(url)
            try {
                val conn = Jsoup.connect(url)
                val doc = conn.get()
                val ogTags = doc.select("meta[property^=og:]")

                if (ogTags.isNotEmpty()) {
                    for (tag in ogTags) {
                        when (val text = tag.attr("property")) {
                            "og:title" -> metaData.title = tag.attr("content")
                            "og:description" -> metaData.desc = tag.attr("content")
                            "og:image" -> metaData.image = tag.attr("content")
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            withContext(Dispatchers.Main) {
                try {
                    ivMetadataImage.load(metaData.image) {
                        crossfade(true)
                        scale(Scale.FILL)
                        placeholder(R.mipmap.image_placeholder)
                        transformations(RoundedCornersTransformation(radius))
                    }
                    tvMetadataTitle.text = Util.shorten(metaData.title)
                    tvMetadataUrl.text = Util.shorten(metaData.domain)
                    rlMetadata.visibility = View.VISIBLE
                } catch (e: Exception) {}
            }
        }
    }

}