package com.wishpoke.fanciticon.presenation.base

import android.app.Application
import android.os.Build
import coil.Coil
import coil.ImageLoader
import coil.decode.GifDecoder
import coil.decode.ImageDecoderDecoder
import com.bumptech.glide.Glide
import com.facebook.ads.AudienceNetworkAds
import com.iapxk.sdvem.nkzspq.op.ISN
import com.wishpoke.fanciticon.di.*
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import com.wishpoke.fanciticon.util.TestUtil
import com.wishpoke.fanciticon.util.Util
import com.wishpoke.fanciticon.util.constants.Constants.AdisonAppKey
import com.wishpoke.fanciticon.util.constants.Constants.AdisonTestAppKey
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        ISN.Isni(applicationContext)
        initCoil()
        // Koin
        startKoin {
            androidContext(this@BaseApplication)
            modules(listOf(
                    appModule,
                    viewModelModule,
                    databaseModule,
                    useCaseModule,
                    repoModule))
        }
        AudienceNetworkAds.initialize(this)
    }

    private fun initCoil() {
        val imageLoader = ImageLoader.Builder(this.applicationContext)
                .availableMemoryPercentage(0.15)
                .componentRegistry {
                    if (Build.VERSION.SDK_INT >= 28) {
                        add(ImageDecoderDecoder())
                    } else {
                        add(GifDecoder())
                    }
                }
                .build()
        Coil.setImageLoader(imageLoader)
    }

    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        Glide.get(this).trimMemory(level)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        Glide.get(this).clearMemory()
    }

}