package com.wishpoke.fanciticon.presenation.base

import android.view.View
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.wishpoke.fanciticon.util.extention.isVisibleWhen
import com.wishpoke.fanciticon.util.listener.ItemClick

abstract class BaseRecyclerViewAdapter<VH : RecyclerView.ViewHolder, T: Any> : RecyclerView.Adapter<VH>() {
    companion object {
        const val TypeHeader = 1
        const val TypeItem = 2
        const val TypeLoading = 3
    }

    var list: ArrayList<T> = ArrayList()

    var columnCount = 1
    private var isLoading: Boolean = false
    var itemClick: ItemClick? = null
    var headerItemClick: ItemClick? = null
    var emptyStateView: View? = null
    var recyclerView: RecyclerView? = null
    var params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)

    open fun setLoading(isLoading: Boolean) {
        this.isLoading = isLoading
        notifyItemRangeChanged(itemCount - 1, columnCount)
    }

    open fun isLoading(): Boolean {
        return this.isLoading
    }

    open fun clear() {
        this.list = ArrayList()
        notifyDataSetChanged()
    }

    open fun setList(list: List<T>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    open fun addList(list: List<T>) {
        val initialSize = this.list.size + 1
        this.list.addAll(list)
        notifyItemRangeInserted(initialSize, list.size)
    }

    open fun addListDistinct(list: List<T>) {
        // 중복된 리스트는 추가하지 않음
        val initialSize = this.list.size + 1
        for (item in list) {
            if (!this.list.contains(item)) {
                this.list.add(item)
            }
        }
        val addedCount = list.size - initialSize
        if (addedCount > 0) {
            notifyItemRangeInserted(initialSize, addedCount)
        }
    }

    open fun addList(realPosition: Int, list: List<T>) {
        this.list.addAll(realPosition, list)
        notifyItemRangeInserted(getAdapterPosition(realPosition), list.size)
    }

    open fun addItem(item: T?) {
        item?.let {
            this.list.add(it)
            notifyItemInserted(this.list.size)
        }
    }

    open fun setItem(index: Int, item: T?) {
        item?.let {
            this.list[getRealPosition(index)] = item
            notifyItemChanged(index)
        }
    }

    open fun getItemAt(position: Int): T {
        return this.list[getRealPosition(position)]
    }

    fun addItem(item: T?, position: Int) {
        item?.let {
            this.list.add(getRealPosition(position), it)
            notifyDataSetChanged()
        }
    }

    fun removeItem(position: Int) {
        this.list.removeAt(getRealPosition(position))
        notifyItemRemoved(position)
    }

    fun changeItem(item: T?, position: Int) {
        item?.let {
            this.list[position] = it
            notifyItemChanged(position)
        }
    }

    open fun getRealPosition(position: Int): Int {
        return if (headerItemClick != null) position - 1 else position
    }

    open fun getAdapterPosition(position: Int): Int {
        return if (headerItemClick != null) position + 1 else position
    }

    fun showEmptyStateWhenEmpty() {
        val isEmpty = list.isEmpty()
        emptyStateView?.isVisibleWhen(isEmpty)
        recyclerView?.isVisibleWhen(!isEmpty)
    }

    fun showEmptyState(isEmpty: Boolean) {
        emptyStateView?.isVisibleWhen(isEmpty)
        recyclerView?.isVisibleWhen(!isEmpty)
    }

    override fun getItemCount(): Int {
        return if (headerItemClick != null) list.size + 1 else list.size
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            headerItemClick != null && position == 0 -> TypeHeader
            else -> TypeItem
        }
    }
}