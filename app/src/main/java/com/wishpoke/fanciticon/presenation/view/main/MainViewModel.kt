package com.wishpoke.fanciticon.presenation.view.main

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.wishpoke.fanciticon.BuildConfig
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.Banner
import com.wishpoke.fanciticon.data.model.Category
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.VersionInfo
import com.wishpoke.fanciticon.data.model.chat.ChatRoom
import com.wishpoke.fanciticon.data.model.response.ResultResponse
import com.wishpoke.fanciticon.data.repository.MainRepository
import com.wishpoke.fanciticon.domain.usecase.chat.GetChatRoomDetailUseCase
import com.wishpoke.fanciticon.presenation.base.BaseViewModel
import com.wishpoke.fanciticon.presenation.resource.ResponseHandler
import com.wishpoke.fanciticon.presenation.resource.SingleResponse
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File
import java.util.*

class MainViewModel(application: Application,
                    val getChatRoomDetailUseCase: GetChatRoomDetailUseCase) : BaseViewModel(application) {

    private val mainRepository = MainRepository(application)
    private var categories: List<Category> = ArrayList()
    private val compositeDisposable = CompositeDisposable()
    private val context = application

    private val _chatRoom = MutableLiveData<LoadingState<ChatRoom>>()
    val chatRoom: LiveData<LoadingState<ChatRoom>>
        get() = _chatRoom

    private val _versionInfo = MutableLiveData<LoadingState<VersionInfo>>()
    val versionInfo: LiveData<LoadingState<VersionInfo>>
        get() = _versionInfo

    private val _banners = MutableLiveData<LoadingState<List<Banner>>>()
    val banners: LiveData<LoadingState<List<Banner>>>
        get() = _banners

    var emojiCategoryStart = mainRepository.emojiCategoryStart // 이모티콘 관련 카테고리 시작점

    val recent: MutableLiveData<ArrayList<String>>
        get() = mainRepository.recent

    fun parseUpdateCategories() {
        val updateCategories = context.resources.getStringArray(R.array.new_categories)
        storage.saveUpdateCategories(BuildConfig.VERSION_CODE, updateCategories)
    }

    // 즐겨찾기 제외 다른 카테고리는 제거
    val categoriesForChat: List<Category>
        get() {
            if (categories.isEmpty()) {
                categories = mainRepository.categories
                emojiCategoryStart = mainRepository.emojiCategoryStart
            }
            val chatCategories: ArrayList<Category> = ArrayList(categories)
            // 즐겨찾기 제외 다른 카테고리는 제거
            val removeCategories = arrayOf("pung", "chat")
            for (id in removeCategories) {
                chatCategories.remove(Category(id))
            }
            return chatCategories
        }

    fun getCategories(): List<Category> {
        if (categories.isEmpty()) {
            categories = mainRepository.categories
            emojiCategoryStart = mainRepository.emojiCategoryStart
        }
        return categories
    }

    fun resetCategories() {
        categories = mainRepository.categories
    }

    fun getEmojiList(categoryId: String): List<String> {
        return mainRepository.getEmojiList(categoryId)
    }

    fun getBanners() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _banners.postValue(LoadingState.loading())
                _banners.postValue(ResponseHandler.handle(mainRepository.getBanners()))
            } catch (e: Exception) {
                _banners.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun getVersion() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _versionInfo.updateValue(mainRepository.getVersion())
            } catch (e: Exception) {
                _versionInfo.postValue(ResponseHandler.handle(e))
            }
        }
    }

    fun downloadFile(fileName: String, saveFileName: String, singleResponse: SingleResponse<File>) {
        val disposable = mainRepository.downloadFile(fileName, saveFileName, singleResponse)
        compositeDisposable.add(disposable)
    }

    fun getChatRoomDetail(roomId: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _chatRoom.updateValue(getChatRoomDetailUseCase(storage.userId, roomId))
            } catch (e: Exception) {
                _chatRoom.postValue(ResponseHandler.handle(e))
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}