package com.wishpoke.fanciticon.presenation.view.pung

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Vibrator
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.airbnb.lottie.LottieAnimationView
import com.google.gson.Gson
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.PungMore
import com.wishpoke.fanciticon.data.model.pung.Post
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.presenation.resource.ui.StickyHeaderItemDecoration
import com.wishpoke.fanciticon.presenation.view.pung.reply.PungReplyViewModel
import com.wishpoke.fanciticon.presenation.view.pung.reply.ReplyAdapter
import com.wishpoke.fanciticon.presenation.view.pung.reply.WritePungReplyActivity
import com.wishpoke.fanciticon.util.*
import com.wishpoke.fanciticon.util.constants.Constants
import com.wishpoke.fanciticon.util.constants.Errors
import com.wishpoke.fanciticon.util.extention.createDialog
import com.wishpoke.fanciticon.util.listener.DialogClickListener
import com.wishpoke.fanciticon.util.listener.ItemClick
import kotlinx.android.synthetic.main.activity_pung.*
import org.koin.android.viewmodel.ext.android.viewModel

class PungActivity : BaseActivity() {
    
    @BindView(R.id.rv_reply)
    lateinit var rvReply: RecyclerView

    private lateinit var replyAdapter: ReplyAdapter
    private var post: Post? = null
    private var reply: Post.Reply? = null
    private var requestPage = 0
    private var position = -1
    private var isMyPost = false
    private var passwordInput: String? = null
    private var replyPasswordInput: String? = null

    private val pungReplyViewModel: PungReplyViewModel by viewModel()
    private val pungViewModel: PungViewModel by viewModel()
    
    private var validateJob = ValidateJob.DELETE

    companion object {
        const val REQUEST_VIEW = 1011
    }
    
    private enum class ValidateJob {
        DELETE, UPDATE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pung)
        ButterKnife.bind(this)
        adLoader = FacebookAdLoader(this, banner_container)

        init()
        setPost()
        loadReplies()
        subscribeObservers()
        setClickListeners()
    }
    
    private fun init() {
        adLoader = FacebookAdLoader(this)

        post = Gson().fromJson(intent.getStringExtra("post"), Post::class.java)
        position = intent.getIntExtra("position", -1)
        isMyPost = post?.isCreator == 1 && storage.isLoggedIn
        
        // 댓글 더 불러오기
        rvReply.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                val lm = recyclerView.layoutManager as LinearLayoutManager
                val pastVisibleItems = lm.findFirstVisibleItemPosition()
                val totalItemCount = lm.itemCount
                val visibleItemCount = lm.childCount
                if (visibleItemCount + pastVisibleItems >= totalItemCount) {
                    loadReplies()
                }
                super.onScrollStateChanged(recyclerView, newState)
            }
        })
    }

    private fun subscribeObservers() {
        pungViewModel.validatePost.observe(this, Observer { 
            when (it.status) {
                LoadingState.Status.SUCCESS -> {
                    when (validateJob) {
                        ValidateJob.DELETE -> {
                            pungViewModel.deletePost(post?.postNo ?: 0, passwordInput)
                        }
                        ValidateJob.UPDATE -> {
                            EventBusManager.getInstance().cast(post)
                            val intent = Intent(mActivity, WritePungActivity::class.java)
                            startActivityForResult(intent, WritePungActivity.REQUEST_UPDATE_POST)
                        }
                    }
                }
                LoadingState.Status.FAILED -> {
                    when (it.throwable?.code ?: 0) {
                        Errors.PASSWORD_REQUIRED,
                        Errors.PERMISSION_DENIED -> {
                            when (validateJob) {
                                ValidateJob.DELETE -> showPasswordDialog(false, false)
                                ValidateJob.UPDATE -> showPasswordDialog(false, false)
                            }
                        }
                        else -> {
                            Toaster.show(applicationContext, R.string.error_unknown, it.throwable?.code ?: 0)
                        }
                    }
                }
            }
        })
        pungViewModel.deletePost.observe(this, Observer {
            when (it.status) {
                LoadingState.Status.SUCCESS -> {
                    Toaster.showCustomWhite(applicationContext, R.string.toast_deleted_post)
                    onDeleteSucceed()
                }
                LoadingState.Status.FAILED -> {
                    Toaster.show(applicationContext, R.string.error_unknown, it.throwable?.code ?: 0)
                }
            }
        })
        pungReplyViewModel.replyList.observe(this, Observer {
            when (it.status) {
                LoadingState.Status.LOADING -> replyAdapter.setLoading(true)
                LoadingState.Status.SUCCESS -> {
                    replyAdapter.setLoading(false)
                    if (requestPage == 0) {
                        replyAdapter.clear()
                    }
                    if (it.data?.isNullOrEmpty() == false) {
                        requestPage++
                    } else {
                        requestPage = -1
                    }
                    replyAdapter.addListDistinct(it.data!!)
                }
                LoadingState.Status.FAILED -> {
                    replyAdapter.setLoading(false)
                    requestPage = -1
                }
            }
        })
        pungReplyViewModel.validateReply.observe(this, Observer {
            when (it.status) {
                LoadingState.Status.SUCCESS -> {
                    when (validateJob) {
                        ValidateJob.DELETE -> {
                            pungReplyViewModel.deleteReply(reply?.replyNo, replyPasswordInput)
                            replyAdapter.delete(reply?.replyNo ?: 0)
                        }
                        ValidateJob.UPDATE -> {
                            EventBusManager.getInstance().cast(reply)
                            val intent = Intent(mActivity, WritePungReplyActivity::class.java)
                            startActivityForResult(intent, WritePungReplyActivity.REQUEST_UPDATE_REPLY)
                        }
                    }
                }
                LoadingState.Status.FAILED -> {
                    when (it.throwable?.code ?: 0) {
                        Errors.PASSWORD_REQUIRED,
                        Errors.PERMISSION_DENIED -> {
                            when (validateJob) {
                                ValidateJob.DELETE -> showPasswordDialog(true, false)
                                ValidateJob.UPDATE -> showPasswordDialog(true, false)
                            }
                        }
                        else -> {
                            Toaster.show(applicationContext, R.string.error_unknown, it.throwable?.code ?: 0)
                        }
                    }
                }
            }
        })
    }
    
    private fun setClickListeners() {
        iv_more.setOnClickListener { 
            val isAnonymous = post?.userId.isNullOrEmpty()
            mDialog = DialogModule.createPungMoreDialog(mActivity, isMyPost, isAnonymous, object: DialogClickListener<PungMore> {
                override fun onClick(job: PungMore) {
                    mDialog?.dismiss()
                    when (job) {
                        PungMore.DELETE -> delete()
                        PungMore.REPORT -> report()
                        PungMore.HIDE -> hide()
                        PungMore.MODIFY -> modify()
                    }
                }
            })
            mDialog?.show()
        }
    }

    private fun setPost() {
        replyAdapter = ReplyAdapter(this)
        replyAdapter.headerItemClick = ItemClick { view, position ->
            when (view.id) {
                R.id.ll_share -> {
                    // 게시글 공유
                    pungViewModel.shareCountUp(post?.postNo ?: 0)
                }
                R.id.ll_like -> {
                    // 게시물 좋아요
                    val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                    vibrator.vibrate(Constants.VibrateTime)
                    pungViewModel.likeCountUp(post?.postNo ?: 0)

                    if (storage.countPungLike()) {
                        adLoader?.showInterstitial()
                    }
                }
                R.id.rl_metadata -> {
                    // 링크로 이동
                    openWebViewer(post?.linkUrl)
                }
                R.id.iv_play_video -> {
                    // 유튜브 영상 재생
                    view.startAnimation(Animator.getClickAnimation(this))
                    openVideoViewer(post?.youtubeUrl)
                }
            }
        }
        replyAdapter.itemClick = ItemClick { view, position ->
            val reply = replyAdapter.getItemAt(position)
            when (view.id) {
                R.id.iv_play_video -> {
                    // 영상 재생
                    val linkUrl = Util.extractUrlFromText(reply.comment)
                    val youtubeUrl = if (YoutubeUtils.isYoutube(linkUrl)) linkUrl else reply.movieName
                    view.startAnimation(Animator.getClickAnimation(mActivity))
                    openVideoViewer(youtubeUrl)
                }
                R.id.av_like -> {
                    // 좋아요 클릭
                    val vibrator = mContext.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                    vibrator.vibrate(Constants.VibrateTime)
                    (view as LottieAnimationView).playAnimation()
                    pungReplyViewModel.likeCountUpReply(reply.replyNo)
            }
                R.id.rl_metadata -> {
                    // 링크로 이동
                    val linkUrl = Util.extractUrlFromText(reply.comment)
                    openWebViewer(linkUrl)
                }
                R.id.iv_more -> {
                    val isMine = reply.isCreator == 1 && storage.isLoggedIn
                    val isAnonymous = reply.userId.isNullOrEmpty()
                    mDialog = DialogModule.createPungMoreDialog(mContext, isMine, isAnonymous, object: DialogClickListener<PungMore> {
                        override fun onClick(value: PungMore) {
                            mDialog?.dismiss()
                            this@PungActivity.reply = reply
                            when (value) {
                                PungMore.MODIFY -> modifyReply()
                                PungMore.DELETE -> deleteReply()
                                PungMore.HIDE -> hideReply()
                                PungMore.REPORT -> reportReply()
                            }
                        }
                    })
                    mDialog?.show()
                }
            }
        }
        replyAdapter.setPost(post)
        rvReply.layoutManager = LinearLayoutManager(mContext)
        rvReply.isNestedScrollingEnabled = false
        rvReply.adapter = replyAdapter

        val animator = rvReply.itemAnimator
        if (animator is SimpleItemAnimator) {
            animator.supportsChangeAnimations = false
        }
    }

    private fun loadReplies() {
        if (!replyAdapter.isLoading() && requestPage > -1) {
            pungReplyViewModel.getReplyList(post?.postNo, requestPage)
        }
    }

    @OnClick(R.id.btn_back, R.id.et_write)
    fun onClick(v: View) {
        when (v.id) {
            R.id.btn_back -> {
                v.startAnimation(Animator.getClickAnimation(mContext))
                onBackPressed()
            }
            R.id.et_write -> {
                // 댓글 작성
                val intent = Intent(this, WritePungReplyActivity::class.java)
                intent.putExtra("postNo", post?.postNo)
                startActivityForResult(intent, WritePungReplyActivity.REQUEST_WRITE_REPLY)
            }
        }
    }

    private fun showPasswordDialog(isReply: Boolean, isValid: Boolean = true) {
        val message = when {
            isReply && validateJob == ValidateJob.UPDATE -> R.string.dialog_update_reply
            isReply && validateJob == ValidateJob.DELETE -> R.string.dialog_delete_reply
            validateJob == ValidateJob.UPDATE -> R.string.dialog_update_pung
            else -> R.string.dialog_delete_pung
        }

        val passwordInput = if (isReply) replyPasswordInput else passwordInput
        mDialog = DialogManager.getInstance().createPungPasswordDialog(mActivity, message, isValid,
                passwordInput ?: "", object: DialogClickListener<String> {
            override fun onClick(password: String) {
                // 게시물 삭제
                if (isReply) {
                    this@PungActivity.replyPasswordInput = password
                    pungReplyViewModel.validateReply(reply?.replyNo, password)
                } else {
                    this@PungActivity.passwordInput = password
                    pungViewModel.validatePost(post?.postNo ?: 0, password)
                }
            }
        })
        mDialog?.show()
    }

    private fun onDeleteSucceed() {
        val data = Intent()
        data.putExtra("deletePost", post?.postNo)
        setResult(Activity.RESULT_OK, data)
        finish()
    }
    
    private fun report() {
        post?. let {
            mDialog = createDialog(
                    message = R.string.dialog_report_post,
                    negativeLabel = R.string.cancel,
                    positiveBlock = {
                        Toaster.show(applicationContext, R.string.toast_reported)
                        pungViewModel.reportPost(it.postNo, it.comment)
                        iv_more.postDelayed({
                            onDeleteSucceed()
                        }, 500)
                    }
            ).apply { show() }
        }
    }
    
    private fun hide() {
        post?. let {
            mDialog = createDialog(
                    message = R.string.dialog_hide_post,
                    negativeLabel = R.string.cancel,
                    positiveBlock = {
                        pungViewModel.hidePost(it.postNo)
                        iv_more.postDelayed({
                            onDeleteSucceed()
                        }, 500)
                    }
            ).apply { show() }
        }
    }
    
    private fun modify() {
        validateJob = ValidateJob.UPDATE
        if (isMyPost) {
            EventBusManager.getInstance().cast(post)
            val intent = Intent(mActivity, WritePungActivity::class.java)
            startActivityForResult(intent, WritePungActivity.REQUEST_UPDATE_POST)
        } else {
            showPasswordDialog(false, true)
        }
    }
    
    private fun delete() {
        validateJob = ValidateJob.DELETE
        if (isMyPost) {
            // 로그인 하고 글을 썼을때
            mDialog = createDialog(
                    message = R.string.dialog_sure_delete_post,
                    positiveLabel = R.string.yes,
                    negativeLabel = R.string.no,
                    positiveBlock = {
                        pungViewModel.deletePost(post?.postNo, null)
                    }
            ).apply { show() }
        } else {
            // 비밀번호로 글을 썼을때
            showPasswordDialog(false, true)
        }
    }

    private fun modifyReply() {
        reply?.run {
            if (isCreator == 1 && storage.isLoggedIn) {
                EventBusManager.getInstance().cast(reply)
                val intent = Intent(mActivity, WritePungReplyActivity::class.java)
                startActivityForResult(intent, WritePungReplyActivity.REQUEST_UPDATE_REPLY)
            } else {
                validateJob = ValidateJob.UPDATE
                showPasswordDialog(true, true)
            }
        }
    }

    private fun deleteReply() {
        reply?.run {
            if (isCreator == 1 && storage.isLoggedIn) {
                pungReplyViewModel.deleteReply(replyNo, null)
                replyAdapter.delete(replyNo)
            } else {
                validateJob = ValidateJob.DELETE
                showPasswordDialog(true, true)
            }
        }
    }

    private fun reportReply() {
        reply?.run {
            mDialog = createDialog(
                    message = R.string.dialog_report_reply,
                    negativeLabel = R.string.cancel,
                    positiveBlock = {
                        Toaster.show(applicationContext, R.string.toast_reported)
                        pungReplyViewModel.reportReply(replyNo, comment)
                        replyAdapter.delete(replyNo)
                    }
            ).apply { show() }
        }
    }

    private fun hideReply() {
        reply?.run {
            mDialog = createDialog(
                    message = R.string.dialog_hide_reply,
                    negativeLabel = R.string.cancel,
                    positiveBlock = {
                        pungReplyViewModel.hideReply(replyNo)
                        replyAdapter.delete(replyNo)
                    }
            ).apply { show() }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                WritePungReplyActivity.REQUEST_WRITE_REPLY -> {
                    requestPage = 0
                    replyAdapter.clear()
                    loadReplies()
                }
                WritePungReplyActivity.REQUEST_UPDATE_REPLY -> {
                    val reply = Gson().fromJson(data?.getStringExtra("update_reply"), Post.Reply::class.java)
                    replyAdapter.update(reply)
                }
                WritePungActivity.REQUEST_UPDATE_POST -> {
                    post = Gson().fromJson(data?.getStringExtra("update_post"), Post::class.java)
                    setPost()
                    requestPage = 0
                    replyAdapter.clear()
                    loadReplies()
                }
            }
        }
    }

    override fun onBackPressed() {
        val data = Intent()
        val post = replyAdapter.getPost()
        if (post != null) {
            data.putExtra("post", Gson().toJson(post))
            data.putExtra("position", position)
            setResult(Activity.RESULT_OK, data)
            finish()
        } else {
            super.onBackPressed()
        }
    }
    
}