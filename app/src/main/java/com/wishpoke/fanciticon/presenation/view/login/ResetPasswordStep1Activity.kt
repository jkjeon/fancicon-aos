package com.wishpoke.fanciticon.presenation.view.login

import android.os.Bundle
import com.google.gson.Gson
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.presenation.controller.PhoneController
import com.wishpoke.fanciticon.presenation.resource.SingleResponse
import com.wishpoke.fanciticon.util.DialogManager
import com.wishpoke.fanciticon.util.Toaster
import com.wishpoke.fanciticon.util.constants.Errors
import com.wishpoke.fanciticon.util.extention.createDialog
import com.wishpoke.fanciticon.util.extention.open
import kotlinx.android.synthetic.main.activity_reset_password_step1.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

class ResetPasswordStep1Activity : BaseActivity() {

    private val loginViewModel: LoginViewModel by viewModel()
    private lateinit var phoneController: PhoneController
    private var timer: Timer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password_step1)
        init()
        setOnClickListeners()
    }

    private fun init() {
        phoneController = PhoneController(this, et_phone, et_code, btn_send_code, btn_verify_code, tv_timer, tv_verification_desc)
    }

    override fun onResume() {
        super.onResume()
        try {
            if (timer == null) {
                timer = Timer()
            }
            timer?.scheduleAtFixedRate(object : TimerTask() {
                override fun run() {
                    runOnUiThread {
                        btn_submit.isEnabled = phoneController.phoneInfo.isVerified
                    }
                }
            }, 0, 1000)
        } catch (e: Exception) {
            btn_submit.isEnabled = true
        }
    }

    private fun setOnClickListeners() {
        btn_back.setOnClickListener { onBackPressed() }
        btn_submit.setOnClickListener {
            val phoneInfo = phoneController.phoneInfo
            if (phoneInfo.isReliable()) {
                mDialog = DialogManager.getInstance().createLoadingDialog(mContext)
                mDialog?.show()

                loginViewModel.findId(phoneInfo, object : SingleResponse<Any>() {
                    override fun onSuccess(t: Any) {
                        mDialog?.dismiss()
                        // 비밀번호 등록 (Step2)로 이동
                        open<ResetPasswordStep2Activity> {
                            putExtra("phoneInfo", Gson().toJson(phoneInfo))
                        }
                        finish()
                    }

                    override fun onFailure(e: Throwable, code: Int) {
                        mDialog?.dismiss()
                        if (code == Errors.NON_EXIST_USER) {
                            // 가입정보 없음, 회원가입 페이지로 유도
                            mDialog = mContext.createDialog(
                                    message = R.string.dialog_find_id_user_not_found,
                                    negativeLabel = R.string.cancel,
                                    positiveBlock = {
                                        open<SignUpActivity>()
                                        finish()
                                    }
                            ).apply { show() }
                        } else {
                            Toaster.show(applicationContext, R.string.error_unknown, code)
                        }
                    }
                })
            } else {
                Toaster.show(applicationContext, R.string.toast_check_phone_verification)
            }
        }
    }

    override fun onStop() {
        timer?.cancel()
        super.onStop()
    }
}