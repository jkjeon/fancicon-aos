package com.wishpoke.fanciticon.presenation.view.chat

import android.os.Bundle
import android.os.SystemClock
import android.view.View
import android.webkit.WebView
import android.widget.LinearLayout
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.chat.ChatRoom
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.presenation.resource.ui.RegularEditText
import com.wishpoke.fanciticon.util.*
import com.wishpoke.fanciticon.util.constants.PrefKeys
import com.wishpoke.fanciticon.util.extention.isVisibleWhen
import com.wishpoke.fanciticon.util.listener.DialogClickListener
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

class EnterChatActivity : BaseActivity() {

    @BindView(R.id.et_nickname)
    lateinit var etNickname: RegularEditText
    @BindView(R.id.et_password)
    lateinit var etPassword: RegularEditText
    @BindView(R.id.iv_clear_nickname)
    lateinit var ivClearNickname: View
    @BindView(R.id.iv_clear_password)
    lateinit var ivClearPassword: View
    @BindView(R.id.cl_password)
    lateinit var clPassword: View
    @BindView(R.id.divider_password)
    lateinit var dividerPassword: View
    @BindView(R.id.banner_container)
    lateinit var bannerContainer: LinearLayout
    @BindView(R.id.ll_isn)
    lateinit var llIsn: LinearLayout
    @BindView(R.id.wv_edge)
    lateinit var wvEdge: WebView

    private var chatRoom: ChatRoom? = null
    val chatRoomViewModel: ChatRoomViewModel by viewModel()
    val chatViewModel: ChatViewModel by viewModel()
    var hexColorList: MutableList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_chat)
        ButterKnife.bind(this)
        adLoader = FacebookAdLoader(this, bannerContainer, llIsn, wvEdge)
        chatRoom = EventBusManager.getInstance().clearAccept(ChatRoom::class.java) as? ChatRoom
        init()
        subsribeObservers()
    }

    fun init() {
        etNickname.setClearButton(ivClearNickname)
        etPassword.setClearButton(ivClearPassword)

        clPassword.isVisibleWhen(chatRoom?.isPrivate ?: 0 == 1)
        dividerPassword.isVisibleWhen(chatRoom?.isPrivate ?: 0 == 1)
    }

    private fun subsribeObservers() {
        chatRoomViewModel.canJoinToChatRoom.observe(this, androidx.lifecycle.Observer {
            when (it.status) {
                LoadingState.Status.SUCCESS -> {
                    var result = ""
                    try {
                        result = it.data!! as String
                    } catch (e: Exception) {

                    }
                    when (result) {
                        "BAN" -> {
                            Toaster.show(mContext, R.string.error_banned)
                        }
                        "PASSWORD" -> {
                            Toaster.show(mContext, R.string.error_password)
                        }
                        "FULL" -> {
                            Toaster.show(mContext, R.string.error_chat_room_full)
                        }
                        else -> {
                            enterChat()
                        }
                    }
                }
                LoadingState.Status.FAILED -> {
                    Toaster.show(mContext, R.string.error_unknown, it.throwable?.code ?: 0)
                }
            }
        })
        chatViewModel.chatRoomMembers.observe(this, androidx.lifecycle.Observer {
            when (it.status) {
                LoadingState.Status.SUCCESS -> {
                    for (chatMember in it.data!!) {
                        if (chatMember.fontColorType < hexColorList.size) {
                            hexColorList[chatMember.fontColorType] = ""
                        }
                    }
                    // 사용중인 색상을 지우고 남은 색상들 중 랜덤히 배정
                    hexColorList = (hexColorList.filter { it.isNotEmpty() }).toMutableList()
                    chatRoom?.fontColorType = (Math.random() * hexColorList.size).toInt()
                    chatRoomViewModel.canJoinToChatRoom(chatRoom)
                }
            }
        })
    }

    @OnClick(R.id.btn_back, R.id.tv_warnings, R.id.tv_submit)
    fun onClick(v: View) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) return
        mLastClickTime = SystemClock.elapsedRealtime().toInt()
        when (v.id) {
            R.id.btn_back -> onBackPressed()
            R.id.tv_warnings -> {
                v.startAnimation(Animator.getClickAnimation(this))
                mDialog = DialogManager.getInstance().createChatWarningDialog(mContext)
                mDialog?.show()
            }
            R.id.tv_submit -> {
                v.startAnimation(Animator.getClickAnimation(this))
                chatRoom?.tempStageName = etNickname.text.toString()
                chatRoom?.password = etPassword.text.toString().trim()
                if (isValid()) {
                    assignFontColor()
                }
            }
        }
    }

    private fun enterChat() {
        val sawChatGuide = storage.getBoolean(PrefKeys.SAW_CHAT_GUIDE, false)
        if (sawChatGuide) {
            EventBusManager.getInstance().cast(chatRoom)
            goTo(mActivity, ChatActivity::class.java, true, false)
        } else {
            // 채팅 가이드 보여주기
            mDialog = DialogManager.getInstance().createChatGuideDialog(mContext, object: DialogClickListener<View> {
                override fun onClick(value: View) {
                    storage.put(PrefKeys.SAW_CHAT_GUIDE, true)
                    EventBusManager.getInstance().cast(chatRoom)
                    goTo(mActivity, ChatActivity::class.java, true, false)
                }
            })
            mDialog?.show()
        }
    }

    private fun isValid(): Boolean {
        return when {
            chatRoom?.tempStageName.isNullOrEmpty() -> {
                Toaster.show(mContext, R.string.toast_input_nickname)
                false
            }
            chatRoom?.isPrivate == 1 && chatRoom?.password?.length != 4 -> {
                Toaster.show(mContext, R.string.toast_check_password)
                false
            }
            else -> true
        }
    }

    private fun assignFontColor() {
        hexColorList = Arrays.asList(*resources.getStringArray(R.array.chat_colors))
        chatRoom?.fontColorType = (Math.random() * hexColorList.size).toInt()
        chatViewModel.getChatRoomMembers(chatRoom?.roomId)
    }
}