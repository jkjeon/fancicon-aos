package com.wishpoke.fanciticon.presenation.view.pung.reply

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.ActivityResultEvent
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.MetaData
import com.wishpoke.fanciticon.data.model.pung.Post
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.presenation.controller.SingleImageController
import com.wishpoke.fanciticon.presenation.view.pung.WriteViewModule
import com.wishpoke.fanciticon.util.*
import com.wishpoke.fanciticon.util.constants.Errors
import com.wishpoke.fanciticon.util.extention.isVisibleWhen
import com.wishpoke.fanciticon.util.extention.setOnSingleClickListener
import com.wishpoke.fanciticon.util.listener.DialogClickListener
import com.wishpoke.fanciticon.util.listener.PermissionDoneListener
import com.wishpoke.fanciticon.util.listener.RequestListener
import kotlinx.android.synthetic.main.activity_write_pung.*
import kotlinx.coroutines.*
import org.jsoup.Jsoup
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File

class WritePungReplyActivity : BaseActivity() {

    private var reply: Post.Reply? = null
    private var postNo = -1
    private var metaDataJob: Job? = null
    private lateinit var singleImageController: SingleImageController
    private lateinit var writeViewModule: WriteViewModule

    private val pungReplyViewModel: PungReplyViewModel by viewModel()

    private val requestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_write_pung)

        postNo = intent.getIntExtra("postNo", -1)
        reply = EventBusManager.getInstance().clearAccept(Post.Reply::class.java) as? Post.Reply
        init()
        writeViewModule.setNickNameView()
        setClickListeners()
        subscribeObservers()

        // 로그인 안했다면 닉네임 호출
        if (!storage.isLoggedIn) {
            pungReplyViewModel.getNickname()
        }
    }

    private fun init() {
        writeViewModule = WriteViewModule(this, et_username, et_comment, et_password, iv_clear)
        singleImageController = SingleImageController(this, object : RequestListener<File?> {
            override fun onSuccess(result: File?) {
                val requestOptions = RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)

                Glide.with(mActivity).load(result)
                        .apply(requestOptions)
                        .into(iv_upload)
            }

            override fun onFailure() { // 파일 첨부 실패
                Toaster.show(mContext, R.string.error_failed_to_upload_image)
            }
        }, 1, 1)
        adLoader = FacebookAdLoader(this)

        ll_category.visibility = View.GONE
        category_divider.visibility = View.GONE

        et_password.isVisibleWhen(!storage.isLoggedIn)
        password_divider.isVisibleWhen(!storage.isLoggedIn)

        // 문자 길이에 따라 올리기 버튼 활성화
        val textWatcher: TextWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                iv_clear.isVisibleWhen(et_username.isFocusable && et_username.text.toString().isNotEmpty())
                setSubmitEnabled(writeViewModule.isValid())
                checkUrl()
            }
            override fun afterTextChanged(s: Editable) {}
        }
        et_username.addTextChangedListener(textWatcher)
        et_comment.addTextChangedListener(textWatcher)
        setSubmitEnabled(writeViewModule.isValid())

        reply?.let {
            et_username.setText(it.userName)
            et_comment.setText(it.comment)
            et_password.setText(it.password)
            if (!it.fileName.isNullOrEmpty()) {
                Glide.with(mActivity).load(Util.getImageDownloadUrl(it.fileName))
                        .apply(requestOptions)
                        .into(iv_upload)
            }
            if (!it.movieName.isNullOrEmpty()) {
                Glide.with(mActivity).load(YoutubeUtils.getThumbnail(it.movieName))
                        .apply(requestOptions)
                        .into(iv_upload)
            }
            writeViewModule.movieName = it.movieName
        }
    }

    private fun subscribeObservers() {
        pungReplyViewModel.nickname.observe(this, Observer {
            if (it.status == LoadingState.Status.SUCCESS) {
                Logger.e("Test", "Nickname: ${it?.data}")
                writeViewModule.setNickNameView(it?.data)
            } else {
                Logger.e("Test", "Nickname Error: ${it?.throwable?.message}")
            }
        })
        pungReplyViewModel.uploadReply.observe(this, androidx.lifecycle.Observer {
            when (it.status) {
                LoadingState.Status.SUCCESS -> {
                    mDialog?.dismiss()
                    storage.nickname = writeViewModule.userName
                    Toaster.showCustomWhite(applicationContext, R.string.complete_reply)
                    setResult(Activity.RESULT_OK)
                    adLoader?.showInterstitial(object: FacebookAdLoader.AdCallback {
                        override fun onNext() {
                            tv_submit.isEnabled = true
                            finish()
                        }
                    })
                }
                LoadingState.Status.FAILED -> {
                    mDialog?.dismiss()
                    tv_submit.isEnabled = true
                    handleErrors(it.throwable?.code)
                }
                LoadingState.Status.LOADING -> {
                    mDialog = DialogManager.getInstance().createLoadingDialog(this@WritePungReplyActivity)
                    mDialog?.show()
                    tv_submit.isEnabled = false
                }
            }
        })

        pungReplyViewModel.updateReply.observe(this, Observer {
            when (it.status) {
                LoadingState.Status.SUCCESS -> {
                    mDialog?.dismiss()
                    tv_submit.isEnabled = true
                    Toaster.showCustomWhite(applicationContext, R.string.pung_update_complete)
                    reply?.comment = writeViewModule.comment
                    reply?.password = writeViewModule.password
                    reply?.movieName = writeViewModule.movieName
                    reply?.fileName = it?.data

                    val data = Intent()
                    data.putExtra("update_reply", Gson().toJson(reply))
                    setResult(Activity.RESULT_OK, data)
                    finish()
                }
                LoadingState.Status.FAILED -> {
                    mDialog?.dismiss()
                    tv_submit.isEnabled = true
                    handleErrors(it.throwable?.code)
                }
                LoadingState.Status.LOADING -> {
                    mDialog = DialogManager.getInstance().createLoadingDialog(this@WritePungReplyActivity)
                    mDialog?.show()
                    tv_submit.isEnabled = false
                }
            }
        })
    }

    private fun handleErrors(errorCode: Int?) {
        when (errorCode ?: 0) {
            Errors.PERMISSION_DENIED,
            Errors.REGISTERED_ALIAS -> {
                Toaster.show(applicationContext, R.string.error_registered_alias)
            }
            Errors.NON_EXIST_USER -> {
                Toaster.show(applicationContext, R.string.error_user_not_found)
            }
            Errors.PASSWORD_REQUIRED -> {
                Toaster.show(applicationContext, R.string.error_input_4digit_password)
            }
            else -> {
                Toaster.show(applicationContext, R.string.error_unknown, errorCode)
            }
        }
    }

    private fun setClickListeners() {
        btn_back.setOnClickListener {
            it.startAnimation(Animator.getClickAnimation(mContext))
            onBackPressed()
        }
        // 메타 데이터 지우기
        iv_remove_metadata.setOnClickListener {
            it.startAnimation(Animator.getClickAnimation(this))
            if (singleImageController.file == null) {
                // 유튜브 업로드를 클릭해서 썸네일을 채웠다면 Clear
                iv_upload.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_image))
            }
            rl_metadata.visibility = View.GONE
        }
        // 닉네임 지우기
        iv_clear.setOnClickListener {
            et_username.setText("")
        }
        // 주의 사항 클릭
        tv_warnings.setOnClickListener {
            it.startAnimation(Animator.getClickAnimation(mContext))
            mDialog = DialogManager.getInstance().createPungWarningDialog(mContext)
            mDialog?.show()
        }
        // 이미지 업로드
        iv_upload.setOnClickListener {
            clickImage()
        }
        tv_submit.setOnSingleClickListener {
            it.startAnimation(Animator.getClickAnimation(mContext))
            writeViewModule.run {
                if (isValid(true)) {
                    if (reply == null) {
                        pungReplyViewModel.uploadReply(postNo, userName, comment,
                                singleImageController.file, movieName, password)
                    } else {
                        pungReplyViewModel.updateReply(reply?.replyNo, password, comment,
                        singleImageController.file, reply?.fileName, movieName)
                    }
                }
            }
        }
    }

    // 이미지 클릭
    private fun clickImage() {
        val buttonLabels = arrayOf(getString(R.string.camera), getString(R.string.gallery), getString(R.string.youtube_link))
        mDialog = DialogManager.getInstance().createNChoiceDialog(mContext, buttonLabels, object : DialogClickListener<Int> {
            override fun onClick(value: Int) {
                mDialog?.dismiss()
                when (value) {
                    R.id.btn_1 ->
                        // 카메라
                        onRequiredPermissionChecked(true, object : PermissionDoneListener {
                            override fun onDone(granted: Boolean) {
                                if (granted) {
                                    singleImageController.setDefaultImage(false)
                                    singleImageController.takeCamera()
                                }
                            }
                        })
                    R.id.btn_2 ->
                        // 갤러리
                        onRequiredPermissionChecked(false, object : PermissionDoneListener {
                            override fun onDone(granted: Boolean) {
                                if (granted) {
                                    singleImageController.setDefaultImage(false)
                                    singleImageController.takeGallery()
                                }
                            }
                        })
                    R.id.btn_3 -> {
                        // 유튜브
                        mDialog = DialogManager.getInstance().createYoutubeLinkDialog(mContext, object : DialogClickListener<String> {
                            override fun onClick(url: String) {
                                val thumbnail = YoutubeUtils.getThumbnail(url)
                                Glide.with(mActivity).load(thumbnail)
                                        .apply(requestOptions)
                                        .into(iv_upload)
                                singleImageController.file = null
                                writeViewModule.movieName = url
                            }
                        })
                        if (!isFinishing) mDialog?.show()
                    }
                }
            }
        })
        if (!isFinishing) {
            mDialog?.show()
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        singleImageController.onActivityResult(ActivityResultEvent(requestCode, resultCode, data))
    }

    private fun setSubmitEnabled(flag: Boolean) {
        val color = if (flag) R.color.colorPrimary else R.color.aaaaaa
        tv_submit.setTextColor(resources.getColor(color))
        tv_submit.isEnabled = flag
    }

    private fun checkUrl() {
        val content = et_comment.text.toString()
        val url = Util.extractUrlFromText(content)
        if (!url.isNullOrEmpty()) {
            metaDataJob = CoroutineScope(Dispatchers.IO).launch {
                val metaData = fetchMetadata(url)
                withContext(Dispatchers.Main) {
                    if (!metaData.isEmpty()) {
                        Glide.with(mContext)
                                .load(metaData.image)
                                .placeholder(R.mipmap.image_placeholder)
                                .into(iv_metadata_image)
                        tv_metdata_title.text = Util.shorten(metaData.title)
                        tv_metdata_url.text = Util.shorten(metaData.domain)
                        rl_metadata.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun fetchMetadata(url: String): MetaData {
        val metaData = MetaData()
        metaData.url = url
        metaData.domain = Validator.getDomainName(url)
        try {
            val conn = Jsoup.connect(url)
            val doc = conn.get()
            val ogTags = doc.select("meta[property^=og:]")

            if (ogTags.isNotEmpty()) {
                for (tag in ogTags) {
                    when (val text = tag.attr("property")) {
                        "og:title" -> metaData.title = tag.attr("content")
                        "og:description" -> metaData.desc = tag.attr("content")
                        "og:image" -> metaData.image = tag.attr("content")
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return metaData
    }

    override fun onStop() {
        metaDataJob?.cancel()
        super.onStop()
    }

    companion object {
        const val REQUEST_WRITE_REPLY = 1111
        const val REQUEST_UPDATE_REPLY = 1122
    }
}