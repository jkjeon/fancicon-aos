package com.wishpoke.fanciticon.presenation.view.chat

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.emoji.Emoji
import com.wishpoke.fanciticon.presenation.base.BaseRecyclerViewAdapter
import com.wishpoke.fanciticon.presenation.view.main.EmojiAdapter

open class ChatEmojiFavoriteAdapter(val context: Context) : BaseRecyclerViewAdapter<RecyclerView.ViewHolder, String>() {

    var recentList: MutableList<String> = ArrayList()
    var favoriteList: MutableList<Emoji> = ArrayList()

    open fun submitRecentList(list: MutableList<String>) {
        recentList = list
        notifyDataSetChanged()
    }

    open fun submitFavoriteList(list: MutableList<Emoji>) {
        favoriteList = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == TypeHeader) {
            val itemView = LayoutInflater.from(context).inflate(R.layout.item_header_emoji_favorite, parent, false)
            HeaderViewHolder(itemView)
        } else {
            val itemView = LayoutInflater.from(context).inflate(R.layout.item_emoji, parent, false)
            EmojiAdapter.ViewHolder(itemView)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        with (holder) {
            when (this) {
                is HeaderViewHolder -> {
                    tvTitle.text = if (position == 0) {
                        "Recent"
                    } else {
                        "Favorite"
                    }
                }
                is EmojiAdapter.ViewHolder -> {
                    val emoji = getItemAt(position)

                    Glide.with(context).load(
                    if (isFavorite(emoji)) {
                        R.drawable.ic_star_purple
                    } else {
                        R.drawable.ic_star_gray
                    }).into(btnFavorite)

                    tvEmoji.text = emoji

                    btnFavorite.setOnClickListener {
                        Glide.with(context).load(
                        if (isFavorite(emoji)) {
                            R.drawable.ic_star_gray
                        } else {
                            R.drawable.ic_star_purple
                        }).into(btnFavorite)
                        itemClick?.onItemClick(it, position)
                    }
                    itemView.setOnClickListener {
                        itemClick?.onItemClick(it, position)
                    }
                }
            }
        }
    }

    fun isFavorite(emoji: String): Boolean {
        return favoriteList.contains(Emoji(emoji))
    }

    open fun isRecentPosition(position: Int): Boolean {
        return position < recentList.size + 1
    }

    override fun getItemAt(position: Int): String {
        return if (isRecentPosition(position)) {
            recentList[getRealPosition(position)]
        } else {
            val emoji = favoriteList[getRealPosition(position)]
            emoji.emoji
        }
    }

    override fun getRealPosition(position: Int): Int {
        return if (isRecentPosition(position)) {
            // 최근
            position - 1
        } else {
            // Fav
            position - 2 - recentList.size
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0, recentList.size + 1 -> TypeHeader
            else -> TypeItem
        }
    }

    override fun getItemCount(): Int {
        return recentList.size + favoriteList.size + 2
    }

    class HeaderViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.tv_title)
        lateinit var tvTitle: TextView
        init {
            ButterKnife.bind(this, itemView)
        }
    }
}