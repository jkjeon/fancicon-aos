package com.wishpoke.fanciticon.presenation.resource

import com.google.gson.Gson
import com.wishpoke.fanciticon.data.model.response.ObjectResponse
import com.wishpoke.fanciticon.data.model.response.SuccessResponse
import com.wishpoke.fanciticon.util.Logger
import com.wishpoke.fanciticon.util.constants.Errors

abstract class SingleResponse<T> {

    open fun onSuccess(t: T) {}
    open fun onFailure(throwable: Throwable, code: Int) {}

    fun accept(t: T) {
        when (t) {
            is ObjectResponse<*> -> {
                if (t.status.code == 0) {
                    onSuccess(t)
                } else {
                    Logger.e("Test", "AppThrowable Error: " + t.status.msg)
                    val throwable = Throwable(t.status.msg)
                    onFailure(throwable, t.status.code)
                }
            }
            is SuccessResponse -> {
                if (t.success) {
                    onSuccess(t)
                } else {
                    val throwable = Throwable(t.msg)
                    onFailure(throwable, Errors.SERVER)
                }
            }
            else -> onSuccess(t)
        }
    }

    fun accept(throwable: Throwable?, code: Int) {
        Logger.e("Test", "Error: " + Gson().toJson(throwable))
        onFailure(throwable ?: Throwable(), Errors.NETWORK)
    }
}