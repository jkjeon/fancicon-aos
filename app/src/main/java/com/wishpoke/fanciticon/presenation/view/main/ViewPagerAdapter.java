package com.wishpoke.fanciticon.presenation.view.main;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.wishpoke.fanciticon.data.model.Category;
import com.wishpoke.fanciticon.presenation.view.chat.ChatRoomFragment;
import com.wishpoke.fanciticon.presenation.view.custom_emoji.CustomFragment;
import com.wishpoke.fanciticon.presenation.view.favorite.FavoriteFragment;
import com.wishpoke.fanciticon.presenation.view.pung.PungFragment;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private List<Category> categories = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager fragmentManager, List<Category> categories) {
        super(fragmentManager);
        this.categories = categories;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Fragment getItem(int position) {
        String categoryId = categories.get(position).getId();
        switch (categoryId) {
            case "favorite": return FavoriteFragment.create();
            case "pung": return PungFragment.create();
            case "custom": return CustomFragment.create();
            case "chat": return ChatRoomFragment.create();
            default: return ListFragment.Companion.create(categoryId);
        }
    }
}

