package com.wishpoke.fanciticon.presenation.service;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.os.IBinder;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.wishpoke.fanciticon.R;
import com.wishpoke.fanciticon.presenation.view.main.MainActivity;
import com.wishpoke.fanciticon.util.Animator;
import com.wishpoke.fanciticon.util.ImageResolver;

public class FloatingWidgetService extends Service {
    private WindowManager windowManager;
    private View widgetView, overlayView;
    private ImageView widget;
    private Rect cornerArea;
    private int cornerDelta;
    private int categoryIndex = 0;
    boolean in = false, shouldClick = false;

    private void fadeIn(WindowManager windowManager, WindowManager.LayoutParams params, View v) {
        v.setAlpha(0);
        windowManager.addView(v, params);
        v.animate().alpha(0).alphaBy(1)
                .setInterpolator(new AccelerateDecelerateInterpolator()).setDuration(600)
                .start();
    }

    private Point getDisplaySize(WindowManager windowManager) {
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand (Intent intent, int flags, int startId) {
        try {
            categoryIndex = intent.getIntExtra("categoryIndex", 0);
        } catch (Exception e) {
            categoryIndex = 0;
            e.printStackTrace();
        }
        return START_STICKY;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate() {
        super.onCreate();
        final Context context = FloatingWidgetService.this;
        setTheme(R.style.AppTheme);

        overlayView = LayoutInflater.from(this).inflate(R.layout.dialog_widget, null);
        overlayView.setVisibility(View.GONE);
        final ImageView ivRemove = overlayView.findViewById(R.id.iv_remove);
        widgetView = LayoutInflater.from(this).inflate(R.layout.view_floating_widget, null);
        widgetView.setAlpha(0);

        final int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }

        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
        LAYOUT_FLAG,
        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
        PixelFormat.TRANSLUCENT);

        final WindowManager.LayoutParams params2 = new WindowManager.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        // 모서리
        cornerArea = new Rect(0, 0, getDisplaySize(windowManager).x, getDisplaySize(windowManager).y);
        cornerDelta = (int) ImageResolver.convertDpToPixel(10, this);

        //Specify the view position
        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = getDisplaySize(windowManager).x;
        params.y = cornerDelta * 2;

        params2.gravity = Gravity.TOP | Gravity.LEFT;
        params2.x = 0; params2.y = 0;

        try {
            windowManager.addView(overlayView, params2);
            fadeIn(windowManager, params, widgetView);
        } catch (Exception e) { }

        widget = (ImageView) widgetView.findViewById(R.id.widget);

        widget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(context, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("categoryIndex", categoryIndex);

                    PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),
                            0, intent, 0);
                    pendingIntent.send();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        magneticCornerWidget(params, params.x);
        deactivateWidget();

        widget.setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // remove box area
                int widgetWidth = (int) ImageResolver.convertDpToPixel(50, context);
                int margin = (int) ImageResolver.convertDpToPixel(52, context);
                final Rect removeArea = new Rect(
                        getDisplaySize(windowManager).x / 2  - widgetWidth,
                        getDisplaySize(windowManager).y - margin - (widgetWidth*2),
                        getDisplaySize(windowManager).x / 2 + widgetWidth,
                        getDisplaySize(windowManager).y - margin);

                switch (event.getAction()) {
                    // 처음 눌렸을때
                    case MotionEvent.ACTION_DOWN:
                        releaseMagneticCornerWidget();
                        activateWidget();
                        shouldClick = true;

                        widget.animate().scaleX(0.9f).scaleY(0.9f).setDuration(50).start();

                        //remember the initial position.
                        initialX = params.x;
                        initialY = params.y;

                        //get the touch location
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        break;
                    // 누른걸 뗐을 때
                    case MotionEvent.ACTION_UP:
                        if (shouldClick) {
                            widget.performClick();
                        } else {
                            overlayView.setVisibility(View.GONE);
                            if (removeArea.contains(params.x, params.y)) {
                                stopSelf();
                            } else {
                                magneticCornerWidget(params, event.getRawX());
                                deactivateWidget();
                                widget.animate().scaleX(1.0f).scaleY(1.0f).setDuration(50).start();
                            }
                        }
                        break;
                    // 누르고 움직였을
                    case MotionEvent.ACTION_MOVE:
                        activateWidget();
                        float Xdiff = Math.round(event.getRawX() - initialTouchX);
                        float Ydiff = Math.round(event.getRawY() - initialTouchY);

                        //Calculate the X and Y coordinates of the view.
                        params.x = initialX + (int) Xdiff;
                        params.y = initialY + (int) Ydiff;


                        if (Math.abs(initialTouchX - event.getRawX()) > 20 || Math.abs(initialTouchY - event.getRawY()) > 20) {
                            shouldClick = false;
                            overlayView.setVisibility(View.VISIBLE);

                            if (removeArea.contains(params.x, params.y) && !in) {
                                in = true;
                                ivRemove.startAnimation(Animator.getWidgetGrowAnimation(context));
                            } else if (!removeArea.contains(params.x, params.y) && in) {
                                in = false;
                                ivRemove.startAnimation(Animator.getWidgetShrinkAnimation(context));
                            }
                        }

                        //Update the layout with new X & Y coordinates
                        windowManager.updateViewLayout(widgetView, params);
                        break;
                }
                return true;
            }
        });
    }

    private void releaseMagneticCornerWidget() {
        widget.animate().translationX(0).setDuration(10).start();
    }

    private void magneticCornerWidget(WindowManager.LayoutParams params, float x) {
        try {
            boolean directionLeft;
            int moveX, translateX;
            directionLeft = Math.abs(cornerArea.left - x) < Math.abs(cornerArea.right - x);
            if (directionLeft) {
                moveX = 0;
                translateX = -cornerDelta;
            } else {
                moveX = cornerArea.right;
                translateX = cornerDelta;
            }
            params.x = moveX;
            windowManager.updateViewLayout(widgetView, params);
            widget.animate().translationX(translateX).setDuration(200).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deactivateWidget() {
        widget.animate().scaleX(1.0f).scaleY(1.0f).setDuration(200).start();
        widget.animate().alpha(0.5f).setDuration(200).start();
    }

    private void activateWidget() {
        widget.animate().scaleX(1.2f).scaleY(1.2f).setDuration(200).start();
        widget.animate().alpha(1.0f).setDuration(200).start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (widgetView != null) {
                windowManager.removeView(widgetView);
            }
            if (overlayView != null) {
                windowManager.removeView(overlayView);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
