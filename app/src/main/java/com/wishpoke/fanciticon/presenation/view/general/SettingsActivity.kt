package com.wishpoke.fanciticon.presenation.view.general

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.view.View
import butterknife.ButterKnife
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.BillingProcessor.IBillingHandler
import com.anjlab.android.iab.v3.Constants.BILLING_RESPONSE_RESULT_USER_CANCELED
import com.anjlab.android.iab.v3.TransactionDetails
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.data.model.LoadingState
import com.wishpoke.fanciticon.data.model.PhoneInfo
import com.wishpoke.fanciticon.data.model.User
import com.wishpoke.fanciticon.presenation.base.BaseActivity
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import com.wishpoke.fanciticon.presenation.view.login.*
import com.wishpoke.fanciticon.presenation.view.main.MainActivity
import com.wishpoke.fanciticon.util.*
import com.wishpoke.fanciticon.util.constants.Constants
import com.wishpoke.fanciticon.util.constants.PrefKeys
import com.wishpoke.fanciticon.util.extention.createDialog
import com.wishpoke.fanciticon.util.extention.isVisibleWhen
import com.wishpoke.fanciticon.util.extention.open
import com.wishpoke.fanciticon.util.extention.openBrowser
import com.wishpoke.fanciticon.util.listener.DialogClickListener
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.include_sub_action_bar.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

class SettingsActivity : BaseActivity(), IBillingHandler {

    private lateinit var bp: BillingProcessor
    private var hasSwitchedFirstScreen = false
    private val loginViewModel: LoginViewModel by viewModel()
    
    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        ButterKnife.bind(this)
        init()
        setOnClickListeners()
        subscribeObservers()
    }

    private fun init() {
        bp = BillingProcessor(this, Constants.GooglePlayLicenseKey, this)
        bp.initialize()

        adLoader = FacebookAdLoader(this, banner_container, ll_isn, wv_edge)

        val versionLabel = "${getString(R.string.app_name)} ${getString(R.string.version)} ${Util.getVersionName(this)}"
        tv_version.text = versionLabel

        sw_push_notifications.isChecked = storage.pushNotification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            sw_floating_shortcut.isChecked = Settings.canDrawOverlays(this)
        }
        sw_push_notifications.setOnCheckedChangeListener { buttonView, isChecked ->
            if (!isChecked) {
                if (Util.isKorean()) {
                    mDialog = createDialog(
                            message = R.string.turn_off_push,
                            negativeLabel = R.string.cancel,
                            positiveBlock = {
                                storage.pushNotification = isChecked
                            },
                            negativeBlock = {
                                storage.pushNotification = true
                                sw_push_notifications.isChecked = true
                            }
                    ).apply { show() }
                } else {
                    storage.pushNotification = isChecked
                }
            }
        }
        val checked = sw_floating_shortcut.isChecked
        sw_floating_shortcut.setOnClickListener {
            sw_floating_shortcut.isChecked = checked
            askForSystemOverlayPermission()
        }

        // 로그인 / 미션 관리 / 첫화면 설정은 한국인들한테만 보이기
        btn_first_screen.isVisibleWhen(Util.isKorean())
        btn_login.isVisibleWhen(Util.isKorean())
        btn_logout.isVisibleWhen(storage.isLoggedIn)
        btn_edit_profile.isVisibleWhen(storage.isLoggedIn)
        updateLoginStateView(btn_login)

        btn_share.visibility = View.VISIBLE
    }

    private fun subscribeObservers() {
        loginViewModel.logout.observe(this, androidx.lifecycle.Observer {
            when (it.status) {
                LoadingState.Status.SUCCESS -> {
                    storage.put(PrefKeys.USER, "")
                    updateLoginStateView(btn_login)
                    btn_logout.isVisibleWhen(storage.isLoggedIn)
                }
                LoadingState.Status.FAILED -> {
                    Toaster.show(applicationContext, R.string.error_unknown, it.throwable?.code ?: 0)
                }
            }
        })
    }

    // Widget 권한 묻기
    private fun askForSystemOverlayPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:$packageName"))
            startActivityForResult(intent, DRAW_OVER_OTHER_APP_PERMISSION)
        }
    }

    override fun onPause() {
        storage.setQuitAt()
        super.onPause()
    }

    override fun onResume() {
        val handler = Handler()
        handler.postDelayed({
            val checked = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(mContext)
            sw_floating_shortcut.isChecked = checked
        }, 500)
        updateLoginStateView(btn_login)
        btn_logout.isVisibleWhen(storage.isLoggedIn)
        btn_remove_ads.isVisibleWhen(!storage.purchasedRemoveAds())
        btn_edit_profile.isVisibleWhen(storage.isLoggedIn)
        super.onResume()
    }

    override fun onProductPurchased(productId: String, details: TransactionDetails?) {
        bp.loadOwnedPurchasesFromGoogle()
        storage.setPurchasedRemoveAds(bp.isPurchased(Constants.NoAdsSku))
        btn_remove_ads.isVisibleWhen(!storage.purchasedRemoveAds())
    }

    override fun onPurchaseHistoryRestored() {
        storage.setPurchasedRemoveAds(bp.isPurchased(Constants.NoAdsSku))
    }

    override fun onBillingError(errorCode: Int, error: Throwable?) {
        if (errorCode != BILLING_RESPONSE_RESULT_USER_CANCELED) {
            Snackbar.make(findViewById(android.R.id.content), R.string.error_unknown, Snackbar.LENGTH_SHORT).show()
        }
    }

    override fun onBillingInitialized() {
        bp.loadOwnedPurchasesFromGoogle()
        storage.setPurchasedRemoveAds(bp.isPurchased(Constants.NoAdsSku))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == DRAW_OVER_OTHER_APP_PERMISSION) {
            val handler = Handler()
            handler.postDelayed({
                val checked = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(mContext)
                sw_floating_shortcut.isChecked = checked
            }, 200)
        }
        if (!bp.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    public override fun onDestroy() {
        bp.release()
        super.onDestroy()
    }
    
    private fun setOnClickListeners() {
        btn_back.setOnClickListener { onBackPressed() }
        btn_login.setOnClickListener {
            val user = storage.getObject(PrefKeys.USER, User::class.java)
            if (user == null) {
                // 로그인
                goTo(mActivity, LoginActivity::class.java)
            } else {
                // 로그아웃 또는 회원 탈퇴
                val buttons = arrayOf(getString(R.string.settings_logout), getString(R.string.settings_leave), getString(R.string.settings_change_password))
                mDialog = DialogManager.getInstance().createNChoiceDialog(mContext, buttons, object : DialogClickListener<Int> {
                    override fun onClick(viewId: Int) {
                        mDialog?.dismiss()
                        when (viewId) {
                            R.id.btn_1 -> {
                                // 로그아웃
                                btn_logout.performClick()
                            }
                            R.id.btn_2 -> {
                                // 회원탈퇴
                                goTo(mActivity, LeaveActivity::class.java)
                            }
                            R.id.btn_3 -> {
                                // 비밀번호 변경
                                val user = storage.getObject(PrefKeys.USER, User::class.java)
                                if (user != null) {
                                    val phoneInfo = PhoneInfo(user.nacd, user.phone)
                                    open<ResetPasswordStep2Activity> {
                                        putExtra("phoneInfo", Gson().toJson(phoneInfo))
                                        putExtra("isChangePassword", true)
                                    }
                                }

                            }
                        }
                    }
                })
                mDialog?.show()
            }
        }
        btn_logout.setOnClickListener {
            // 로그아웃
            mDialog = createDialog(
                    message = R.string.dialog_logout,
                    negativeLabel = R.string.cancel,
                    positiveBlock = {
                        val fcmToken = storage.getString(PrefKeys.FCM_TOKEN, "")
                        loginViewModel.logout(storage.userId, fcmToken)
                    }
            ).apply { show() }
        }
        btn_how_to_use.setOnClickListener {
            it.startAnimation(Animator.getClickAnimation(mContext))
            open<WebViewActivity> {
                if (Locale.getDefault().language == "ko") {
                    putExtra("url", Constants.Url.YOUTUBE)
                } else {
                    putExtra("url", Constants.Url.YOUTUBE_EN)
                }
            }
        }
        btn_feedback.setOnClickListener { goTo(this, FeedbackActivity::class.java) }
        btn_share.setOnClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_SEND
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name))
            intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_message) + " " + getString(R.string.store_url))
            val chooser = Intent.createChooser(intent, getString(R.string.share_this_app))
            startActivity(chooser)
        }
        btn_review.setOnClickListener {
            val appPackageName = packageName
            try {
                openBrowser("market://details?id=$appPackageName")
            } catch (anfe: ActivityNotFoundException) {
                openBrowser("https://play.google.com/store/apps/details?id=$appPackageName")
            }
        }
        tv_terms.setOnClickListener {
            it.startAnimation(Animator.getClickAnimation(mContext))
            // 이용약관 열기
            open<WebViewActivity> {
                putExtra("url", Constants.Url.TERMS)
            }
        }
        btn_remove_ads.setOnClickListener {
            if (!storage.purchasedRemoveAds()) {
                bp.purchase(this@SettingsActivity, Constants.NoAdsSku)
            }
        }
        tv_wishpoke.setOnClickListener {
            it.startAnimation(Animator.getClickAnimation(mContext))
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(Constants.Url.DEVELOPER_GOOGLE_PLAY)))
        }
        btn_first_screen.setOnClickListener {
            // 첫화면 순서변경
            val labels = resources.getStringArray(R.array.first_screens)
            val categoryIds = listOf("favorite", "pung", "chat")
            val categoryId = storage.getString(AppStorage.FIRST_SCREEN_CATEGORY_ID, "favorite")
            val index = categoryIds.indexOf(categoryId)
            mDialog = DialogManager.getInstance().createSingleChoiceDialog(this, labels, index) { dialogInterface: DialogInterface?, selectedIndex: Int ->
                mDialog?.dismiss()
                if (index != selectedIndex) {
                    hasSwitchedFirstScreen = true
                    storage.put(AppStorage.FIRST_SCREEN_CATEGORY_ID, categoryIds[selectedIndex])
                    mMainViewModel.resetCategories()
                }
            }
            mDialog?.show()
        }
        btn_edit_profile.setOnClickListener {
            open<EditProfileActivity>()
        }
    }

    override fun onBackPressed() {
        if (hasSwitchedFirstScreen) {
            // 첫화면을 바꾸었다면 새로 시작
            open<MainActivity> {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            finish()
        } else {
            super.onBackPressed()
        }
    }

    companion object {
        private const val DRAW_OVER_OTHER_APP_PERMISSION = 1232
    }
}