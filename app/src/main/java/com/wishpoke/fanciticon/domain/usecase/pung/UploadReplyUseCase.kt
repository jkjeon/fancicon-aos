package com.wishpoke.fanciticon.domain.usecase.pung

import com.wishpoke.fanciticon.data.repository.PungReplyRepository


class UploadReplyUseCase(private val repository: PungReplyRepository) {
    suspend operator fun invoke(userId: String?, postNo: Int, uuid: String?, password: String?, username: String?,
                                comment: String?, fileName: String?, movieName: String?)
            = repository.uploadReply(userId, postNo, uuid, password, username, comment, fileName, movieName)
}