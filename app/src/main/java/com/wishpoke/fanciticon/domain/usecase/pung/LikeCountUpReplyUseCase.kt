package com.wishpoke.fanciticon.domain.usecase.pung

import com.wishpoke.fanciticon.data.repository.PungReplyRepository

class LikeCountUpReplyUseCase(private val repository: PungReplyRepository) {
    suspend operator fun invoke(replyNo: Int)
            = repository.likeCountUpReply(replyNo)
}