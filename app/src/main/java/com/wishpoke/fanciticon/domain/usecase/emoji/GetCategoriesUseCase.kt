package com.wishpoke.fanciticon.domain.usecase.emoji

import com.wishpoke.fanciticon.data.repository.EmojiRepository

class GetCategoriesUseCase(private val repository: EmojiRepository) {
    suspend operator fun invoke(firstScreen: String, categoryHashMap: HashMap<String, String>)
            = repository.getCategories(firstScreen, categoryHashMap)
}