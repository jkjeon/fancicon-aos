package com.wishpoke.fanciticon.domain.usecase.chat

import com.wishpoke.fanciticon.data.repository.ChatRepository

class JoinChatRoomUseCase(private val chatRepository: ChatRepository) {
    operator fun invoke(userId: String?, roomId: String, stageName: String, fontColorType: Int, fcmToken: String)
            = chatRepository.joinChatRoom(userId, roomId, stageName, fontColorType, fcmToken)
}