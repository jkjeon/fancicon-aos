package com.wishpoke.fanciticon.domain.usecase.chat

import com.wishpoke.fanciticon.data.repository.ChatRepository

class ExitChatRoomUseCase(private val chatRepository: ChatRepository) {
    operator fun invoke(userId: String?, roomId: String, stageName: String)
            = chatRepository.exitChatRoom(userId, roomId, stageName)
}