package com.wishpoke.fanciticon.domain.usecase.pung

import com.wishpoke.fanciticon.data.repository.PungRepository

class GetNicknameUseCase(private val repository: PungRepository) {
    suspend operator fun invoke(uuid: String?)
            = repository.getNickname(uuid)
}