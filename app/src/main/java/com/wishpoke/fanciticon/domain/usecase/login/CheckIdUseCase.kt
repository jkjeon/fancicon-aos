package com.wishpoke.fanciticon.domain.usecase.login

import com.wishpoke.fanciticon.data.model.response.DefaultResponse
import com.wishpoke.fanciticon.data.repository.LoginRepository
import com.wishpoke.fanciticon.presenation.resource.SingleResponse

class CheckIdUseCase(private val repository: LoginRepository) {
    operator fun invoke(userId: String, referralCode: String, singleResponse: SingleResponse<DefaultResponse>)
            = repository.checkId(userId, referralCode, singleResponse)
}