package com.wishpoke.fanciticon.domain.usecase.main

import com.wishpoke.fanciticon.data.repository.MainRepository

class GetVersionUseCase(private val repository: MainRepository) {
    suspend operator fun invoke() = repository.getVersion()
}