package com.wishpoke.fanciticon.domain.usecase.pung

import com.wishpoke.fanciticon.data.repository.PungReplyRepository

class ReportReplyUseCase(private val repository: PungReplyRepository) {
    suspend operator fun invoke(userId: String?, replyNo: Int?, comment: String?)
            = repository.reportReply(userId, replyNo, comment)
}