package com.wishpoke.fanciticon.domain.usecase.pung

import com.wishpoke.fanciticon.data.repository.PungRepository

class DeletePostUseCase(private val repository: PungRepository) {
    suspend operator fun invoke(userId: String?, postNo: Int, uuid: String?, password: String?)
            = repository.deletePost(userId, postNo, uuid, password)
}