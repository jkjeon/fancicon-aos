package com.wishpoke.fanciticon.domain.usecase.pung

import com.wishpoke.fanciticon.data.repository.PungRepository

class GetPopularPostNoUseCase(private val repository: PungRepository) {
    suspend operator fun invoke(userId: String?, boardNo: Int?)
            = repository.getPopularPostNo(userId, boardNo)
}