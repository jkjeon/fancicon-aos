package com.wishpoke.fanciticon.domain.usecase.login

import com.wishpoke.fanciticon.data.model.User
import com.wishpoke.fanciticon.data.repository.LoginRepository

class LogInUseCase(private val repository: LoginRepository) {
    suspend operator fun invoke(user: User, fcmToken: String)
            = repository.login(user, fcmToken)
}