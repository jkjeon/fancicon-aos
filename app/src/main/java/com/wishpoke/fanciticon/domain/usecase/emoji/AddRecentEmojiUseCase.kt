package com.wishpoke.fanciticon.domain.usecase.emoji

import com.wishpoke.fanciticon.presenation.resource.AppStorage

class AddRecentEmojiUseCase(private val storage: AppStorage) {
    operator fun invoke(text: String?)
            = storage.addRecent(text)
}