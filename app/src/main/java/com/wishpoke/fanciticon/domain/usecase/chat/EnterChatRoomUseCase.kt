package com.wishpoke.fanciticon.domain.usecase.chat

import com.wishpoke.fanciticon.data.repository.ChatRepository
import com.wishpoke.fanciticon.util.listener.ValueCallback
import ua.naiksoftware.stomp.dto.StompMessage

class EnterChatRoomUseCase(private val chatRepository: ChatRepository) {
    operator fun invoke(roomId: String, valueCallback: ValueCallback<StompMessage>)
            = chatRepository.enterChatRoom(roomId, valueCallback)
}