package com.wishpoke.fanciticon.domain.usecase.pung

import com.wishpoke.fanciticon.data.repository.PungRepository

class UploadPostUseCase(private val repository: PungRepository) {
    suspend operator fun invoke(userId: String?, fcmToken: String?, uuid: String?, password: String?, username: String?, boardNo: Int, comment: String?,
                                fileName: String?, movieName: String?, linkUrl: String?)
            = repository.uploadPost(userId, fcmToken, uuid, password, username, boardNo, comment, fileName, movieName, linkUrl)
}