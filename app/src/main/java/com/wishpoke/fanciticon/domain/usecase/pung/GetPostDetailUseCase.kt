package com.wishpoke.fanciticon.domain.usecase.pung

import com.wishpoke.fanciticon.data.repository.PungRepository

class GetPostDetailUseCase(private val repository: PungRepository) {
    suspend operator fun invoke(userId: String?, postNo: Int?)
            = repository.getPostDetail(userId, postNo)
}