package com.wishpoke.fanciticon.domain.usecase.login

import com.wishpoke.fanciticon.data.repository.LoginRepository

class LogOutUseCase(private val repository: LoginRepository) {
    suspend operator fun invoke(userId: String?, fcmToken: String)
            = repository.logout(userId, fcmToken)
}