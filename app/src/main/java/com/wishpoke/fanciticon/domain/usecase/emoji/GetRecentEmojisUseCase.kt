package com.wishpoke.fanciticon.domain.usecase.emoji

import com.wishpoke.fanciticon.presenation.resource.AppStorage

class GetRecentEmojisUseCase(private val storage: AppStorage) {
    operator fun invoke()
            = storage.recentArrayList
}