package com.wishpoke.fanciticon.domain.usecase.chat

import com.wishpoke.fanciticon.data.repository.ChatRepository

class SendMessageUseCase(private val chatRepository: ChatRepository) {
    operator fun invoke(userId: String?, roomId: String, stageName: String, fontColorType: Int, message: String, fileName: String)
            = chatRepository.sendMessage(userId, roomId, stageName, fontColorType, message, fileName)
}