package com.wishpoke.fanciticon.domain.usecase.chat

import com.wishpoke.fanciticon.data.repository.ChatRoomRepository

class CanJoinToChatRoomUseCase(private val repository: ChatRoomRepository) {
    suspend operator fun invoke(userId: String?, roomId: String?, password: String?, stageName: String?)
            = repository.canJoinToChatRoom(userId, roomId, password, stageName)
}