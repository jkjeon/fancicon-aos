package com.wishpoke.fanciticon.domain.usecase.pung

import com.wishpoke.fanciticon.data.repository.PungRepository

class LikeCountUpUseCase(private val repository: PungRepository) {
    suspend operator fun invoke(postNo: Int)
            = repository.likeCountUp(postNo)
}