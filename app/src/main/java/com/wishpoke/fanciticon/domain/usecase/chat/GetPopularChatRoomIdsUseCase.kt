package com.wishpoke.fanciticon.domain.usecase.chat

import com.wishpoke.fanciticon.data.repository.ChatRoomRepository

class GetPopularChatRoomIdsUseCase(private val repository: ChatRoomRepository) {
    suspend operator fun invoke(boardNo: Int)
            = repository.getPopularChatRoomIds(boardNo)
}