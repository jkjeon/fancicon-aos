package com.wishpoke.fanciticon.domain.usecase.file

import com.wishpoke.fanciticon.data.repository.FileRepository

class DeleteFileUseCase(private val repository: FileRepository) {
    suspend operator fun invoke(fileName: String?)
            = repository.deleteFile(fileName)
}