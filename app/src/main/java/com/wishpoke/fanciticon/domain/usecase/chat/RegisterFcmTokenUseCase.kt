package com.wishpoke.fanciticon.domain.usecase.chat

import com.wishpoke.fanciticon.data.model.response.DefaultResponse
import com.wishpoke.fanciticon.data.repository.ChatRepository
import com.wishpoke.fanciticon.presenation.resource.SingleResponse

class RegisterFcmTokenUseCase(private val chatRepository: ChatRepository) {
    operator fun invoke(userId: String, fcmToken: String, singleResponse: SingleResponse<DefaultResponse>)
            = chatRepository.registerFcmToken(userId, fcmToken, singleResponse)
}