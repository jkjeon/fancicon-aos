package com.wishpoke.fanciticon.domain.usecase.pung

import com.wishpoke.fanciticon.data.repository.PungReplyRepository

class DeleteReplyUseCase(private val repository: PungReplyRepository) {
    suspend operator fun invoke(userId: String?, replyNo: Int, uuid: String?, password: String?)
            = repository.deleteReply(userId, replyNo, uuid, password)
}