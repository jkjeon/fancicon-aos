package com.wishpoke.fanciticon.domain.usecase.login

import com.wishpoke.fanciticon.data.model.PhoneInfo
import com.wishpoke.fanciticon.data.model.response.ObjectResponse
import com.wishpoke.fanciticon.data.repository.LoginRepository
import com.wishpoke.fanciticon.presenation.resource.SingleResponse

class FindIdUseCase(private val repository: LoginRepository) {
    operator fun invoke(phoneInfo: PhoneInfo, singleResponse: SingleResponse<ObjectResponse<Any>>)
            = repository.findId(phoneInfo, singleResponse)
}