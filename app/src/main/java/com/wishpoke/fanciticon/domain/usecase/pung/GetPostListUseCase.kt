package com.wishpoke.fanciticon.domain.usecase.pung

import com.wishpoke.fanciticon.data.repository.PungRepository

class GetPostListUseCase(private val repository: PungRepository) {
    suspend operator fun invoke(userId: String?, orderType: Int?, boardNo: Int?, requestPage: Int?)
            = repository.getPostList(userId, orderType, boardNo, requestPage)
}