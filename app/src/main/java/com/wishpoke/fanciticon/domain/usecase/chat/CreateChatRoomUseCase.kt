package com.wishpoke.fanciticon.domain.usecase.chat

import com.wishpoke.fanciticon.data.model.chat.ChatRoom
import com.wishpoke.fanciticon.data.repository.ChatRoomRepository

class CreateChatRoomUseCase(private val repository: ChatRoomRepository) {
    suspend operator fun invoke(userId: String?, chatRoom: ChatRoom, fcmToken: String)
    = repository.createChatRoom(userId, chatRoom, fcmToken)
}