package com.wishpoke.fanciticon.domain.usecase.login

import com.wishpoke.fanciticon.data.model.PhoneInfo
import com.wishpoke.fanciticon.data.model.response.DefaultResponse
import com.wishpoke.fanciticon.data.repository.LoginRepository
import com.wishpoke.fanciticon.presenation.resource.SingleResponse

class UpdatePasswordUseCase(private val repository: LoginRepository) {
    operator fun invoke(phoneInfo: PhoneInfo, password: String, singleResponse: SingleResponse<DefaultResponse>)
            = repository.updatePassword(phoneInfo, password, singleResponse)
}