package com.wishpoke.fanciticon.domain.usecase.chat

import com.wishpoke.fanciticon.data.repository.ChatRepository

class LoadMessagesUseCase(private val chatRepository: ChatRepository) {
    suspend operator fun invoke(userId: String?, roomId: String, messageNo: Int)
            = chatRepository.loadMessages(userId, roomId, messageNo)
}