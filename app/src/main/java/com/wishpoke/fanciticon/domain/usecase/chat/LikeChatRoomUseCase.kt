package com.wishpoke.fanciticon.domain.usecase.chat

import com.wishpoke.fanciticon.data.repository.ChatRepository

class LikeChatRoomUseCase(private val chatRepository: ChatRepository) {
    suspend operator fun invoke(roomId: String)
            = chatRepository.likeChatRoom(roomId)
}