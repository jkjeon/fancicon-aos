package com.wishpoke.fanciticon.domain.usecase.chat

import com.wishpoke.fanciticon.data.repository.ChatRepository

class TogglePushUseCase(private val chatRepository: ChatRepository) {
    suspend operator fun invoke(userId: String?, roomId: String)
            = chatRepository.togglePush(userId, roomId)
}