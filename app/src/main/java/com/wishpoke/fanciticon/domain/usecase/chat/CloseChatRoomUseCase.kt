package com.wishpoke.fanciticon.domain.usecase.chat

import com.wishpoke.fanciticon.data.repository.ChatRepository

class CloseChatRoomUseCase(private val chatRepository: ChatRepository) {
    operator fun invoke(roomId: String)
            = chatRepository.closeChatRoom(roomId)
}