package com.wishpoke.fanciticon.domain.usecase.pung

import com.wishpoke.fanciticon.data.repository.PungReplyRepository

class UpdateReplyUseCase(private val repository: PungReplyRepository) {
    suspend operator fun invoke(userId: String?, replyNo: Int?,
                                uuid: String?, password: String?,
                                comment: String?, fileName: String?, movieName: String?)
            = repository.updateReply(userId, replyNo, uuid, password, comment, fileName, movieName)
}