package com.wishpoke.fanciticon.domain.usecase.chat

import com.wishpoke.fanciticon.data.repository.ChatRepository

class KickChatMemberUseCase(private val chatRepository: ChatRepository) {
    operator fun invoke(roomId: String, userId: String, stageName: String, userNo: Int)
            = chatRepository.kickChatMember(roomId, userId, stageName, userNo)
}