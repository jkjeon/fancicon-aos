package com.wishpoke.fanciticon.domain.usecase.pung

import com.wishpoke.fanciticon.data.repository.PungRepository

class UpdatePostUseCase(private val repository: PungRepository) {
    suspend operator fun invoke(userId: String?, postNo: Int, uuid: String?, password: String?,
                                comment: String?, fileName: String?, movieName: String?, linkUrl: String?)
            = repository.updatePost(userId, postNo, uuid, password, comment, fileName, movieName, linkUrl)
}