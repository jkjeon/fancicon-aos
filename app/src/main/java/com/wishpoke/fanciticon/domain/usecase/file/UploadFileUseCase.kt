package com.wishpoke.fanciticon.domain.usecase.file

import com.wishpoke.fanciticon.data.repository.FileRepository
import java.io.File

class UploadFileUseCase(private val repository: FileRepository) {
    suspend operator fun invoke(file: File)
            = repository.uploadFile(file)
}