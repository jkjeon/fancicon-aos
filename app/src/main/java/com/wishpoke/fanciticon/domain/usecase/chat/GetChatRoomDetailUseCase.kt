package com.wishpoke.fanciticon.domain.usecase.chat

import com.wishpoke.fanciticon.data.repository.ChatRepository

class GetChatRoomDetailUseCase(private val chatRepository: ChatRepository) {
    suspend operator fun invoke(userId: String?,
                                roomId: String?)
            = chatRepository.getChatRoomDetail(userId, roomId ?: "")
}