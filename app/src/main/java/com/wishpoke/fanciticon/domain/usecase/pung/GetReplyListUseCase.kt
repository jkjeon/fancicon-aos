package com.wishpoke.fanciticon.domain.usecase.pung

import com.wishpoke.fanciticon.data.repository.PungReplyRepository

class GetReplyListUseCase(private val repository: PungReplyRepository) {
    suspend operator fun invoke(userId: String?, postNo: Int?, requestPage: Int)
            = repository.getReplyList(userId, postNo, requestPage)
}