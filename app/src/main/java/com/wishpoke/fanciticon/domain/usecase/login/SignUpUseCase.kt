package com.wishpoke.fanciticon.domain.usecase.login

import com.wishpoke.fanciticon.data.model.User
import com.wishpoke.fanciticon.data.model.response.ObjectResponse
import com.wishpoke.fanciticon.data.repository.LoginRepository
import com.wishpoke.fanciticon.presenation.resource.SingleResponse

class SignUpUseCase(private val repository: LoginRepository) {
    operator fun invoke(user: User, singleResponse: SingleResponse<ObjectResponse<User>>)
            = repository.insertUser(user, singleResponse)
}