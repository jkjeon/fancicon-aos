package com.wishpoke.fanciticon.domain.usecase.chat

import com.wishpoke.fanciticon.data.repository.ChatRoomRepository

class GetChatRoomListUseCase(private val repository: ChatRoomRepository) {
    suspend operator fun invoke(userId: String? = null,
                                requestPage: Int,
                                boardNo: Int,
                                orderType: Int)
            = repository.getChatRoomList(userId, requestPage, boardNo, orderType)
}