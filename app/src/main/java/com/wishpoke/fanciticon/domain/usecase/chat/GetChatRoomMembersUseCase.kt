package com.wishpoke.fanciticon.domain.usecase.chat

import com.wishpoke.fanciticon.data.repository.ChatRepository

class GetChatRoomMembersUseCase(private val chatRepository: ChatRepository) {
    suspend operator fun invoke(roomId: String)
            = chatRepository.getChatRoomMembers(roomId)
}