package com.wishpoke.fanciticon.domain.usecase.chat

import com.wishpoke.fanciticon.data.repository.ChatRepository
import com.wishpoke.fanciticon.util.listener.SocketStateListener


class ConnectSocketUseCase(private val chatRepository: ChatRepository) {
    operator fun invoke(socketStateListener: SocketStateListener)
            = chatRepository.connectSocket(socketStateListener)
}