package com.wishpoke.fanciticon.domain.usecase.pung

import com.wishpoke.fanciticon.data.repository.PungRepository

class ReportPostUseCase(private val repository: PungRepository) {
    suspend operator fun invoke(userId: String?, postNo: Int?, comment: String?)
            = repository.reportPost(userId, postNo, comment)
}