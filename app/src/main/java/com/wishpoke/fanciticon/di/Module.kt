package com.wishpoke.fanciticon.di

import com.wishpoke.fanciticon.data.repository.*
import com.wishpoke.fanciticon.domain.usecase.chat.*
import com.wishpoke.fanciticon.domain.usecase.emoji.AddRecentEmojiUseCase
import com.wishpoke.fanciticon.domain.usecase.emoji.GetCategoriesUseCase
import com.wishpoke.fanciticon.domain.usecase.emoji.GetRecentEmojisUseCase
import com.wishpoke.fanciticon.domain.usecase.file.DeleteFileUseCase
import com.wishpoke.fanciticon.domain.usecase.file.UploadFileUseCase
import com.wishpoke.fanciticon.domain.usecase.login.*
import com.wishpoke.fanciticon.domain.usecase.pung.*
import com.wishpoke.fanciticon.presenation.resource.AppStorage
import org.koin.dsl.module

val appModule = module {
    factory { AppStorage(get()) }
}

val useCaseModule = module {
    // Chat
    factory { CreateChatRoomUseCase(get()) }
    factory { GetChatRoomListUseCase(get()) }
    factory { GetChatRoomDetailUseCase(get()) }
    factory { EnterChatRoomUseCase(get()) }
    factory { ConnectSocketUseCase(get()) }
    factory { JoinChatRoomUseCase(get()) }
    factory { SendMessageUseCase(get()) }
    factory { LikeChatRoomUseCase(get()) }
    factory { ShareChatRoomUseCase(get()) }
    factory { CloseChatRoomUseCase(get()) }
    factory { CanJoinToChatRoomUseCase(get()) }
    factory { ExitChatRoomUseCase(get()) }
    factory { KickChatMemberUseCase(get()) }
    factory { GetChatRoomMembersUseCase(get()) }
    factory { GetPopularChatRoomIdsUseCase(get()) }
    factory { TogglePushUseCase(get()) }
    factory { LoadMessagesUseCase(get()) }
    factory { RegisterFcmTokenUseCase(get()) }

    // Login
    factory { CheckIdUseCase(get()) }
    factory { FindIdUseCase(get()) }
    factory { GetAuthTokenUseCase(get()) }
    factory { GetUserDetailUseCase(get()) }
    factory { LeaveUseCase(get()) }
    factory { LogInUseCase(get()) }
    factory { LogOutUseCase(get()) }
    factory { SignUpUseCase(get()) }
    factory { UpdatePasswordUseCase(get()) }
    factory { UpdateUserPointUseCase(get()) }

    // Emoji
    factory { GetCategoriesUseCase(get()) }
    factory { AddRecentEmojiUseCase(get()) }
    factory { GetRecentEmojisUseCase(get()) }

    // Pung
    factory { DeletePostUseCase(get()) }
    factory { GetPopularPostNoUseCase(get()) }
    factory { GetPostListUseCase(get()) }
    factory { GetReplyListUseCase(get()) }
    factory { LikeCountUpUseCase(get()) }
    factory { LikeCountUpReplyUseCase(get()) }
    factory { ShareCountUpUseCase(get()) }
    factory { ReportPostUseCase(get()) }
    factory { UploadPostUseCase(get()) }
    factory { ReportReplyUseCase(get()) }
    factory { UploadReplyUseCase(get()) }
    factory { GetPostDetailUseCase(get()) }
    factory { UpdatePostUseCase(get()) }
    factory { ValidatePostUseCase(get()) }
    factory { ValidateReplyUseCase(get()) }
    factory { DeleteReplyUseCase(get()) }
    factory { UpdateReplyUseCase(get()) }
    factory { GetNicknameUseCase(get()) }

    // File
    factory { UploadFileUseCase(get()) }
    factory { DeleteFileUseCase(get()) }
}

val repoModule = module {
    single { ChatRepositoryImpl() as ChatRepository }
    single { ChatRoomRepositoryImpl() as ChatRoomRepository }
    single { LoginRepository() }
    single { EmojiRepositoryImpl() as EmojiRepository }
    single { PungReplyRepositoryImpl() as PungReplyRepository }
    single { PungRepositoryImpl() as PungRepository }
    single { FileRepositoryImpl() as FileRepository }
}