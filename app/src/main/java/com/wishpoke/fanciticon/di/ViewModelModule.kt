package com.wishpoke.fanciticon.di

import com.wishpoke.fanciticon.presenation.base.BaseViewModel
import com.wishpoke.fanciticon.presenation.view.chat.ChatRoomViewModel
import com.wishpoke.fanciticon.presenation.view.chat.ChatViewModel
import com.wishpoke.fanciticon.presenation.view.custom_emoji.CustomEmojiViewModel
import com.wishpoke.fanciticon.presenation.view.login.LoginViewModel
import com.wishpoke.fanciticon.presenation.view.main.EmojiViewModel
import com.wishpoke.fanciticon.presenation.view.main.MainViewModel
import com.wishpoke.fanciticon.presenation.view.pung.PungViewModel
import com.wishpoke.fanciticon.presenation.view.pung.reply.PungReplyViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel {
        LoginViewModel(androidApplication(), get(), get(),
                get(), get(),
                get(), get(),
                get(), get(),
                get(), get(), get())
    }
    viewModel { MainViewModel(androidApplication(), get()) }
    viewModel {
        PungViewModel(androidApplication(),
                get(), get(), get(), get(), get(), get(),
                get(), get(), get(), get(), get(), get(), get())
    }
    viewModel {
        ChatViewModel(androidApplication(), get(), get(), get(),
                get(), get(), get(),
                get(), get(), get(),
                get(), get(), get(),
                get(), get())
    }
    viewModel {
        ChatRoomViewModel(androidApplication(), get(), get(),
                get(), get())
    }
    viewModel { EmojiViewModel(androidApplication(), get(), get(), get(), get(), get()) }
    viewModel { PungReplyViewModel(androidApplication(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get()) }
    viewModel { BaseViewModel(androidApplication()) }
    viewModel { CustomEmojiViewModel(androidApplication(), get(), get()) }
}