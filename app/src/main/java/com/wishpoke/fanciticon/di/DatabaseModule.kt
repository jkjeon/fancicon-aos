package com.wishpoke.fanciticon.di

import androidx.room.Room
import com.wishpoke.fanciticon.R
import com.wishpoke.fanciticon.database.AppDatabase
import com.wishpoke.fanciticon.database.OldDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val databaseModule = module {
    single {
        Room.databaseBuilder(
                androidApplication(),
                AppDatabase::class.java,
                androidApplication().baseContext.getString(R.string.app_name)
        ).build()
    }


    single {
        Room.databaseBuilder(
                androidApplication(),
                OldDatabase::class.java,
                "custum_emojis"
        ).build()
    }

    single {
        get<AppDatabase>().emojiDao()
    }

    single {
        get<AppDatabase>().customEmojiDao()
    }

    single {
        get<OldDatabase>().customEmojiDao()
    }
}