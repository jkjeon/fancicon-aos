// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.chat;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class ChatAdapter$HeaderViewHolder_ViewBinding implements Unbinder {
  private ChatAdapter.HeaderViewHolder target;

  @UiThread
  public ChatAdapter$HeaderViewHolder_ViewBinding(ChatAdapter.HeaderViewHolder target,
      View source) {
    this.target = target;

    target.tvTimer = Utils.findRequiredViewAsType(source, R.id.tv_timer, "field 'tvTimer'", TextView.class);
  }

  @Override
  public void unbind() {
    ChatAdapter.HeaderViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvTimer = null;
  }
}
