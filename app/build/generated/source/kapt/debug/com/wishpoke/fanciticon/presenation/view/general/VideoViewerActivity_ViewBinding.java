// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.general;

import android.view.View;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class VideoViewerActivity_ViewBinding implements Unbinder {
  private VideoViewerActivity target;

  @UiThread
  public VideoViewerActivity_ViewBinding(VideoViewerActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public VideoViewerActivity_ViewBinding(VideoViewerActivity target, View source) {
    this.target = target;

    target.youTubePlayerView = Utils.findRequiredViewAsType(source, R.id.youtube_player_view, "field 'youTubePlayerView'", YouTubePlayerView.class);
  }

  @Override
  public void unbind() {
    VideoViewerActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.youTubePlayerView = null;
  }
}
