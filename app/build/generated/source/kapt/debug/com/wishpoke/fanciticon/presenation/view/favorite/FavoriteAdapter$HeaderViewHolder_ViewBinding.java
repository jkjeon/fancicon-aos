// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.favorite;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class FavoriteAdapter$HeaderViewHolder_ViewBinding implements Unbinder {
  private FavoriteAdapter.HeaderViewHolder target;

  @UiThread
  public FavoriteAdapter$HeaderViewHolder_ViewBinding(FavoriteAdapter.HeaderViewHolder target,
      View source) {
    this.target = target;

    target.tvLabel = Utils.findRequiredViewAsType(source, R.id.tv_label, "field 'tvLabel'", TextView.class);
  }

  @Override
  public void unbind() {
    FavoriteAdapter.HeaderViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvLabel = null;
  }
}
