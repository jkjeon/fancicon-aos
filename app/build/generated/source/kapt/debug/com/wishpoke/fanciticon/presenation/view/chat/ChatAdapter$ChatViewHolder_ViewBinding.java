// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.chat;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class ChatAdapter$ChatViewHolder_ViewBinding implements Unbinder {
  private ChatAdapter.ChatViewHolder target;

  @UiThread
  public ChatAdapter$ChatViewHolder_ViewBinding(ChatAdapter.ChatViewHolder target, View source) {
    this.target = target;

    target.tvName = Utils.findRequiredViewAsType(source, R.id.tv_name, "field 'tvName'", TextView.class);
    target.tvMessage = Utils.findRequiredViewAsType(source, R.id.tv_message, "field 'tvMessage'", TextView.class);
    target.llMetadata = Utils.findRequiredView(source, R.id.ll_metadata, "field 'llMetadata'");
    target.ivMetadataImageView = Utils.findRequiredViewAsType(source, R.id.iv_metadata_image, "field 'ivMetadataImageView'", ImageView.class);
    target.tvMetadataTitle = Utils.findRequiredViewAsType(source, R.id.tv_metadata_title, "field 'tvMetadataTitle'", TextView.class);
    target.tvMetadataDesc = Utils.findRequiredViewAsType(source, R.id.tv_metadata_desc, "field 'tvMetadataDesc'", TextView.class);
    target.tvMetadataDomain = Utils.findRequiredViewAsType(source, R.id.tv_metadata_domain, "field 'tvMetadataDomain'", TextView.class);
    target.viewChat = Utils.findRequiredView(source, R.id.view_chat, "field 'viewChat'");
    target.ivImage = Utils.findRequiredViewAsType(source, R.id.iv_image, "field 'ivImage'", ImageView.class);
    target.tvTime = Utils.findRequiredViewAsType(source, R.id.tv_time, "field 'tvTime'", TextView.class);
    target.llChatWrapper = Utils.findRequiredViewAsType(source, R.id.ll_chat_wrapper, "field 'llChatWrapper'", LinearLayout.class);
  }

  @Override
  public void unbind() {
    ChatAdapter.ChatViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvName = null;
    target.tvMessage = null;
    target.llMetadata = null;
    target.ivMetadataImageView = null;
    target.tvMetadataTitle = null;
    target.tvMetadataDesc = null;
    target.tvMetadataDomain = null;
    target.viewChat = null;
    target.ivImage = null;
    target.tvTime = null;
    target.llChatWrapper = null;
  }
}
