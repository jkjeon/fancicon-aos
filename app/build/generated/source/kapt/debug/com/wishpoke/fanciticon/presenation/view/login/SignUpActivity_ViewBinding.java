// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.login;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.UiThread;
import androidx.core.widget.NestedScrollView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import com.wishpoke.fanciticon.presenation.resource.ui.FormEditText;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class SignUpActivity_ViewBinding implements Unbinder {
  private SignUpActivity target;

  private View view7f0a02e4;

  private View view7f0a0088;

  private View view7f0a0075;

  private View view7f0a009e;

  @UiThread
  public SignUpActivity_ViewBinding(SignUpActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SignUpActivity_ViewBinding(final SignUpActivity target, View source) {
    this.target = target;

    View view;
    target.etId = Utils.findRequiredViewAsType(source, R.id.et_id, "field 'etId'", FormEditText.class);
    target.etPw = Utils.findRequiredViewAsType(source, R.id.et_pw, "field 'etPw'", FormEditText.class);
    target.etConfirmPw = Utils.findRequiredViewAsType(source, R.id.et_confirm_pw, "field 'etConfirmPw'", FormEditText.class);
    target.etPhone = Utils.findRequiredViewAsType(source, R.id.et_phone, "field 'etPhone'", EditText.class);
    target.etCode = Utils.findRequiredViewAsType(source, R.id.et_code, "field 'etCode'", FormEditText.class);
    target.btnSendCode = Utils.findRequiredViewAsType(source, R.id.btn_send_code, "field 'btnSendCode'", TextView.class);
    target.btnVerifyCode = Utils.findRequiredViewAsType(source, R.id.btn_verify_code, "field 'btnVerifyCode'", TextView.class);
    view = Utils.findRequiredView(source, R.id.tv_terms, "field 'tvTerms' and method 'onClick'");
    target.tvTerms = Utils.castView(view, R.id.tv_terms, "field 'tvTerms'", TextView.class);
    view7f0a02e4 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.tvTimer = Utils.findRequiredViewAsType(source, R.id.tv_timer, "field 'tvTimer'", TextView.class);
    target.tvVerificationDesc = Utils.findRequiredViewAsType(source, R.id.tv_verification_desc, "field 'tvVerificationDesc'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btn_id_check, "field 'btnIdCheck' and method 'onClick'");
    target.btnIdCheck = Utils.castView(view, R.id.btn_id_check, "field 'btnIdCheck'", TextView.class);
    view7f0a0088 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.tvIdMessage = Utils.findRequiredViewAsType(source, R.id.tv_id_message, "field 'tvIdMessage'", TextView.class);
    target.tvPwMessage = Utils.findRequiredViewAsType(source, R.id.tv_pw_message, "field 'tvPwMessage'", TextView.class);
    target.tvConfirmPwMessage = Utils.findRequiredViewAsType(source, R.id.tv_confirm_pw_message, "field 'tvConfirmPwMessage'", TextView.class);
    target.etReferralCode = Utils.findRequiredViewAsType(source, R.id.et_referral_code, "field 'etReferralCode'", FormEditText.class);
    target.nestedScrollView = Utils.findRequiredViewAsType(source, R.id.nestedScrollView, "field 'nestedScrollView'", NestedScrollView.class);
    view = Utils.findRequiredView(source, 2131361909, "method 'onClick'");
    view7f0a0075 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, 2131361950, "method 'onClick'");
    view7f0a009e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  public void unbind() {
    SignUpActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etId = null;
    target.etPw = null;
    target.etConfirmPw = null;
    target.etPhone = null;
    target.etCode = null;
    target.btnSendCode = null;
    target.btnVerifyCode = null;
    target.tvTerms = null;
    target.tvTimer = null;
    target.tvVerificationDesc = null;
    target.btnIdCheck = null;
    target.tvIdMessage = null;
    target.tvPwMessage = null;
    target.tvConfirmPwMessage = null;
    target.etReferralCode = null;
    target.nestedScrollView = null;

    view7f0a02e4.setOnClickListener(null);
    view7f0a02e4 = null;
    view7f0a0088.setOnClickListener(null);
    view7f0a0088 = null;
    view7f0a0075.setOnClickListener(null);
    view7f0a0075 = null;
    view7f0a009e.setOnClickListener(null);
    view7f0a009e = null;
  }
}
