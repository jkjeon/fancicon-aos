// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.chat;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.airbnb.lottie.LottieAnimationView;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class ChatRoomAdapter$ViewHolder_ViewBinding implements Unbinder {
  private ChatRoomAdapter.ViewHolder target;

  @UiThread
  public ChatRoomAdapter$ViewHolder_ViewBinding(ChatRoomAdapter.ViewHolder target, View source) {
    this.target = target;

    target.tvJoined = Utils.findRequiredView(source, R.id.tv_joined, "field 'tvJoined'");
    target.ivImage = Utils.findRequiredViewAsType(source, R.id.iv_image, "field 'ivImage'", ImageView.class);
    target.tvMaster = Utils.findRequiredViewAsType(source, R.id.tv_master, "field 'tvMaster'", TextView.class);
    target.tvDate = Utils.findRequiredViewAsType(source, R.id.tv_date, "field 'tvDate'", TextView.class);
    target.ivTier = Utils.findRequiredViewAsType(source, R.id.iv_tier, "field 'ivTier'", ImageView.class);
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'tvTitle'", TextView.class);
    target.tvCountShare = Utils.findRequiredViewAsType(source, R.id.tv_count_share, "field 'tvCountShare'", TextView.class);
    target.llShare = Utils.findRequiredView(source, R.id.ll_share, "field 'llShare'");
    target.llLike = Utils.findRequiredView(source, R.id.ll_like, "field 'llLike'");
    target.tvCountMember = Utils.findRequiredViewAsType(source, R.id.tv_count_member, "field 'tvCountMember'", TextView.class);
    target.ivLocked = Utils.findRequiredView(source, R.id.iv_locked, "field 'ivLocked'");
    target.avLike = Utils.findRequiredViewAsType(source, R.id.av_like, "field 'avLike'", LottieAnimationView.class);
    target.tvCountLike = Utils.findRequiredViewAsType(source, R.id.tv_count_like, "field 'tvCountLike'", TextView.class);
  }

  @Override
  public void unbind() {
    ChatRoomAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvJoined = null;
    target.ivImage = null;
    target.tvMaster = null;
    target.tvDate = null;
    target.ivTier = null;
    target.tvTitle = null;
    target.tvCountShare = null;
    target.llShare = null;
    target.llLike = null;
    target.tvCountMember = null;
    target.ivLocked = null;
    target.avLike = null;
    target.tvCountLike = null;
  }
}
