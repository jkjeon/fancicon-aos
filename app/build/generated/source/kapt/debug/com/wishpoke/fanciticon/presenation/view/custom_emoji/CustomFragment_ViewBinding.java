// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.custom_emoji;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.UiThread;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class CustomFragment_ViewBinding implements Unbinder {
  private CustomFragment target;

  @UiThread
  public CustomFragment_ViewBinding(CustomFragment target, View source) {
    this.target = target;

    target.rvEmoji = Utils.findRequiredViewAsType(source, R.id.rv_emoji, "field 'rvEmoji'", RecyclerView.class);
    target.btnCreateEmoji = Utils.findRequiredViewAsType(source, R.id.btn_create_emoji, "field 'btnCreateEmoji'", TextView.class);
    target.btnHowToCreate = Utils.findRequiredViewAsType(source, R.id.btn_how_to_create, "field 'btnHowToCreate'", LinearLayout.class);
    target.nestedScrollView = Utils.findRequiredViewAsType(source, R.id.nestedScrollView, "field 'nestedScrollView'", NestedScrollView.class);
  }

  @Override
  public void unbind() {
    CustomFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvEmoji = null;
    target.btnCreateEmoji = null;
    target.btnHowToCreate = null;
    target.nestedScrollView = null;
  }
}
