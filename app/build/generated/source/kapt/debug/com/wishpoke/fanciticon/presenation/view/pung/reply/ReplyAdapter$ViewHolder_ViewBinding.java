// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.pung.reply;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.airbnb.lottie.LottieAnimationView;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class ReplyAdapter$ViewHolder_ViewBinding implements Unbinder {
  private ReplyAdapter.ViewHolder target;

  @UiThread
  public ReplyAdapter$ViewHolder_ViewBinding(ReplyAdapter.ViewHolder target, View source) {
    this.target = target;

    target.tvUsername = Utils.findRequiredViewAsType(source, R.id.tv_username, "field 'tvUsername'", TextView.class);
    target.tvTime = Utils.findRequiredViewAsType(source, R.id.tv_time, "field 'tvTime'", TextView.class);
    target.tvComment = Utils.findRequiredViewAsType(source, R.id.tv_comment, "field 'tvComment'", TextView.class);
    target.tvScore = Utils.findRequiredViewAsType(source, R.id.tv_score, "field 'tvScore'", TextView.class);
    target.avLike = Utils.findRequiredViewAsType(source, R.id.av_like, "field 'avLike'", LottieAnimationView.class);
    target.ivImage = Utils.findRequiredViewAsType(source, R.id.iv_image, "field 'ivImage'", ImageView.class);
    target.rlMetadata = Utils.findRequiredViewAsType(source, R.id.rl_metadata, "field 'rlMetadata'", RelativeLayout.class);
    target.ivMetadataImage = Utils.findRequiredViewAsType(source, R.id.iv_metadata_image, "field 'ivMetadataImage'", ImageView.class);
    target.tvMetadataUrl = Utils.findRequiredViewAsType(source, R.id.tv_metdata_url, "field 'tvMetadataUrl'", TextView.class);
    target.tvMetadataTitle = Utils.findRequiredViewAsType(source, R.id.tv_metdata_title, "field 'tvMetadataTitle'", TextView.class);
    target.clVideo = Utils.findRequiredView(source, R.id.cl_video, "field 'clVideo'");
    target.ivPlayVideo = Utils.findRequiredView(source, R.id.iv_play_video, "field 'ivPlayVideo'");
    target.ivVideo = Utils.findRequiredViewAsType(source, R.id.iv_video, "field 'ivVideo'", ImageView.class);
    target.ivMore = Utils.findRequiredView(source, R.id.iv_more, "field 'ivMore'");
  }

  @Override
  public void unbind() {
    ReplyAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvUsername = null;
    target.tvTime = null;
    target.tvComment = null;
    target.tvScore = null;
    target.avLike = null;
    target.ivImage = null;
    target.rlMetadata = null;
    target.ivMetadataImage = null;
    target.tvMetadataUrl = null;
    target.tvMetadataTitle = null;
    target.clVideo = null;
    target.ivPlayVideo = null;
    target.ivVideo = null;
    target.ivMore = null;
  }
}
