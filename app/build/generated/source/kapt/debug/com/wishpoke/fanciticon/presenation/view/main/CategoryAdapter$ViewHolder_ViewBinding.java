// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.main;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class CategoryAdapter$ViewHolder_ViewBinding implements Unbinder {
  private CategoryAdapter.ViewHolder target;

  @UiThread
  public CategoryAdapter$ViewHolder_ViewBinding(CategoryAdapter.ViewHolder target, View source) {
    this.target = target;

    target.btnCategory = Utils.findRequiredViewAsType(source, R.id.btn_category, "field 'btnCategory'", CardView.class);
    target.tvCategory = Utils.findRequiredViewAsType(source, R.id.tv_category, "field 'tvCategory'", TextView.class);
    target.ivCategory = Utils.findRequiredViewAsType(source, R.id.iv_category, "field 'ivCategory'", ImageView.class);
    target.wrapper = Utils.findRequiredViewAsType(source, R.id.wrapper, "field 'wrapper'", RelativeLayout.class);
    target.viewNew = Utils.findRequiredViewAsType(source, R.id.view_new, "field 'viewNew'", LinearLayout.class);
  }

  @Override
  public void unbind() {
    CategoryAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnCategory = null;
    target.tvCategory = null;
    target.ivCategory = null;
    target.wrapper = null;
    target.viewNew = null;
  }
}
