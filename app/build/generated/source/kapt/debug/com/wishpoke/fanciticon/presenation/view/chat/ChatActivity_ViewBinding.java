// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.chat;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.airbnb.lottie.LottieAnimationView;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class ChatActivity_ViewBinding implements Unbinder {
  private ChatActivity target;

  private View view7f0a02cb;

  private View view7f0a005b;

  private View view7f0a0116;

  private View view7f0a01a0;

  private View view7f0a019d;

  private View view7f0a015c;

  private View view7f0a019f;

  private View view7f0a0075;

  private View view7f0a0161;

  private View view7f0a008e;

  @UiThread
  public ChatActivity_ViewBinding(ChatActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ChatActivity_ViewBinding(final ChatActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.tv_member, "field 'tvMember' and method 'onClick'");
    target.tvMember = Utils.castView(view, R.id.tv_member, "field 'tvMember'", TextView.class);
    view7f0a02cb = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.tvRoomTitle = Utils.findRequiredViewAsType(source, R.id.tv_room_title, "field 'tvRoomTitle'", TextView.class);
    view = Utils.findRequiredView(source, R.id.av_like, "field 'avLike' and method 'onClick'");
    target.avLike = Utils.castView(view, R.id.av_like, "field 'avLike'", LottieAnimationView.class);
    view7f0a005b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.et_message, "field 'etMessage', method 'onClick', and method 'onFocusChange'");
    target.etMessage = Utils.castView(view, R.id.et_message, "field 'etMessage'", EditText.class);
    view7f0a0116 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override
      public void onFocusChange(View p0, boolean p1) {
        target.onFocusChange(p0, p1);
      }
    });
    target.rvChat = Utils.findRequiredViewAsType(source, R.id.rv_chat, "field 'rvChat'", RecyclerView.class);
    target.pager = Utils.findRequiredViewAsType(source, R.id.pager, "field 'pager'", ViewPager.class);
    target.rvEmojiCategory = Utils.findRequiredViewAsType(source, R.id.rv_emoji_category, "field 'rvEmojiCategory'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.ll_send, "field 'llSend' and method 'onClick'");
    target.llSend = view;
    view7f0a01a0 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ll_photo, "field 'llPhoto' and method 'onClick'");
    target.llPhoto = view;
    view7f0a019d = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.ivPhoto = Utils.findRequiredView(source, R.id.iv_photo, "field 'ivPhoto'");
    view = Utils.findRequiredView(source, R.id.iv_emoji, "field 'ivEmoji' and method 'onClick'");
    target.ivEmoji = view;
    view7f0a015c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.ivThumbnail = Utils.findRequiredViewAsType(source, R.id.iv_thumbnail, "field 'ivThumbnail'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.ll_preview_container, "field 'llPreviewContainer' and method 'onClick'");
    target.llPreviewContainer = view;
    view7f0a019f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.ivPreviewThumbnail = Utils.findRequiredViewAsType(source, R.id.iv_preview_thumbnail, "field 'ivPreviewThumbnail'", ImageView.class);
    target.tvPreviewName = Utils.findRequiredViewAsType(source, R.id.tv_preview_name, "field 'tvPreviewName'", TextView.class);
    target.tvPreviewMessage = Utils.findRequiredViewAsType(source, R.id.tv_preview_message, "field 'tvPreviewMessage'", TextView.class);
    view = Utils.findRequiredView(source, 2131361909, "method 'onClick'");
    view7f0a0075 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, 2131362145, "method 'onClick'");
    view7f0a0161 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, 2131361934, "method 'onClick'");
    view7f0a008e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  public void unbind() {
    ChatActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvMember = null;
    target.tvRoomTitle = null;
    target.avLike = null;
    target.etMessage = null;
    target.rvChat = null;
    target.pager = null;
    target.rvEmojiCategory = null;
    target.llSend = null;
    target.llPhoto = null;
    target.ivPhoto = null;
    target.ivEmoji = null;
    target.ivThumbnail = null;
    target.llPreviewContainer = null;
    target.ivPreviewThumbnail = null;
    target.tvPreviewName = null;
    target.tvPreviewMessage = null;

    view7f0a02cb.setOnClickListener(null);
    view7f0a02cb = null;
    view7f0a005b.setOnClickListener(null);
    view7f0a005b = null;
    view7f0a0116.setOnClickListener(null);
    view7f0a0116.setOnFocusChangeListener(null);
    view7f0a0116 = null;
    view7f0a01a0.setOnClickListener(null);
    view7f0a01a0 = null;
    view7f0a019d.setOnClickListener(null);
    view7f0a019d = null;
    view7f0a015c.setOnClickListener(null);
    view7f0a015c = null;
    view7f0a019f.setOnClickListener(null);
    view7f0a019f = null;
    view7f0a0075.setOnClickListener(null);
    view7f0a0075 = null;
    view7f0a0161.setOnClickListener(null);
    view7f0a0161 = null;
    view7f0a008e.setOnClickListener(null);
    view7f0a008e = null;
  }
}
