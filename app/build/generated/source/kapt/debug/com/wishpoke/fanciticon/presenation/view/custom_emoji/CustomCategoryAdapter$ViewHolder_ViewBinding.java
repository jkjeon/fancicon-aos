// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.custom_emoji;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CustomCategoryAdapter$ViewHolder_ViewBinding implements Unbinder {
  private CustomCategoryAdapter.ViewHolder target;

  @UiThread
  public CustomCategoryAdapter$ViewHolder_ViewBinding(CustomCategoryAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.tvName = Utils.findRequiredViewAsType(source, R.id.tv_name, "field 'tvName'", TextView.class);
    target.viewLine = Utils.findRequiredView(source, R.id.view_line, "field 'viewLine'");
  }

  @Override
  @CallSuper
  public void unbind() {
    CustomCategoryAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvName = null;
    target.viewLine = null;
  }
}
