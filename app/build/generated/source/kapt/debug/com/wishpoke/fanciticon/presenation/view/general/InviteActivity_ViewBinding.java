// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.general;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class InviteActivity_ViewBinding implements Unbinder {
  private InviteActivity target;

  private View view7f0a009b;

  @UiThread
  public InviteActivity_ViewBinding(InviteActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public InviteActivity_ViewBinding(final InviteActivity target, View source) {
    this.target = target;

    View view;
    target.tvShare = Utils.findRequiredViewAsType(source, R.id.tv_share, "field 'tvShare'", TextView.class);
    target.tvSubDesc = Utils.findRequiredViewAsType(source, R.id.tv_sub_desc, "field 'tvSubDesc'", TextView.class);
    target.btnLogin = Utils.findRequiredViewAsType(source, R.id.btn_login, "field 'btnLogin'", TextView.class);
    view = Utils.findRequiredView(source, 2131361947, "method 'onClick'");
    view7f0a009b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  public void unbind() {
    InviteActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvShare = null;
    target.tvSubDesc = null;
    target.btnLogin = null;

    view7f0a009b.setOnClickListener(null);
    view7f0a009b = null;
  }
}
