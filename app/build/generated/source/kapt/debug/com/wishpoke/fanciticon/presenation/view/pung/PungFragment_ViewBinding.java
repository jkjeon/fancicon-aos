// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.pung;

import android.view.View;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class PungFragment_ViewBinding implements Unbinder {
  private PungFragment target;

  @UiThread
  public PungFragment_ViewBinding(PungFragment target, View source) {
    this.target = target;

    target.rvReply = Utils.findRequiredViewAsType(source, R.id.rv_reply, "field 'rvReply'", RecyclerView.class);
    target.swipeRefreshLayout = Utils.findOptionalViewAsType(source, R.id.swipeRefreshLayout, "field 'swipeRefreshLayout'", SwipeRefreshLayout.class);
  }

  @Override
  public void unbind() {
    PungFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvReply = null;
    target.swipeRefreshLayout = null;
  }
}
