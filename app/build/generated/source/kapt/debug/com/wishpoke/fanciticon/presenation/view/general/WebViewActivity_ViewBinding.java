// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.general;

import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class WebViewActivity_ViewBinding implements Unbinder {
  private WebViewActivity target;

  @UiThread
  public WebViewActivity_ViewBinding(WebViewActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public WebViewActivity_ViewBinding(WebViewActivity target, View source) {
    this.target = target;

    target.btnCancel = Utils.findRequiredViewAsType(source, R.id.btn_cancel, "field 'btnCancel'", ImageView.class);
    target.btnBack = Utils.findRequiredViewAsType(source, R.id.btn_back, "field 'btnBack'", ImageView.class);
    target.btnForward = Utils.findRequiredViewAsType(source, R.id.btn_forward, "field 'btnForward'", ImageView.class);
    target.etUrl = Utils.findRequiredViewAsType(source, R.id.et_url, "field 'etUrl'", EditText.class);
    target.pbLoading = Utils.findRequiredViewAsType(source, R.id.pb_loading, "field 'pbLoading'", ProgressBar.class);
    target.webView = Utils.findRequiredViewAsType(source, R.id.webView, "field 'webView'", WebView.class);
    target.llTools = Utils.findRequiredViewAsType(source, R.id.ll_tools, "field 'llTools'", LinearLayout.class);
    target.tvCancel = Utils.findRequiredViewAsType(source, R.id.tv_cancel, "field 'tvCancel'", TextView.class);
    target.tvShare = Utils.findRequiredViewAsType(source, R.id.tv_share, "field 'tvShare'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    WebViewActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnCancel = null;
    target.btnBack = null;
    target.btnForward = null;
    target.etUrl = null;
    target.pbLoading = null;
    target.webView = null;
    target.llTools = null;
    target.tvCancel = null;
    target.tvShare = null;
  }
}
