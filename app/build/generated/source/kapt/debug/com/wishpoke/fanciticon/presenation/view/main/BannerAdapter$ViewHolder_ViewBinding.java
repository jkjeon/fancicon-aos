// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.main;

import android.view.View;
import android.widget.ImageView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BannerAdapter$ViewHolder_ViewBinding implements Unbinder {
  private BannerAdapter.ViewHolder target;

  @UiThread
  public BannerAdapter$ViewHolder_ViewBinding(BannerAdapter.ViewHolder target, View source) {
    this.target = target;

    target.ivImage = Utils.findRequiredViewAsType(source, R.id.iv_image, "field 'ivImage'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    BannerAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivImage = null;
  }
}
