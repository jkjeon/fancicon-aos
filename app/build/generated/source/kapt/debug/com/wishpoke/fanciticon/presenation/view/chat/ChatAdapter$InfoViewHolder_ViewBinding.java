// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.chat;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class ChatAdapter$InfoViewHolder_ViewBinding implements Unbinder {
  private ChatAdapter.InfoViewHolder target;

  @UiThread
  public ChatAdapter$InfoViewHolder_ViewBinding(ChatAdapter.InfoViewHolder target, View source) {
    this.target = target;

    target.tvInfo = Utils.findRequiredViewAsType(source, R.id.tv_info, "field 'tvInfo'", TextView.class);
  }

  @Override
  public void unbind() {
    ChatAdapter.InfoViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvInfo = null;
  }
}
