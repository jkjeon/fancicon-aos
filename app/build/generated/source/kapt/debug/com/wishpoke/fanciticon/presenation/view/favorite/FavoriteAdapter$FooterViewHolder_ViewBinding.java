// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.favorite;

import android.view.View;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class FavoriteAdapter$FooterViewHolder_ViewBinding implements Unbinder {
  private FavoriteAdapter.FooterViewHolder target;

  @UiThread
  public FavoriteAdapter$FooterViewHolder_ViewBinding(FavoriteAdapter.FooterViewHolder target,
      View source) {
    this.target = target;

    target.btnHowToUse = Utils.findRequiredView(source, R.id.btn_how_to_use, "field 'btnHowToUse'");
  }

  @Override
  public void unbind() {
    FavoriteAdapter.FooterViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnHowToUse = null;
  }
}
