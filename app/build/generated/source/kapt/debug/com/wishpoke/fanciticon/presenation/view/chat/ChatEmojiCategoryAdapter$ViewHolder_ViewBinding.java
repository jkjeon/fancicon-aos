// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.chat;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class ChatEmojiCategoryAdapter$ViewHolder_ViewBinding implements Unbinder {
  private ChatEmojiCategoryAdapter.ViewHolder target;

  @UiThread
  public ChatEmojiCategoryAdapter$ViewHolder_ViewBinding(ChatEmojiCategoryAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.ivCategory = Utils.findRequiredViewAsType(source, R.id.iv_category, "field 'ivCategory'", ImageView.class);
    target.tvCategory = Utils.findRequiredViewAsType(source, R.id.tv_category, "field 'tvCategory'", TextView.class);
    target.llCategory = Utils.findRequiredView(source, R.id.ll_category, "field 'llCategory'");
  }

  @Override
  public void unbind() {
    ChatEmojiCategoryAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivCategory = null;
    target.tvCategory = null;
    target.llCategory = null;
  }
}
