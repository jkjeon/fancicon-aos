// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.chat;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class ChatMemberAdapter$ViewHolder_ViewBinding implements Unbinder {
  private ChatMemberAdapter.ViewHolder target;

  @UiThread
  public ChatMemberAdapter$ViewHolder_ViewBinding(ChatMemberAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.viewOnline = Utils.findRequiredView(source, R.id.view_online, "field 'viewOnline'");
    target.tvName = Utils.findRequiredViewAsType(source, R.id.tv_name, "field 'tvName'", TextView.class);
    target.ivImage = Utils.findRequiredViewAsType(source, R.id.iv_image, "field 'ivImage'", ImageView.class);
    target.tvMaster = Utils.findRequiredViewAsType(source, R.id.tv_master, "field 'tvMaster'", TextView.class);
    target.ivDeleteMember = Utils.findRequiredView(source, R.id.iv_delete_member, "field 'ivDeleteMember'");
  }

  @Override
  public void unbind() {
    ChatMemberAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.viewOnline = null;
    target.tvName = null;
    target.ivImage = null;
    target.tvMaster = null;
    target.ivDeleteMember = null;
  }
}
