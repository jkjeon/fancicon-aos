// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.main;

import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MenuActivity_ViewBinding implements Unbinder {
  private MenuActivity target;

  private View view7f0a008d;

  @UiThread
  public MenuActivity_ViewBinding(MenuActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MenuActivity_ViewBinding(final MenuActivity target, View source) {
    this.target = target;

    View view;
    target.rvMenuCategory = Utils.findRequiredViewAsType(source, R.id.rv_menu_category, "field 'rvMenuCategory'", RecyclerView.class);
    target.bannerContainer = Utils.findRequiredViewAsType(source, R.id.banner_container, "field 'bannerContainer'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.btn_menu_close, "method 'onClick'");
    view7f0a008d = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MenuActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvMenuCategory = null;
    target.bannerContainer = null;

    view7f0a008d.setOnClickListener(null);
    view7f0a008d = null;
  }
}
