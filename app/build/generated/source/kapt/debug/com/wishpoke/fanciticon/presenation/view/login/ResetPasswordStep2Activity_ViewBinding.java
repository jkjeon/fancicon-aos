// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.login;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import com.wishpoke.fanciticon.presenation.resource.ui.FormEditText;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class ResetPasswordStep2Activity_ViewBinding implements Unbinder {
  private ResetPasswordStep2Activity target;

  private View view7f0a009e;

  private View view7f0a0075;

  @UiThread
  public ResetPasswordStep2Activity_ViewBinding(ResetPasswordStep2Activity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ResetPasswordStep2Activity_ViewBinding(final ResetPasswordStep2Activity target,
      View source) {
    this.target = target;

    View view;
    target.etPw = Utils.findRequiredViewAsType(source, R.id.et_pw, "field 'etPw'", FormEditText.class);
    target.etConfirmPw = Utils.findRequiredViewAsType(source, R.id.et_confirm_pw, "field 'etConfirmPw'", FormEditText.class);
    target.tvPwMessage = Utils.findRequiredViewAsType(source, R.id.tv_pw_message, "field 'tvPwMessage'", TextView.class);
    target.tvConfirmPwMessage = Utils.findRequiredViewAsType(source, R.id.tv_confirm_pw_message, "field 'tvConfirmPwMessage'", TextView.class);
    target.ivConfirmVisible = Utils.findRequiredViewAsType(source, R.id.iv_confirm_visible, "field 'ivConfirmVisible'", ImageView.class);
    target.ivVisible = Utils.findRequiredViewAsType(source, R.id.iv_visible, "field 'ivVisible'", ImageView.class);
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'tvTitle'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btn_submit, "field 'btnSubmit' and method 'onClick'");
    target.btnSubmit = Utils.castView(view, R.id.btn_submit, "field 'btnSubmit'", TextView.class);
    view7f0a009e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.llPrevPw = Utils.findRequiredView(source, R.id.ll_prev_pw, "field 'llPrevPw'");
    target.clPrevPw = Utils.findRequiredView(source, R.id.cl_prev_pw, "field 'clPrevPw'");
    target.tvTitlePw = Utils.findRequiredViewAsType(source, R.id.tv_title_pw, "field 'tvTitlePw'", TextView.class);
    target.tvTitleConfirmPw = Utils.findRequiredViewAsType(source, R.id.tv_title_confirm_pw, "field 'tvTitleConfirmPw'", TextView.class);
    target.etPrevPw = Utils.findRequiredViewAsType(source, R.id.et_prev_pw, "field 'etPrevPw'", FormEditText.class);
    target.ivPrevVisible = Utils.findRequiredViewAsType(source, R.id.iv_prev_visible, "field 'ivPrevVisible'", ImageView.class);
    view = Utils.findRequiredView(source, 2131361909, "method 'onClick'");
    view7f0a0075 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  public void unbind() {
    ResetPasswordStep2Activity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etPw = null;
    target.etConfirmPw = null;
    target.tvPwMessage = null;
    target.tvConfirmPwMessage = null;
    target.ivConfirmVisible = null;
    target.ivVisible = null;
    target.tvTitle = null;
    target.btnSubmit = null;
    target.llPrevPw = null;
    target.clPrevPw = null;
    target.tvTitlePw = null;
    target.tvTitleConfirmPw = null;
    target.etPrevPw = null;
    target.ivPrevVisible = null;

    view7f0a009e.setOnClickListener(null);
    view7f0a009e = null;
    view7f0a0075.setOnClickListener(null);
    view7f0a0075 = null;
  }
}
