// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.general;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PermissionActivity_ViewBinding implements Unbinder {
  private PermissionActivity target;

  private View view7f0a0092;

  private View view7f0a008f;

  @UiThread
  public PermissionActivity_ViewBinding(PermissionActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public PermissionActivity_ViewBinding(final PermissionActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.btn_positive, "field 'btnPositive' and method 'clickPositive'");
    target.btnPositive = Utils.castView(view, R.id.btn_positive, "field 'btnPositive'", CardView.class);
    view7f0a0092 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.clickPositive(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_negative, "field 'btnNegative' and method 'clickNegative'");
    target.btnNegative = Utils.castView(view, R.id.btn_negative, "field 'btnNegative'", CardView.class);
    view7f0a008f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.clickNegative(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    PermissionActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnPositive = null;
    target.btnNegative = null;

    view7f0a0092.setOnClickListener(null);
    view7f0a0092 = null;
    view7f0a008f.setOnClickListener(null);
    view7f0a008f = null;
  }
}
