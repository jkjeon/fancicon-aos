// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.chat;

import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import com.wishpoke.fanciticon.presenation.resource.ui.RegularEditText;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class EnterChatActivity_ViewBinding implements Unbinder {
  private EnterChatActivity target;

  private View view7f0a0075;

  private View view7f0a02ef;

  private View view7f0a02e3;

  @UiThread
  public EnterChatActivity_ViewBinding(EnterChatActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public EnterChatActivity_ViewBinding(final EnterChatActivity target, View source) {
    this.target = target;

    View view;
    target.etNickname = Utils.findRequiredViewAsType(source, R.id.et_nickname, "field 'etNickname'", RegularEditText.class);
    target.etPassword = Utils.findRequiredViewAsType(source, R.id.et_password, "field 'etPassword'", RegularEditText.class);
    target.ivClearNickname = Utils.findRequiredView(source, R.id.iv_clear_nickname, "field 'ivClearNickname'");
    target.ivClearPassword = Utils.findRequiredView(source, R.id.iv_clear_password, "field 'ivClearPassword'");
    target.clPassword = Utils.findRequiredView(source, R.id.cl_password, "field 'clPassword'");
    target.dividerPassword = Utils.findRequiredView(source, R.id.divider_password, "field 'dividerPassword'");
    target.bannerContainer = Utils.findRequiredViewAsType(source, R.id.banner_container, "field 'bannerContainer'", LinearLayout.class);
    target.llIsn = Utils.findRequiredViewAsType(source, R.id.ll_isn, "field 'llIsn'", LinearLayout.class);
    target.wvEdge = Utils.findRequiredViewAsType(source, R.id.wv_edge, "field 'wvEdge'", WebView.class);
    view = Utils.findRequiredView(source, 2131361909, "method 'onClick'");
    view7f0a0075 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, 2131362543, "method 'onClick'");
    view7f0a02ef = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, 2131362531, "method 'onClick'");
    view7f0a02e3 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  public void unbind() {
    EnterChatActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etNickname = null;
    target.etPassword = null;
    target.ivClearNickname = null;
    target.ivClearPassword = null;
    target.clPassword = null;
    target.dividerPassword = null;
    target.bannerContainer = null;
    target.llIsn = null;
    target.wvEdge = null;

    view7f0a0075.setOnClickListener(null);
    view7f0a0075 = null;
    view7f0a02ef.setOnClickListener(null);
    view7f0a02ef = null;
    view7f0a02e3.setOnClickListener(null);
    view7f0a02e3 = null;
  }
}
