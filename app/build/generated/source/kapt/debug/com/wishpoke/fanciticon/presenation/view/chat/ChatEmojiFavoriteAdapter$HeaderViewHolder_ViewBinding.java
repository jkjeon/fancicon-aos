// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.chat;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class ChatEmojiFavoriteAdapter$HeaderViewHolder_ViewBinding implements Unbinder {
  private ChatEmojiFavoriteAdapter.HeaderViewHolder target;

  @UiThread
  public ChatEmojiFavoriteAdapter$HeaderViewHolder_ViewBinding(
      ChatEmojiFavoriteAdapter.HeaderViewHolder target, View source) {
    this.target = target;

    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'tvTitle'", TextView.class);
  }

  @Override
  public void unbind() {
    ChatEmojiFavoriteAdapter.HeaderViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvTitle = null;
  }
}
