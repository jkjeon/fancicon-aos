// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.login;

import android.view.View;
import android.widget.ImageView;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import com.wishpoke.fanciticon.presenation.resource.ui.FormEditText;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class LeaveActivity_ViewBinding implements Unbinder {
  private LeaveActivity target;

  private View view7f0a0075;

  private View view7f0a009e;

  @UiThread
  public LeaveActivity_ViewBinding(LeaveActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LeaveActivity_ViewBinding(final LeaveActivity target, View source) {
    this.target = target;

    View view;
    target.etPw = Utils.findRequiredViewAsType(source, R.id.et_pw, "field 'etPw'", FormEditText.class);
    target.ivVisible = Utils.findRequiredViewAsType(source, R.id.iv_visible, "field 'ivVisible'", ImageView.class);
    view = Utils.findRequiredView(source, 2131361909, "method 'onClick'");
    view7f0a0075 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, 2131361950, "method 'onClick'");
    view7f0a009e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  public void unbind() {
    LeaveActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etPw = null;
    target.ivVisible = null;

    view7f0a0075.setOnClickListener(null);
    view7f0a0075 = null;
    view7f0a009e.setOnClickListener(null);
    view7f0a009e = null;
  }
}
