// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.chat;

import android.view.View;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class ChatEmojiFragment_ViewBinding implements Unbinder {
  private ChatEmojiFragment target;

  @UiThread
  public ChatEmojiFragment_ViewBinding(ChatEmojiFragment target, View source) {
    this.target = target;

    target.rvEmoji = Utils.findRequiredViewAsType(source, R.id.rv_emoji, "field 'rvEmoji'", RecyclerView.class);
    target.swipeRefreshLayout = Utils.findOptionalViewAsType(source, R.id.swipeRefreshLayout, "field 'swipeRefreshLayout'", SwipeRefreshLayout.class);
  }

  @Override
  public void unbind() {
    ChatEmojiFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvEmoji = null;
    target.swipeRefreshLayout = null;
  }
}
