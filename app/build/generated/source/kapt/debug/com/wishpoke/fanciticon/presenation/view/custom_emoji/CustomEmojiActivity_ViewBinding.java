// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.custom_emoji;

import android.view.View;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class CustomEmojiActivity_ViewBinding implements Unbinder {
  private CustomEmojiActivity target;

  private View view7f0a007d;

  private View view7f0a0098;

  private View view7f0a0075;

  @UiThread
  public CustomEmojiActivity_ViewBinding(CustomEmojiActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CustomEmojiActivity_ViewBinding(final CustomEmojiActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, 2131361917, "method 'onClickDetail'");
    view7f0a007d = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickDetail(p0);
      }
    });
    view = Utils.findRequiredView(source, 2131361944, "method 'onClickSave'");
    view7f0a0098 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickSave(p0);
      }
    });
    view = Utils.findRequiredView(source, 2131361909, "method 'onClickBack'");
    view7f0a0075 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickBack(p0);
      }
    });
  }

  @Override
  public void unbind() {
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    target = null;


    view7f0a007d.setOnClickListener(null);
    view7f0a007d = null;
    view7f0a0098.setOnClickListener(null);
    view7f0a0098 = null;
    view7f0a0075.setOnClickListener(null);
    view7f0a0075 = null;
  }
}
