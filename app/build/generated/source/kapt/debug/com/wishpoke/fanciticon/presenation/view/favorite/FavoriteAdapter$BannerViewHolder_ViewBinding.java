// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.favorite;

import android.view.View;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class FavoriteAdapter$BannerViewHolder_ViewBinding implements Unbinder {
  private FavoriteAdapter.BannerViewHolder target;

  @UiThread
  public FavoriteAdapter$BannerViewHolder_ViewBinding(FavoriteAdapter.BannerViewHolder target,
      View source) {
    this.target = target;

    target.banner = Utils.findRequiredView(source, R.id.banner, "field 'banner'");
    target.btnBannerDesc = Utils.findRequiredView(source, R.id.btn_banner_desc, "field 'btnBannerDesc'");
    target.rvBanner = Utils.findRequiredViewAsType(source, R.id.rv_banner, "field 'rvBanner'", RecyclerView.class);
  }

  @Override
  public void unbind() {
    FavoriteAdapter.BannerViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.banner = null;
    target.btnBannerDesc = null;
    target.rvBanner = null;
  }
}
