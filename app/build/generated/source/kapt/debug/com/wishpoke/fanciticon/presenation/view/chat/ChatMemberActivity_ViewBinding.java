// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.chat;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class ChatMemberActivity_ViewBinding implements Unbinder {
  private ChatMemberActivity target;

  private View view7f0a0075;

  @UiThread
  public ChatMemberActivity_ViewBinding(ChatMemberActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ChatMemberActivity_ViewBinding(final ChatMemberActivity target, View source) {
    this.target = target;

    View view;
    target.tvRoomTitle = Utils.findRequiredViewAsType(source, R.id.tv_room_title, "field 'tvRoomTitle'", TextView.class);
    target.tvCountMember = Utils.findRequiredViewAsType(source, R.id.tv_count_member, "field 'tvCountMember'", TextView.class);
    target.rvMember = Utils.findRequiredViewAsType(source, R.id.rv_member, "field 'rvMember'", RecyclerView.class);
    view = Utils.findRequiredView(source, 2131361909, "method 'onClick'");
    view7f0a0075 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  public void unbind() {
    ChatMemberActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvRoomTitle = null;
    target.tvCountMember = null;
    target.rvMember = null;

    view7f0a0075.setOnClickListener(null);
    view7f0a0075 = null;
  }
}
