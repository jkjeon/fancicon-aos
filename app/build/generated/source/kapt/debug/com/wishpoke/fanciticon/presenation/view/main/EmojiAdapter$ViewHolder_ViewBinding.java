// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.main;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class EmojiAdapter$ViewHolder_ViewBinding implements Unbinder {
  private EmojiAdapter.ViewHolder target;

  @UiThread
  public EmojiAdapter$ViewHolder_ViewBinding(EmojiAdapter.ViewHolder target, View source) {
    this.target = target;

    target.btnFavorite = Utils.findRequiredViewAsType(source, R.id.btn_favorite, "field 'btnFavorite'", ImageView.class);
    target.btnRemove = Utils.findRequiredViewAsType(source, R.id.btn_remove, "field 'btnRemove'", ImageView.class);
    target.tvEmoji = Utils.findRequiredViewAsType(source, R.id.tv_emoji, "field 'tvEmoji'", TextView.class);
    target.rlItem = Utils.findRequiredViewAsType(source, R.id.rl_item, "field 'rlItem'", RelativeLayout.class);
    target.viewNew = Utils.findRequiredViewAsType(source, R.id.view_new, "field 'viewNew'", LinearLayout.class);
  }

  @Override
  public void unbind() {
    EmojiAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnFavorite = null;
    target.btnRemove = null;
    target.tvEmoji = null;
    target.rlItem = null;
    target.viewNew = null;
  }
}
