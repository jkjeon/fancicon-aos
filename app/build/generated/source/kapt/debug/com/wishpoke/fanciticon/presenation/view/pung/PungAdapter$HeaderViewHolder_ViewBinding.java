// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.pung;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class PungAdapter$HeaderViewHolder_ViewBinding implements Unbinder {
  private PungAdapter.HeaderViewHolder target;

  @UiThread
  public PungAdapter$HeaderViewHolder_ViewBinding(PungAdapter.HeaderViewHolder target,
      View source) {
    this.target = target;

    target.btnInfo = Utils.findRequiredView(source, R.id.btn_info, "field 'btnInfo'");
    target.btnBoard = Utils.findRequiredView(source, R.id.btn_board, "field 'btnBoard'");
    target.tvBoard = Utils.findRequiredViewAsType(source, R.id.tv_board, "field 'tvBoard'", TextView.class);
    target.btnOrder = Utils.findRequiredView(source, R.id.btn_order, "field 'btnOrder'");
    target.tvOrder = Utils.findRequiredViewAsType(source, R.id.tv_order, "field 'tvOrder'", TextView.class);
    target.btnWrite = Utils.findRequiredView(source, R.id.btn_write, "field 'btnWrite'");
  }

  @Override
  public void unbind() {
    PungAdapter.HeaderViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnInfo = null;
    target.btnBoard = null;
    target.tvBoard = null;
    target.btnOrder = null;
    target.tvOrder = null;
    target.btnWrite = null;
  }
}
