// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.pung;

import android.view.View;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class PungActivity_ViewBinding implements Unbinder {
  private PungActivity target;

  private View view7f0a0075;

  private View view7f0a0122;

  @UiThread
  public PungActivity_ViewBinding(PungActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public PungActivity_ViewBinding(final PungActivity target, View source) {
    this.target = target;

    View view;
    target.rvReply = Utils.findRequiredViewAsType(source, R.id.rv_reply, "field 'rvReply'", RecyclerView.class);
    view = Utils.findRequiredView(source, 2131361909, "method 'onClick'");
    view7f0a0075 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, 2131362082, "method 'onClick'");
    view7f0a0122 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  public void unbind() {
    PungActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvReply = null;

    view7f0a0075.setOnClickListener(null);
    view7f0a0075 = null;
    view7f0a0122.setOnClickListener(null);
    view7f0a0122 = null;
  }
}
