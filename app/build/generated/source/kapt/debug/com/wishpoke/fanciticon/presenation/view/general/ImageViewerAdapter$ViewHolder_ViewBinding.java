// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.general;

import android.view.View;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.otaliastudios.zoom.ZoomImageView;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class ImageViewerAdapter$ViewHolder_ViewBinding implements Unbinder {
  private ImageViewerAdapter.ViewHolder target;

  @UiThread
  public ImageViewerAdapter$ViewHolder_ViewBinding(ImageViewerAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.ivImage = Utils.findRequiredViewAsType(source, R.id.iv_image, "field 'ivImage'", ZoomImageView.class);
  }

  @Override
  public void unbind() {
    ImageViewerAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivImage = null;
  }
}
