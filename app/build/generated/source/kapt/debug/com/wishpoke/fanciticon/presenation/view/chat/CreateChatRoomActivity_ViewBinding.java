// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.chat;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import com.wishpoke.fanciticon.presenation.resource.ui.RegularEditText;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class CreateChatRoomActivity_ViewBinding implements Unbinder {
  private CreateChatRoomActivity target;

  private View view7f0a0171;

  private View view7f0a00b9;

  private View view7f0a0075;

  private View view7f0a02ef;

  private View view7f0a02e3;

  @UiThread
  public CreateChatRoomActivity_ViewBinding(CreateChatRoomActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CreateChatRoomActivity_ViewBinding(final CreateChatRoomActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.iv_upload, "field 'ivUpload' and method 'onClick'");
    target.ivUpload = Utils.castView(view, R.id.iv_upload, "field 'ivUpload'", ImageView.class);
    view7f0a0171 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.etMasterNickname = Utils.findRequiredViewAsType(source, R.id.et_master_nickname, "field 'etMasterNickname'", RegularEditText.class);
    target.etRoomName = Utils.findRequiredViewAsType(source, R.id.et_room_name, "field 'etRoomName'", RegularEditText.class);
    target.ivClearRoomName = Utils.findRequiredView(source, R.id.iv_clear_room_name, "field 'ivClearRoomName'");
    target.ivClearMasterNickname = Utils.findRequiredView(source, R.id.iv_clear_master_nickname, "field 'ivClearMasterNickname'");
    target.etPassword = Utils.findRequiredViewAsType(source, R.id.et_password, "field 'etPassword'", EditText.class);
    view = Utils.findRequiredView(source, 2131361977, "method 'onCheckedChanged'");
    view7f0a00b9 = view;
    ((CompoundButton) view).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton p0, boolean p1) {
        target.onCheckedChanged(p0, p1);
      }
    });
    view = Utils.findRequiredView(source, 2131361909, "method 'onClick'");
    view7f0a0075 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, 2131362543, "method 'onClick'");
    view7f0a02ef = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, 2131362531, "method 'onClick'");
    view7f0a02e3 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  public void unbind() {
    CreateChatRoomActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivUpload = null;
    target.etMasterNickname = null;
    target.etRoomName = null;
    target.ivClearRoomName = null;
    target.ivClearMasterNickname = null;
    target.etPassword = null;

    view7f0a0171.setOnClickListener(null);
    view7f0a0171 = null;
    ((CompoundButton) view7f0a00b9).setOnCheckedChangeListener(null);
    view7f0a00b9 = null;
    view7f0a0075.setOnClickListener(null);
    view7f0a0075 = null;
    view7f0a02ef.setOnClickListener(null);
    view7f0a02ef = null;
    view7f0a02e3.setOnClickListener(null);
    view7f0a02e3 = null;
  }
}
