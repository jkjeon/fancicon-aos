// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.general;

import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FeedbackActivity_ViewBinding implements Unbinder {
  private FeedbackActivity target;

  @UiThread
  public FeedbackActivity_ViewBinding(FeedbackActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public FeedbackActivity_ViewBinding(FeedbackActivity target, View source) {
    this.target = target;

    target.webView = Utils.findRequiredViewAsType(source, R.id.webView, "field 'webView'", WebView.class);
    target.btnBack = Utils.findRequiredViewAsType(source, R.id.btn_back, "field 'btnBack'", ImageView.class);
    target.isnContainer = Utils.findRequiredViewAsType(source, R.id.ll_isn, "field 'isnContainer'", LinearLayout.class);
    target.isnWebView = Utils.findRequiredViewAsType(source, R.id.wv_edge, "field 'isnWebView'", WebView.class);
    target.bannerContainer = Utils.findRequiredViewAsType(source, R.id.banner_container, "field 'bannerContainer'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FeedbackActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.webView = null;
    target.btnBack = null;
    target.isnContainer = null;
    target.isnWebView = null;
    target.bannerContainer = null;
  }
}
