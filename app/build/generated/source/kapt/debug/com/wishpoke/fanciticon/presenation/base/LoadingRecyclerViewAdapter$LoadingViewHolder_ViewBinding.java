// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.base;

import android.view.View;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.airbnb.lottie.LottieAnimationView;
import com.wishpoke.fanciticon.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class LoadingRecyclerViewAdapter$LoadingViewHolder_ViewBinding implements Unbinder {
  private LoadingRecyclerViewAdapter.LoadingViewHolder target;

  @UiThread
  public LoadingRecyclerViewAdapter$LoadingViewHolder_ViewBinding(
      LoadingRecyclerViewAdapter.LoadingViewHolder target, View source) {
    this.target = target;

    target.llLoading = Utils.findRequiredView(source, R.id.ll_loading, "field 'llLoading'");
    target.avLoading = Utils.findRequiredViewAsType(source, R.id.av_loading, "field 'avLoading'", LottieAnimationView.class);
  }

  @Override
  public void unbind() {
    LoadingRecyclerViewAdapter.LoadingViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.llLoading = null;
    target.avLoading = null;
  }
}
