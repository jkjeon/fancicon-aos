// Generated code from Butter Knife. Do not modify!
package com.wishpoke.fanciticon.presenation.view.login;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wishpoke.fanciticon.R;
import com.wishpoke.fanciticon.presenation.resource.ui.FormEditText;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class FindIdActivity_ViewBinding implements Unbinder {
  private FindIdActivity target;

  private View view7f0a009e;

  private View view7f0a0075;

  @UiThread
  public FindIdActivity_ViewBinding(FindIdActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public FindIdActivity_ViewBinding(final FindIdActivity target, View source) {
    this.target = target;

    View view;
    target.etPhone = Utils.findRequiredViewAsType(source, R.id.et_phone, "field 'etPhone'", EditText.class);
    target.etCode = Utils.findRequiredViewAsType(source, R.id.et_code, "field 'etCode'", FormEditText.class);
    target.btnSendCode = Utils.findRequiredViewAsType(source, R.id.btn_send_code, "field 'btnSendCode'", TextView.class);
    target.btnVerifyCode = Utils.findRequiredViewAsType(source, R.id.btn_verify_code, "field 'btnVerifyCode'", TextView.class);
    target.tvTimer = Utils.findRequiredViewAsType(source, R.id.tv_timer, "field 'tvTimer'", TextView.class);
    target.tvVerificationDesc = Utils.findRequiredViewAsType(source, R.id.tv_verification_desc, "field 'tvVerificationDesc'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btn_submit, "field 'btnSubmit' and method 'onClick'");
    target.btnSubmit = view;
    view7f0a009e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, 2131361909, "method 'onClick'");
    view7f0a0075 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  public void unbind() {
    FindIdActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etPhone = null;
    target.etCode = null;
    target.btnSendCode = null;
    target.btnVerifyCode = null;
    target.tvTimer = null;
    target.tvVerificationDesc = null;
    target.btnSubmit = null;

    view7f0a009e.setOnClickListener(null);
    view7f0a009e = null;
    view7f0a0075.setOnClickListener(null);
    view7f0a0075 = null;
  }
}
